/*
SQLyog Ultimate v10.42 
MySQL - 5.5.46-0ubuntu0.14.04.2 : Database - tamoda
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*CREATE DATABASE /*!32312 IF NOT EXISTS*/`tamoda` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*USE `tamoda`;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `admins` */

insert  into `admins`(`id`,`username`,`email`,`password`,`first_name`,`last_name`,`avatar`) values (4,'admin','admin@tamoda.com','39dfa55283318d31afe5a3ff4a0e3253e2045e43','admin','admin2','snapshot4.png');

/*Table structure for table `basket` */

DROP TABLE IF EXISTS `basket`;

CREATE TABLE `basket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `session_id` int(11) unsigned DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `quantity` mediumint(5) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `offered_price` decimal(10,2) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT '0',
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `basket_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `basket_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

/*Data for the table `basket` */

insert  into `basket`(`id`,`product_id`,`user_id`,`session_id`,`price`,`quantity`,`total`,`offered_price`,`confirmed`,`date_added`) values (57,1,19,NULL,500.00,3,1500.00,50.00,1,'2016-01-20 19:03:04'),(59,2,19,NULL,100.00,1,100.00,NULL,0,'2016-01-21 14:24:54');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '0-disabled,1-enabled',
  `sort_order` smallint(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

insert  into `categories`(`id`,`parent_id`,`created_date`,`status`,`sort_order`) values (7,0,'2016-01-08 13:02:35',1,0),(8,0,'2016-01-08 13:03:08',1,0),(9,8,'2016-01-08 13:03:30',1,0),(11,9,'2016-01-09 11:11:11',1,0),(12,7,'2016-01-12 12:46:59',0,2);

/*Table structure for table `categories_filter` */

DROP TABLE IF EXISTS `categories_filter`;

CREATE TABLE `categories_filter` (
  `categories_id` int(11) unsigned NOT NULL,
  `filter_group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`categories_id`,`filter_group_id`),
  KEY `filter_group_id` (`filter_group_id`),
  CONSTRAINT `categories_filter_ibfk_1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `categories_filter_ibfk_2` FOREIGN KEY (`filter_group_id`) REFERENCES `filter_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `categories_filter` */

insert  into `categories_filter`(`categories_id`,`filter_group_id`) values (12,20),(12,21);

/*Table structure for table `categories_labels` */

DROP TABLE IF EXISTS `categories_labels`;

CREATE TABLE `categories_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `categories_id` int(11) unsigned NOT NULL,
  `languages_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `categories_id` (`categories_id`),
  KEY `languages_id` (`languages_id`),
  CONSTRAINT `categories_labels_ibfk_1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `categories_labels_ibfk_2` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `categories_labels` */

insert  into `categories_labels`(`id`,`categories_id`,`languages_id`,`title`,`description`) values (13,7,1,'Womens','sdf'),(14,7,2,'w','sdf'),(15,7,3,'w','sdf'),(16,8,1,'Mens','dfg'),(17,8,2,'m','dfg'),(18,8,3,'m','dfg'),(19,9,1,'submen','g'),(20,9,2,'gf','f'),(21,9,3,'f','ff'),(25,11,1,'sub submen','sasd'),(26,11,2,'ss','as'),(27,11,3,'ss','as'),(28,12,1,'shoes','shs'),(29,12,2,'obuv\'','dfgd'),(30,12,3,'koshikner','fgd');

/*Table structure for table `favorite_products` */

DROP TABLE IF EXISTS `favorite_products`;

CREATE TABLE `favorite_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `favorite_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `favorite_products_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

/*Data for the table `favorite_products` */

insert  into `favorite_products`(`id`,`product_id`,`user_id`,`created_date`) values (71,2,19,'2016-01-21 10:58:09');

/*Table structure for table `filter` */

DROP TABLE IF EXISTS `filter`;

CREATE TABLE `filter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_group_id` int(11) unsigned NOT NULL,
  `sort_order` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filter_group_id` (`filter_group_id`),
  CONSTRAINT `filter_ibfk_1` FOREIGN KEY (`filter_group_id`) REFERENCES `filter_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `filter` */

insert  into `filter`(`id`,`filter_group_id`,`sort_order`) values (26,20,2),(27,20,3),(28,21,1),(29,21,1);

/*Table structure for table `filter_group` */

DROP TABLE IF EXISTS `filter_group`;

CREATE TABLE `filter_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `filter_group` */

insert  into `filter_group`(`id`,`sort_order`) values (20,1),(21,0);

/*Table structure for table `filter_group_label` */

DROP TABLE IF EXISTS `filter_group_label`;

CREATE TABLE `filter_group_label` (
  `filter_group_id` int(11) unsigned NOT NULL,
  `languages_id` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_group_id`,`languages_id`),
  KEY `languages_id` (`languages_id`),
  CONSTRAINT `filter_group_label_ibfk_2` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `filter_group_label_ibfk_3` FOREIGN KEY (`filter_group_id`) REFERENCES `filter_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `filter_group_label` */

insert  into `filter_group_label`(`filter_group_id`,`languages_id`,`name`) values (20,1,'color'),(20,2,'cvet'),(20,3,'guyn'),(21,1,'size'),(21,2,'razmer'),(21,3,'chaps');

/*Table structure for table `filter_label` */

DROP TABLE IF EXISTS `filter_label`;

CREATE TABLE `filter_label` (
  `filter_id` int(11) unsigned NOT NULL,
  `languages_id` int(11) unsigned NOT NULL,
  `filter_group_id` int(11) unsigned NOT NULL,
  `name` varchar(64) COLLATE utf8_estonian_ci NOT NULL,
  PRIMARY KEY (`filter_id`,`languages_id`),
  KEY `languages_id` (`languages_id`),
  KEY `filter_group_id` (`filter_group_id`),
  CONSTRAINT `filter_label_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `filter_label_ibfk_2` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `filter_label_ibfk_3` FOREIGN KEY (`filter_group_id`) REFERENCES `filter_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;

/*Data for the table `filter_label` */

insert  into `filter_label`(`filter_id`,`languages_id`,`filter_group_id`,`name`) values (26,1,20,'blue'),(26,2,20,'siniy'),(26,3,20,'kapuyt'),(27,1,20,'red'),(27,2,20,'krasn'),(27,3,20,'karmir2'),(28,1,21,'xs'),(28,2,21,'xs'),(28,3,21,'xs'),(29,1,21,'s'),(29,2,21,'s'),(29,3,21,'m');

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) DEFAULT NULL,
  `iso` varchar(3) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '0-disabled, 1-enabled',
  `flag_url` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `languages` */

insert  into `languages`(`id`,`title`,`iso`,`status`,`flag_url`) values (1,'English','en',1,'http://www.cerkno-cup.com/2015/template/img/icon_flag_us.gif'),(2,'Russian','ru',1,'http://footballbettips.net/images/flags/russia-flag.gif'),(3,'Armenian','am',1,'http://www.depop.am/wp-content/themes/depopulation/img/am.png');

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_user` int(11) unsigned NOT NULL,
  `to_user` int(11) unsigned NOT NULL,
  `basket_id` int(11) unsigned NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `from_user` (`from_user`),
  KEY `to_user` (`to_user`),
  KEY `basket_id` (`basket_id`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`from_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_4` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `messages` */

insert  into `messages`(`id`,`from_user`,`to_user`,`basket_id`,`message`) values (30,19,1,57,'lil offered 50 for testfhfhfhfh'),(33,1,19,57,'Your offered price for testfhfhfhfh(50.00) has been confirmed');

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `activity_type` tinyint(1) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notifications` */

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `payment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `payments` */

/*Table structure for table `product_filter` */

DROP TABLE IF EXISTS `product_filter`;

CREATE TABLE `product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `product_filter` */

insert  into `product_filter`(`product_id`,`filter_id`) values (1,14),(1,26),(1,28),(2,13),(2,16);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `location` varchar(150) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `users_id` (`users_id`),
  KEY `categories_id` (`categories_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `products` */

insert  into `products`(`id`,`users_id`,`categories_id`,`location`,`price`,`name`,`description`,`status`,`created_date`) values (1,1,7,'sdgsdgsdgsdgsdg',500.00,'testfhfhfhfh',NULL,1,NULL),(2,1,7,'gdfgdfg',100.00,'sfsgd',NULL,1,NULL);

/*Table structure for table `products_comments` */

DROP TABLE IF EXISTS `products_comments`;

CREATE TABLE `products_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `review` text NOT NULL,
  `rating` tinyint(5) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `products_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_comments_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `products_comments` */

insert  into `products_comments`(`id`,`user_id`,`product_id`,`review`,`rating`,`created_date`) values (4,19,1,'nbnb',2,'2016-01-19 18:08:14');

/*Table structure for table `products_media` */

DROP TABLE IF EXISTS `products_media`;

CREATE TABLE `products_media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `sort_order` smallint(1) DEFAULT '0',
  `main_photo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

/*Data for the table `products_media` */

insert  into `products_media`(`id`,`product_id`,`photo`,`video`,`sort_order`,`main_photo`) values (65,2,'images (1).jpg',NULL,0,1),(66,2,'images (2).jpg',NULL,0,0),(67,1,'images88.jpg',NULL,0,1),(68,1,'images (1).jpg',NULL,0,0);

/*Table structure for table `search_log` */

DROP TABLE IF EXISTS `search_log`;

CREATE TABLE `search_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `languages_id` int(11) unsigned NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `search_log` */

/*Table structure for table `sliders` */

DROP TABLE IF EXISTS `sliders`;

CREATE TABLE `sliders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `sliders` */

insert  into `sliders`(`id`,`created_date`,`status`) values (1,'2016-01-13 11:24:11',0),(2,'2016-01-13 11:25:19',1);

/*Table structure for table `sliders_labels` */

DROP TABLE IF EXISTS `sliders_labels`;

CREATE TABLE `sliders_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sliders_id` int(11) unsigned NOT NULL,
  `languages_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `url` varchar(255) DEFAULT '#',
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sliders_id` (`sliders_id`),
  KEY `languages_id` (`languages_id`),
  CONSTRAINT `sliders_labels_ibfk_1` FOREIGN KEY (`sliders_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sliders_labels_ibfk_2` FOREIGN KEY (`languages_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `sliders_labels` */

insert  into `sliders_labels`(`id`,`sliders_id`,`languages_id`,`title`,`description`,`url`,`image`) values (1,1,1,'n','cvn','http://127.0.0.1/tamoda.sk/admin/sliders/create',''),(2,1,2,'cn','cvn','http://127.0.0.1/tamoda.sk/admin/sliders/create',''),(3,1,3,'cv','cv','http://127.0.0.1/tamoda.sk/admin/sliders/create',''),(4,2,1,'nb','v','http://127.0.0.1/tamoda.sk/admin/sliders/create',''),(5,2,2,'vn','vn','http://127.0.0.1/tamoda.sk/admin/sliders/create',''),(6,2,3,'xcc','xcv','http://127.0.0.1/tamoda.sk/admin/sliders/create','');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(1) unsigned DEFAULT '1' COMMENT '1-buyer,2-seller',
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL COMMENT 'sha256',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_ip` varchar(15) DEFAULT '0.0.0.0',
  `activated` tinyint(1) unsigned DEFAULT '0' COMMENT '0-no,1-yes',
  `blocked` tinyint(1) unsigned DEFAULT '0' COMMENT '0-no,1-yes',
  `confirmation_id` varchar(255) NOT NULL,
  `password_recovery` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`user_type`,`username`,`email`,`password`,`created_date`,`modified_date`,`last_login_date`,`last_login_ip`,`activated`,`blocked`,`confirmation_id`,`password_recovery`) values (1,2,'test2','test@mail.ru','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','2015-12-09 11:37:55','0000-00-00 00:00:00','0000-00-00 00:00:00','0.0.0.0',1,0,'','c04532ca4e12438bcd37d2ae1676d3f5a27241062095eaccdbf0102b78d2a948'),(2,1,'user','usxxxxxer@gmail.com','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','2015-12-28 12:12:55','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',0,0,'',NULL),(3,1,'user','user@gmail.com','7c4a8d09ca3762af61e59520943dc26494f8941b','2015-12-28 12:26:12','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',1,0,'',NULL),(4,1,'test','test@gmail.com','test','2015-12-28 12:54:06','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',0,1,'87924606b4131a8aceeeae8868531fbb9712aaa07a5d3a756b26ce0f5d6ca674',NULL),(5,1,'sss','ss@hdjf.ddd','123456','2015-12-28 12:57:03','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',0,0,'698b02502ce020364fbe4d9d21d7d05fc45811e7cccdddc62d9d4946771c6b3b',NULL),(6,1,'usik','test@gmail.comaa','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','2015-12-28 13:34:35','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',1,0,'588c5bd9bbc19118c295905c4fd81740c23c479ac4a42eec3aa9b3c99ef1d54a',NULL),(7,1,'qew','new@gmail.com','ad4941386c090ac54142d38b390d313075deff4d873a1c82e3a25540cf611127','2015-12-28 14:09:33','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',1,0,'f619e87c6fc32065033b7c9a0ac67cce03a9c283be787bda947349fb575d1374',''),(19,1,'lil','lilitbakunts@gmail.com','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','2016-01-13 10:44:30','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',1,0,'7bcaa99ea0cfacbf3d4f026f7d660b52d0f2ce8cd9c73cb2c8e7b02938bbbabd',NULL),(20,1,'','agelancer@gmail.com','','2016-01-18 19:04:53','0000-00-00 00:00:00','0000-00-00 00:00:00','127.0.0.1',0,0,'7064cd00b18c93164c9a2646a7369dc9e887565d0577896df53edc645c597c18',NULL);

/*Table structure for table `users_additional` */

DROP TABLE IF EXISTS `users_additional`;

CREATE TABLE `users_additional` (
  `users_id` int(11) unsigned NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `age` smallint(2) DEFAULT NULL,
  `sex` tinyint(1) unsigned DEFAULT '0' COMMENT '1-man, 2-woman',
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`users_id`),
  CONSTRAINT `users_additional_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users_additional` */

insert  into `users_additional`(`users_id`,`first_name`,`last_name`,`age`,`sex`,`avatar`) values (1,'test','test',NULL,1,NULL),(7,'Tigran','Harutyunyan',98,1,'snapshot3.png');

/* Trigger structure for table `basket` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TotalPriceAdd` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `TotalPriceAdd` BEFORE INSERT ON `basket` FOR EACH ROW SET NEW.total = NEW.price * NEW.quantity */$$


DELIMITER ;

/* Trigger structure for table `basket` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `TotalPriceUpdate` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `TotalPriceUpdate` BEFORE UPDATE ON `basket` FOR EACH ROW SET NEW.total = NEW.price * NEW.quantity */$$


DELIMITER ;

/*Table structure for table `users_all` */

DROP TABLE IF EXISTS `users_all`;

/*!50001 DROP VIEW IF EXISTS `users_all` */;
/*!50001 DROP TABLE IF EXISTS `users_all` */;

/*!50001 CREATE TABLE  `users_all`(
 `id` int(11) unsigned ,
 `user_type` tinyint(1) unsigned ,
 `username` varchar(64) ,
 `email` varchar(64) ,
 `password` varchar(64) ,
 `created_date` timestamp ,
 `modified_date` timestamp ,
 `last_login_date` timestamp ,
 `last_login_ip` varchar(15) ,
 `activated` tinyint(1) unsigned ,
 `blocked` tinyint(1) unsigned ,
 `users_id` int(11) unsigned ,
 `first_name` varchar(64) ,
 `last_name` varchar(64) ,
 `age` smallint(2) ,
 `sex` tinyint(1) unsigned ,
 `avatar` varchar(255) 
)*/;

/*View structure for view users_all */

/*!50001 DROP TABLE IF EXISTS `users_all` */;
/*!50001 DROP VIEW IF EXISTS `users_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_all` AS (select `t1`.`id` AS `id`,`t1`.`user_type` AS `user_type`,`t1`.`username` AS `username`,`t1`.`email` AS `email`,`t1`.`password` AS `password`,`t1`.`created_date` AS `created_date`,`t1`.`modified_date` AS `modified_date`,`t1`.`last_login_date` AS `last_login_date`,`t1`.`last_login_ip` AS `last_login_ip`,`t1`.`activated` AS `activated`,`t1`.`blocked` AS `blocked`,`t2`.`users_id` AS `users_id`,`t2`.`first_name` AS `first_name`,`t2`.`last_name` AS `last_name`,`t2`.`age` AS `age`,`t2`.`sex` AS `sex`,`t2`.`avatar` AS `avatar` from (`users` `t1` join `users_additional` `t2` on((`t1`.`id` = `t2`.`users_id`)))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
