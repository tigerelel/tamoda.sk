(function ($) {
 "use strict";

     /*--tooltip--*/

    $('[data-toggle="tooltip"]').tooltip({
       animated : 'fade',
       placement : 'top',
       container: 'body'
    });
    /*left category menu*/

    $('.rx-parent').on('click', function(){
        $('.rx-child').slideToggle();
        $(this).toggleClass('rx-change');

    });

    $(".embed-responsive iframe").addClass("embed-responsive-item");
    $(".carousel-inner .item:first-child").addClass("active");

    $('.category-heading').on('click', function(){
    $('.category-menu-list').slideToggle(300);

    });
    

    /*---countdown---*/
    $('[data-countdown]').each(function() {
    var $this = $(this), finalDate = $(this).data('countdown');
    $this.countdown(finalDate, function(event) {
    $this.html(event.strftime('<span class="cdown day"><span class="time-count separator">%-D</span> <p class="cdown-tex">Days</p>  </span> <span class="cdown hour"><span class="time-count separator">%-H</span> <p class="cdown-tex">Hrs</p>  </span> <span class="cdown minutes"><span class="time-count separator">%M</span> <p class="cdown-tex">Min</p>  </span> <span class="cdown"><span class="time-count">%S</span> <p class="cdown-tex">Sec</p> </span>'));
	  });
	});	
    
/*----- main slider -----*/
	$('#mainSlider').nivoSlider({
        manualAdvance:true,
		controlNav: false,
        animSpeed: 1,
        startSlide: 0,
        effect: 'fold',
        prevText: '<i class="fa fa-angle-left nivo-prev-icon"></i>',
		nextText: '<i class="fa fa-angle-right nivo-next-icon"></i>'
	});		
	
    /*---Owl Carousel hot deals---*/
    $(".product-carousel").owlCarousel({
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :2,
        itemsDesktop : [1199,1],
        itemsTablet : [991,3],
        itemsTabletSmall : [767,2],
        itemsMobile : [480,1],
        autoPlay : false
        });  
    
    /*---Owl Carousel hot deals---*/
    $(".left-carousel").owlCarousel({
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :1,
        itemsDesktop : [991,2],
        itemsTablet : [768,2],
        itemsTabletSmall : [680,2],
        itemsMobile : [480,1],
        autoPlay : false
    });  
    
    /*Owl Carousel tab*/
    $(".tab-carosel-start").owlCarousel({
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :5,
        itemsDesktop : [1600,4],
        itemsTablet : [1199,3],
        itemsTabletSmall : [767,2],
        itemsMobile : [480,1],
        autoPlay : false
    }); 
    
    /*---Owl Carousel category---*/
    $(".cat-carousel").owlCarousel({
        navigation: true,
       navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :5,
        itemsDesktop : [1599,3],
        itemsTablet : [500,2],
        itemsTabletSmall : [767,2],
        itemsMobile : [480,1],
        autoPlay : true
    }); 
    
    /*---Owl Carousel clients---*/
     $(".client-carousel").owlCarousel({
        navigation: true,
       navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :10,
        itemsDesktop : [1199,6],
        itemsTablet : [991,5],
        itemsTabletSmall : [767,4],
        itemsMobile : [480,3],
        autoPlay : false
    }); 
    
    
    /*--mobile-menu--*/
    
    jQuery('.mobile-menu-start').meanmenu();
    
    /*--------------------------
        Index two start
    --------------------------*/
    
    /*---Owl Carousel tab--*/
    $(".tab-carosel-h2-start").owlCarousel({
        navigation: true,
       navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :5,
        itemsDesktop : [1199,4],
        itemsTablet : [767,2],
        itemsTabletSmall : [680,1],
        itemsMobile : [480,1]
        }); 
    
    /*latest  Carousel */
    $(".latest-post-carousel-two").owlCarousel({
        navigation: true,
       navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :4,
        itemsDesktop : [1199,3],
        itemsTablet : [767,2],
        itemsTabletSmall : [680,1],
        itemsMobile : [480,1],
        autoPlay : false
        });
    
     /*---price slider---- */


    $( ".control-car" ).each(function( i,v ) {
        var min = parseInt($( this).find('.price_from').val());
        var max = $( this).find('.price_to').val();
        $( "#slider-range"+i ).slider({
            range: true,
            min: min,
            max: max,
            values: [min, max ],
            change: function( event, ui ) {

                $( "#slider-range"+i).find('.price_from').val(ui.values[ 0 ]);
                $( "#slider-range"+i).find('.price_to').val(ui.values[ 1 ]);
               
            },
            slide: function( event, ui ) {

                $( "#amount"+i ).val( "\u20AC" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                $( "#amount-left-"+i ) .val( "\u20AC" + ui.values[ 0 ] );
                $( "#amount-right-"+i ).val( "\u20AC" + ui.values[ 1 ] );
            }

        });
        $( "#amount-left-"+i ) .val( "\u20AC" + $( "#slider-range"+i ).slider( "values", 0 ) );
        $( "#amount-right-"+i ).val( "\u20AC" + $( "#slider-range"+i ).slider( "values", 1 ) );


        $( "#one-slide"+i ).slider({
            disabled: ($('.geoloc_lat').val() > 0 && $('.geoloc_lng').val() > 0) ? false : true,
            range: "min",
            //max: 5000,
            change: function( event, ui ) {
                $('.destination'+i).val(ui.value);
            },
            slide: function( event, ui ) {
                $( "#dest"+i ).val(ui.value +" km");
            }

        });
        $( "#dest"+i ).val($( "#one-slide"+i ).slider( "values", 0 )+" km") ;

    });


    $( "#slider-range").slider({
        range: true,
        min: parseInt($( ".price_from").val()),
        max: parseInt($( ".price_to").val()),
        values: [ 0, $('#price-filter0').val() ],
        change: function( event, ui ) {

            $('#price-filter0').val( ui.values[ 1 ]);
            $('#price-filter1').val( ui.values[ 0 ]);
        },
        slide: function( event, ui ) {

            $( "#amount-left") .val( "\u20AC" + ui.values[ 0 ] );
            $( "#amount-right").val( "\u20AC" + ui.values[ 1 ] );
        }
    });
    $( "#amount-left" ) .val( "\u20AC" + $( ".price_from").val());
    $( "#amount-right" ).val( "\u20AC" + $( ".price_to").val() );



    $( "#one-slide").slider({
        disabled: ($('.geoloc_lat').val() > 0 && $('.geoloc_lng').val() > 0) ? false : true,
        range: "min",
        max: 5000,
        change: function( event, ui ) {
            $('#dest').val(ui.value+" km");
            $('.ajax-dest').val(ui.value);
        },
        slide: function( event, ui ) {
            $( "#dest").val(ui.value +" km");
        }
    });
    $( "#dest" ).val($( "#one-slide" ).slider( "values", 0 )+" km") ;



    $(".fancybox-button").fancybox({
        prevEffect		: 'none',
        nextEffect		: 'none',
        closeBtn		: false,
        helpers		: {
            title	: { type : 'inside' },
            buttons	: {}
        },
        'overlayShow'	:	false
    });

    /*---zoom---*/
      //$("#zoom1").elevateZoom({
      //  gallery:'gallery_01',
      //  cursor: 'pointer',
      //  galleryActiveClass: "active",
      //  imageCrossfade: true
      //  });
    
    /*---zoom  Carousel ---*/
    $(".zoom-slider").owlCarousel({
        navigation: true,
       navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        items :4,
        itemsDesktop : [1199,4],
        itemsTablet : [767,3],
        itemsTabletSmall : [680,3],
        itemsMobile : [480,3],
        autoPlay : false
        });
    
    /*---Plus Minus Start--- */
     $(".cart-plus-minus-button").append();
      $(".qtybutton").on("click", function() {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        if ($button.text() == "+") {
          var newVal = parseFloat(oldValue) + 1;
        } else {
           // Don't allow decrementing below zero
          if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
            } else {
            newVal = 0;
          }
          }
        $button.parent().find("input").val(newVal);
      });

    /*---cart page---*/
    $(".cart-carousel-start").owlCarousel({
        navigation: false,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: true,
        items :2,
        itemsDesktop : [1199,1],
        itemsTablet : [767,2],
        itemsTabletSmall : [680,2],
        itemsMobile : [480,1],
        autoPlay : false
        });
    
    /*----blog page---*/   
    $(".sin-blog-carousel").owlCarousel({
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: true,
        items :1,
        itemsDesktop : [1199,1],
        itemsTablet : [991,1],
        itemsTabletSmall : [680,1],
        itemsMobile : [480,1],
        autoPlay : false
        });
    
    
  /* ---payment-accordion---*/ 
	//$(".payment-accordion").collapse({
	//	accordion:true,
	//  open: function() {
	//	this.slideDown(550);
	//  },
	//  close: function() {
	//	this.slideUp(550);
	//  }
	//});
    
    
    /*---mixitup active---*/
   
    //$('.mixitup-content').mixItUp();
    
    /*---fancybox active---- */	
	$('.fancybox').fancybox();
    
    /*---showlogin toggle function----*/
	 $( '#showlogin' ).on('click', function() {
        $( '#checkout-login' ).slideToggle(900);
     }); 
	
    /*---showcoupon toggle function----*/
     //$( '#showcoupon' ).on('click', function() {
     //   $( '#checkout_coupon' ).slideToggle(900);
     //});
	 
    /*---Create an account toggle function---*/
     //$( '#cbox' ).on('click', function() {
     //   $( '#cbox_info' ).slideToggle(900);
     //});
	 
    /*---Create an account toggle function----*/
     //$( '#ship-box' ).on('click', function() {
     //   $( '#ship-box-info' ).slideToggle(1000);
     //});
    
    /*---scrollUp---*/	
    //$.scrollUp({
    //    scrollText: '<i class="fa fa-chevron-up"></i>',
    //    easingType: 'linear',
    //    scrollSpeed: 900,
    //    animation: 'fade'
    //});


    
})(jQuery);
