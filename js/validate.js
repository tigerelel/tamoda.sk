/*!
 * Validation of forms
 */
(function( factory ) {
    if ( typeof define === "function" && define.amd ) {
        define( ["jquery"], factory );
    } else {
        factory( jQuery );
    }
}(function( $ ) {
    $(document).ready(function(){
        $('#login-form').validate({
            rules: {
                'Users[username]': "required",
                'Users[password]': "required"
            },
            messages: {
                'Users[username]': "Please Enter Login",
                'Users[password]': "Please Enter Password"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        $('#login-form-modal').validate({
            rules: {
                'Users[username]': "required",
                'Users[password]': "required"
            },
            messages: {
                'Users[username]': "Please Enter Login",
                'Users[password]': "Please Enter Password"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });


        $("#users-register-form").validate({
            rules: {
                'Users[username]': {
                    required: true,
                },
                'Users[email]': {
                    required: true,
                }
            },
            messages: {
                'Users[username]': {
                    required: "Please Enter Username",
                },
                'Users[email]': {
                    required: "Please Enter Email"
                }
            },
            onkeyup: function(element) {
                if ($(element) == $('#username_reg') || $('#username_reg').val().length == 0) {
                    $('#avbl_username_error').hide();
                }
                if ($(element) == $('#email_reg') || $('#email_reg').val().length == 0) {
                    $('#avbl_email_error').hide();
                }
            }
        });

        $('#users-confirmation-form').validate({
            rules: {
                'Users[password]': "required",
                'Users[confirm_password]': "required",
                'Users[confirm_password]': {
                    equalTo: '#password'
                }
            },
            submitHandler: function(form) {
                $('#users-confirmation-form').submit();
            }
        });


        $('#address-form').validate({
            rules: {
                'Users[username]': "required",
                'Users[password]': "required"
            },
            messages: {
                'Users[username]': "Please Enter Login",
                'Users[password]': "Please Enter Password"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        $('#profile-form').validate({
            rules: {
                'UsersAdditional[first_name]': "required",
                'UsersAdditional[last_name]': "required",
                'Users[username]': {
                    required: true
                },
                'Users[email]': {
                    required: true
                }
            },
            messages: {
                'UsersAdditional[first_name]': "This field is required",
                'UsersAdditional[last_name]': "This field is required"

            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        $( "#recover-form" ).validate({
            rules: {
                'Users[password]': "required",
                'new_password': "required",
                'new_password': {
                    equalTo: '#password'
                }
            }
        });

        $( "#forgot-form" ).validate({
            rules: {
                'Users[email]': {
                    required: true,
                    email: true
                }
            },
            messages: {
                'Users[email]': {
                    email: "Please enter valid email"
                }
            }
        });

        $( "#question-form" ).validate({
            rules: {
                'ProductsComments[question]': "required",
            }
        });

        $('#review-form').validate({
            ignore: []
        });

        $.validator.addMethod("validUrl",

            function(value, element) {

                var matchYtb = value.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);

                var matchVm = value.match(/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);

                var result = (matchYtb && matchYtb[2].length == 11) || (matchVm && matchVm[2]) || (value == '') ? true : false;

                return result;
            },
            "Please enter vimeo or youtube link."
        );

        $('#product-form').validate({
            ignore: ":hidden",
            rules: {
                'Products[categories_id]': "required",
                'Products[name]': "required",
                'Products[description]': "required",
                'Products[location]': "required",
                'Products[price]': {
                    required: true,
                    number: true
                },
                'delivery[]': "required",
                'payment[]': "required",
                'Products[video_link]': {
                    url: true,
                    validUrl: true
                }


            },
            messages: {
                'Products[categories_id]': "Please choose a category.",
                'Products[name]': 'Please enter a title.',
                'Products[description]': 'Please enter a description.',
                'Products[location]': 'Please enter a location.',
                'delivery[]': 'Please select at least one shipping option.',
                'payment[]': 'Please select at least one payment option.',
                'Products[price]': {
                    required: 'Please enter a price.'
                },
                'Products[subcategories_id]': 'Please choose a category.',
                'Products[subSubcategories_id]': 'Please choose a category.',
                //'sizes[size][]': 'Please select at least one size.',
                //'colors[color][]': 'Please select at least one color.'

            },
            submitHandler: function(form) {
                form.submit();
            },
            errorElement: "div",
            errorPlacement: function(error, element) {
                error.appendTo($(element).parents('.form-group'));
            }
        });

        $('#comment-form').validate({
           rules: {
               'ProductsComments[comment]': 'required'
           }
        });

        $('#message-form').validate({
            ignore: [],
            rules: {
                'Messages[message]': 'required',
                'message_id': 'required'
            },
            messages: {
                'Messages[message]': 'Please enter a message',
                'message_id': 'Please choose a conversation'
            }
        });

        //$('#offer-form').validate({
        //    rules: {
        //        'ProductsComments[offer_price]': 'number',
        //    },
            //messages: {
            //    'ProductsComments[offer_price]': 'Please enter a price',
            //}
        //})

    })


}))