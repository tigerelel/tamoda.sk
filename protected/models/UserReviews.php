<?php

/**
 * This is the model class for table "user_reviews".
 *
 * The followings are the available columns in table 'user_reviews':
 * @property string $id
 * @property string $from_user
 * @property string $to_user
 * @property string $review
 * @property integer $rating
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Users $fromUser
 * @property Users $toUser
 */
class UserReviews extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_reviews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_user, to_user, review, rating, product_id', 'required'),
			array('rating', 'numerical', 'integerOnly'=>true),
			array('from_user, to_user', 'length', 'max'=>11),
			array('review', 'filter', 'filter' => 'trim'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_user, to_user, review, rating, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fromUser' => array(self::BELONGS_TO, 'Users', 'from_user'),
			'toUser' => array(self::BELONGS_TO, 'Users', 'to_user'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'from_user' => 'From User',
			'to_user' => 'To User',
			'review' => 'Review',
			'rating' => 'Rating',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('from_user',$this->from_user,true);
		$criteria->compare('to_user',$this->to_user,true);
		$criteria->compare('review',$this->review,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserReviews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
