<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property integer $users_id
 * @property integer $categories_id
 * @property string $location
 * @property string $price
 * @property string $name
 * @property integer $status
 * @property integer $giveaway
 * @property integer $ad_type
 * @property integer $created_date
 * @property integer $updated
 */
class Products extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categories_id, location, name, description', 'required'),
			array('users_id, categories_id, new, ad_type', 'numerical', 'integerOnly'=>true),
			array('price', 'match', 'pattern'=>'/^[0-9]{1,12}(\.[0-9]{0,4})?$/'),
			array('lat, lng', 'numerical'),
			array('location', 'length', 'max'=>150),
			array('price', 'length', 'max'=>10),
			array('name', 'length', 'max'=>255),
            array('name, description, location, price', 'filter', 'filter' => 'trim'),
			array('description, delivery, payment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, users_id, categories_id, location, lat, lng, price, name, status, video_link, new created_date, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::BELONGS_TO, 'Categories', 'categories_id'),
			'filters' => array(self::HAS_MANY, 'ProductFilter', 'product_id'),
			'media' => array(self::HAS_MANY, 'ProductsMedia', 'product_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'users_id'),
			'comments' => array(self::HAS_MANY, 'ProductsComments', 'product_id'),
			'mainPhoto' => array(self::HAS_ONE, 'ProductsMedia', 'product_id', 'condition' => 'main_photo=1'),
			'favourite' => array(self::HAS_ONE, 'FavoriteProducts', 'product_id', 'condition' => 'user_id='.Yii::app()->session['user_id']),
			'video_file' => array(self::HAS_ONE, 'ProductVideos', 'product_id'),
			'productTop' => array(self::HAS_ONE, 'ProductsTop', 'product_id'),
			'productsReports' => array(self::HAS_MANY, 'ProductsReports', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'users_id' => 'Users',
			'categories_id' => 'Categories',
			'location' => 'Location',
			'lat' => 'Lat',
			'lng' => 'Lng',
			'price' => 'Price',
			'name' => 'Title',
			'status' => 'Status',
			'giveaway' => 'Giveaway',
			'created_date' => 'Created Date',
			'updated' => 'Updated Date'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('users_id',$this->users_id);
		$criteria->compare('categories_id',$this->categories_id);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lng',$this->lng);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('giveaway',$this->giveaway);
		$criteria->compare('new',$this->new);
		$criteria->compare('video_link',$this->video_link);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function maxPrice($catid = null, $products = null) {

		if (!isset($products)) {
			$products = Products::model()->findAll();
		}

		$arr = [];
		if (isset($catid)) {
			foreach($products as $key=>$product) {
				if ($product->categories->parentCategory->parent_id == $catid ||
					$product->categories->parent_id == $catid ||
					$product->categories_id == $catid)
				{
					$arr[$catid][$key] = $product->price;
				}
			}
			$max = !empty($arr[$catid]) ? round(max($arr[$catid])) : 0;

		} else {

			foreach($products as $key=>$product) {
				$arr[$key] = $product['price'];
			}
			$max = round(max($arr));
		}

		return intval($max);
	}

	public function minPrice($catid = null, $products = null) {

		if (!isset($products)) {
			$products = Products::model()->findAll();
		}

		$arr = [];
		if (isset($catid)) {
			foreach ($products as $key => $product) {
				if ($product->categories->parentCategory->parent_id == $catid ||
					$product->categories->parent_id == $catid ||
					$product->categories_id == $catid
				) {
					$arr[$catid][$key] = $product->price;
				}
			}
			$min = !empty($arr[$catid]) ? round(min($arr[$catid])) : 0;

		} else {
			foreach ($products as $key => $product) {
				$arr[$key] = $product['price'];
			}
			$min = round(min($arr));
		}

		return intval($min);
	}

	public function countAds($t) {

		$count = Products::model()->countByAttributes(array('users_id' => Yii::app()->session['user_id'], 'status' => $t));
		return $count;

	}


}
