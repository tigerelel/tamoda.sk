<?php

/**
 * This is the model class for table "transactions".
 *
 * The followings are the available columns in table 'transactions':
 * @property string $id
 * @property integer $type
 * @property string $payment_date
 * @property integer $payment_status
 * @property string $receiver_email
 * @property string $payer_id
 *
 * The followings are the available model relations:
 * @property Users $payer
 */
class Transactions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transactions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payment_date, receiver_email, payer_id', 'required'),
			array('type, payment_status', 'numerical', 'integerOnly'=>true),
			array('receiver_email', 'length', 'max'=>60),
			array('payer_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, payment_date, payment_status, receiver_email, payer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'payer' => array(self::BELONGS_TO, 'Users', 'payer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'payment_date' => 'Payment Date',
			'payment_status' => 'Payment Status',
			'receiver_email' => 'Receiver Email',
			'payer_id' => 'Payer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('payment_status',$this->payment_status);
		$criteria->compare('receiver_email',$this->receiver_email,true);
		$criteria->compare('payer_id',$this->payer_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transactions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	function validateTransaction($myPost,$product) {
		$valid = true;

		/*
		 * Проверка соответствия цен
		 */
		if($product->getTotalPrice($myPost['quantity']) != $myPost['payment_gross']){
			$valid = false;
		}
		/*
		 * Проверка на нулевую цену
		 */
		elseif($myPost['payment_gross'] == 0){
			$valid = false;
		}
		/*
		 * Проверка статуса платежа
		 */
		elseif($myPost['payment_status'] !== 'Completed'){
			$valid = false;
		}
		/*
		 * Проверка получателя платежа
		 */
		elseif($myPost['receiver_email'] != 'YOUR PAYPAL ACCOUNT'){
			$valid = false;
		}
		/*
		 * Проверка валюты
		 */
		elseif($myPost['mc_currency'] != 'USD'){
			$valid = false;
		}

		return $valid;
	}
}
