<?php

/**
 * This is the model class for table "products_media".
 *
 * The followings are the available columns in table 'products_media':
 * @property string $id
 * @property string $product_id
 * @property string $prod_tmp_id
 * @property string $photo
 * @property integer $sort_order
 * @property integer $main_photo
 *
 * The followings are the available model relations:
 * @property Products $product
 * @property ProductsTmp $prodTmp
 */
class ProductsMedia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sort_order, main_photo', 'numerical', 'integerOnly'=>true),
			array('product_id, prod_tmp_id', 'length', 'max'=>11),
			array('photo', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, prod_tmp_id, photo, sort_order, main_photo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'prodTmp' => array(self::BELONGS_TO, 'ProductsTmp', 'prod_tmp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'prod_tmp_id' => 'Prod Tmp',
			'photo' => 'Photo',
			'sort_order' => 'Sort Order',
			'main_photo' => 'Main Photo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('prod_tmp_id',$this->prod_tmp_id,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('main_photo',$this->main_photo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsMedia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
