<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $emailTo;
	//public $subject;
	public $body;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('name, email, emailTo, body', 'required', 'on' => 'referTo'),
			array('name, email, body', 'required', 'on' => 'contactAdmin'),
			// email has to be a valid email address
			array('email, emailTo', 'email'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Your name',
			'email' => 'Your email',
			'body' => 'Message'
			//'verifyCode'=>'Verification Code',
		);
	}
}