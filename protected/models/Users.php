<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $created_date
 * @property string $modified_date
 * @property string $last_login_date
 * @property string $last_login_ip
 * @property integer $activated
 * @property integer $blocked
 * @property string $confirmation_id
 *
 * The followings are the available model relations:
 * @property UsersAdditional $usersAdditional
 */
class Users extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    public $confirm_password;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password', 'required', 'on' => 'login'),
            array('username, email', 'required', 'on' => 'create'),
            array('password, confirm_password', 'required', 'on' => 'createPassword'),
            array('username, email', 'required', 'on' => 'register'),
            array('confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'Passwords don\'t match'), 'on' => 'register'),

            array('password, username', 'required', 'on' => 'update'),
            array('activated, blocked', 'numerical', 'integerOnly'=>true),
            array('username, email, password', 'length', 'max'=>64),
            array('last_login_ip', 'length', 'max'=>15),
            array('confirmation_id', 'length', 'max'=>255),
            array('modified_date, last_login_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, email, password, created_date, modified_date, last_login_date, last_login_ip, activated, blocked, confirmation_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usersAdditional' => array(self::HAS_ONE, 'UsersAdditional', 'users_id'),
            'reviews' => array(self::HAS_MANY, 'UserReviews', 'to_user')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'last_login_date' => 'Last Login Date',
            'last_login_ip' => 'Last Login Ip',
            'activated' => 'Activated',
            'blocked' => 'Blocked',
            'confirmation_id' => 'Confirmation',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('created_date',$this->created_date,true);
        $criteria->compare('modified_date',$this->modified_date,true);
        $criteria->compare('last_login_date',$this->last_login_date,true);
        $criteria->compare('last_login_ip',$this->last_login_ip,true);
        $criteria->compare('activated',$this->activated);
        $criteria->compare('blocked',$this->blocked);
        $criteria->compare('confirmation_id',$this->confirmation_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

//    public function behaviors() {
//        return array(
//            // attach channel behavior
//            'channel' => array(
//                'class' => '\YiiNodeSocket\Behaviors\ArChannel',
//                'updateOnSave' => true
//            ),
//
//        );
//    }
}
