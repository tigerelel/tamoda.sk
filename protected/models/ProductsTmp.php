<?php

/**
 * This is the model class for table "products_tmp".
 *
 * The followings are the available columns in table 'products_tmp':
 * @property string $id
 * @property string $categories_id
 * @property string $location
 * @property double $lat
 * @property double $lng
 * @property string $price
 * @property string $name
 * @property string $description
 * @property integer $new
 * @property string $delivery
 * @property string $payment
 * @property string $created_date
 */
class ProductsTmp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_tmp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categories_id, location, name, description', 'required'),
			array('new', 'numerical', 'integerOnly'=>true),
			array('lat, lng', 'numerical'),
			array('name', 'length', 'max'=>255),
			array('categories_id', 'length', 'max'=>11),
			array('location', 'length', 'max'=>150),
			array('price', 'length', 'max'=>10),
			array('delivery, payment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, categories_id, location, lat, lng, price, name, description, new, delivery, payment, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories' => array(self::BELONGS_TO, 'Categories', 'categories_id'),
			'media' => array(self::HAS_MANY, 'ProductsMedia', 'prod_tmp_id'),
			'mainPhoto' => array(self::HAS_ONE, 'ProductsMedia', 'prod_tmp_id', 'condition' => 'main_photo=1'),
			'video_file' => array(self::HAS_ONE, 'ProductVideos', 'prod_tmp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'categories_id' => 'Categories',
			'location' => 'Location',
			'lat' => 'Lat',
			'lng' => 'Lng',
			'price' => 'Price',
			'name' => 'Name',
			'description' => 'Description',
			'new' => '1-new,0-used',
			'delivery' => 'Delivery',
			'payment' => 'Payment',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('categories_id',$this->categories_id,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lng',$this->lng);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('new',$this->new);
		$criteria->compare('delivery',$this->delivery,true);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
