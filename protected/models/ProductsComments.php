<?php

/**
 * This is the model class for table "products_comments".
 *
 * The followings are the available columns in table 'products_comments':
 * @property string $id
 * @property string $user_id
 * @property string $product_id
 * @property string comment
 * @property string offer_price
 * @property string confirmed
 * @property string reply_to
 * @property string created_date
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Products $product
 */
class ProductsComments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, product_id, comment', 'required', 'on' => 'comment'),
			array('user_id, product_id, offer_price', 'required', 'on' => 'offerPrice'),
			array('user_id, product_id, reply_to', 'length', 'max'=>11),
			array('confirmed', 'numerical', 'integerOnly'=>true),
			array('offer_price', 'length', 'max'=>10),
			array('offer_price', 'match', 'pattern'=>'/^[0-9]{1,12}(\.[0-9]{0,4})?$/'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, product_id, offer_price, confirmed, reply_to, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
            'replyToUser' => array(self::BELONGS_TO, 'ProductsComments', 'reply_to')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'product_id' => 'Product',
			'comment' => 'Comment',
			'offered_price' => 'Offered Price',
            'confirmed' => 'Confirmded',
			'created_date' => 'Created Date'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('offer_price',$this->offer_price,true);
		$criteria->compare('confirmed',$this->confirmed,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
