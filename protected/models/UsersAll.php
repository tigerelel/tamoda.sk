<?php

/**
 * This is the model class for table "users_all".
 *
 * The followings are the available columns in table 'users_all':
 * @property string $id
 * @property integer $user_type
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $created_date
 * @property string $modified_date
 * @property string $last_login_date
 * @property string $last_login_ip
 * @property integer $activated
 * @property integer $blocked
 * @property string $users_id
 * @property string $first_name
 * @property string $last_name
 * @property integer $age
 * @property integer $sex
 * @property string $avatar
 */
class UsersAll extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_all';
	}

	public function primaryKey() {
		return 'users_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, email, password, users_id', 'required'),
			array('user_type, activated, blocked, age, sex', 'numerical', 'integerOnly'=>true),
			array('id, users_id', 'length', 'max'=>11),
			array('username, email, password, first_name, last_name', 'length', 'max'=>64),
			array('last_login_ip', 'length', 'max'=>15),
			array('avatar', 'length', 'max'=>255),
			array('created_date, modified_date, last_login_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_type, username, email, password, created_date, modified_date, last_login_date, last_login_ip, activated, blocked, users_id, first_name, last_name, age, sex, avatar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_type' => 'User Type',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'created_date' => 'Created Date',
			'modified_date' => 'Modified Date',
			'last_login_date' => 'Last Login Date',
			'last_login_ip' => 'Last Login Ip',
			'activated' => 'Activated',
			'blocked' => 'Blocked',
			'users_id' => 'Users',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'age' => 'Age',
			'sex' => 'Sex',
			'avatar' => 'Avatar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_type',$this->user_type);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modified_date',$this->modified_date,true);
		$criteria->compare('last_login_date',$this->last_login_date,true);
		$criteria->compare('last_login_ip',$this->last_login_ip,true);
		$criteria->compare('activated',$this->activated);
		$criteria->compare('blocked',$this->blocked);
		$criteria->compare('users_id',$this->users_id,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('age',$this->age);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('avatar',$this->avatar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersAll the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
