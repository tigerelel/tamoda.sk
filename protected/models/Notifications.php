<?php

/**
 * This is the model class for table "notifications".
 *
 * The followings are the available columns in table 'notifications':
 * @property string $id
 * @property string $user_id
 * @property string $product_id
 * @property string $comment_id
 * @property string $review_id
 * @property string $text
 * @property integer $read
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Notifications extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notifications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, text', 'required'),
			array('read', 'numerical', 'integerOnly'=>true),
			array('user_id, product_id, comment_id, review_id', 'length', 'max'=>11),
			array('text, created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, comment_id, review_id, text, read, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id'),
			'comment' => array(self::BELONGS_TO, 'ProductsComments', 'comment_id'),
			'review' => array(self::BELONGS_TO, 'UserReviews', 'review_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'product_id' => 'Product',
			'comment_id' => 'Comment',
			'review_id' => 'Feedback',
			'text' => 'Text',
			'read' => 'Read',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('comment_id',$this->comment_id,true);
		$criteria->compare('review_id',$this->review_id,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('read',$this->read);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Notifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function notify($user_id, $product_id = null, $comment_id = null, $review_id = null, $text)
	{
		$notification = new Notifications;
		$notification->user_id = $user_id;
		$notification->product_id = $product_id;
		$notification->comment_id = $comment_id;
		$notification->review_id = $review_id;
		$notification->text = $text;
		$notification->save();

	}
}
