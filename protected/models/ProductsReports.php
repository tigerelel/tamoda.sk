<?php

/**
 * This is the model class for table "product_report".
 *
 * The followings are the available columns in table 'product_report':
 * @property string $id
 * @property string $report_type
 * @property string $product_id
 * @property string $report_text
 */
class ProductsReports extends CActiveRecord
{
	public static $reportType = [
		1 => "Wrong category",
		2 => "No valid asking price",
		3 => "Duplicate ad",
		4 => "Property ad",
		5 => "Inappropriate material",
		6 => "Copyright infringement",
		7 => "Suspected stolen item",
		8 => "Not marked as sold"
	];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_reports';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('report_type, product_id', 'required'),
			array('report_type, product_id', 'length', 'max'=>11),
			array('report_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, report_type, product_id, report_text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Products', 'product_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'report_type' => 'Report Type',
			'product_id' => 'Product',
			'report_text' => 'Report Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('report_type',$this->report_type,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('report_text',$this->report_text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
