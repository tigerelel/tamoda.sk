<?php

/**
 * This is the model class for table "threads".
 *
 * The followings are the available columns in table 'threads':
 * @property string $id
 * @property string $seller_id
 * @property string $buyer_id
 * @property string $product_id
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Messages[] $messages
 * @property Users $seller
 * @property Users $buyer
 * @property Users $product
 */
class Threads extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'threads';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('seller_id, buyer_id, product_id', 'required'),
			array('seller_id, buyer_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, from_user, to_user, product_id, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Messages', 'thread_id'),
			'seller' => array(self::BELONGS_TO, 'Users', 'seller_id'),
			'buyer' => array(self::BELONGS_TO, 'Users', 'buyer_id'),
			'product' => array(self::BELONGS_TO, 'Products', 'product_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'seller_id' => 'Seller',
			'buyer_id' => 'Buyer',
			'product_id' => 'Product',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('seller_id',$this->seller_id,true);
		$criteria->compare('buyer_id',$this->buyer_id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Threads the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
