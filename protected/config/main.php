<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Tamoda.en',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.fbLikeShareButton.*',
		'ext.dzRaty.*',
		'application.extensions.easyPaypal.*'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>false,
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

        'admin',
//		'newsletter' =>array(
//			'components' => array(
//				'tools' => array(
//					'class'=>'ToolsComponent',
//				),
//			)
//		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'loginUrl'=>array('profile/login'),
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format

//        'request' => array(
//            'enableCookieValidation'=>true,
//            'enableCsrfValidation'=>true,
//        ),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
					'admin/admins' => 'admin/',
					'<action:(profile)>' => 'profile/profile',
					'/shops/<id:\d+>' => 'shops/view'
			),
		),

		'Paypal' => array(
		    'class'=>'application.components.Paypal',
		    'apiUsername' => 'sergey.arushanyan-facilitator-1_api2.gmail.com',
		    'apiPassword' => 'VSP6F52WPTPGWQHX',
		    'apiSignature' => 'AByCNr1.zCg-2w3USM3enpno1HoLA.6E--tmE5VeyPjAnWYWH0umdqGC',
		    'apiLive' => false,
		 
		    'returnUrl' => 'profile/myAds/', //regardless of url management component
		    'cancelUrl' => 'profile/myAds/', //regardless of url management component
		 
		    // Default currency to use, if not set USD is the default
		    'currency' => 'USD',
		 
		    // Default description to use, defaults to an empty string
		    //'defaultDescription' => '',
		 
		    // Default Quantity to use, defaults to 1
		    //'defaultQuantity' => '1',
		 
		    //The version of the paypal api to use, defaults to '3.0' (review PayPal documentation to include a valid API version)
		    //'version' => '3.0',
		),

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

//				array(
//					'class'=>'CWebLogRoute',
//				),

			),
		)
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);