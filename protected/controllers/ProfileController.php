<?php
/**
 * Created by PhpStorm.
 * User: gohar
 * Date: 12/11/15
 * Time: 10:45 AM
 */

class ProfileController extends Controller {

    public $defaultAction = "login";

    public function init() {
        parent::init();

    }


    public function actionRegister() {

        $clientScript=Yii::app()->getClientScript();
        $clientScript->registerScript("validate","var baseUrl=".CJSON::encode(Yii::app()->request->baseUrl).";",CClientScript::POS_HEAD);

        $newModel = new Users('register');
        $secModel = new Users('createPassword');

        $toSave = true;

        if(isset($_GET['hash']) && !empty($_GET['hash'])) {
            $confirmation_id = $_GET['hash'];

            $model = Users::model()->findByAttributes(array('confirmation_id'=>$confirmation_id,'activated'=>0));
            if (!empty($model)) {
                $toSave = false;
                //$model->scenario = 'register';
            }
        }

        $length = 8;
        $randomPassword = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

        if(!empty($_POST['Users'])){

            if(!$toSave) {

                $model->password  = FrontHelpers::hashString(strip_tags($_POST['Users']['password']));
                $model->activated = 1;
                $model->confirmation_id = null;
//                var_dump($model->validate());die;
                if($model->validate()){

                    if($model->update()){

                        $session = Yii::app()->session;
                        $session->open();
                        $session['user_id'] = $model->id;
                        $session['username'] = $model->username;
                        Yii::app()->user->setFlash('success', 'Welcome '.$session['username']);

                        if (isset(Yii::app()->request->cookies['prod_tmp'])) {
                            $this->redirect(array('products/prodTemp', 'id' => Yii::app()->request->cookies['prod_tmp']->value));
                        }
                        elseif (isset(Yii::app()->request->cookies['prod_id'])) {
                            $prod_id = Yii::app()->request->cookies['prod_id']->value;
                            unset(Yii::app()->request->cookies['prod_id']);

                            $this->redirect(array("products/advert", 'id' => $prod_id));
                        }
                        $this->redirect(Yii::app()->homeUrl);
                    } else {
                        Yii::app()->user->setFlash('notice', "Oooops. Something wrong.");
                    }
                }
            } else {
                $errors = [];

                $model = new Users;
                $model->attributes=$_POST['Users'];
                $model->password = FrontHelpers::hashString(strip_tags($randomPassword));
                $model->created_date    = FrontHelpers::toSQLDateTime();
                $model->activated       = 0;
                $model->blocked         = 0;
                $model->last_login_ip = $_SERVER['REMOTE_ADDR'];
                $model->modified_date = '0000-00-00 00:00:00';
                $model->last_login_date = '0000-00-00 00:00:00';
                $model->confirmation_id = FrontHelpers::hashString(strip_tags($_POST['Users']['email']));

                if ($model->validate()) {
                    if ($model->save()) {
                        FrontHelpers::confirmationEmail($model->confirmation_id, $model->email,'activation',$randomPassword, $model->username);
                        Yii::app()->user->setFlash('success', "Please check your email for confirmation.");
                        $this->redirect(Yii::app()->homeUrl);
                    } else {
                        Yii::app()->user->setFlash('notice', "Oooops. Something wrong.");
                    }
                } else {
                    foreach ($model->getErrors() as $error) {

                        Yii::app()->user->setFlash('danger', $error[0]);
                    }
                }
            }
        } elseif (isset($_POST['token'])) {
            $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
            $data = json_decode($s, true);

            if (isset($data['uid'])) {
                $user = Users::model()->findByAttributes(array('email' => $data['email']));
                if (!$user) {
                    $user = new Users;
                    $user->email = $data['email'];
                    $user->username = substr($data['email'], 0, strpos($data['email'], '@'));

                    if ($user->save()) {
                        $usersAdd = new UsersAdditional;
                        $usersAdd->first_name = $data['first_name'];
                        $usersAdd->last_name = $data['last_name'];

                        $usersAdd->users_id = $user->id;
                        $usersAdd->save();

                        $session = Yii::app()->session;
                        $session->open();
                        $session['user_id'] = $user->id;
                        $session['username'] = $user->username;

                        Yii::app()->user->setFlash('success', 'Welcome, '.$user->username);

                        if (isset(Yii::app()->request->cookies['prod_tmp'])) {
                            $this->redirect(array('products/prodTemp', 'id' => Yii::app()->request->cookies['prod_tmp']->value));
                        }
                        elseif(isset(Yii::app()->request->cookies['prod_id'])){
                            $prod_id = Yii::app()->request->cookies['prod_id']->value;
                            unset(Yii::app()->request->cookies['prod_id']);

                            $this->redirect(array("products/advert", 'id' => $prod_id));
                        }
                    }
                } else {
                    Yii::app()->user->setFlash('danger', 'This account already exists, please sign in to continue.');
                }
            }
        }

        if (!Yii::app()->session['user_id']) {
            $this->render('register', array('newModel' => $newModel, 'secModel' => $secModel));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionCheckUserNameOrEmail() {

        if (Yii::app()->request->isAjaxRequest) {
            $id = 0;
            if (Yii::app()->session['user_id']) {
                $id = Yii::app()->session['user_id'];
            }
            if (!empty ($_GET['username'])) {
                $searchVal = strip_tags($_GET['username']);
                $result = Users::model()->findByAttributes(array('username' => $searchVal), 'id <>'.$id);
                $valid = !empty($result) ? "false" : "true";
                echo $valid;
            }
            if (!empty ($_GET['email'])) {
                $searchVal = strip_tags($_GET['email']);

                $result = Users::model()->findByAttributes(array('email' => $searchVal), 'id <>'.$id);
                $valid = !empty($result) ? "false" : "true";
                echo $valid;
            }
        }
    }

    public function actionLogin() {

        $model = new Users('login');

        if(isset($_POST['Users'])) {
            $model->attributes=$_POST['Users'];

            $username = strip_tags($_POST['Users']['username']);
            $password = FrontHelpers::hashString(strip_tags($_POST['Users']['password']));
            $criteria=new CDbCriteria();
            $criteria->compare('username',$username, false);
            $criteria->compare('password',$password, false);
            $criteria2 = new CDbCriteria;
            $criteria2->compare('email', $username, false);
            $criteria->mergeWith($criteria2, 'OR');

            $model = Users::model()->find($criteria);

            if($model == null) {
                Yii::app()->user->setFlash('danger', 'Incorrect email or password');
                $this->redirect(array('login'));
            }else {
                $session = Yii::app()->session;
                $session->open();
                $session['user_id'] = $model->id;
                $session['username'] = $model->username;

                if (isset($_POST['remember_pass'])) {
                    $cookie = new CHttpCookie('remember_me', 'remember_me');
                    $cookie->expire = time()+3600*24*30;
                    Yii::app()->request->cookies['remember_me'] = $cookie;
                }

                Yii::app()->user->setFlash('success', 'Welcome back '.$model->username);

                if (isset(Yii::app()->request->cookies['prod_tmp'])) {
                    $this->redirect(array('products/prodTemp', 'id' => Yii::app()->request->cookies['prod_tmp']->value));
                }
                elseif(isset(Yii::app()->request->cookies['prod_id'])){
                    $prod_id = Yii::app()->request->cookies['prod_id']->value;
                    unset(Yii::app()->request->cookies['prod_id']);
                    
                    $this->redirect(array("products/advert", 'id' => $prod_id));
                }
                else {
                    $this->redirect(Yii::app()->homeUrl);
                }
            }
        } elseif (isset($_POST['token'])) {

            $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
            $data = json_decode($s, true);

            if (isset($data['uid'])) {
                $user = Users::model()->findByAttributes(array('email' => $data['email']));
                if (!$user) {
                    Yii::app()->user->setFlash('error', 'Your social account isn\'t connected yet, '.
                        'you can sign in with an existing account, or Sign Up to get started.');
                } else {
                    $session = Yii::app()->session;
                    $session->open();
                    $session['user_id'] = $user->id;
                    $session['username'] = $user->username;

                    Yii::app()->user->setFlash('success', 'Welcome back, '.$user->username);

                    if (isset(Yii::app()->request->cookies['prod_tmp'])) {
                        $this->redirect(array('products/prodTemp', 'id' => Yii::app()->request->cookies['prod_tmp']->value));
                    } else {
                        $this->redirect(Yii::app()->homeUrl);
                    }
                }
            }
        }
        
        if (!Yii::app()->session['user_id']) {
            $this->render('login', array('model'=>$model));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionLogout()
    {
        if (isset(Yii::app()->request->cookies['prod_tmp'])) {
            $model = ProductsTmp::model()->findByPk(Yii::app()->request->cookies['prod_tmp']);
            if ($model) {
                $model->delete();
            }
            unset(Yii::app()->request->cookies['prod_tmp']);
        }
        Yii::app()->session->destroy();

        $this->redirect(Yii::app()->homeUrl);
    }


    public function actionForgotPassword() {

        $model = new Users;

        $randomPassword = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);

        if (!empty($_POST['Users']['email'])) {

            $email = $_POST['Users']['email'];

            $user = Users::model()->findByAttributes(array('email' => $email, 'activated' => 1));

            if (!empty($user)) {

                $user->password_recovery = FrontHelpers::hashString(strip_tags($email));

                if ($user->update()) {
                    FrontHelpers::confirmationEmail($user->password_recovery, $user->email,'recover',$randomPassword, $user->username);
                    Yii::app()->user->setFlash('success', "Please check your email.");
                    $this->redirect('login', array('model'=>$user));
                } else {
                    Yii::app()->user->setFlash('notice', "Oooops. Something wrong.");
                }
            } else {
                Yii::app()->user->setFlash('danger', "User not found");
            }
        }
        if (!Yii::app()->session['user_id']) {
            $this->render('forgot_password', array('model' => $model));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionRecoverPassword($key) {

        if (!empty($_POST['Users'])) {

            $model = Users::model()->findByAttributes(array('password_recovery' => $key));
            if ($model) {
                $model->password = FrontHelpers::hashString(strip_tags($_POST['Users']['password']));
                $model->password_recovery = '';

                if($model->update()){
                    Yii::app()->user->setFlash('success', "Password has successfully recovered");
                    $this->redirect('login', array('model'=>$model));
                }
            } else {
                Yii::app()->user->setFlash('danger', "Recovery key not found");
            }
        }
        $model = new Users;

        if (!Yii::app()->session['user_id']) {
            $this->render('recover_password', array('model'=>$model));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionEditProfile() {

        $sub_model = new Subscribers();

        $id = Yii::app()->session['user_id'];

        if ($id) {
            if (!empty($_POST['Subscribers']['user_id'])) {
                $sub_model->user_id = $id;
                $sub_model->save();
            }

            $model=Users::model()->findByPk($id);
            $sec_model = $this->loadSecModel($id);
            if (!$sec_model) {
                $sec_model = new UsersAdditional;
                $sec_model->users_id = $id;
            }

            $errors = [];
            if (!empty($_POST['Users'])) {

                if ($_POST['Users']['oldPassword']) {
                    if ($_POST['Users']['newPassword']) {
                        $old_password = FrontHelpers::hashString(strip_tags($_POST['Users']['oldPassword']));
                        if ($model->password == $old_password) {
                            $model->password = FrontHelpers::hashString(strip_tags($_POST['Users']['newPassword']));
                        } else {
                            $errors[0] = 'Incorrect old password';
                            Yii::app()->user->setFlash('danger', "Incorrect old password");
                        }
                    } else {
                        $errors[1] = 'Please enter a new password';
                    }
                }

                $model->attributes = $_POST['Users'];

                $sec_model->attributes = $_POST['Users'];
                $sec_model->save();

                if (!empty($errors)) {
                    foreach ($errors as $key=>$error) {
                        Yii::app()->user->setFlash('danger', $error);
                    }
                } else {
                    $model->save();
                    Yii::app()->user->setFlash('success', 'success');
                    $this->redirect(array('myProfile'));
                }
            }
            $this->render('edit_profile', array('model'=>$model, 'sec_model' => $sec_model, 'sub_model' => $sub_model));

        } else {
            $this->redirect(Yii::app()->homeUrl);
        }


    }

    public function actionUploadProfilePic($id) {

        if (Yii::app()->request->isAjaxRequest) {

            $sec_model = $this->loadSecModel($id);
            if (!$sec_model) {
                $sec_model = new UsersAdditional;
                $sec_model->users_id = $id;
            }

            if (!empty($_FILES)) {

                $image = $_FILES['file'];

                $success = null;

                $filename = $image['name'];
                $ext = explode('.', basename($filename));

                $filename = md5(uniqid()) . "." . array_pop($ext);
                $target = Yii::getPathOfAlias('webroot').'/images/user_pics/' . $filename;

                if(move_uploaded_file($image['tmp_name'], $target)) {
                    $success = true;
                    $path = $target;
                } else {
                    $success = false;
                }

                if ($success === true) {
                    if ($sec_model->avatar && file_exists(YiiBase::getPathOfAlias('webroot').'/images/user_pics/'. $sec_model->avatar)) {
                        unlink(YiiBase::getPathOfAlias("webroot") . '/images/user_pics/' . $sec_model->avatar);
                    }
                    $sec_model->avatar = $filename;
                    $sec_model->save();
                    $output = $filename;

                } elseif ($success === false) {
                    $output = ['error'=>'Error while uploading images. Contact the system administrator'];
                    unlink($path);

                } else {
                    $output = ['error'=>'No files were processed.'];
                }

                echo json_encode($output);
            }
        }
    }

    public function actionDeleteProfilePic()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $sec_model = $this->loadSecModel($_GET['id']);

            if($sec_model) {

                if ($sec_model->avatar && file_exists(YiiBase::getPathOfAlias('webroot').'/images/user_pics/'. $sec_model->avatar)) {
                    if (unlink(YiiBase::getPathOfAlias("webroot").'/images/user_pics/'. $sec_model->avatar)) {
                        $sec_model->avatar = null;
                        if ($sec_model->update()) {

                            echo 'success';die;
                        } else {
                            var_dump('error');die;
                        }
                    }

                } else {
                    var_dump('not found');die;
                }
            } else {
                var_dump('no id found');die;
            }
        }
    }

    public function actionMyProfile() {

        if (Yii::app()->session['user_id']) {
            $id = Yii::app()->session['user_id'];
            $model = new Users;
            if (isset($id)) {
                $user = Users::model()->findByPk($id);
                $user_add = UsersAdditional::model()->findByPk(array('user_id'=> $id));
                $newReview = new UserReviews;

                if (!empty($_POST['UserReviews'])) {

                    $newReview->rating = $_POST['UserReviews']['rating'];
                    $newReview->review = $_POST['UserReviews']['review'];
                    $newReview->from_user = Yii::app()->session['user_id'];
                    $newReview->to_user = $user->id;

                    if ($newReview->validate()) {
                        $newReview->save();
                        $note = new Notifications;
                        $note->user_id = $user->id;

                        $note->text = 'You have one new review';
                        $note->save();
                        $this->redirect(array('myProfile'));
                    }
                }

                $commentCount = ProductsComments::model()->countByAttributes(array('user_id' => $id));
                $comments = Products::model()->with('comments')->findAll('comments.user_id='.$id);

                $userProducts = Products::model()->count('users_id='.Yii::app()->session['user_id'].' AND status="sold"');

            } else {
                $this->redirect(Yii::app()->homeUrl);
            }
            $this->render('my_profile', array(
                'model'=>$model,
                'user' => $user,
                'user_add' => $user_add,
                'newReview' => $newReview,
                'commentCount' => $commentCount,
                'comments' => $comments,
                'userProducts' => $userProducts
            ));

        } else {
            $this->redirect(Yii::app()->homeUrl);
        }

    }

    public function actionProfile($username) {

        $user = Users::model()->findByAttributes(array('username' => CHtml::encode($username)));
        if ($user) {
//            $newReview = new UserReviews;

            $checkAccepted2 = null;
            if (Yii::app()->session['user_id']) {
                $checkAccepted2 = ProductsComments::model()->with('product')->findAll(
                    't.confirmed=1 AND (t.user_id='.$user->id.' OR t.user_id='.Yii::app()->session['user_id'].
                    ') AND (product.users_id='.Yii::app()->session['user_id'].' OR product.users_id='.$user->id.')');
            }

            $reviewCount = UserReviews::model()->countByAttributes(array('to_user' => $user->id));
            $commentCount = ProductsComments::model()->countByAttributes(array('user_id' => $user->id));
            $commentProds = Products::model()->with(array('comments'))->findAll('comments.user_id='.$user->id, array('group' => 'id'));

            $newReview = new UserReviews;

            if (!empty($_POST)) {

                $newReview->product_id = $_POST['product_id'];
                $newReview->rating = $_POST['score'];
                $newReview->review = CHtml::encode($_POST['UserReviews']['review']);
                $newReview->from_user = Yii::app()->session['user_id'];
                $newReview->to_user = $user->id;

                if ($newReview->validate()) {

                    $newReview->save();
                    if (isset($_POST['sold'])) {
                        $product = Products::model()->findByPk($newReview->product_id);
                        $product->status = 'sold';
                        $product->update();
                    }
                    Notifications::model()->notify($user->id, null, null, null, $newReview->id, $newReview->fromUser->username.' left feedback for you');
                    Yii::app()->user->setFlash('success', 'Your feedback was added successfully');
                    $this->redirect(array('profile', 'username' => CHtml::encode($username)));
                }
            }

        } else {
            $this->redirect(Yii::app()->homeUrl);
        }


        $this->render('profile', array(
            'user' => $user,
            'newReview' => $newReview,
            'count' => $reviewCount,
            'commentCount' => $commentCount,
            'comments' => $commentProds,
            'checkAccepted2' => $checkAccepted2
        ));
    }

    public function actionRemoveFeedback($id, $return_url = null) {

        if (Yii::app()->session['user_id']) {
            $feedback = UserReviews::model()->findByAttributes(array('id' => $id, 'from_user' => Yii::app()->session['user_id']));
            if (!$feedback) {
                Yii::app()->user->setFlash('danger', 'You are unable to remove this feedback!');
                $this->redirect(Yii::app()->baseUrl . urldecode($return_url));
            } else {

                if ($feedback->delete()) {
                    Yii::app()->user->setFlash('success', 'Your feedback has been removed!');
                    $this->redirect(Yii::app()->baseUrl . urldecode($return_url));
                }
            }
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionMyAds($t = null) {

        if (Yii::app()->session['user_id']) {

            $products = Products::model()->findAllByAttributes(array('users_id' => Yii::app()->session['user_id'], 'status' => 'active'));
            if (isset($t)) {
                $products = Products::model()->findAllByAttributes(array('users_id' => Yii::app()->session['user_id'], 'status' => CHtml::encode($t)));
            }
            $count = count($products);
            $this->render('myAds', array('products' => $products, 'count' => $count));

        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionNotifications() {

        $notes = Notifications::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user_id']), array('order' => 'id DESC'));

        $this->render('notifications', array('notes' => $notes));
    }

    public function actionMarkAsRead() {

        if (!Yii::app()->request->isAjaxRequest) {

            $unreads = Notifications::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user_id'], 'read' => 0));

            foreach ($unreads as $unread) {
                $unread->read = 1;
                $unread->update();
            }
            Yii::app()->user->setState('notes', null);
            $this->redirect('notifications');

        } else {
            if (isset($_POST['note_id'])) {
                $note = Notifications::model()->findByPk($_POST['note_id']);
                $note->read = 1;
                $note->update();
            }

        }
    }

    public function actionViewFeedback($id) {

        $review = UserReviews::model()->findByPk($id);

        $this->render('viewFeedback', array('review' => $review));
    }

    public function loadModel($id)
    {
        $model=Users::model()->findByPk($id);

        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');

        return $model;
    }

    public function loadSecModel($id)
    {
        $model=UsersAdditional::model()->findByPk($id);

        return $model;
    }
}