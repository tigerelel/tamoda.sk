<?php

class ProductsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public $defaultAction = "index";

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */


	public function filters() {
		return array(
			//'accessControl'
		);
	}

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array(
                    'logout',
					'toggleFavourite',
					'favourites'
                ),
                'users' => array('@'),
            ),

			array('allow',
				'actions' => array(
					'advert'
				),
			),
            array('allow',
                'actions' => array('register', 'login', 'captcha', 'resetPassword', 'forgotPassword', 'productsList'),
                'users' => array('?')
            ),

            array('allow',
                'actions' => array('index', 'users'),
                'users' => array('*')
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionGetSubCats() {
		if (Yii::app()->request->isAjaxRequest) {
			$subsList = [];
			if (!empty($_GET['categories_id'])) {
				$subs = Categories::model()->findAll(array('condition' => 'parent_id='.$_GET['categories_id'], 'order' => 't.id DESC'));

				$subsList = CHtml::listData($subs, 'id', 'title');

			}
			echo json_encode($subsList);
		}
	}

    public function actionGetSubSubCats() {

        if (Yii::app()->request->isAjaxRequest) {

			$subsList = [[]];

			if (!empty($_GET['id'])) {

				$subs = Categories::model()->findAll(array('condition' => 'parent_id='.$_GET['id'], 'order' => 't.id DESC'));
				$subsList[0] = CHtml::listData($subs, 'id', 'title');
				$catFilters = CategoriesFilter::model()->findByAttributes(array('categories_id' => $_GET['id']));

				if ($catFilters) {
					$subsList['filter_id'] = $catFilters->filter_new;
				}
			}
            echo json_encode($subsList);
        }
    }

	public function actionGetSubsubFilters() {

		if (Yii::app()->request->isAjaxRequest) {
			$subsubcatFilters = CategoriesFilter::model()->findAllByAttributes(array('categories_id' => $_GET['id']));
			$filterList = [];
			if ($subsubcatFilters) {
				foreach ($subsubcatFilters as $cat=>$group) {
					$filterList[$cat]['filters']['group_title'] = $group->filterGroup->type;
					$filterList[$cat]['cat'] = $group->categories_id;
					$filterList[$cat]['group'] = $group->filter_group_id;

					$filters = FilterAdditional::model()->findAllByAttributes(array('filter_group_id' => $group->filter_group_id));
					foreach ($filters as $key=>$filter) {
						$filterList[$cat]['filters'][$filter->id] = $filter->title;
					}
				}
			}
			echo json_encode($filterList);
		}
	}


	public function actionGetFilters() {

		if (Yii::app()->request->isAjaxRequest) {
			$filters = FilterLabel::model()->findAll(array('condition' => 'filter_group_id='.$_GET['filter_group_id']));
			$filtersList = CHtml::listData($filters, 'filter_id', 'name');
			echo json_encode($filtersList);
		}
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Products;

		$categories = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');
		$product = '';$filters = [];$sizes = [];$photos = [];$mainPhoto = '';$productFilters = [];
		$status = 0;$createRow = 1;$type = 0;

		if (isset($_POST['Products']) && !isset($_POST['preview']) && !isset($_POST['back']) && !isset($_POST['choose_ad_type'])) {
//			var_dump($_POST);die;
			$createRow = 1;
			if (isset($_POST['Products']['subSubcategories_id'])) {
				$model->categories_id = $_POST['Products']['subSubcategories_id'];
			} else {
				$model->categories_id = $_POST['Products']['subcategories_id'];
			}
			$model->location = CHtml::encode($_POST['Products']['location']);
			$model->lat = CHtml::encode($_POST['lat']);
			$model->lng = CHtml::encode($_POST['lng']);
			$model->price = CHtml::encode($_POST['Products']['price']);
			$model->name = CHtml::encode($_POST['Products']['name']);
			$model->description = CHtml::encode($_POST['Products']['description']);
			$model->new = $_POST['Products']['new'];
			$model->video_link = isset($_POST['Products']['video_link']) ? CHtml::encode($_POST['Products']['video_link']) : '';
			if (Yii::app()->session['user_id']) {
				$model->users_id = Yii::app()->session['user_id'];
			}
			$model->payment = !empty($_POST['payment'])?json_encode($_POST['payment']):null;
			$model->delivery = !empty($_POST['delivery'])?json_encode($_POST['delivery']):null;
			$model->giveaway = $_POST['giveaway'];

			$model->status = 'pending';
			switch ($_POST['type']) {
				case "0":
					$model->ad_type = 0;
					$model->status = 'active';
					break;
				case "15":
					$model->ad_type = 1;
					break;
				case "35":
					$model->ad_type = 2;
					break;
			}

			if ($model->validate() && $model->save()) {

				if (!empty($_POST['file'])) {

					foreach ($_POST['file'] as $id) {

						$productImage = ProductsMedia::model()->findByPk($id);
						if ($productImage) {
							$productImage->product_id = $model->id;
							$productImage->prod_tmp_id = null;
							$productImage->update();
						}
					}
					$mainImage = ProductsMedia::model()->findByPk($_POST['main']);
					if ($mainImage) {
						$mainImage->main_photo = 1;
						$mainImage->update();
					}
				}

				if (isset($_POST['sizes'])) {

					$productFilter = new ProductFilter;
					$productFilter->product_id = $model->id;
					$productFilter->other_filter = $_POST['sizes'];
					$productFilter->type = 2;
					if ($productFilter->validate()) {
						$productFilter->save();
					}
				}

				if (isset($_POST['colors'])) {

					$productFilter = new ProductFilter;
					$productFilter->product_id = $model->id;
					$productFilter->other_filter = json_encode($_POST['colors']);
					$productFilter->type = 1;
					if ($productFilter->validate()) {
						$productFilter->save();
					}
				}

                if (isset($_POST['video_file'])) {
                	$videoFile = ProductVideos::model()->findByAttributes(array('title' => $_POST['video_file']));
					$videoFile->product_id = $model->id;
					$videoFile->update();
                }

				echo $model->id;die;
//				Yii::app()->user->setFlash('success', 'Congratulations! Ad has been successfully saved!  It will be  activated  after a short review.');
//				$this->redirect(array('advert','id'=>$model->id));

			} else {
				if (!empty($_POST['file'])) {
					foreach ($_POST['file'] as $id) {
						$productImage = ProductsMedia::model()->findByPk($id);
						if ($productImage) {
							if (file_exists(YiiBase::getPathOfAlias('webroot').'/images/product_images/'. $productImage->photo)) {
								unlink(YiiBase::getPathOfAlias("webroot").'/images/product_images/'. $productImage->photo);
							}
							$productImage->delete();
						}
					}
				}
			}
		}
		if (isset($_POST['back'])) {

			$createRow = 1;
			if (isset($_POST['Products']['subSubcategories_id'])) {
				$model->categories_id = $_POST['Products']['subSubcategories_id'];
			} else {
				$model->categories_id = $_POST['Products']['subcategories_id'];
			}
			$model->location = CHtml::encode($_POST['Products']['location']);
			$model->lat = CHtml::encode($_POST['lat']);
			$model->lng = CHtml::encode($_POST['lng']);
			$model->price = CHtml::encode($_POST['Products']['price']);
			$model->name = CHtml::encode($_POST['Products']['name']);
			$model->description = CHtml::encode($_POST['Products']['description']);
			$model->new = $_POST['Products']['new'];
			$model->users_id = Yii::app()->session['user_id'];
			$model->payment = isset($_POST['payment'])?json_encode($_POST['payment']):null;
			$model->delivery = isset($_POST['delivery'])?json_encode($_POST['delivery']):null;
			$model->video_link = isset($_POST['Products']['video_link']) ? CHtml::encode($_POST['Products']['video_link']) : '';

			if (isset($_POST['colors'])) {
				foreach ($_POST['colors']['color'] as $key=>$val) {
					$filters[$key] = $val;
				}
			}
			if (isset($_POST['sizes'])) {
				$sizes = json_encode($_POST['sizes']);
			}
		}

		if (isset($_POST['preview'])) {
//			var_dump($_POST);die;
			$status = 1;
			$createRow = 0;

			$product = $_POST['Products'];

			if (isset($_POST['colors'])) {
				foreach ($_POST['colors']['color'] as $key=>$val) {
					$filters[$key] = $val;
				}
			}
			if (isset($_POST['sizes'])) {
				$sizes = json_encode($_POST['sizes']);
			}
			if (isset($_POST['file'])) {

				foreach ($_POST['file'] as $key=>$id) {
					$photo = ProductsMedia::model()->findByPk($id);
					$photos[$key] = $photo;
				}
				$mainPhoto = ProductsMedia::model()->findByPk($_POST['main']);

			}
		}

		if (isset($_POST['choose_ad_type'])) {
//			var_dump($_POST['sizes']);die;

			$type = 1;
			$createRow = 0;
			$status = 0;
			$product = $_POST['Products'];
//			if (isset($_POST['Products']['subSubcategories_id'])) {
//				$model->categories_id = $_POST['Products']['subSubcategories_id'];
//			} else {
//				$model->categories_id = $_POST['Products']['subcategories_id'];
//			}
//			$model->location = CHtml::encode($_POST['Products']['location']);
//			$model->lat = CHtml::encode($_POST['lat']);
//			$model->lng = CHtml::encode($_POST['lng']);
//			$model->price = CHtml::encode($_POST['Products']['price']);
//			$model->name = CHtml::encode($_POST['Products']['name']);
//			$model->description = CHtml::encode($_POST['Products']['description']);
//			$model->new = $_POST['Products']['new'];
//			$model->users_id = Yii::app()->session['user_id'];
//			$model->payment = isset($_POST['payment'])?json_encode($_POST['payment']):null;
//			$model->delivery = isset($_POST['delivery'])?json_encode($_POST['delivery']):null;
//			$model->video_link =  isset($_POST['Products']['video_link']) ? CHtml::encode($_POST['Products']['video_link']) : '';
//
//			$model->giveaway = isset($_POST['giveaway']) ? 1 : 0;

			if (isset($_POST['colors'])) {
				foreach ($_POST['colors']['color'] as $key=>$val) {
					$filters[$key] = $val;
				}
			}

//			if (isset($_POST['sizes'])) {
//				$sizes = json_encode($_POST['sizes']);
//			}

			if (isset($_POST['file'])) {
				foreach ($_POST['file'] as $key=>$id) {
					$photo = ProductsMedia::model()->findByPk($id);
					$photos[$key] = $photo;
				}
				$mainPhoto = ProductsMedia::model()->findByPk($_POST['main']);
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'categories' => $categories,

			'product'=>$product,
			'filters' => $filters,
			'sizes' => $sizes,
			'photos' => $photos,
			'mainPhoto' => $mainPhoto,

			'productFilters' => $productFilters,

			'status' => $status,
			'createRow' => $createRow,
			'type' => $type,
			'user' => new Users
		));
	}

	public function actionUpdate($id)
	{

		$model=$this->loadModel($id);

		if (!Yii::app()->session['user_id'] && $model->users_id != Yii::app()->session['user_id']) {
			$this->redirect(Yii::app()->homeUrl);
		}

		$category = Categories::model()->findByAttributes(array('id' => $model->categories->parentCategory->parent_id));

		$subcategory = Categories::model()->findByAttributes(array('id' => $model->categories->parent_id));

		$productFilters = ProductFilter::model()->findAll('product_id='.$id);

		$adSizes = ProductFilter::model()->find('product_id='.$id.' AND type=2');

		$adColors = ProductFilter::model()->find('product_id='.$id.' AND type=1');


		$filters = CHtml::listData(FilterGroup::model()->findAll(), 'id', 'name');

		$productImages = ProductsMedia::model()->findAllByAttributes(array('product_id' => $id));

		$productVideo = ProductVideos::model()->findByAttributes(array('product_id' => $id));

		if (isset($_POST['cancel'])) {
			$this->redirect(array('advert', 'id' => $id));
		}

		if(isset($_POST['Products'])) {
//			var_dump($_POST);die;

			if (isset($_POST['Products']['subSubcategories_id'])) {
				$model->categories_id = $_POST['Products']['subSubcategories_id'];
			} else {
				$model->categories_id = $_POST['Products']['subcategories_id'];
			}
			$model->location = CHtml::encode($_POST['Products']['location']);
			$model->lat = $_POST['lat'];
			$model->lng = $_POST['lng'];
			$model->price = CHtml::encode($_POST['Products']['price']);
			$model->name = CHtml::encode($_POST['Products']['name']);
			$model->new = $_POST['Products']['new'];
			$model->users_id = Yii::app()->session['user_id'];
			$model->payment = isset($_POST['payment'])?json_encode($_POST['payment']):null;
			$model->delivery = isset($_POST['delivery'])?json_encode($_POST['delivery']):null;
			$model->video_link = CHtml::encode($_POST['Products']['video_link']);
			$model->giveaway = $_POST['Products']['giveaway'];
            $model->updated = date('Y-m-d H:i:s');


			if ($model->save()) {

				if (!empty($_POST['file'])) {
					foreach ($_POST['file'] as $id) {
						$productImage = ProductsMedia::model()->findByPk($id);
						if ($productImage) {
							$productImage->product_id = $model->id;
							$productImage->update();
						}
					}
				}

				$mainImage = ProductsMedia::model()->findByPk($_POST['main']);
				if ($mainImage) {
					$mainImage->main_photo = 1;
					$mainImage->update();
				}

				if (isset($_POST['colors'])) {

					if (isset($_POST['color_id'])) {
						$productFilter = ProductFilter::model()->findByPk($_POST['color_id']);
					} else {
						$productFilter = new ProductFilter;
						$productFilter->product_id = $model->id;
						$productFilter->type = 1;
					}
					$productFilter->other_filter = json_encode($_POST['colors']);
					$productFilter->save();
				}

				if (isset($_POST['sizes'])) {
					if (isset($_POST['size_id'])) {
						$productFilter = ProductFilter::model()->findByPk($_POST['size_id']);
					} else {
						$productFilter = new ProductFilter;
						$productFilter->product_id = $model->id;
						$productFilter->type = 2;
					}
					$productFilter->other_filter = json_encode($_POST['sizes']);
					$productFilter->save();
				}

				Yii::app()->user->setFlash('success', 'Your ad was successfully updated!');
				$this->redirect(array('advert','id'=>$model->id));

			} else {
				Yii::app()->user->setFlash('danger', "Something went wrong");
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'subcategory' => $subcategory,
			'category' => $category,
			'filters' => $filters,
			'adSizes' => $adSizes,
			'adColors' => $adColors,
			'productFilters' => $productFilters,
			'productImages' => $productImages,
			'productVideo' => $productVideo
		));
	}


	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionUploadPhoto($id=null) {

		if (Yii::app()->request->isAjaxRequest) {

			$model = new ProductsMedia;

			if (!empty($_FILES)) {

				$image = $_FILES['file'];

				$success = null;

				$filename = $image['name'][0];
				$ext = explode('.', basename($filename));

				$filename = md5(uniqid()) . "." . array_pop($ext);
				$target = Yii::getPathOfAlias('webroot') . '/images/product_images/' . $filename;

				if (move_uploaded_file($image['tmp_name'][0], $target)) {

					$success = true;
					$path = $target;
				} else {
					$success = false;
				}

				if ($success === true) {

					$model->photo = $filename;
					$model->sort_order = $_POST['sort_order'];
					if ($id != null) {
						$model->product_id = $id;
					}
					$model->save();
					$output = [];
					$output = $model->id;

				} elseif ($success === false) {
					$output = ['error' => 'Error while uploading images. Contact the system administrator'];
					if (isset($path)) {
						unlink($path);
					}

				} else {
					$output = ['error' => 'No files were processed.'];
				}

				echo json_encode($output);
			}

		}
	}

	public function actionSortPhotos() {

		if (Yii::app()->request->isAjaxRequest) {

			foreach ($_POST['sort_order'] as $data) {
				$model = ProductsMedia::model()->findByPk($data['photo']);

				if ($model) {
					$model->sort_order = $data['order'];
					$model->update();
				}
			}
		}
	}

	public function actionUploadVideo($id=null) {

		if (Yii::app()->request->isAjaxRequest) {

			$model = new ProductVideos;

			if (!empty($_FILES)) {

				$video = $_FILES['file'];

				$success = null;

				$filename = $video['name'];
				$ext = explode('.', basename($filename));

				$filename = md5(uniqid()) . "." . array_pop($ext);
				$target = Yii::getPathOfAlias('webroot') . '/images/product_videos/' . $filename;

				if (move_uploaded_file($video['tmp_name'], $target)) {

					$success = true;
					$path = $target;
				} else {
					$success = false;
				}

				if ($success === true) {

					$model->title = $filename;

					if ($id != null) {
						$model->product_id = $id;
					}
					$model->save();
					$output = [];
					$output = $filename;

				} elseif ($success === false) {
					$output = ['error' => 'Error while uploading video. Contact the system administrator'];
					if (isset($path)) {
						unlink($path);
					}

				} else {
					$output = ['error' => 'No files were processed.'];
				}

				echo json_encode($output);
			}
		}
	}

	public function actionPayForTop(){
		$payment = $_POST['payment'];
		$product_id = $_POST['product_id'];


		if(!$payment){
			echo "free"; die;
		}
		$user =  Yii::app()->session['user_id'];
		$payment_for_day = Yii::app()->Paypal->paymentForDay;
		$number_of_days = $payment==3 ? 3 : 7;


		setcookie($user."_number_of_days", $number_of_days);
		$paymentInfo['Order']['theTotal'] = $payment;
		$paymentInfo['Order']['description'] = "Payment to make top";
		$paymentInfo['Order']['quantity'] = '1';
		$paymentInfo['Order']['product_id'] = $product_id;
		// call paypal
		$result = Yii::app()->Paypal->SetExpressCheckout($paymentInfo);

		//Detect Errors
		if(!Yii::app()->Paypal->isCallSucceeded($result)){
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
			echo $error;
			Yii::app()->end();

		}else {
			// send user to paypal
			$token = urldecode($result["TOKEN"]);

			$payPalURL = Yii::app()->Paypal->paypalUrl.$token;

			$this->redirect($payPalURL);
		}
	}

	public function actionSetTop()
	{
		$product_id = isset($_POST['product_id']) ? $_POST['product_id'] : false;
		$token = isset($_POST['token']) ? $_POST['token'] : false;
		$payer_id = isset($_POST['payer_id']) ? $_POST['payer_id'] : false;

		$tokens = ProductsTop::model()->findAll();

		$token_array = CHtml::listData(ProductsTop::model()->findAll(), "id", "token");
		if(!$product_id || !$payer_id || !$token || in_array($token, $token_array)){
			echo 'nok';
			die;
		}
		$user = Yii::app()->session['user_id'];
		$model = ProductsTop::model()->findByAttributes(["product_id" => $product_id]);
		$number_of_days = ($_COOKIE[$user.'_number_of_days']);
		if(!$model){
			$model = new ProductsTop;
			$model->product_id = $product_id;
			$model->token = $token;
			$model->payer_id = $payer_id;
			$model->date_added = date("Y-m-d H:i:s");
			$model->expiration_date = date("Y-m-d H:i:s", strtotime("+".$number_of_days." days"));
			$model->status = 1;
		}
		else{
			$expire_date_old = $model->expiration_date;
			$model->expiration_date = date("Y-m-d H:i:s", strtotime($expire_date_old." +".$number_of_days." days"));
			$model->token = $token;
		}

		if($model->save()){
			$model_history = new ProductsTopHistory;
			$history_data = json_encode($model->attributes);
			$model_history->product_id = $product_id;
			$model_history->history_data = $history_data;
			$model_history->save();
			$product = Products::model()->findByPk($product_id);
			if ($product) {
				$product->status = 'active';
				$product->update();
			}

			echo "ok";
			die;
		}
		else{
			echo "nok_k";
			die;
		}
	}

	public function actionPayForTopModal($product_id = false){
		if(!$product_id)
        	$product_id =$_POST['product_id'];


        $min_day = 0;

        $tops = Yii::app()->db->createCommand()
                ->select("
	                (UNIX_TIMESTAMP(p.expiration_date) - UNIX_TIMESTAMP(p.date_added))/(60*60*24) AS date_diff
                ")
                ->from('products_top p')->where('p.product_id=:id',array(':id'=>$product_id))
                ->queryScalar();

        if($tops){
             $min_day = (int)$tops;
        }
        echo $min_day;

    }

	public function actionAdmin()
	{
		$products = Products::model()->with('categories')->findAll(array('condition' => 'users_id=1'));

		$this->render('admin',array(
			'products' => $products,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Products the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Products::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Products $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='products-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionSearch($id = null) {

		$products = [];
		$subsubcats = [];
		$subcats = [];
		$cats = [];
    	$totalCount = 0;
    	$pageSize = 8;
    	$count = 0;
		$page = 0;

        if (!empty($_GET['q'])) {

	        $page = isset($_GET["page"]) ? $_GET["page"] : 1;

	        $from = $pageSize * ($page - 1);
	        $offset = $pageSize;
	        $limit = "LIMIT ". $from .",".$offset;

			$name = CHtml::encode($_GET['q']);

			$catId = isset($_GET['cat_id']) && !empty($_GET['cat_id']) ? ' AND cat.id = '.intval($_GET['cat_id']) : '';

            $cat = $id!=null ? ' AND (p.`categories_id` ='.$id.' OR subsub.parent_id='.$id.' OR sub.parent_id ='.$id.')' : '';

			$q = Yii::app()->db->createCommand('SELECT p.*, photo.photo, u.username, fp.id as fav, subsub.title as category,
                    subsub.id AS subsubId, sub.id AS subId, subsub.title AS subsubTitle,
                    sub.title AS subTitle, cat.`id` AS catId, cat.`title` AS catTitle
                    FROM products p
					LEFT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN categories AS `cat` ON (cat.`id` = sub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id AND photo.`main_photo` = 1)
                    LEFT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN favorite_products AS fp ON (fp.`product_id` = p.`id`)
                    WHERE (p.name LIKE "%'.$name.'%" OR sub.`title` LIKE "%'.$name.'%" OR subsub.`title` LIKE "%'.$name.'%" OR u.`username` LIKE "%'.$name.'%")'.$cat.$catId.
            		' GROUP BY p.id ORDER BY p.id DESC '.$limit);

			$q_count = Yii::app()->db->createCommand('SELECT COUNT(*)
                    FROM products p
					LEFT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN categories AS `cat` ON (cat.`id` = sub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id AND photo.`main_photo` = 1)
                    LEFT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    WHERE (p.name LIKE "%'.$name.'%" OR sub.`title` LIKE "%'.$name.'%" OR subsub.`title` LIKE "%'.$name.'%" OR u.`username` LIKE "%'.$name.'%")'.$cat.$catId.
            		' GROUP BY p.id ORDER BY p.id DESC ');

			$q2 = Yii::app()->db->createCommand('SELECT subsub.id AS subsubId, sub.id AS subId, subsub.title AS subsubTitle, sub.title AS subTitle, cat.`id` AS catId, cat.`title` AS catTitle
                    FROM products
					LEFT JOIN categories AS `subsub` ON (subsub.`id` = products.`categories_id`)
                    LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN categories AS `cat` ON (cat.`id` = sub.`parent_id`)
                    LEFT JOIN users AS `u` ON (u.`id` = products.`users_id`)
                    WHERE (products.name LIKE "%'.$name.'%" OR sub.`title` LIKE "%'.$name.'%" OR subsub.`title` LIKE "%'.$name.'%" OR u.`username` LIKE "%'.$name.'%")'.$cat.$catId.
					' GROUP BY products.id ORDER BY catId');

			$products = $q->queryAll();
			$count = count($products);
            $totalCount = count($q_count->queryAll());


			$allProducts = $q2->queryAll();

			foreach ($allProducts as $key => $pr) {
				if ($pr['catId'] != NULL) {
					if (!in_array($pr['catId'], $cats, true)) {
						$cats[$pr['catId']] = $pr['catTitle'];

					}
					if (!in_array($pr['subId'], $subcats, true)) {
						$subcats[$pr['subId']]['title'] = $pr['subTitle'];
						$subcats[$pr['subId']]['parentId'] = $pr['catId'];

					}
					if (!in_array($pr['subsubId'], $subsubcats, true)) {
						$subsubcats[$pr['subsubId']]['title'] = $pr['subsubTitle'];
						$subsubcats[$pr['subsubId']]['parentId'] = $pr['subId'];

					}
				} else {
					if (!in_array($pr['subId'], $cats, true)) {
						$cats[$pr['subId']] = $pr['subTitle'];

					}
					if (!in_array($pr['subsubId'], $subcats, true)) {
						$subcats[$pr['subsubId']]['title'] = $pr['subsubTitle'];
						$subcats[$pr['subsubId']]['parentId'] = $pr['subId'];
					}
				}
			}
        }
        $pages = new CPagination($totalCount);

        $pages->pageSize=$pageSize;

        $this->render('search', array(
            'products' => $products,
            'subcats' => $subcats,
            'cats' => $cats,
            'subsubcats' => $subsubcats,
            'pages' => $pages,
            'totalCount' => $totalCount,
            'countPage' => $count,
            'page' => $page,
            'q' => CHtml::encode($_GET['q']),
            'category_id' => false,

        ));

	}

	public function actionAdvert($id)
	{

		$model = Products::model()->findByPk($id);
		if (!$model) {
			$this->redirect(Yii::app()->homeUrl);
		}
		$this->pageTitle = Yii::app()->name . ' - ' . $model->name;
		Yii::app()->clientScript->registerMetaTag($model->description, 'description');
		$photo = isset($model->mainPhoto->photo) ? $model->mainPhoto->photo : 'none.jpg';
		Yii::app()->clientScript->registerMetaTag($this->createAbsoluteUrl('/images/product_images/' . $photo), 'image');
		$this->pageOgDesc = $model->description;
		$this->pageOgImage = $this->createAbsoluteUrl('/images/product_images/' . $photo);


		if (!empty($_POST['accept'])) {
			$accept = CHtml::encode($_POST['accept_comment']);

			$checkAccepted = ProductsComments::model()->findByAttributes(array('confirmed' => 1, 'product_id' => $id)); //offer accepted

			$replyTo = ProductsComments::model()->findByPk($_POST['replyTo']);

			$acceptComment = new ProductsComments;
			$acceptComment->user_id = Yii::app()->session['user_id'];
			$acceptComment->product_id = $id;
			if ($_POST['type'] == 'offer') {
				$acceptComment->comment = '@'.$replyTo->user->username.': &#8364;'.$replyTo->offer_price .' OFFER ACCEPTED<br>'.$accept;
			} else {
				$acceptComment->comment = '@'.$replyTo->user->username.': REQUEST ACCEPTED<br>'.$accept;
			}
			$acceptComment->reply_to = $_POST['replyTo'];


			if ($acceptComment->save()) {

				$replyTo->confirmed = 1;
				$replyTo->update();

				$commenters = FavoriteProducts::model()->findAllByAttributes(array('product_id' => $id), 'user_id != '.$replyTo->user_id);
				if ($commenters) {
					foreach ($commenters as $commenter) {

						$text = 'Request accepted on the add you\'re watching "'.$replyTo->product->name.'"';
						if ($_POST['type'] == 'offer') {
							$text = 'Offer accepted on the add you\'re watching "'.$replyTo->product->name.'"';
						}
						Notifications::model()->notify($commenter->user_id, null, $acceptComment->id, null, $text);
					}
					if ($checkAccepted) {
						$confirmed = ProductsComments::model()->findByAttributes(array('reply_to' => $checkAccepted->id));
						$checkAccepted->confirmed = 0;
						$checkAccepted->update();

						if (!empty($confirmed)) {
							if ($confirmed->delete()) {

								foreach ($commenters as $commenter) {
									if ($commenter->user_id != Yii::app()->session['user_id']) {
										$text = 'Request cancelled on the add you\'re watching "'.$model->name.'"';
										if ($_POST['type'] == 'offer') {
											$text = 'Offer accepted on the add you\'re watching "'.$replyTo->product->name.'"';
										}
										Notifications::model()->notify($commenter->user_id, null, $acceptComment->id, null, $text);
									}
								}
								$text = $model->user->username.' cancelled your request on "'.$model->name.'"';
								if ($_POST['type'] == 'offer') {
									$text = 'Offer accepted on the add you\'re watching "'.$replyTo->product->name.'"';
								}
								Notifications::model()->notify($checkAccepted->user_id, null, $acceptComment->id, null, $text);
							}
						}
					}
				}
				$text = $model->user->username.' accepted your request on "'.$model->name.'"';
				if ($_POST['type'] == 'offer') {
					$text = $model->user->username.' accepted your offer on "'.$model->name.'"';
				}
				Notifications::model()->notify($replyTo->user_id, null, $acceptComment->id, null, $text);
				Yii::app()->user->setFlash('success', 'Successfully accepted');
				$this->redirect(array('advert', 'id' => $id));
			}
		}

		if (!empty($_POST['request'])) {
			$requestComment = new ProductsComments;
			$requestComment->user_id = Yii::app()->session['user_id'];
			$requestComment->product_id = $id;
			$requestComment->comment =  '<b>REQUESTED</b>';
			$requestComment->request = 1;
			if (!empty($_POST['request_comment'])) {
				$requestComment->comment .= '<br>'.$_POST['request_comment'];
			}
			$requestComment->save();

			Yii::app()->user->setFlash('success', 'Your request was successfully added.');
			$this->redirect(array('advert', 'id' => $id));
		}

		if (!empty($_POST['cancel'])) {

			$comment= new ProductsComments('comment');
			$comment->comment = CHtml::encode($_POST['ProductsComments']['comment']);
			$comment->user_id = Yii::app()->session['user_id'];
			$comment->product_id = $id;
			$replyTo = ProductsComments::model()->findByPk($_POST['replyTo']);

			$checkAccepted = ProductsComments::model()->findByPk($replyTo->reply_to);

			if ($comment->save()) {
				$checkAccepted->confirmed = 0;
				$checkAccepted->update();

				$commenters = FavoriteProducts::model()->findAllByAttributes(array('product_id' => $id), 'user_id != '.$replyTo->user_id.' AND user_id !='.$checkAccepted->user_id);

				if ($commenters) {
					foreach ($commenters as $commenter) {
						Notifications::model()->notify($commenter->user_id, null, $comment->id, null, 'Offer cancelled on the add you\'re watching "'.$model->name.'"');
					}
				}
				Notifications::model()->notify($checkAccepted->user_id, null, $comment->id, null, $model->user->username.' cancelled your offer on "'.$model->name.'"');
				if ($replyTo) {
					$replyTo->delete();
				}

				Yii::app()->user->setFlash('success', 'Successfully cancelled');
				$this->redirect(array('advert', 'id' => $id));
			}
		}

		$message = new Messages;

		if (!empty($_POST['Messages']['message'])) {

			$thread = Threads::model()->findByAttributes(array(
                'seller_id' => $_POST['Threads']['seller_id'],
                'buyer_id' => $_POST['Threads']['buyer_id'],
                'product_id' => $id
            ));
			if (!$thread) {
				$thread = new Threads;
				$thread->seller_id = Yii::app()->session['user_id'];
				$thread->buyer_id = Yii::app()->session['user_id'];
				if (!empty ($_POST['Threads']['buyer_id'])) {
					$thread->buyer_id = $_POST['Threads']['buyer_id'];
				}
				if (!empty ($_POST['Threads']['seller_id'])) {
					$thread->seller_id = $_POST['Threads']['seller_id'];
				}
				$thread->product_id = $id;
				$thread->save();
			}

			$message->attributes = $_POST['Messages'];

			$message->thread_id = $thread->id;

			$message->message = CHtml::encode($_POST['Messages']['message']);

			if ($message->validate()) {
				$message->save();
				$userId = $_POST['Threads']['seller_id'];
				if ($message->user_id == $_POST['Threads']['seller_id']) {
					$userId = $_POST['Threads']['buyer_id'];
				}
			}
			Yii::app()->user->setFlash('success', 'Your message has been sent');
			$this->redirect(array('advert', 'id' => $id));
		}

		$subCatProducts = Products::model()->findAllByAttributes(array('categories_id' => $model->categories_id), array('condition' => 'id!='.$model->id, 'limit' => 4));
		$categoryProducts = [];
		foreach (Products::model()->findAll(array('condition' => 'id !='.$model->id.' AND categories_id!='.$model->categories_id)) as $key=>$prod) {
			if(!empty($model->categories->parentCategory->parentCategory) && $model->categories->parentCategory->parentCategory->parent_id == 0) {
				if ($prod->categories->parentCategory->parent_id == $model->categories->parentCategory->parent_id) {
					$categoryProducts[$key] = $prod;
					if (count($categoryProducts) == 4) {
						break;
					}
				}
			}
		}

		$sizes = ProductFilter::model()->findByAttributes(array('product_id' => $id, 'type' => 2));
		$newF = [];
		if ($sizes) {
			foreach (json_decode($sizes->other_filter) as $key => $size) {
				$test = json_decode(FiltersNew::model()->findByPk($key)->filters);
				foreach ($test as $k=>$f) {
					foreach ($size as $s) {
						if ($f->id == $s) {
							$newF[$s] = $f;
						}
					}
				}
			}
		}
//		echo'<pre>';print_r($newF);echo'</pre>';

        $this->render('advert', array(
			'model' => $model,
			'categoryProducts' => $categoryProducts,
			'subCatProducts' => $subCatProducts,
			'newComment' => new ProductsComments('comment'),
			'newOffer' => new ProductsComments('offerPrice'),
			'message' => $message,
			'contact' => new ContactForm('referTo'),
			'newF' => $newF,
			'size_filter_id' => $key
		));
	}

	public function actionAddComment()
	{
		if (Yii::app()->request->isAjaxRequest) {

			if (!empty($_POST['ProductsComments'])) {

				$model = Products::model()->findByPk($_POST['ad_id']);

				if ($_POST['type'] == 'offer') {
					$comment = new ProductsComments('offerPrice');
					$comment->offer_price = CHtml::encode($_POST['ProductsComments']['offer_price']);
					$comment->comment = '&#8364;' . $comment->offer_price . ' OFFERED';
//					$msg = 'Yout offered price has been added';
				} elseif ($_POST['type'] == 'question') {
					$comment = new ProductsComments('comment');
					if (isset($_POST['ProductsComments']['question'])) {
						$comment->comment = CHtml::encode($_POST['ProductsComments']['question']);
					} else {
						$comment->comment = CHtml::encode($_POST['ProductsComments']['comment']);
					}

					if (!empty($_POST['ProductsComments']['reply_to'])) {
						$comment->reply_to = intval($_POST['ProductsComments']['reply_to']);
					}
//					$msg = 'Your comment has been added';
				} else {

					$comment = new ProductsComments;

					$comment->comment =  '<b>REQUESTED</b>';
					if (!empty($_POST['request_comment'])) {
						$comment->comment .= '<br>' . $_POST['request_comment'];
					}
					$comment->request = 1;
					//}

//					Notifications::model()->notify($model->users_id, null, $requestComment->id, null, $text);

//					Yii::app()->user->setFlash('success', 'Your request was successfully added.');
//					$this->redirect(array('advert', 'id' => $id));

				}
				$comment->user_id = Yii::app()->session['user_id'];
				$comment->product_id = $_POST['ad_id'];

				if ($comment->validate() && $comment->save()) {

					if ($model->users_id != $comment->user_id) {

						$checkFavProduct = FavoriteProducts::model()->findByAttributes(array('user_id' => $comment->user->id, 'product_id' => $model->id));
						if (!$checkFavProduct) {
							$favProduct = new FavoriteProducts;
							$favProduct->product_id = $model->id;
							$favProduct->user_id = Yii::app()->session['user_id'];
							$favProduct->save();
						}

						if ($_POST['type'] == 'offer') {
							$text = 'New offer on your add "' . $model->name . '"';
						} elseif ($_POST['type'] == 'question') {
							$text = 'New question regarding "' . $model->name . '"';
						} elseif ($_POST['type'] == 'request') {
							$text = 'New request on your add "' . $model->name . '"';
						} else {
							$text = 'New reply to your add "' . $model->name . '"';
						}
						Notifications::model()->notify($model->users_id, null, $comment->id, null, $text);

					} else {

						if ($comment->reply_to != null) {
							Notifications::model()->notify($comment->reply_to, null, $comment->id, null, 'New reply to your comment on add "' . $model->name . '"');
						}
					}

					$commenters = FavoriteProducts::model()->findAllByAttributes(array('product_id' => $model->id), 'user_id!=' . $comment->user_id);

					if ($commenters) {
						foreach ($commenters as $commenter) {
							if ($commenter->user_id != $model->users_id && $commenter->user_id != $comment->reply_to) {
								if ($_POST['type'] == 'offer') {
									$text = 'New offer on the add you are watching "' . $model->name . '"';
								} elseif ($_POST['type'] == 'question') {
									$text = 'New question to the add you are watching "' . $model->name . '"';
								} elseif ($_POST['type'] == 'request') {
									$text = 'New request to the add you are watching "' . $model->name . '"';
								} else {
									$text = 'New reply to your add "' . $model->name . '"';
								}
								Notifications::model()->notify($commenter->user_id, null, $comment->id, null, $text);
							}
						}
					}
					$commentArr['comment'] = $comment->comment;
					$commentArr['username'] = $comment->user->username;
					$commentArr['comment_id'] = $comment->id;
					$commentArr['offered_price'] = $comment->offer_price;
					$commentArr['avatar'] = (!empty($comment->user->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot').'/images/user_pics/'.$comment->user->usersAdditional->avatar))
						? $comment->user->usersAdditional->avatar : 'avatar.png';
					$commentArr['date'] = date('F j, Y');
					$commentArr['type'] = $comment->request==1 ? 'true' : 'false';
//					$commentArr['note'] = $msg;

					echo json_encode($commentArr);die;
				} else {
					echo 'Something went wrong';die;

				}
			}
		}
	}


	public function actionWithdrawOffer($id) {

		$comment = ProductsComments::model()->findByPk($id);
		if ($comment) {

			$watchers = FavoriteProducts::model()->findAllByAttributes(array('product_id' => $comment->product_id), 'user_id != '.$comment->user_id);

			foreach ($watchers as $watcher) {

				$note = new Notifications;
				$note->user_id = $watcher->user_id;
				$note->product_id = $comment->product_id;
				$note->text = $comment->user->username.' has withdrawn their offer on "'.$comment->product->name.'"';

				$note->save();
			}
			$note = new Notifications;
			$note->user_id = $comment->product->users_id;
			$note->product_id = $comment->product_id;
			$note->text = $comment->user->username.' has withdrawn their offer on "'.$comment->product->name.'"';
			$note->save();

			$offerNote = Notifications::model()->findByAttributes(array('comment_id' => $comment->id));
			$offerNote->comment_id = null;
			$offerNote->product_id = $comment->product_id;
			$offerNote->update();

			$prodId = $comment->product_id;
			$comment->delete();
			$this->redirect(array('advert', 'id' => $prodId));
		}
	}

	public function actionWithdrawRequest() {

		if (!empty($_POST['request_comment_id'])) {

			$comment = ProductsComments::model()->findByPk($_POST['request_comment_id']);

			if ($comment) {
				$watchers = FavoriteProducts::model()->findAllByAttributes(array('product_id' => $comment->product_id), 'user_id != '.$comment->user_id);
				if ($watchers) {
					foreach ($watchers as $watcher) {
						Notifications::model()->notify($watcher->user_id, $comment->product_id, null, null, $comment->user->username.' has withdrawn their request on "'.$comment->product->name.'"');
					}
				}

				Notifications::model()->notify($comment->product->users_id, $comment->product_id, null, null, $comment->user->username.' has withdrawn their request on "'.$comment->product->name.'"');

				$offerNote = Notifications::model()->findByAttributes(array('comment_id' => $_POST['request_comment_id']));
				if ($offerNote) {
					$offerNote->comment_id = null;
					$offerNote->product_id = $comment->product_id;
					$offerNote->update();
				}
				$prodId = $comment->product_id;
				$comment->delete();
				Yii::app()->user->setFlash('success', 'Your request has been withdrawn.');
				$this->redirect(array('advert', 'id' => $prodId));

			}
		}
	}

	public function actionToggleFavourite() {

		if (Yii::app()->request->isAjaxRequest) {
			$favProduct = FavoriteProducts::model()->findByAttributes(array('user_id' => Yii::app()->session['user_id'], 'product_id' => $_POST['product_id']));
			if (!$favProduct) {
				$favProduct = new FavoriteProducts;
				$favProduct->user_id = Yii::app()->session['user_id'];
				$favProduct->product_id = $_POST['product_id'];
				$favProduct->save();
			} else {
				$favProduct->delete();
			}
		}
	}

	public function actionFavourites() {

		$page = isset($_GET["page"]) ? $_GET["page"] : 1;
		$pageSize = 8;

		$q = Yii::app()->db->createCommand('
			SELECT p.*, photo.photo, u.username FROM products p
			LEFT JOIN favorite_products fp ON p.id = fp.product_id
			LEFT JOIN users u ON p.`users_id` = u.`id`
			LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
			WHERE fp.user_id ='.Yii::app()->session['user_id'].' GROUP BY p.`id`'
			);
		$favourites = $q->queryAll();
		$totalCount = count($favourites);
		$pages=new CPagination($totalCount);
                    // results per page
        $pages->pageSize = $pageSize;
		$products = array_slice($favourites, $pageSize * ($page-1), $pageSize);
        $count = count($products);
		$this->render('favourites', array(
			'products' => $products,
			'totalCount' => $totalCount,
			'pages' => $pages,
			'countPage' => $count,
			'page' => $page,
			'q' => false
			)
		);
	}

	public function actionChangePrice() {

		if (Yii::app()->request->isAjaxRequest) {
			$product = Products::model()->findByPk($_POST['product_id']);

			if ($product) {
				$product->price = CHtml::encode($_POST['new_price']);
				$product->update();

				$checkFavourite = FavoriteProducts::model()->findAllByAttributes(array('product_id' => $product->id));

				foreach ($checkFavourite as $fav) {
					$note = new Notifications;
					$note->user_id = $fav->user_id;
					$note->product_id = $product->id;
					$note->text = 'Price changed on item "'.$fav->product->name.'"in your watchlist';
					$note->save();
				}
			}
		}
	}

	public function actionGetUserPosition() {

		if (Yii::app()->request->isAjaxRequest) {
			if (isset($_POST['geoloc_lat']) && isset($_POST['geoloc_lng'])) {
				$product = Products::model()->findByPk($_POST['product_id']);
				$user_lat = $_POST['geoloc_lat'];
				$user_lng = $_POST['geoloc_lng'];

				$R = 6371000;
				$lat1 = deg2rad($user_lat);
				$lat2 = deg2rad($product['lat']);

				$dlat = deg2rad($product['lat']-$user_lat);
				$dlang = deg2rad($product['lng']-$user_lng);

				$a = sin($dlat/2) * sin($dlat/2) +
					cos($lat1) * cos($lat2) *
					sin($dlang/2) * sin($dlang/2);
				$c = 2 * atan2(sqrt($a), sqrt(1-$a));
				$d = $R * $c;
				echo $d;                                                                                                              ;
			}
		}
	}

    public function actionDeletePhoto() {

        if (Yii::app()->request->isAjaxRequest) {

			if ($_GET['media'] == 'photo') {

				$photo = ProductsMedia::model()->findByPk($_GET['photo_id']);

				if($photo) {
					if (file_exists(YiiBase::getPathOfAlias('webroot').'/images/product_images/'. $photo->photo)) {
						unlink(YiiBase::getPathOfAlias("webroot").'/images/product_images/'. $photo->photo);
					}
					$photo->delete();
				}
			} else {
				$video = ProductVideos::model()->findByAttributes(array('title' => $_GET['video_id']));

				if($video) {
					if (file_exists(YiiBase::getPathOfAlias('webroot').'/images/product_videos/'. $video->title)) {
						unlink(YiiBase::getPathOfAlias("webroot").'/images/product_videos/'. $video->title);
					}
					$video->delete();
				}
			}

        }
    }

	public function actionSetMainPhoto() {

		if (Yii::app()->request->isAjaxRequest) {

			foreach ($_GET['all_id'] as $id) {

				$photo = ProductsMedia::model()->findByPk(intval($id));
				if ($photo) {
					if ($photo->main_photo == 1) {
						$photo->main_photo = 0;
						$photo->update();
					}
				}
			}

			$main = ProductsMedia::model()->findByPk($_GET['set_main']);

			if($main) {
				$main->main_photo = 1;
				$main->update();
				echo 'ok';
			}
		}
	}

	public function actionDeleteProd($id) {

		$product = Products::model()->findByAttributes(array('id' => $id, 'users_id' => Yii::app()->session['user_id']));
		if ($product) {
			$product->delete();
			Yii::app()->user->setFlash('success', 'Your Add has been successfully deleted.');
			$this->redirect(array('profile/myAds', 't' => $product->status));
		}
	}


	public function actionCreateTempProduct() {

		if (Yii::app()->request->isAjaxRequest) {

			if (!Yii::app()->request->cookies['prod_tmp']) {

				$prodTmp = new ProductsTmp;
				$prodTmp->categories_id = isset($_POST['Products']['subSubcategories_id']) ? $_POST['Products']['subSubcategories_id'] : $_POST['Products']['subcategories_id'];
				$prodTmp->location = CHtml::encode($_POST['Products']['location']);
				$prodTmp->name = CHtml::encode($_POST['Products']['name']);
				$prodTmp->description = CHtml::encode($_POST['Products']['description']);
				$prodTmp->new = $_POST['Products']['new'];
				$prodTmp->price = CHtml::encode($_POST['Products']['price']);
				$prodTmp->lat = $_POST['lat'];
				$prodTmp->lng = $_POST['lng'];
				$prodTmp->payment = isset($_POST['payment'])?json_encode($_POST['payment']):null;
				$prodTmp->delivery = isset($_POST['delivery'])?json_encode($_POST['delivery']):null;
				$prodTmp->giveaway = isset($_POST['giveaway']) ? 1 : 0;

				if ($prodTmp->validate() && $prodTmp->save()) {

					if (isset($_POST['colors'])) {

						$productFilter = new ProductFilter;
						$productFilter->prod_tmp_id = $prodTmp->id;
						$productFilter->other_filter = json_encode($_POST['colors']);
						$productFilter->type = 1;
						if ($productFilter->validate()) {
							$productFilter->save();
						}
					}

					if (isset($_POST['sizes'])) {

						$productFilter = new ProductFilter;
						$productFilter->prod_tmp_id = $prodTmp->id;
						$productFilter->other_filter = json_encode($_POST['sizes']);
						$productFilter->type = 2;
						if ($productFilter->validate()) {
							$productFilter->save();
						}
					}

					if (!empty($_POST['file'])) {

						foreach ($_POST['file'] as $key=>$id) {
							$productImage = ProductsMedia::model()->findByPk($id);
							if ($productImage) {
								$productImage->prod_tmp_id = $prodTmp->id;
								$productImage->update();
							}
						}

						$mainImage = ProductsMedia::model()->findByPk($_POST['main']);
						if ($mainImage) {
							$mainImage->main_photo = 1;
							$mainImage->update();
						}
					}

					if (!empty($_POST['video_link'])) {
						$video = new ProductVideos;
						$video->title = CHtml::encode($_POST['video_link']);
						$video->prod_tmp_id = $prodTmp->id;
						$video->save();
					}
					if (!empty($_POST['video_file'])) {
						$videoFile = ProductVideos::model()->findByPk($_POST['video_file']);
						$videoFile->prod_tmp_id = $prodTmp->id;
						$videoFile->update();
					}
				} else {
					echo json_encode('Please fill in all required fields.');
				}

				if ($prodTmp->save()) {
					$cookie = new CHttpCookie('prod_tmp', $prodTmp->id);
					Yii::app()->request->cookies['prod_tmp'] = $cookie;
				}
			}
		}
	}

	public function actionProdTemp($id)
	{
		$model = ProductsTmp::model()->findByPk($id);

		if ($model) {
			$type = 0;
			$photos = ProductsMedia::model()->findAllByAttributes(array('prod_tmp_id' => $model->id));

			$colors = ProductFilter::model()->findByAttributes(array('prod_tmp_id' => $model->id, 'type' => 1));

			$prodFilters = ProductFilter::model()->findAllByAttributes(array('prod_tmp_id' => $model->id));

			$sizes = ProductFilter::model()->findByAttributes(array('prod_tmp_id' => $id, 'type' => 2));
			$newF = [];
			if ($sizes) {
				foreach (json_decode($sizes->other_filter) as $key => $size) {
					$test = json_decode(FiltersNew::model()->findByPk($key)->filters);
					foreach ($test as $k=>$f) {
						foreach ($size as $s) {
							if ($f->id == $s) {
								$newF[$s] = $f;
							}
						}
					}
				}
			}

			if (isset($_POST['choose_ad_type'])) {

				$type = 1;

				if (isset($_POST['Products']['subSubcategories_id'])) {
					$model->categories_id = $_POST['Products']['subSubcategories_id'];
				} else {
					$model->categories_id = $_POST['Products']['subcategories_id'];
				}
				$model->location = CHtml::encode($_POST['Products']['location']);
				$model->lat = CHtml::encode($_POST['lat']);
				$model->lng = CHtml::encode($_POST['lng']);
				$model->price = CHtml::encode($_POST['Products']['price']);
				$model->name = CHtml::encode($_POST['Products']['name']);
				$model->description = CHtml::encode($_POST['Products']['description']);
				$model->new = $_POST['Products']['new'];
				$model->payment = !empty($_POST['payment'])?json_encode($_POST['payment']):null;
				$model->delivery = !empty($_POST['delivery'])?json_encode($_POST['delivery']):null;

				if (isset($_POST['colors'])) {
					foreach ($_POST['colors']['color'] as $key=>$val) {
						$filters[$key] = $val;
					}
				}

				if (isset($_POST['file'])) {
					foreach ($_POST['file'] as $key=>$id) {
						$photo = ProductsMedia::model()->findByPk($id);
						$photos[$key] = $photo;
					}
//				$mainPhoto = ProductsMedia::model()->findByPk($_POST['main']);
				}
			}

			$this->render('prodTemp', array('model' => $model, 'photos' => $photos, 'colors' => $colors, 'type' => $type, 'newF' => $newF, 'sizes' => $sizes));
		} else {
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	public function actionPublishTmp($id)
	{
		$model = ProductsTmp::model()->findByPk($id);
		if ($model) {

			$product = new Products;
			$product->users_id = Yii::app()->session['user_id'];
			$product->attributes = $model->attributes;
			$product->save();

			if ($product->save()) {
				$photos = ProductsMedia::model()->findAllByAttributes(array('prod_tmp_id' => $id));
				foreach ($photos as $photo) {
					$newphoto = new ProductsMedia;
					$newphoto->product_id = $product->id;
					$newphoto->photo = $photo->photo;
					$newphoto->main_photo = $photo->main_photo;
					$newphoto->save();
				}

				$videos = ProductVideos::model()->findAllByAttributes(array('prod_tmp_id' => $id));
				foreach ($videos as $video) {
					$newvideo = new ProductVideo;
					$newvideo->product_id = $product->id;
					$newvideo->title = $video->title;
					$newvideo->type = $video->type;
					$newvideo->save();
				}

				$filters = ProductFilter::model()->findAllByAttributes(array('prod_tmp_id' => $id));

				if (isset($_POST['sizes'])) {

					$productFilter = new ProductFilter;
					$productFilter->product_id = $product->id;
					$productFilter->other_filter = $_POST['sizes'];
					if ($productFilter->validate()) {
						$productFilter->save();
					}
				}

//				foreach ($filters as $filter) {
//					$productFilter = new ProductFilter;
//					$productFilter->filter_additional_id = $filter->filter_additional_id;
//					$productFilter->filter_id = $filter->filter_id;
//					$productFilter->product_id = $product->id;
//					$productFilter->other_filter = $filter->other_filter;
//
//					if ($productFilter->validate()) {
//						$productFilter->save();
//					}
//				}

				$model->delete();

				unset(Yii::app()->request->cookies['prod_tmp']);

				Yii::app()->user->setFlash('success', 'Congratulations! Ad has been successfully saved!  It will be  activated  after a short review.');
				$this->redirect(array('advert','id'=>$product->id));
			} else {
				Yii::app()->user->setFlash('danger', 'Something went wrong.');
			}

		} else {
			$this->redirect(Yii::app()->homeUrl);
		}

	}

	public function actionCreateReport()
	{
		$type = $_POST['type'];
		$product_id = $_POST['product_id'];
		$text = CHtml::encode($_POST['text']);

		$model = new ProductsReports;
		$model->report_type = $type;
		$model->product_id = $product_id;
		$model->report_text = $text;

		if($model->save())
		{
			echo "ok";
			die;
		}
		else
		{
			echo "nok";
			die;
		}

	}

	public function actionReportAd()
	{
		$this->render('report_ad');
	}

	public function actionAdsType()
	{
		$this->render('ads_type');
	}


}
