<?php

class MessagesController extends Controller
{

	public function actionIndex($m=null)
	{
		if (Yii::app()->session['user_id']) {

			$threads = Threads::model()->findAll(array('condition' => 'seller_id='.Yii::app()->session['user_id'].' OR buyer_id='.Yii::app()->session['user_id'], 'order' => 't.id DESC'));

			$newMessage = new Messages;

			$conv = [];
			if (isset($m)) {
				$conv = Threads::model()->findByPk($m);
				$convMessages = Messages::model()->findAll('thread_id = '.$conv['id']);
				foreach ($convMessages as $message) {
					$message->read = 1;
					$message->update();
				}
			}

			$messages = Threads::model()->with('messages')->find(array('condition' => 't.seller_id='.Yii::app()->session['user_id'].' OR t.buyer_id='.Yii::app()->session['user_id'], 'order' => 't.id DESC'));
			if ($messages) {
				$lastMessage = Messages::model()->with('thread')->find(array('condition' => 'thread.id='.$messages->id, 'order' => 't.id DESC', 'limit' => 1));
			}

			$this->render('index', array('threads' => $threads, 'newMessage' => $newMessage, 'conv' => $conv, 'messages' => $messages, 'lastMessage' => $lastMessage));
		} else {
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	public function actionLoadNewMessages($id) {

		$this->layout = false;
		if (Yii::app()->request->isAjaxRequest) {

			$newMessage = null;
			$lastMsg = Messages::model()->with('thread')->find(array('condition' => 'thread.id='.$id, 'order' => 't.id DESC', 'limit' => 1));

			$check = false;
			if ($lastMsg->id > $_POST['last_msg']) {
				$newMessage = $lastMsg;
				$check = true;
			}

			$this->render('loadNewMessages', array('newMessage' => $newMessage, 'last_msg' => $lastMsg, 'check' => $check));
		} else {
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	public function actionLoadThreads() {

		if (Yii::app()->request->isAjaxRequest) {

			$threads = Threads::model()->findAll(array('condition' => 'seller_id='.Yii::app()->session['user_id'].' OR buyer_id='.Yii::app()->session['user_id'], 'order' => 't.id DESC'));

			$messages['id'] = $_POST['thread_id'];

			$this->renderPartial('_threads', array('threads' => $threads, 'messages' => $messages));
		}
	}

	public function actionView() {

		if (Yii::app()->request->isAjaxRequest) {

			$threadMsgs = Threads::model()->with('messages')->find(array('condition' => 't.id='.$_POST['thread_id']));

			foreach ($threadMsgs->messages as $m) {
				$m->read = 1;
				$m->update();

			}

			$this->renderPartial('_view', array('messages' => $threadMsgs));
		}
	}


	public function actionMarkAllRead()
	{
		if (isset($_POST['checked'])) {
			$checked = $_POST['checked'];

			foreach ($checked as $id) {
				$models = Messages::model()->findAllByAttributes(["thread_id" => $id], 'user_id <> '.Yii::app()->session['user_id']);

				foreach ($models as $model) {
					if ($model) {
						$model->read = 1;
						$model->update();
					}
				}
			}
		} else {
			if (isset($_POST)) {

				if (isset($_POST['thread_id'])) {
					$messages = Messages::model()->findAll('user_id!='.Yii::app()->session['user_id'].' AND t.read=0 AND thread_id='.$_POST['thread_id']);
				} else {
					$messages = Messages::model()->findAll('user_id!=' . Yii::app()->session['user_id'] . ' AND t.read=0');
				}
				foreach ($messages as $model) {
					if ($model) {
						$model->read = 1;
						$model->update();
					}
				}
			}
		}
	}

	public function actionMarkOneRead() {

		if (Yii::app()->request->isAjaxRequest) {

			$msg = Messages::model()->findByPk($_POST['id']);
			if ($msg) {
				$msg->read = 1;
				$msg->update();
			}
		}
	}


	public function actionSendRoomEvent() {
		if (isset($_POST)) {
//			var_dump($_POST);die;
			$newMessage = new Messages;
			$newMessage->thread_id = $_POST['thread_id'];
			$newMessage->user_id = Yii::app()->session['user_id'];
			$newMessage->message = CHtml::encode($_POST['Messages']['message']);

			if ($newMessage->validate()) {
				$newMessage->save();
				$user_id = $newMessage->thread->seller_id != $newMessage->user_id ? $newMessage->thread->seller_id : $newMessage->thread->buyer_id;
				$data['user_id'] = $user_id;
				$data['thread_id'] = $newMessage->thread_id;
				$data['last_msg'] = $newMessage->id;
				echo json_encode($data);die;
				//$this->redirect(array('view', 'id' => $id));
			}


		}

	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}