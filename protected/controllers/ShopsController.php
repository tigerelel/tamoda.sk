<?php

class ShopsController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}


	public function actionView($id, $category=null)
	{
		$userProducts = Products::model()->count('users_id='.Yii::app()->session['user_id'].' AND status="sold"');
		if ($userProducts < 20) {
			$this->redirect(Yii::app()->homeUrl);
		}
		$shop = Shops::model()->findByPk($id);

		if (!$shop) {
			$shop = new Shops;
			$shop->user_id = Yii::app()->session['user_id'];
			$shop->name = Yii::app()->session['username'].'\'s shop';
			$shop->save();
		}

		$products = [];
		$page = isset($_GET["page"]) ? $_GET["page"] : 1;
		$pageSize = 8;
		$from = $pageSize * ($page - 1);
		$offset = $pageSize;
		$limit = "LIMIT ". $from .",".$offset;

		$totalCount = 0;

		$catCond = !empty($category) ? ' AND (p.`categories_id` ='.$category.' OR subsub.`parent_id`='.$category.' OR sub.`parent_id` ='.$category.')' : '';
		$q = Yii::app()->db->createCommand("SELECT p.*, photo.photo, u.username, subsub.title as category, sub.title as subTitle, sub.id as subId FROM products AS p
					LEFT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
					LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    WHERE p.users_id = ".$id.$catCond." GROUP BY p.`id` ORDER BY p.`created_date` DESC ".$limit);

		$q_count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM products AS `p`
					LEFT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
					LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    WHERE p.users_id = ".$id.$catCond." GROUP BY p.`id`");

		$q2 = Yii::app()->db->createCommand('SELECT subsub.id AS subsubId, sub.id AS subId, subsub.title AS subsubTitle,
					sub.title AS subTitle, cat.`id` AS catId, cat.`title` AS catTitle
                    FROM products p
					LEFT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN categories AS `cat` ON (cat.`id` = sub.`parent_id`)
                    LEFT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    WHERE p.users_id = '.$id.$catCond.
			' GROUP BY p.id ORDER BY catId');

		$allProducts = $q2->queryAll();
		$subsubcats = [];
		$subcats = [];
		$cats = [];
		foreach ($allProducts as $key => $pr) {
			if ($pr['catId'] != NULL) {
				if (!in_array($pr['catId'], $cats, true)) {
					$cats[$pr['catId']] = $pr['catTitle'];

				}
				if (!in_array($pr['subId'], $subcats, true)) {
					$subcats[$pr['subId']]['title'] = $pr['subTitle'];
					$subcats[$pr['subId']]['parentId'] = $pr['catId'];

				}
				if (!in_array($pr['subsubId'], $subsubcats, true)) {
					$subsubcats[$pr['subsubId']]['title'] = $pr['subsubTitle'];
					$subsubcats[$pr['subsubId']]['parentId'] = $pr['subId'];

				}
			} else {
				if (!in_array($pr['subId'], $cats, true)) {
					$cats[$pr['subId']] = $pr['subTitle'];

				}
				if (!in_array($pr['subsubId'], $subcats, true)) {
					$subcats[$pr['subsubId']]['title'] = $pr['subsubTitle'];
					$subcats[$pr['subsubId']]['parentId'] = $pr['subId'];
				}
			}
		}

		$products = $q->queryAll();

		$count = count($products);
		$totalCount = count($q_count->queryAll());

		$pages = new CPagination($totalCount);

		$pages->pageSize=$pageSize;


		$this->render('view', array(
			'shop' => $shop,
			'products' => $products,
			'pages' => $pages,
			'totalCount' => $totalCount,
			'countPage' => $count,
			'page' => $page,
			'subcats' => $subcats,
			'cats' => $cats,
			'subsubcats' => $subsubcats,
			));
	}

	public function actionEdit($id = null)
	{
		$shop = Shops::model()->findByPk($id);
		if (!$shop) {
			$this->redirect(Yii::app()->homeUrl);
		}

		if (!empty($_POST)) {
			$shop->name = strip_tags($_POST['Shops']['name']);
			$shop->update();
			$this->redirect(array('shops/view', 'id' => $shop->user_id));
		}

	}

	public function actionUploadShopLogo($id)
	{
		if (Yii::app()->request->isAjaxRequest) {

			$shop = $this->loadModel($id);

			if (!empty($_FILES)) {

				$image = $_FILES['file'];

				$success = null;

				$filename = $image['name'];
				$ext = explode('.', basename($filename));

				$filename = md5(uniqid()) . "." . array_pop($ext);
				$target = Yii::getPathOfAlias('webroot').'/images/shop_logos/' . $filename;

				if(move_uploaded_file($image['tmp_name'], $target)) {
					$success = true;
					$path = $target;
				} else {
					$success = false;
				}

				if ($success === true) {
					if ($shop->logo && file_exists(YiiBase::getPathOfAlias('webroot').'/images/shop_logos/'. $shop->logo)) {
						unlink(YiiBase::getPathOfAlias("webroot") . '/images/shop_logos/' . $shop->logo);
					}
					$shop->logo = $filename;
					$shop->update();
					$output = $filename;

				} elseif ($success === false) {
					$output = ['error'=>'Error while uploading images. Contact the system administrator'];
					unlink($path);

				} else {
					$output = ['error'=>'No files were processed.'];
				}

				echo json_encode($output);
			}
		}
	}

	public function loadModel($id)
	{
		$model=Shops::model()->findByPk($id);

		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');

		return $model;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}