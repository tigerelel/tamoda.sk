<?php

class SiteController extends Controller
{

    public $param;
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: search.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{

        $colors = json_decode(FiltersNew::model()->find('type=1')->filters);

        $prod_colors = ProductFilter::model()->findAll('product_id IS NOT NULL AND type=1');

        $arr=[];
        foreach ($prod_colors as $k=>$c) {
            $arr[$c->product_id] = json_decode($c->other_filter)->color;
        };

        $others = [];
        if (!empty($colors)) {
            foreach ($colors as $i=>$v) {
                foreach ($arr as $p => $a) {
                    foreach ($a as $i2=>$n) {
                        if (!in_array($n, $colors)) {
                            $others[$p][$i2] = $n;
                        }
                    }
                }
            }
        }

        $q = Yii::app()->db->createCommand("SELECT p.*, photo.photo, u.username, fp.id as fav, subsub.title as category FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    RIGHT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    LEFT JOIN favorite_products AS `fp` ON (fp.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    GROUP BY p.`id` ORDER BY p.`created_date` DESC LIMIT 8");

        $recentAds = $q->queryAll();

        $products = Products::model()->with('productTop')->findAll(array('condition' => '', 'order' => 'productTop.id DESC'));

        $model = new Categories;
        $categories = Categories::model()->findAll();

        $categoriesList = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');

        $criteria = new CDbCriteria;
        $criteria->select = 'image';

        $data = Sliders::model()->findAll($criteria);

        $this->render('index',array(
            'categories' => $categories,
            'images'=>$data,
            'model' => $model,
            'categoriesList' => $categoriesList,
            'products' => $products,
            'others' => $others,
            'recentAds' => $recentAds
        ));

	}

    public function actionShowMoreRecentAds($page=null) {

        if (Yii::app()->request->isAjaxRequest) {

            $page = isset($_GET["page"]) ? $_GET["page"] : 1;
            $pageSize = 8;
            $from = $pageSize * ($page - 1);
            $offset = $pageSize;
            $limit = "LIMIT ". $from .",".$offset;

            $q = Yii::app()->db->createCommand("SELECT p.*, photo.photo, u.username, fp.id as fav, subsub.title as category FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    RIGHT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    LEFT JOIN favorite_products AS `fp` ON (fp.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    WHERE p.status = 'active'
                    GROUP BY p.`id` ORDER BY p.`created_date` DESC ".$limit);

            $data = $q->queryAll();

            $q_count = Yii::app()->db->createCommand("SELECT p.id FROM products AS `p`
                    WHERE p.status = 'active'
                    GROUP BY p.`id` ORDER BY p.`created_date` DESC");
            $count = count($q_count->queryAll());

            $end = false;
            if ($count%$pageSize > 0) {
                $end = true;
            }

            $this->renderPartial('_recent_ads', array(
                'recentAds' => $data,
                'end' => $end,
                'page' => $page
            ));
        }
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page

	public function actionLogin()
	{
		$model=new LoginForm;

		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];

			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}

		$this->render('login',array('model'=>$model));
	}


	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
     */

//    public function actionlanguage($id) {
//        if($id == 1) {
//            $iso = 'sk';
//        }
//        Yii::app()->request->cookies['language'] = new CHttpCookie('language', $iso);
//        Yii::app()->session['language'] = $iso;
//    }


    public function actionCategory($id = null, $subId = null)
    {
        $products = [];
        $page = isset($_GET["page"]) ? $_GET["page"] : 1;
        $pageSize = 8;
        $from = $pageSize * ($page - 1);
        $offset = $pageSize;
        $limit = "LIMIT ". $from .",".$offset;

        $totalCount = 0;

        $checkParent = Categories::model()->findByPk($id);
        $checkSub = Categories::model()->findByPk($subId);

        if (isset($id) && $checkParent && $checkParent->parent_id == 0) {

            $q = Yii::app()->db->createCommand("SELECT p.*, photo.photo, u.username, fp.id as fav, subsub.title as category FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    RIGHT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    LEFT JOIN favorite_products AS `fp` ON (fp.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    WHERE (subsub.parent_id = ".$id." OR sub.parent_id=".$id.") GROUP BY p.`id` ORDER BY p.`created_date` DESC ".$limit);

            $q_count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    RIGHT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    WHERE (subsub.parent_id = ".$id." OR sub.parent_id=".$id.") GROUP BY p.`id`");


            $products = $q->queryAll();

            $count = count($products);
            $totalCount = count($q_count->queryAll());

        } elseif (isset($subId) && $checkSub && $checkSub->parent_id != 0) {

            $id = $subId;

            $q = Yii::app()->db->createCommand("SELECT p.*, photo.photo, u.username, fp.id as fav, subsub.title as category FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id )
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    LEFT JOIN favorite_products AS `fp` ON (fp.`product_id` = p.id)
                    WHERE (p.`categories_id`= ".$subId." OR subsub.`parent_id`=".$subId.") GROUP BY p.`id` ORDER BY p.`created_date` DESC ".$limit);

            $q_count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    WHERE (p.`categories_id`= ".$subId." OR subsub.`parent_id`=".$subId.") GROUP BY p.`id`");


            $products = $q->queryAll();
            $count = count($products);
            $totalCount = count($q_count->queryAll());

        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
        $pages = new CPagination($totalCount);

        $pages->pageSize=$pageSize;

        $this->render('category', array(
            'category_id' => $id,
            'products' => $products,
            'pages' => $pages,
            'totalCount' => $totalCount,
            'countPage' => $count,
            'page' => $page
        ));
    }


    public function actionAdvertsList()
    {
        if (Yii::app()->request->isAjaxRequest) {

            $adverts = Products::model()->with('media', 'categories', 'productTop')->findAllByAttributes(array(), array(
                'condition' => 'categories.parent_id='.$_GET['cat_id'].' OR categories_id='.$_GET['cat_id'],
                'order' => 'productTop.id DESC'
            ));
            $array = [];

            foreach ($adverts as $key => $advert) {
                $array[$key]['id'] =  $advert->id;
                $array[$key]['name'] =  $advert->name;
                $array[$key]['price'] =  $advert->price;
                $array[$key]['giveaway'] =  $advert->giveaway;

                $array[$key]['image'] =  isset($advert->mainPhoto) ? $advert->mainPhoto->photo : '';

                $array[$key]['description'] = $advert->description;
                $array[$key]['location'] = $advert->location;
                $array[$key]['username'] = $advert->user['username'];
                $array[$key]['cat_id'] = $_GET['cat_id'];
                if (isset($advert->productTop)) {
                    $array[$key]['top'] = $advert->productTop->id;
                }
            }
            echo json_encode($array);
        }

    }

    public function actionFilterProduct() {

        if (Yii::app()->request->isAjaxRequest) {

            $page = isset($_GET["page"]) ? $_GET["page"] : 1;
            $q_filter = isset($_GET['q']) ? $_GET['q'] : false;

            $pageSize = 8;
            $from = $pageSize * ($page - 1);
            $offset = $pageSize;
            $limit = "LIMIT ". $from .",".$offset;
            if(!empty($_GET)){

                $products = [];
                $advertArr = [];

                $qFilters = '';
                $orderby = isset($_GET['order']) ? ",".$_GET['order'] : ", p.id DESC";
                if (isset($_GET['filters'])) {
                    $qFilters = " AND (";
                    foreach ($_GET['filters'] as $key=>$f) {
                        if ($f!=end($_GET['filters'])) {
                            $qFilters .= "f.`other_filter` LIKE '%".$f."%' OR ";
                        } else {
                            $qFilters .= "f.`other_filter` LIKE '%".$f."%')";
                        }
                    }
                }
                $cond = '';
                if (isset($_GET['q']) && $_GET['q']) {
                    $name = CHtml::encode($_GET['q']);
                    $cond = " AND (p.name LIKE '%".$name."%' OR sub.`title` LIKE '%".$name."%' OR subsub.`title` LIKE '%".$name."%' OR u.username LIKE '%".$name."%') ";
                }

                $cur_cat = isset($_GET['current_cat']) && $_GET['current_cat'] ? " AND (sub.`parent_id` = ".$_GET['current_cat']." OR subsub.`parent_id` = ".$_GET['current_cat']."
                    OR subsub.`id` = ".$_GET['current_cat'].")" : "";



                $q = Yii::app()->db->createCommand("
                    SELECT p.*, photo.photo, u.username, top.id AS top, fp.id as fav, subsub.title as category FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    RIGHT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    LEFT JOIN products_media AS `photo` ON (photo.`product_id` = p.id AND photo.main_photo = 1)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    LEFT JOIN product_filter AS `f` ON (f.`product_id` = p.`id`)
                    LEFT JOIN products_top AS top ON (top.`product_id` = p.`id`)
                    LEFT JOIN favorite_products AS fp ON (fp.`product_id` = p.`id`)
                    WHERE ((p.price >=".$_GET['price_from']." AND p.price <=".$_GET['price_to'].")
                        OR (p.`lat`=".$_GET['geoloc_lat']." AND p.`lng`=".$_GET['geoloc_lng']."))"
                        .$cur_cat.
                        " ".$qFilters.$cond." GROUP BY p.`id` ORDER BY top.id DESC ".$orderby);

                 $q_count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM products AS `p`
                    RIGHT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    RIGHT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    RIGHT JOIN users AS `u` ON (u.`id` = p.`users_id`)
                    WHERE ((p.price >=".$_GET['price_from']." AND price <=".$_GET['price_to'].")
                        OR (p.`lat`=".$_GET['geoloc_lat']." AND p.`lng`=".$_GET['geoloc_lng']."))"
                        .$cur_cat.
                        " ".$qFilters.$cond." GROUP BY p.`id`");

                $filtered_products = $q->queryAll();

                $i=0;
                foreach ($filtered_products as $key => $product) {

                    if (isset($_GET['geoloc_lat']) && isset($_GET['geoloc_lng']) && !empty($_GET['dest'])) {

                        $user_lat = $_GET['geoloc_lat'];
                        $user_lng = $_GET['geoloc_lng'];

                        $dLat = deg2rad($product['lat'] - $user_lat);
                        $dLng = deg2rad($product['lng'] - $user_lng);
                        $R = 6371000;
                        $a = sin($dLat / 2) * sin($dLat / 2) +
                            cos(deg2rad($user_lat)) * cos(deg2rad($product['lat'])) *
                            sin($dLng / 2) * sin($dLng / 2);
                        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
                        $dist = $R * $c;
                        $distKm = round($dist / 1000);

                        if ($distKm <= $_GET['dest']) {
                            $advertArr[$i] = $product;
                            $i++;
                        }
                    } else {
                        $advertArr[$i] = $product;
                        $i++;
                    }
                }

                $products = $advertArr;

                $array = [];

                foreach ($products as $key => $advert) {

                    $array[$key]['id'] =  $advert['id'];
                    $array[$key]['name'] =  $advert['name'];
                    $array[$key]['price'] =  $advert['price'];
                    $array[$key]['giveaway'] =  $advert['giveaway'];
                    $array[$key]['image'] = 'none.jpg';
                    if ($advert['photo'] && file_exists(YiiBase::getPathOfAlias('webroot').'/images/product_images/'. $advert['photo'])) {

                        $array[$key]['image'] =  $advert['photo'];
                    }

                    $array[$key]['description'] = $advert['description'];
                    $array[$key]['location'] = $advert['location'];
                    $array[$key]['username'] = $advert['username'];
                    $array[$key]['category'] = $advert['category'];
                    if ($advert['top']) {
                        $array[$key]['top'] = $advert['top'];
                    }
                    if ($advert['fav']) {
                        $array[$key]['fav'] = $advert['fav'];
                    }
                }
                if (isset($_GET['is_cat'])&&$_GET['is_cat']) {

                    $totalCount = count($products);
                    $products = array_slice($products, $pageSize * ($page-1), $pageSize);
                    $count = count($products);
                    $cat_id = $_GET['current_cat'] ? $_GET['current_cat'] : false;

                    $pages=new CPagination($totalCount);
                    // results per page
                    $pages->pageSize = $pageSize;

                    $this->renderPartial('_shop_area', array(
                        'products' => $products,
                        'pages' => $pages,
                        'totalCount' => $totalCount,
                        'countPage' => $count,
                        'page' => $page,
                        'is_filter' => true,
                        'category_id' => $cat_id,
                        'q' => $q_filter
                    ));
                }
                else echo json_encode($array);
            }
        }
    }

    public function actionMaxDestination()
    {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_GET['q'])) {
                $name = CHtml::encode($_GET['q']);
                $cur_cat = !empty($_GET['cat']) ? " AND (sub.`parent_id` = ".$_GET['cat']." OR subsub.`parent_id` = ".$_GET['cat']."
                    OR subsub.`id` = ".$_GET['cat'].")" : "";

                $q = Yii::app()->db->createCommand("SELECT p.lat, p.lng FROM products AS `p`
                    LEFT JOIN categories AS `subsub` ON (subsub.`id` = p.`categories_id`)
                    LEFT JOIN categories AS `sub` ON (sub.`id` = subsub.`parent_id`)
                    WHERE (p.name LIKE '%".$name."%' OR sublb.`title` LIKE '%".$name."%' OR subsublb.`title` LIKE '%".$name."%')".$cur_cat."
                    GROUP BY p.`id`");

                $products = $q->queryAll();
                //var_dump($products);die;

            } else {
                $products = Products::model()->findAll();
            }
            $arr = [];
            foreach ($products as $key => $product) {

                if (isset($_GET['geoloc_lat']) && isset($_GET['geoloc_lng'])) {

                    $user_lat = $_GET['geoloc_lat'];
                    $user_lng = $_GET['geoloc_lng'];

                    if (isset($_GET['current_cat'])) {
                        if ($product->categories->parentCategory->parent_id == $_GET['current_cat'] ||
                            $product->categories->parent_id == $_GET['current_cat'] ||
                            $product->categories_id == $_GET['current_cat'])
                        {
                            $dLat = deg2rad($product['lat'] - $user_lat);
                            $dLng = deg2rad($product['lng'] - $user_lng);
                            $R = 6371000;
                            $a = sin($dLat / 2) * sin($dLat / 2) +
                                cos(deg2rad($user_lat)) * cos(deg2rad($product['lat'])) *
                                sin($dLng / 2) * sin($dLng / 2);
                            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
                            $dist = $R * $c;
                            $distKm = round($dist / 1000);

                            $arr[$_GET['current_cat']][$key] = $distKm;
                        }
                        $max = !empty($arr[$_GET['current_cat']]) ? round(max($arr[$_GET['current_cat']])) : 0;

                    } else {
                        $dLat = deg2rad($product['lat'] - $user_lat);
                        $dLng = deg2rad($product['lng'] - $user_lng);
                        $R = 6371000;
                        $a = sin($dLat / 2) * sin($dLat / 2) +
                            cos(deg2rad($user_lat)) * cos(deg2rad($product['lat'])) *
                            sin($dLng / 2) * sin($dLng / 2);
                        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
                        $dist = $R * $c;
                        $distKm = round($dist / 1000);

                        $arr[$key] = $distKm;
                        $max = round(max($arr));
                    }
                }
            }

            echo json_encode($max);
        }
    }

    public function actionReferToFriend() {

        $model=new ContactForm('referTo');

        if (isset($_POST['ContactForm']) && !empty($_POST['ContactForm'])) {

            $model->attributes=$_POST['ContactForm'];

            if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

                $secret = '6Ldt8x4TAAAAAO_JnxKHLLAqdjMnAMlfxzMWRrHU';

                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);

                $responseData = json_decode($verifyResponse);

                if ($responseData->success) {

                    if ($model->validate()) {

                        FrontHelpers::referToFriendEmail($model->name, $model->email, $model->emailTo, $model->body, $_POST['ad_id']);
                        Yii::app()->user->setFlash('success', 'Your message was successfully sent');
                    }
                }
            } else {
                Yii::app()->user->setFlash('danger', 'Please complete the CAPTCHA correctly.');
            }
            $this->redirect(array('products/advert', 'id' => $_POST['ad_id']));
        }
    }

    public function actionContactAdmin() {

        $model = new ContactForm('contactAdmin');

        if (isset($_POST['ContactForm']) && !empty($_POST['ContactForm'])) {

            $adminEmail = Admins::model()->find()->email;

            $model->attributes=$_POST['ContactForm'];

            if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

                $secret = '6Ldt8x4TAAAAAO_JnxKHLLAqdjMnAMlfxzMWRrHU';

                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);

                $responseData = json_decode($verifyResponse);

                if ($responseData->success && $model->validate()) {

                    FrontHelpers::contactAdminEmail($model->name, $model->email, $adminEmail, $model->body);
                    Yii::app()->user->setFlash('success', 'Your message was successfully sent');

                }
            } else {
                Yii::app()->user->setFlash('danger', 'Please complete the CAPTCHA correctly.');
            }
            $this->redirect(Yii::app()->homeUrl);
        }
    }

}