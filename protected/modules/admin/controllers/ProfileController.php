<?php

class ProfileController extends Controller{

    public function filters() {
//        return array(
//            'accessControl', // perform access control for CRUD operations
//            'postOnly + delete', // we only allow deletion via POST request
//        );
    }

    public function accessRules()
    {
//        return array(
//            array('allow',
//                'actions' => array('login', 'forgotPassword'),
//                'users' => array('?')
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('admin','delete', 'logout', 'changePassword'),
//                'users'=>array('@'),
//            ),
//            array('deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
    }

    //public $defaultAction = "dashboard";


	public function actionDashboard() {
		$this->render(array('admins/index'));
	}

    public function actionLogin() {

//        if (!Yii::app()->user->isGuest) {
//            $this->redirect(array('/admin'));
//        }
        $model = new AdminLoginForm;

        if (isset($_POST['AdminLoginForm'])) {

            $admin = Admins::model()->findByAttributes(array(
                'username' => $_POST['AdminLoginForm']['username']
            ));

            $model->attributes = $_POST['AdminLoginForm'];

            if ($model->validate()) {

                if ($admin != null) {

                    $model->login();

                    $this->redirect(array('/admin'));
                }
            }
        }

        if (Yii::app()->user->isGuest) {
            $this->render('login', array(
                'model' => $model
            ));
        } else {
            $this->redirect(array('/admin'));
        }
    }

    public function actionEditDetails($id) {

        $session = Yii::app()->session;
        $session->open();
        $tst = $session['admin_id'];
//        var_dump($tst);die;
//        var_dump($session);die;
        $this->render('edit_details');
    }


    public function actionForgotPassword() {

        $model = new Admins;

        if (isset($_POST['Admins'])) {

            $admin = Admins::model()->findByAttributes(array('email' => $_POST['Admins']['email']));

            if ($admin) {

                $password = substr(md5(rand()), 0, 8);

                $hashPassword = sha1($password);

                FrontHelpers::passwordResetEmail($password, $admin->email,'resetPassword');

                $admin->password = $hashPassword;

                $admin->save();
                Yii::app()->user->setFlash('success', 'New password was sent to your email');
            } else {
                Yii::app()->user->setFlash('danger', 'Admin with entered email not found.');
            }
        }
        $this->render('forgot_password', array('model' => $model));
    }


    public function actionChangePassword() {

        $model = Admins::model()->findByPk(Yii::app()->user->getId());
        if (!$model) {
            $this->redirect(array('profile/login'));
        }
        $model->scenario = 'changePassword';

        if (isset($_POST['Admins'])) {

            $model->newPassword = $_POST['Admins']['newPassword'];

            if ($model->validate()) {

                if (sha1($_POST['Admins']['password']) == $model->password) {

                    $model->password = sha1($_POST['Admins']['newPassword']);
                    $model->save();
                    Yii::app()->user->setFlash('success', 'Your password has been changed');
                } else {
                    Yii::app()->user->setFlash('danger', 'Wrong password');
                }
            }
        }
        $this->render('change_password', array('model' => $model));
    }

    public function actionAddNewAdmin() {
        $this->render('add_new_admin');
    }

    public function actionRemoveNewAdmin() {
        $this->render('remove_admin');
    }

    public function actionLogout() {

        Yii::app()->user->logout(false);
        $this->redirect(array('profile/login'));
    }
}