<?php

class SlidersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public $defaultAction = "admin";

	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('create', 'update', 'view', 'delete', 'admin', 'deleteImage'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Sliders;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sliders'])){
            $model->attributes=$_POST['Sliders'];

			if (!empty($_FILES)) {
				$tmp_name = $_FILES['image']['tmp_name'];
				$destination = Yii::getPathOfAlias('webroot') . '/images/sliders';
				$model->image = $_FILES['image']['name'];
				move_uploaded_file($tmp_name, "$destination/$model->image");
			}
            if($model->save()) {

				Yii::app()->user->setFlash('success', "success");
				$this->redirect(array('view','id'=>$model->id));
			} else {
				Yii::app()->user->setFlash('danger', "Data3 ignored.");
			}
		}

		$this->render('create',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$slider=$this->loadModel($id);

		if (!$slider) {
			$this->redirect(array('admin'));
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sliders'])){

			$slider->attributes=$_POST['Sliders'];

			if(!empty($_FILES) && isset($_FILES['image']['tmp_name'])){

				$tmp_name = $_FILES['image']['tmp_name'];
				$slider->image = $_FILES['image']['name'];
				$destination = Yii::getPathOfAlias('webroot').'/images/sliders/'.$slider->image;
				move_uploaded_file($tmp_name,"$destination");
			}

			if($slider->save()){
				Yii::app()->user->setFlash('success', "Data has successfully updated.");
				$this->redirect(array('view','id'=>$slider->id));
			} else {
				Yii::app()->user->setFlash('danger', "Something wrong.");
			}

		}

		$this->render('update',array(
				'model'=>$slider
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Sliders('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Sliders']))
			$model->attributes=$_GET['Sliders'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Sliders the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Sliders::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function loadMyModel($id)
    {
        $model=SlidersLabels::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

	/**
	 * Performs the AJAX validation.
	 * @param Sliders $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sliders-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionDeleteImage() {

        if (Yii::app()->request->isAjaxRequest) {
            $image = Sliders::model()->findByAttributes(array('image' => $_POST['filename']));

            if ($image) {
                if ($image->image && file_exists(YiiBase::getPathOfAlias('webroot').'/images/sliders/'. $image->image)) {
                    unlink(YiiBase::getPathOfAlias("webroot").'/images/sliders/'. $image->image);
                    $image->image = null;
                    $image->update();

                    echo 1;
                }
            }
        } else {
            echo 0;
        }
    }
}
