<?php

class ProductsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create', 'update', 'view', 'admin', 'delete', 'approve', 'reject'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public $layout='//layouts/column2';

	public $defaultAction = "admin";

	public function actionAdmin($t = null)
	{
		if (isset($t)) {
			$model = Products::model()->findAllByAttributes(array('status' => CHtml::encode($t)));
		} else {
			$model = Products::model()->findAll();
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id)
		));
	}

	public function actionApprove($id = null) {
		$model = $this->loadModel($id);

		if ($model) {
			$model->status = 'active';
			if ($model->save()) { //add mail
				$note = new Notifications;
				$note->user_id = $model->user->id;
				$note->text = 'Your advert ['.$model->name.'] has been approved';
				$note->save();
				$this->redirect(array('view', 'id' => $id));
			}
		}
	}

	public function actionReject($id = null) {
		$model = $this->loadModel($id);

		if ($model) {
			$model->status = 'rejected';
			if ($model->save()) { //add mail
				$note = new Notifications;
				$note->user_id = $model->user->id;
				$note->text = 'Your advert ['.$model->name.'] has been rejected';
				$note->save();
			}
			$this->redirect(array('view', 'id' => $id));
		}
	}

	public function loadModel($id)
	{
		$model=Products::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
