<?php

class AdminsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('create', 'update', 'view', 'admin','delete'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}

	public $layout='//layouts/column2';

    public $defaultAction = "admin";

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

    public function actionIndex()
    {
        $this->render('index');
    }

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Admins;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Admins']))
		{
			$model->attributes=$_POST['Admins'];

            if(!empty($_FILES)){
//                var_dump($_FILES);die;
                $tmp_name = $_FILES['Admins']['tmp_name']['avatar'];
                $name = $_FILES['Admins']['name']['avatar'];
                $model->password = sha1($_POST['Admins']['password']);
                $model->avatar = $name;
                $destination = Yii::getPathOfAlias('webroot').'/images/admins';
                move_uploaded_file($tmp_name,"$destination/$name");
            }
			if ($model->validate()) {
				$model->save();
				Yii::app()->user->setFlash('success', 'Admin created successfully');
				$this->redirect(array('view','id'=>$model->id));
			}

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Admins']))
		{
			$model->attributes=$_POST['Admins'];

            if(!empty($_FILES)){
                $tmp_name = $_FILES['Admins']['tmp_name']['avatar'];
                $name = $_FILES['Admins']['name']['avatar'];
                $model->avatar = $name;
                $destination = Yii::getPathOfAlias('webroot').'/images/admins';
                move_uploaded_file($tmp_name,"$destination/$name");

            }

			if ($model->validate()) {
				$model->save();
				Yii::app()->user->setFlash('success', 'Admin updated successfully');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Admins('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Admins']))
			$model->attributes=$_GET['Admins'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Admins the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Admins::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Admins $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='admins-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionDeleteImage(){
        if (Yii::app()->request->isAjaxRequest) {
            $image = Admins::model()->findByAttributes(array('avatar' => $_POST['filename']));

            if ($image->avatar) {
                if ($image->avatar && file_exists(YiiBase::getPathOfAlias('webroot').'/images/admins/'. $image->avatar)) {
                    unlink(YiiBase::getPathOfAlias("webroot").'/images/admins/'. $image->avatar);
                    $image->avatar = '';
                    $image->update();

                    echo 1;
                }
            }
        } else {
            echo 0;
        }
    }
}
