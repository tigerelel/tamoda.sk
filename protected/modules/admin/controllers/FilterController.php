<?php

class FilterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public $defaultAction = "admin";

	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
				//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('create', 'update', 'view', 'admin','delete', 'deleteFilter'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = FilterGroup::model()->findByPk($id);

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$filtersNew = new FiltersNew;

		$parent = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');
		$catList = [];
		foreach ($parent as $key=>$val) {
			$parentSub = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key)), 'id', 'title');
			$catList[$key] = $val;
			foreach ($parentSub as $key2=>$sub) {
				$catList[$val][$key2] = $sub;
				$parentSubSubs = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key2)), 'id', 'title');
				foreach ($parentSubSubs as $key3=>$subSub) {
					$catList[$val][$sub][$key3] = $subSub;
				}
			}
		}

		if (isset($_POST['FiltersNew']) && isset($_POST['filters'])) {

			$filtersNew->name = CHtml::encode($_POST['FiltersNew']['name']);
			$filtersNew->filters = json_encode($_POST['filters']);
			if (isset($_POST['color_type'])) {
				$filtersNew->type = 1;
			}

			if ($filtersNew->validate()) {

				$filtersNew->save();

				if (isset($_POST['category_filter'])) {
					foreach ($_POST['category_filter'] as $key=>$val) {

						$modelFilter = new CategoriesFilter;
						$modelFilter->categories_id = $val;
						$modelFilter->filter_new = $filtersNew->id;
						$modelFilter->save();
					}
				}
				Yii::app()->user->setFlash('success', 'success');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create', array(
			'model' => $filtersNew,
			'catList' => $catList
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$filtersNew = FiltersNew::model()->findByPk($id);

		$parent = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');
		$catList = [];
		foreach ($parent as $key=>$val) {
			$parentSub = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key)), 'id', 'title');
			$catList[$key] = $val;
			foreach ($parentSub as $key2=>$sub) {
				$catList[$val][$key2] = $sub;
				$parentSubSubs = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key2)), 'id', 'title');
				foreach ($parentSubSubs as $key3=>$subSub) {
					$catList[$val][$sub][$key3] = $subSub;
				}
			}
		}

		$categoryFilters = CategoriesFilter::model()->findAllByAttributes(array('filter_new' => $id));

		if (isset($_POST['FiltersNew'])) {

			$filtersNew->name = CHtml::encode($_POST['FiltersNew']['name']);
//			$filtersNew->filters = json_encode($_POST['filters']);
			if (isset($_POST['color_type'])) {
				$filtersNew->type = $_POST['color_type'];
			}

			if ($filtersNew->validate()) {

				$filtersNew->update();


				if (isset($_POST['category_filter'])) {

					if ($categoryFilters) {
						foreach ($categoryFilters as $filter) {
							$filter->delete();
						}
					}

					foreach ($_POST['category_filter'] as $key=>$val) {

						$modelFilter = new CategoriesFilter;
						$modelFilter->categories_id = $val;
						$modelFilter->filter_new = $filtersNew->id;
						$modelFilter->save();
					}
				} else {
					if ($categoryFilters) {
						foreach ($categoryFilters as $filter) {
							$filter->delete();
						}
					}
				}

				Yii::app()->user->setFlash('success', 'success');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
			'model' => $filtersNew,
			'catList' => $catList,
			'categoryFilters' => $categoryFilters
		));
		/*
		$filterGroup = FilterGroup::model()->findByPk($id);
		if (!$filterGroup) {
			$this->redirect(array('admin'));
		}

        $parent = CHtml::listData(CategoriesLabels::model()->with('categories')->findAll(array('condition' => 'categories.parent_id=0')), 'categories.id', 'title');
        $catList = [];
        foreach ($parent as $key=>$val) {
            $parentSub = CHtml::listData(CategoriesLabels::model()->with('categories')->findAll(array('condition' => 'categories.parent_id='.$key)), 'categories.id', 'title');
            $catList[$key] = $val;
            foreach ($parentSub as $key2=>$sub) {
                $catList[$val][$key2] = $sub;
                $parentSubSubs = CHtml::listData(CategoriesLabels::model()->with('categories')->findAll(array('condition' => 'categories.parent_id='.$key2)), 'categories.id', 'title');
                foreach ($parentSubSubs as $key3=>$subSub) {
                    $catList[$val][$sub][$key3] = $subSub;
                }
            }
        }
        $categoriesFilter = CategoriesFilter::model()->findAllByAttributes(array('filter_group_id' => $id));

		$filter = new Filter;

		$labelArr = [];
		foreach (Languages::model()->findAll() as $key=>$lang) {
			$labels = FilterLabel::model()->findAll(array('condition' => 'filter_group_id='.$id.' AND languages_id='.$lang->id));
			foreach ($labels as $k=>$label) {
				$labelArr[$key][$k] = $label;
			}
		}
		$filterLabel = FilterLabel::model()->findAll(array('condition' => 'filter_group_id='.$id, 'group' => 'filter_id'));


		$modelGroupLabel = new FilterGroupLabel;
		$filterGroupLabel = FilterGroupLabel::model()->findAll(array('condition' => 'filter_group_id='.$id));

		if(isset($_POST['Filter']) && isset($_POST['FilterLabel']) && isset($_POST['FilterGroup']) && isset($_POST['FilterGroupLabel']))
		{
			$filterGroup->attributes = $_POST['FilterGroup'];

			$save = false;
			if ($filterGroup->save()) {
				foreach (Languages::model()->findAll() as $key => $lang) {
					$filterGroupLabel = FilterGroupLabel::model()->findByAttributes(array('filter_group_id' => $id, 'languages_id' => $lang->id));
					$filterGroupLabel->attributes = $_POST['FilterGroupLabel'][$key];

					if ($filterGroupLabel->validate()) {
						$filterGroupLabel->save();
						$save = true;
					}
				}
				foreach ($_POST['FilterLabel'] as $k=>$val) {
					$filter = new Filter;
					if (isset($_POST['Filter'][$k]['id'])) {
						$filter = $this->loadModel($_POST['Filter'][$k]['id']);
					}
					$filter->attributes = $_POST['Filter'][$k];
					$filter->filter_group_id = $filterGroup->id;

					if ($filter->save()) {
						foreach (Languages::model()->findAll() as $key => $lang) {
							$filterLabel = FilterLabel::model()->findByAttributes(array('filter_id' => $filter->id, 'languages_id' => $lang->id));

							if (!$filterLabel) {
								$filterLabel = new FilterLabel;
								$filterLabel->filter_id = $filter->id;
								$filterLabel->languages_id = $lang->id;
								$filterLabel->filter_group_id = $filterGroup->id;
							}

							$filterLabel->attributes = $_POST['FilterLabel'][$k][$lang->id];
							$filterLabel->save();
						}
					}
				}
                if (isset($_POST['category_filter'])) {

					foreach ($categoriesFilter as $filtergroup) {
                        $filtergroup->delete();
					}

					foreach ($_POST['category_filter'] as $key=>$val) {
                        $categoriesFilter = new CategoriesFilter;
                        $categoriesFilter->categories_id = $val;
                        $categoriesFilter->filter_group_id = $filterGroup->id;
                        $categoriesFilter->save();
					}
				} else {
					foreach ($categoriesFilter as $filtergroup) {
                        $filtergroup->delete();
					}
				}
			}
			if ($save) {
				Yii::app()->user->setFlash('success', 'success');
				$this->redirect(array('admin'));
			} else {
				Yii::app()->user->setFlash('danger', "fail");
			}
		}

		$this->render('update',array(
			'filter' => $filter,
			'filterLabel' => $filterLabel,
			'filterGroup' => $filterGroup,
			'modelGroupLabel' => $modelGroupLabel,
			'filterGroupLabel' => $filterGroupLabel,
			'labelArr' => $labelArr,
            'catList' => $catList,
            'modelFilter' => new CategoriesFilter,
            'categoriesFilter' => $categoriesFilter
		));
		*/
	}

	public function actionDelete($id)
	{
        $filterGroup = FilterGroup::model()->findByPk($id);
        if ($filterGroup) {
            $filterGroup->delete();
            $this->redirect(array('admin'));
        }

	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
//		$filterGroups = FilterGroup::model()->with('filterGroupLabel')->findAll(array('order' => 't.id DESC'));
//		$model=new FilterGroupLabel('search');
//		$model->unsetAttributes();  // clear any default values
//		if(isset($_GET['FilterGroupLabel']))
//			$model->attributes=$_GET['FilterGroupLabel'];
//
//		$this->render('admin',array(
//			'model'=>$model,
//			'filterGroups' => $filterGroups
//		));

		$filterGroups = FiltersNew::model()->findAll(array('order' => 'id DESC'));

		$this->render('admin', array(
			'filterGroups' => $filterGroups
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Filter the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Filter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Filter $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='filter-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
