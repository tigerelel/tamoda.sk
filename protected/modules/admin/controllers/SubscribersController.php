<?php

class SubscribersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public $defaultAction = "admin";

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Subscribers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subscribers']))
		{
			$model->attributes=$_POST['Subscribers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Subscribers']))
		{
			$model->attributes=$_POST['Subscribers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $newslettersList = CHtml::listData(Newsletters::model()->findAll(array()), 'id', 'title');
        if(!empty($_POST['Subscriptions'])){
            $news_letter_id = $_POST['Subscriptions']['newsletter_id'];
            $newsletters = Newsletters::model()->findByAttributes(array('id' => $news_letter_id));

            $title = $newsletters->title;
            $description = $newsletters->description;
            foreach($_POST['Subscriptions']['subscriber_id'] as $sub){
                $model=new Subscriptions();
                $model->subscriber_id = $sub;
                $model->newsletter_id = $_POST['Subscriptions']['newsletter_id'];
                $save = $model->save();



            }

            foreach($_POST['Subscriptions']['user_id'] as $user_id){
                $userData = Users::model()->findByAttributes(array('id' => $user_id));
                $email = $userData['email'];
                if($save){
                    FrontHelpers::subscriptionEmail($title,$description, $email );
                }
            }


        }
        $subscribers = Subscribers::model()->with('subscription')->findAll();

		$this->render('admin',array(
			'model'=>new Subscriptions(),
            'subscribers' => $subscribers,
            'newslettersList' => $newslettersList
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Subscribers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Subscribers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Subscribers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subscribers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionFilterSubscribers(){
        if (Yii::app()->request->isAjaxRequest) {
            $subscribers = Subscriptions::model()->findAll(array('condition' => 'newsletter_id =' .$_POST['newsletter_id']));

            $arr = [];
            if(empty($subscribers)){
                $user_id = Subscribers::model()->findAll();

                foreach($user_id as $value){
                    $arr[$value['user_id']] = $value['id'];
                }
            }else{
                $already_sub = [];
                foreach($subscribers as $subscriber){
                    $already_sub[] = $subscriber->subscriber_id;
                    $test = [];
                    $user_id = Subscribers::model()->findAll();
                    foreach($user_id as $id){
                        $test[$id['user_id']] = $id['id'];
                    }

                }
                $arr = array_diff($test,$already_sub);
            }
            echo json_encode($arr);

        }
    }
}
