<?php

class CategoriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public $defaultAction = "admin";

	public function filters() {
		return array(
				'accessControl', // perform access control for CRUD operations
				//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions'=>array('create', 'update', 'view', 'admin', 'delete', 'deleteImage'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$parent = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');
		$catList = [];
		foreach ($parent as $key=>$val) {
			$parentSub = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key)), 'id', 'title');
			$catList[$key] = $val;
			foreach ($parentSub as $key2=>$sub) {
				$catList[$val][$key2] = $sub;
				$parentSubSubs = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key2)), 'id', 'title');
				foreach ($parentSubSubs as $key3=>$subSub) {
					$catList[$val][$sub][$key3] = $subSub;
				}
			}
		}

		$model = new Categories;

		$modelFilter = new CategoriesFilter;

		if (isset ($_POST['Categories'])) {

			$model->attributes = $_POST['Categories'];

			$model->image = CUploadedFile::getInstance($model,'image');

			if ($model->save()) {

				if ($model->image) {
					$model->image->saveAs(YiiBase::getPathOfAlias('webroot') . '/images/cat_images/' . $model->image);
				}

				if (isset($_POST['category_filter'])) {
					foreach ($_POST['category_filter'] as $key=>$val) {

						$modelFilter = new CategoriesFilter;
						$modelFilter->categories_id = $model->id;
						$modelFilter->filter_new = $val;
						$modelFilter->save();
					}
				}
				Yii::app()->user->setFlash('success', 'success');
				$this->redirect(array('view','id'=>$model->id));
			} else {
				Yii::app()->user->setFlash('danger', "fail");
			}
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
			if($model->save())
				$this->redirect(array('view', 'id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'modelFilter' => $modelFilter,
			'catList' => $catList
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$parent = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');
		$catList = [];
		foreach ($parent as $key=>$val) {
			$parentSub = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key)), 'id', 'title');
			$catList[$key] = $val;
			foreach ($parentSub as $key2=>$sub) {
				$catList[$val][$key2] = $sub;
				$parentSubSubs = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$key2)), 'id', 'title');
				foreach ($parentSubSubs as $key3=>$subSub) {
					$catList[$val][$sub][$key3] = $subSub;
				}
			}
		}

		$model=$this->loadModel($id);

		$categories =  Categories::model()->findAllByPk($id);

		$modelFilter = new CategoriesFilter;

		$categoryFilters = CategoriesFilter::model()->findAllByPk($id);

		if (isset($_POST['Categories'])) {

			$model->attributes = $_POST['Categories'];

			if (!empty($_FILES) && isset($_FILES['image']['tmp_name'])) {

				$tmp_name = $_FILES['image']['tmp_name'];
				$model->image = $_FILES['image']['name'];
				$destination = Yii::getPathOfAlias('webroot').'/images/cat_images/'.$model->image;
				move_uploaded_file($tmp_name,"$destination");
			}


			if ($model->save()) {

				if (isset($_POST['category_filter'])) {

					foreach ($categoryFilters as $filter) {
						$filter->delete();
					}

					foreach ($_POST['category_filter'] as $key=>$val) {

						$modelFilter = new CategoriesFilter;
						$modelFilter->categories_id = $model->id;
						$modelFilter->filter_new = $val;
						$modelFilter->save();
					}
				} else {
					foreach ($categoryFilters as $filter) {
						$filter->delete();
					}
				}

				Yii::app()->user->setFlash('success', 'success update');
				$this->redirect(array('view', 'id'=>$model->id));

			} else {
				Yii::app()->user->setFlash('danger', "fail");
			}
		}

		if(isset($_POST['Categories']))
		{
			$model->attributes=$_POST['Categories'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
			'categories' => $categories,
			'modelFilter' => $modelFilter,
			'categoryFilters' => $categoryFilters,
			'catList' => $catList
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id=null)
	{

//		$catLabs = CategoriesLabels::model()->findAllByAttributes(array('categories_id' => $_POST['id']));
//		foreach ($catLabs as $lab) {
//			$lab->delete();
//		}
		if (Yii::app()->request->isAjaxRequest) {
			if ($_POST['id']) {
				$catId = $_POST['id'];
			}
		} else {
			$catId = $id;
		}
		$this->loadModel($catId)->delete();
		$subs = Categories::model()->findAllByAttributes(array('parent_id' => $catId));
		foreach ($subs as $sub) {
			$sub->delete();

		}
		$this->redirect(array('categories/'));
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
//		$model=new Categories('search');
//
//		$model->unsetAttributes();  // clear any default values
//		if(isset($_GET['Categories']))
//			$model->attributes=$_GET['Categories'];

		$model = Categories::model()->findAll();

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Categories the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Categories::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Categories $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='categories-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDeleteImage() {

		if (Yii::app()->request->isAjaxRequest) {
			$image = Categories::model()->findByAttributes(array('id' => $_POST['file_id']));

			if ($image) {
				if ($image->image && file_exists(YiiBase::getPathOfAlias('webroot').'/images/cat_images/'. $image->image)) {
					unlink(YiiBase::getPathOfAlias("webroot").'/images/cat_images/'. $image->image);
					$image->image = null;
					$image->update();

					echo 1;
				}
			}
		} else {
			echo 0;
		}
	}
}
