<div class="form col-md-4 col-md-offset-3">
    <h3>Reset Password</h3>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'reset-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <div class="form-group">
<!--        --><?php //echo $form->labelEx($model,'email'); ?>
        <?php echo $form->emailField($model,'email',array('size'=>60,'class'=>'form-control', 'required' => true, 'placeholder' => 'Email')); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="clearfix buttons">
        <?php echo CHtml::submitButton('Reset', array('class' => 'btn btn-primary')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
