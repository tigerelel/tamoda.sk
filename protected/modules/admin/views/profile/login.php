<div class="form col-md-4 col-md-offset-3">
    <h3>ADMIN LOGIN</h3>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'profile-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        )); ?>

        <!--    <p class="note">Fields with <span class="required">*</span> are required.</p>-->

        <!--    --><?php //echo $form->errorSummary($model); ?>

        <div class="form-group">
<!--            --><?php //echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>64, 'class'=>'form-control', 'placeholder' => 'Username')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>

        <div class="form-group">
<!--            --><?php //echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>64, 'class'=>'form-control', 'placeholder' => 'Password')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>

        <div class="checkbox">
            <label class="rem-me">
                <?php echo CHtml::activeCheckBox($model,'rememberMe'); ?> Remember
            </label>
        </div>

        <div class="clearfix buttons">
            <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-primary')); ?>
        </div>

        <?php $this->endWidget(); ?>


    <?php echo CHtml::link('Forgot Password?', array('profile/forgotPassword')); ?>
</div><!-- form -->
