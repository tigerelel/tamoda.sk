
<h3>Change Password</h3>
<?php $form=$this->beginWidget('CActiveForm', array(
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

<div class="form-group">
    <?php echo $form->labelEx($model,'Old password'); ?>
    <?php echo $form->passwordField($model,'password',array('size'=>60,'class'=>'form-control', 'value' => '', 'required' => true)); ?>
    <?php echo $form->error($model,'password'); ?>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model,'newPassword'); ?>
    <?php echo $form->passwordField($model,'newPassword',array('size'=>60,'class'=>'form-control', 'value' => '', 'required' => true)); ?>
    <?php echo $form->error($model,'newPassword'); ?>
</div>

<div class="clearfix buttons text-right">
    <?php echo CHtml::submitButton('Reset Password', array('class' => 'btn btn-primary')); ?>
</div>

<?php $this->endWidget(); ?>
