<?php
/* @var $this AdminsController */
/* @var $model Admins */

$this->breadcrumbs=array(
	'Admins'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Admins', 'url'=>array('create')),
	array('label'=>'Update Admins', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Admins', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Admins', 'url'=>array('admin')),
);
?>

<h1>View Admins #<?php echo $model->id; ?>
	<?php echo CHtml::link('<i class="fa fa-upload"></i>', array('admins/update', 'id' => $model->id), array('class' => 'pull-right')); ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'email',
		'first_name',
		'last_name',
		array(
			'label'=>'Avatar',
			'type'=>'raw',
			'value'=> CHtml::image(Yii::app()->request->baseUrl.'/images/admins/' . $model->avatar, '', array('width' => '300px', 'class' => 'img-rounded')),
		),
	),
)); ?>
