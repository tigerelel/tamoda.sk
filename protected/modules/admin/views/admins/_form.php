<?php
/* @var $this AdminsController */
/* @var $model Admins */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admins-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>64, 'class'=>'form-control', 'required' => true)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->emailField($model,'email',array('size'=>60,'maxlength'=>64, 'class'=>'form-control', 'required' => true)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>64, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>64, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>
    <?php if(!empty($model->avatar)){ ?>
        <div class="image-box">
            <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/admins/' . $model->avatar, '', array('name' => $model->avatar, 'class'=>'img-rounded', 'width'=>150, 'height'=>140)); ?>
            <?php echo CHtml::button(Yii::t('app', 'Delete'), array('class' => 'btn btn-danger delete')); ?>
        </div>
    <?php } else{?>
        <div class="form-group">
            <?php echo $form->labelEx($model,'avatar'); ?>
            <?php echo $form->fileField($model,'avatar',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
            <?php echo $form->error($model,'avatar'); ?>
        </div>
    <?php }?>

	<div class="clearfix buttons text-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).on('click', '.delete', function() {
        var that = $(this);
        var filename = $(this).prev()[0].name;
        console.log($(this).prev()[0]);
        $.ajax({
            type: 'POST',
            data: {
                filename: filename
            },
            url: '<?php echo Yii::app()->request->baseUrl;?>/admin/admins/DeleteImage',
            success: function() {
                that.prev().remove();
                that.remove();
            }
        })

    })
</script>