<?php
/* @var $this AdminsController */
/* @var $model Admins */

$this->breadcrumbs=array(
	'Admins'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Admins', 'url'=>array('admin')),
);
?>

<h1>Create Admins</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>