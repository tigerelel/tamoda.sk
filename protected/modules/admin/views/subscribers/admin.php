<?php
/* @var $this SubscribersController */
/* @var $model Subscribers */

$this->breadcrumbs=array(
	'Subscribers'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Subscribers', 'url'=>array('index')),
	array('label'=>'Create Subscribers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#subscribers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>
    Manage
    Subscribers
    <a href="/tamoda.sk/admin/Subscribers/create" class="pull-right">
        <i class="fa fa-plus"></i>
    </a>
</h1>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'subscribe-form',
    'enableAjaxValidation' => false,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> Subscribers List</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
<!--                $form->dropDownList($model,'status',array(0=>'new',1 => 'used'), array('class'=>'prof-form'))-->
                <?php echo $form->dropDownList($model, 'newsletter_id', CHtml::listData(Newsletters::model()->findAll(array()), 'id', 'title'),array('class'=>'prof-form required', 'prompt' => 'Choose newsletter')); ?>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <td class="text-left">
                            Subscriber id
                        </td>
                        <td class="text-right">
                            Sort Order
                        </td>
                        <td class="text-right">Action</td>
                    </tr>
                    </thead>

                    <tbody>

                        <tr>
                            <td class="text-left user-check"></td>
<!--                            <td class="text-right">--><?php //echo $subscriber['user_id'] ?><!--</td>-->
<!--                            <td class="text-right">-->
<!--                                --><?php //echo CHtml::link('<i class="fa fa-eye"></i>', array('subscribers/view', 'id' => $subscriber->id)); ?>
<!--                                --><?php //echo CHtml::link('<i class="fa fa-pencil"></i>', array('subscribers/update', 'id' => $subscriber->id)); ?>
<!--                                --><?php //echo CHtml::link('<i class="fa fa-remove"></i>', array('subscribers/delete', 'id' => $subscriber->id), array('confirm' => 'Are you sure you want to delete this item?')); ?>
<!--                            </td>-->
                        </tr>

                    </tbody>
                </table>
                <?php echo CHtml::submitButton('Send', array('class' => 'btn btn-primary')); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>


<script>
    $(document).ready(function(){
        $('#Subscriptions_newsletter_id').change(function(){
            var newsletter_id = $(this).val();

            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl;?>/admin/subscribers/FilterSubscribers' ,
                type: "post",
                data: {
                    newsletter_id: newsletter_id

                },
                success: function(data)
                {
                    $('.check').remove();
                    var data1 = JSON.parse(data);
                    $.each( data1, function( key, value ) {
                        console.log(value);
                        $('.user-check').append('<span class="check"><input type="checkbox" name="Subscriptions[subscriber_id][]" value="'+value+'">'+value+'<input type="hidden" name="Subscriptions[user_id][]" value="'+key+'"><input type="hidden" name="newslatter_id" value="'+newsletter_id+'"></span>')
                    })


                }
        })
    })
})
</script>
