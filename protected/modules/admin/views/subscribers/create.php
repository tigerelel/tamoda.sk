<?php
/* @var $this SubscribersController */
/* @var $model Subscribers */

$this->breadcrumbs=array(
	'Subscribers'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Subscribers', 'url'=>array('admin')),
);
?>

<h1>Create Subscribers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>