<?php
/* @var $this FilterController */
/* @var $model Filter */

$this->breadcrumbs=array(
	'Filters'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Filter', 'url'=>array('admin')),
);
?>

<h1>Create Filter</h1>

<?php $this->renderPartial('_form', array(
	'model' => $model,
	'catList' => $catList
		/*'filter'=>$filter,
		'filterGroup' => $filterGroup,
		'modelGroupLabel' => $modelGroupLabel,
        'catList' => $catList,
        'modelFilter' => $modelFilter*/
		)); ?>