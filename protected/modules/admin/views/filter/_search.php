<?php
/* @var $this FilterController */
/* @var $model Filter */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="form-group">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>
	</div>

    <div class="form-group">
		<?php echo $form->label($model,'filter_group_id'); ?>
		<?php echo $form->textField($model,'filter_group_id', array('class'=>'form-control')); ?>
	</div>

    <div class="form-group">
		<?php echo $form->label($model,'sort_order'); ?>
		<?php echo $form->textField($model,'sort_order', array('class'=>'form-control')); ?>
	</div>

	<div class="clearfix buttons text-right">
		<?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->