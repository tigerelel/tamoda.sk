<?php
/* @var $this FilterController */
/* @var $model Filter */

$this->breadcrumbs=array(
	'Filters'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Filter', 'url'=>array('create')),
	array('label'=>'Update Filter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Filter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Filter', 'url'=>array('admin')),
);
?>

<h1>View Filter #<?php echo $model->id; ?>
	<?php echo CHtml::link('<i class="fa fa-upload"></i>', array('filter/update', 'id' => $model->id), array('class' => 'pull-right')); ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'name' => 'filter_group_id',
			'value' => function($data) {
				return $data->filterGroupLabel[0]->name;
			},
		),
		'sort_order',
	),
)); ?>

<?php if ($model->type != 'color'): ?>
	<div class="clearfix">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<?php foreach(Languages::model()->findAll() AS $key => $language): ?>
					<li class="lang <?=$key==0?"active":""?>" lang_id="<?=$language->id?>">
						<a href="#tab_<?=$key?>" data-toggle="tab" aria-expanded="true"><?php echo CHtml::image($language->flag_url, '', array('width' => '20px')); ?> <?=$language->title?></a>
					</li>
				<?php endforeach; ?>
			</ul>
			<div class="tab-content">
				<?php foreach(Languages::model()->findAll() AS $key=>$language): ?>
					<?php $labels = FilterLabel::model()->findAllByAttributes(array('languages_id' => $language->id, 'filter_group_id' => $model->id)); ?>
					<?php if($labels): ?>

						<div class="tab-pane <?=$key==0?"active":""?>" id="tab_<?=$key?>">

							<?php foreach($labels as $label): ?>
								<label>Sort order: </label>
								<?php echo $label->filter->sort_order; ?>
								<div class="form-group">
									<label>Name: </label>
									<?php echo $label->name; ?>
								</div>
							<?php endforeach; ?>
						</div>

					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php else: ?>
	<?php foreach (FilterAdditional::model()->findAllByAttributes(array('filter_group_id' => $model->id)) as $color): ?>
		<span style="background: <?php echo $color->title; ?>; padding: 0 10px; margin: 10px;"></span>
	<?php endforeach; ?>
<?php endif; ?>
