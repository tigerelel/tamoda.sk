<?php
/* @var $this FilterController */
/* @var $model Filter */

$this->breadcrumbs=array(
	'Filters'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Filter', 'url'=>array('index')),
	array('label'=>'Create Filter', 'url'=>array('create')),
);

?>

<h1>
    Manage Filters
    <?php echo CHtml::link('<i class="fa fa-plus"></i>', array('filter/create'), array('class' => 'pull-right')); ?>
</h1>

<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> Filter List</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td class="text-left">
                                Filter Group
                            </td>

                            <td class="text-right">Action</td>
                        </tr>
                    </thead>

                    <tbody>

                        <?php
                        if (!empty($filterGroups)) {
                            foreach ($filterGroups as $group) { ?>
                                <tr>
                                    <td class="text-left"><?php echo $group->name; ?></td>

                                    <td class="text-right">
                                        <?php echo CHtml::link('<i class="fa fa-eye"></i>', array('filter/view', 'id' => $group->id)); ?>
                                        <?php echo CHtml::link('<i class="fa fa-pencil"></i>', array('filter/update', 'id' => $group->id)); ?>
                                        <?php echo CHtml::link('<i class="fa fa-remove"></i>', array('filter/delete', 'id' => $group->id), array('confirm' => 'Are you sure you want to delete this item?')); ?>
                                    </td>
                                </tr>
                            <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
