<?php
/* @var $this FilterController */
/* @var $model Filter */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'filter-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="col-md-12">
		<div class="form-group pull-left col-md-3">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Filter group')); ?>
			<br>
			<div class="clearfix">
				<div class="form-group">
					<?php $modelFilter = new CategoriesFilter; ?>
					<?php echo $form->labelEx($modelFilter,'categories_id'); ?>
					<?php echo $form->dropDownList($modelFilter,'categories_id', $catList, array('class'=>'form-control', 'empty' => 'Select category')); ?>
					<?php echo $form->error($modelFilter,'categories_id'); ?>
				</div>
				<div id="category-filter">
					<?php if (!empty($categoryFilters)): ?>
						<?php foreach ($categoryFilters as $filter): ?>
							<div>
								<i class="fa fa-minus-circle" id="<?= $filter->categories_id; ?>"></i>
								<?php echo $filter->categories->title; ?>
								<input type="hidden" value="<?= $filter->categories_id; ?>" name="category_filter[]">
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group col-md-3">
			<a class="btn btn-primary" title="" data-toggle="tooltip" onclick="addFilterRow(this);" data-original-title="Add Filter">
				<i class="fa fa-plus-circle"></i>
			</a>
			<?php if (!empty($model->filters) && $model->type != 1) { ?>
				<?php $filters = json_decode($model->filters); ?>

				<?php foreach ($filters as $key => $val) { ?>
<!--					<div class="form-group relative">-->
<!--						<input class="form-control" type="text" placeholder="Filter name" name="filters[]" value="--><?//=$val;?><!--">-->
<!--						<span class="remove-span">-->
<!--							<button class="btn btn-danger" data-original-title="Remove" title="" data-toggle="tooltip"-->
<!--									type="button" onclick="$(this).parent().parent().remove();">-->
<!--								<i class="fa fa-minus-circle"></i>-->
<!--							</button>-->
<!--						</span>-->
<!--					</div>-->
				<?php }
			} ?>
		</div>

		<button type="button" class="btn btn-success btn-md pull-right" data-toggle="modal" data-target="#color_modal">Color inputs</button>

		<div id="color_modal" class="modal fade" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
						<div class="form-group" id="colors" style="">
							<a class="btn btn-primary" title="" data-toggle="tooltip" onclick="addColorRow(this);" data-original-title="Add Color Filter">
								<i class="fa fa-plus-circle"></i>
							</a>
							<input type="hidden" name="color_type" value="0">
							<?php if (!empty($model->filters) && $model->type == 1) { ?>
								<?php $filters = json_decode($model->filters); ?>

								<?php foreach ($filters as $key => $val) { ?>
									<div class="form-group relative">
										<input class="form-control" type="color" placeholder="Filter name" name="filters[]" value="<?=$val;?>">
										<span class="remove-span">
											<button class="btn btn-danger" data-original-title="Remove" title="" data-toggle="tooltip"
													type="button" onclick="$(this).parent().parent().remove();">
												<i class="fa fa-minus-circle"></i>
											</button>
										</span>
									</div>
								<?php }
							} ?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix buttons text-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-colorpicker.min.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-colorpicker.min.js" type="text/javascript"></script>

<script>

	$('#color_modal').on('hidden.bs.modal', function (e) {

		var color_type = $('input[name="color_type"]');

		var color_inputs = $('#colors').find('.form-group');

		if (color_inputs.length == 0) {
			color_type.attr('value', 0);
		} else {
			color_type.attr('value', 1);
		}
	});

	var i=0;
	var n=0;
	function addFilterRow($this) {

		$($this).parent().append('<div class="form-group relative">'+
				'<input id='+i+' type="text" name="filters['+i+'][main_size]" class="form-control filter" placeholder="Filter name">'+
				'<span class="remove-span">'+
					'<button onclick="$(this).parent().parent().remove();" class="btn btn-danger" type="button" data-toggle="tooltip" title="" data-original-title="Remove">'+
						'<i class="fa fa-minus-circle"></i>'+
					'</button>'+
				'</span>'+
				'<span class="add-span">'+
					'<button onclick="addOtherSizes(this)" class="btn btn-default" type="button" data-toggle="tooltip" title="" data-original-title="Add other sizes">'+
						'<i class="fa fa-plus-circle"></i>'+
					'</button>'+
				'</span>'+
			'</div>');
		i++;
	}


	function addOtherSizes($this) {
		var main = $($this).parent().parent().find('.filter').attr('id');

//		console.log($($this).parent().parent().find('.filter').attr('id'))
		$($this).parent().parent().append('<div><input name="filters['+main+']['+n+'][other_size_name]" type="text" placeholder="Name"><input name="filters['+main+']['+n+'][other_size_val]" type="text" placeholder="value">'+
			'<button onclick="$(this).parent().remove();" class="btn btn-danger btn-sm" type="button" onclick="" data-toggle="tooltip" title="" data-original-title="Remove">'+
				'<i class="fa fa-minus-circle"></i>'+
			'</button></div>');
		n++;
	}

	function addColorRow($this) {
		$($this).parent().append('<div class="form-group relative">'+
				'<input type="color" name="filters[]" class="form-control" placeholder="Filter name">'+
				'<span class="remove-span">'+
					'<button onclick="$(this).parent().parent().remove();" class="btn btn-danger" type="button" onclick="" data-toggle="tooltip" title="" data-original-title="Remove">'+
						'<i class="fa fa-minus-circle"></i>'+
					'</button>'+
				'</span>'+
			'</div>');
	}

	function removeFilter(filter_id, e) {
		var answer = confirm('are you sure?');
		if (answer) {
			$.ajax({
				type: 'POST',
				data: {
					id: filter_id
				},
				url: '<?php echo Yii::app()->request->baseUrl;?>/admin/filter/deleteFilter',
				success: function() {
					e.remove();
				}
			})
		}
	}

	var arr = [];

	var categoryFilters = $('#category-filter').find($('.fa-minus-circle'));
	$.each(categoryFilters, function(i, v) {
		arr.push(v.id)
	})

	$('#CategoriesFilter_categories_id').change(function() {

		var categoryFilter = $('#CategoriesFilter_categories_id :selected');
		console.log(arr)
		var group_id = $('#CategoriesFilter_categories_id :selected').val();
		var filters = '';
		if ($.inArray(group_id, arr) == -1 && group_id != '') {
			filters += '<div><i class="fa fa-minus-circle" id="'+categoryFilter.val()+'"></i> ' + categoryFilter.text() + '<input type="hidden" value="' + categoryFilter.val() + '" name="category_filter[]"></div>'
			$('#category-filter').append(filters);
			arr.push(group_id);
		}

	});

	$('#category-filter').on('click', '.fa-minus-circle', function() {
		arr.splice( $.inArray($(this).attr('id'), arr), 1 );
		$(this).parent().remove();
	});

</script>