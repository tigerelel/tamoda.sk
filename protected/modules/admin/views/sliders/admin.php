<?php
/* @var $this SlidersController */
/* @var $model Sliders */

$this->breadcrumbs=array(
	'Sliders'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Sliders', 'url'=>array('index')),
	array('label'=>'Create Sliders', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sliders-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>
    Manage Sliders
    <?php echo CHtml::link('<i class="fa fa-plus"></i>', array('sliders/create'), array('class' => 'pull-right')); ?>
</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'sliders-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => false,
    'htmlOptions' => array(),
    'itemsCssClass' => 'items table table-responsive table-hover table-bordered table-striped',
    'pagerCssClass'=>'clearfix',
    'pager' => array(
    'htmlOptions'=>array(
    'class'=>'pagination',
    ),
    'header'=>'',
    'firstPageLabel' => '<<',
    'prevPageLabel'  => '<',
    'nextPageLabel'  => '>',
    'lastPageLabel'  => '>>',
    'selectedPageCssClass' => 'active disabled',
    ),
    'filter'=>$model,
    'columns'=>array(
		'id',
        'title',
        'description',
        array(
            'name' => 'image',
            'type'=>'raw',
            'value'=> 'CHtml::image(Yii::app()->baseUrl."/images/sliders/".$data->image, "",array("width"=>300, "class"=>"img-rounded"))',
        ),
		'created_date',
		'status',
		array(
			'class'=>'CButtonColumn',
            'buttons' => array(
                'view' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-eye"></i>',
                    'imageUrl' => false,
                ),
                'update' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-upload"></i>',
                    'imageUrl' => false,
                ),
                'delete' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-remove"></i>',
                    'imageUrl' => false,
                ),
            ),
		),
	),
)); ?>

<style>
    .items.table thead{background:#ecf0f5;}
</style>
<script>
    $(function(){
       $('table.items thead').find('input').addClass('form-control');
    });
</script>