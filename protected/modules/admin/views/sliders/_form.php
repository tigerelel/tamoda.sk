<?php
/* @var $this SlidersController */
/* @var $model Sliders */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sliders-form',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group hidden">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date', array('class'=>'form-control', 'value' => ($model->created_date != "") ? $model->created_date : FrontHelpers::toSQLDateTime())); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array(0=>'disabled',1 => 'enabled'), array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

    <div class="form-group">
        <?php echo $form->textField($model,'title',array('required' => true, 'class'=>'form-control', 'placeholder' => 'Title')); ?>
    </div>
    <div class="form-group">
        <?php echo  $form->textArea($model,'description',array('required' => true, 'class'=>'form-control', 'placeholder' => 'Description')); ?>
    </div>
    <div class="form-group">
        <?php echo $form->urlField($model,'url',array('required' => true, 'class'=>'form-control', 'placeholder' => 'Url')); ?>
    </div>
    <?php if (file_exists(YiiBase::getPathOfAlias('webroot') . '/images/sliders/' . $model->image) && !empty($model->image)) { ?>
        <div class="image-box">
            <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/sliders/' . $model->image, '', array('name' => $model->image, 'class'=>'img-rounded', 'width'=>150, 'height'=>140)); ?>
            <?php echo CHtml::button(Yii::t('app', 'Delete'), array('class' => 'btn btn-danger delete')); ?>
        </div>
    <?php } else { ?>
        <div class="form-group">
            <input name="image" type="file" class="form-control">
        </div>
    <?php } ?>

</div>


	<div class="clearfix buttons text-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).on('click', '.delete', function() {
        var that = $(this);
        var filename = $(this).prev()[0].name;
        $.ajax({
            type: 'POST',
            data: {
                filename: filename
            },
            url: '<?php echo Yii::app()->request->baseUrl;?>/admin/sliders/DeleteImage',
            success: function() {
                that.prev().remove();
                that.remove();
                $('.image-box').append('<div class="form-group">'+
                    '<input name="image" type="file" class="form-control">'+
                    '</div>');
            }
        })
    })
</script>