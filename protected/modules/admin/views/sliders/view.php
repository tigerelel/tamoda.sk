<?php
/* @var $this SlidersController */
/* @var $model Sliders */

$this->breadcrumbs=array(
	'Sliders'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Sliders', 'url'=>array('create')),
	array('label'=>'Update Sliders', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Sliders', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Sliders', 'url'=>array('admin')),
);
?>

<h1>View Sliders #<?php echo $model->id; ?>
<?php echo CHtml::link('<i class="fa fa-upload"></i>', array('sliders/update', 'id' => $model->id), array('class' => 'pull-right')); ?>
</h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'description',
		'url',
		array(
			'name' => 'image',
			'type'=>'raw',
			'value'=> CHtml::image(Yii::app()->request->baseUrl.'/images/sliders/' . $model->image, '', array('width' => '300px', 'class' => 'img-rounded')),
		),
		'created_date',
		'status'
	),
)); ?>
