<?php
/* @var $this SlidersController */
/* @var $model Sliders */

$this->breadcrumbs=array(
	'Sliders'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Sliders', 'url'=>array('admin')),
);
?>

<h1>Create Sliders</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>