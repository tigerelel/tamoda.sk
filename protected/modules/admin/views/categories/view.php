<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Categories', 'url'=>array('create')),
	array('label'=>'Update Categories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Categories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Categories', 'url'=>array('admin')),
);
?>

<h1>View Categories #<?php echo $model->id; ?>
	<?php echo CHtml::link('<i class="fa fa-upload"></i>', array('categories/update', 'id' => $model->id), array('class' => 'pull-right')); ?>
	<?php echo CHtml::link('<i class="fa fa-plus"></i>', array('categories/create'), array('class' => 'pull-right')); ?>
</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'name' => 'parent_id',
				'value' => $model->parent_id!=0 ? $model->parentCategory->title: 'No parent'
		),
		'title',
		'description',
		'created_date',
		'status',
		'sort_order',
	),
)); ?>

