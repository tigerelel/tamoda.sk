<?php
/* @var $this CategoriesController */
/* @var $model Categories */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'categories-form',
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->dropDownList($model,'parent_id', $catList, array('class'=>'form-control', 'empty' => 'Choose parent')); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>

	<?php echo $form->hiddenField($model,'created_date', array('class'=>'form-control', 'value' => ($model->created_date != "") ? $model->created_date : FrontHelpers::toSQLDateTime())); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array(0=>'disabled', 1=>'enabled'), array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'sort_order'); ?>
		<?php echo $form->dropDownList($model,'sort_order',array(0 => '1',1 => '2', 2 => '3'), array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'sort_order'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->textField($model,'title',array('required' => true, 'class'=>'form-control', 'placeholder' => 'Title')); ?>
	</div>
	<div class="form-group">
		<?php echo $form->textField($model,'description',array('required' => true, 'class'=>'form-control', 'placeholder' => 'Description')); ?>
	</div>

	<?php if (file_exists(YiiBase::getPathOfAlias('webroot') . '/images/cat_images/' . $model->image) && !empty($model->image)) { ?>
		<div class="image-box">
			<?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/cat_images/' . $model->image, '', array('class'=>'img-rounded', 'width'=>150, 'height'=>140)); ?>
			<?php echo CHtml::button(Yii::t('app', 'Delete'), array('class' => 'btn btn-danger delete', 'data-id' => $model->id)); ?>
		</div>
	<?php } else { ?>
		<div class="form-group">
			<input name="image" type="file" class="form-control">
		</div>
	<?php } ?>

	<div class="form-group">
		<?php $filters = CHtml::listData(FiltersNew::model()->findAll('type=0'), 'id', 'name'); ?>
		<?php echo $form->labelEx($modelFilter,'filter_new'); ?>

		<?php echo $form->dropDownList($modelFilter,'filter_new', $filters, array('class'=>'form-control', 'empty' => 'Select filter group')); ?>
		<?php echo $form->error($modelFilter,'filter_new'); ?>
	</div>
	<div id="category-filter">
		<?php if (!empty($categoryFilters)): ?>
			<?php foreach ($categoryFilters as $filter): ?>
				<div>
					<i class="fa fa-minus-circle" id="<?php echo $filter->filter_new; ?>"></i>
					<?php echo $filter->filterNew->name; ?>
					<input type="hidden" value="<?php echo $filter->filter_new; ?>" name="category_filter[]">
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>

	<div class="clearfix buttons text-right">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary')); ?>
		<?php echo !$model->isNewRecord ? CHtml::link('Delete', array('categories/delete', 'id' => $model->id), array('confirm' => 'Are you sure?', 'class' => 'btn btn-danger')) : ''; ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>

	var arr = [];

	var categoryFilters = $('#category-filter').find($('.fa-minus-circle'));
	$.each(categoryFilters, function(i, v) {
		arr.push(v.id)
	})

	$('#CategoriesFilter_filter_new').change(function() {

		var categoryFilter = $('#CategoriesFilter_filter_new :selected');
		var group_id = $('#CategoriesFilter_filter_new :selected').val();
		var filters = '';
		if ($.inArray(group_id, arr) == -1 && group_id != '') {
			filters += '<div><i class="fa fa-minus-circle" id="'+categoryFilter.val()+'"></i> ' + categoryFilter.text() + '<input type="hidden" value="' + categoryFilter.val() + '" name="category_filter[]"></div>'
			$('#category-filter').append(filters);
			arr.push(group_id);
		}

	});

	$('#category-filter').on('click', '.fa-minus-circle', function() {
		arr.splice( $.inArray($(this).attr('id'), arr), 1 );
		$(this).parent().remove();
	});
	$(document).on('click', '.delete', function() {
		var that = $(this);
		var file_id = $(this).data('id');
		$.ajax({
			type: 'POST',
			data: {
				file_id: file_id
			},
			url: '<?php echo Yii::app()->request->baseUrl;?>/admin/categories/DeleteImage',
			success: function() {
				that.prev().remove();
				that.remove();
				$('.image-box').append('<div class="form-group">'+
					'<input name="image" type="file" class="form-control">'+
					'</div>');
			}
		})
	})


</script>