<?php
/* @var $this CategoriesController */
/* @var $model Categories */

?>

<h1>
    Manage Categories
    <?php echo CHtml::link('<i class="fa fa-plus"></i>', array('categories/create'), array('class' => 'pull-right')); ?>
</h1>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">

				<div class="box-body">
					<input type="button" id="button" class="btn btn-adn" style="position: absolute;" value="Delete selected row">
					<table id="cat-table" class="table table-bordered table-hover">
						<thead>
						<tr>
							<th>Name</th>
							<th>Parent</th>
							<th>Parent parent</th>
							<th>Status</th>
							<th>Sort order</th>
							<th>Created</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
							<?php foreach ($model as $cat) { ?>
								<tr id="<?php echo $cat->id; ?>">
									<td><?php echo $cat->title; ?></td>
									<td class="parent"><?=  $cat->parentCategory ? $cat->parentCategory->title: ""; ?></td>
									<td><?php echo $cat->parentCategory && $cat->parentCategory->parentCategory ? $cat->parentCategory->parentCategory->title: ""; ?></td>
									<td><?php echo $cat->status==0 ? "disabled" : "enabled"; ?></td>
									<td><?php echo $cat->sort_order; ?></td>
									<td><?php echo $cat->created_date; ?></td>
									<td class="button-column">
										<?php echo CHtml::link('<i class="fa fa-eye"></i>', array('categories/view', 'id' => $cat->id)); ?>
										<?php echo CHtml::link('<i class="fa fa-upload"></i>', array('categories/update', 'id' => $cat->id)); ?>
										<?php echo CHtml::link('<i class="fa fa-remove"></i>', array('categories/delete', 'id' => $cat->id), array('onclick' =>'js:deleteCat('.$cat->id.', $(this)); return false;')); ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->

		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<link rel="stylesheet" href="../templates/AdminLTE/plugins/datatables/dataTables.bootstrap.css">

<script src="../templates/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../templates/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(function () {

		var table = $('#cat-table').DataTable({
			"paging": true,
			"lengthChange": false,
//			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false
		});

		$('#button').hide();

		$('#cat-table tbody').on( 'click', 'tr', function () {

			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$('#button').hide();
			}
			else {
				table.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$('#button').show();
			}

		});

		$('#button').click( function () {
			var answer = confirm('Are you sure you want to delete this item?');
			if (answer) {
				$.ajax({
					type: 'POST',
					data: {
						id: $('table').find('.selected').attr('id')
					},
					url: '<?php echo Yii::app()->request->baseUrl;?>/admin/categories/delete',
					success: function() {
						table.row('.selected').remove().draw( false );
					}
				})
			}

		});
	});

</script>
