<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Categories', 'url'=>array('create')),
	array('label'=>'View Categories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Categories', 'url'=>array('admin')),
);
?>

<h1>Update Categories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array(
		'model'=>$model,
		'categories' => $categories,
		'modelFilter' => $modelFilter,
		'categoryFilters' => $categoryFilters,
		'catList' => $catList
)); ?>