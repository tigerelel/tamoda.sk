<?php
/* @var $this NewslettersController */
/* @var $model Newsletters */

$this->breadcrumbs=array(
	'Newsletters'=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Newsletters', 'url'=>array('create')),
	array('label'=>'View Newsletters', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Newsletters', 'url'=>array('admin')),
);
?>

<h1>Update Newsletters <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>