<?php
/* @var $this NewslettersController */
/* @var $model Newsletters */

$this->breadcrumbs=array(
	'Newsletters'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Newsletters', 'url'=>array('admin')),
);
?>

<h1>Create Newsletters</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>