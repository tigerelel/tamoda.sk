<?php
/* @var $this NewslettersController */
/* @var $model Newsletters */

$this->breadcrumbs=array(
	'Newsletters'=>array('admin'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Create Newsletters', 'url'=>array('create')),
	array('label'=>'Update Newsletters', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Newsletters', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Newsletters', 'url'=>array('admin')),
);
?>

<h1>View Newsletters #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'description',
	),
)); ?>
