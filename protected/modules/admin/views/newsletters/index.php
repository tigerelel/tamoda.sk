<?php
/* @var $this NewslettersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Newsletters',
);

$this->menu=array(
	array('label'=>'Create Newsletters', 'url'=>array('create')),
	array('label'=>'Manage Newsletters', 'url'=>array('admin')),
);
?>

<h1>Newsletters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
