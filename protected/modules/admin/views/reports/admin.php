<?php
/* @var $this CategoriesController */
/* @var $model Categories */

?>

<h1>
	Manage Reports
</h1>
<div class="collapse navbar-collapse">
	<ul class="nav navbar-nav nav-tabs">
		<li><?php echo CHtml::link('All Reports', array('reports/')); ?></li>
	</ul>
</div>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">

				<div class="box-body">
					<input type="button" id="button" class="btn btn-adn" style="position: absolute;" value="Delete selected row">
					<table id="prod-table" class="table table-bordered table-hover">
						<thead>
						<tr>
							<th>Advert Name</th>
							<th>Reports type</th>
							<th>Reports Text</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($model as $report) { ?>
							<tr id="<?php echo $report->id; ?>">
								<td><?php echo CHtml::link($report->product->name, array('products/view', 'id' => $report->product->id)); ?></td>
								<td><?php echo ProductsReports::$reportType[$report->report_type]; ?></td>
								<td><?php echo $report->report_text; ?></td>
								<td class="button-column">
									<?php echo CHtml::link('<i class="fa fa-eye"></i>', array('products/view', 'id' => $report->product_id)); ?>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->

		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<link rel="stylesheet" href="../templates/AdminLTE/plugins/datatables/dataTables.bootstrap.css">

<script src="../templates/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../templates/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(function () {

		var table = $('#prod-table').DataTable({
			"paging": true,
			"lengthChange": false,
//			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false
		});

		$('#button').hide();

		$('#prod-table tbody').on( 'click', 'tr', function () {

			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$('#button').hide();
			}
			else {
				table.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$('#button').show();
			}

			if ($(this).find('.parent').text() == '') {
				console.log('ssd')
			}
		});

		$('#button').click( function () {
			var answer = confirm('Are you sure you want to delete this item?');
			if (answer) {
				$.ajax({
					type: 'POST',
					data: {
						id: $('table').find('.selected').attr('id')
					},
					url: '<?php echo Yii::app()->request->baseUrl;?>/admin/reports/delete',
					success: function() {
						table.row('.selected').remove().draw( false );
					}
				})
			}

		});
	});

	var url = window.location.href;

	$('ul.nav a[href="'+ url +'"]').parent().addClass('active');

	$('ul.nav a').filter(function() {
		return this.href == url;
	}).parent().addClass('active');

</script>
