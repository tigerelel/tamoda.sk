<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>
    Manage Users
</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'users-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => false,
    'htmlOptions' => array(),
    'itemsCssClass' => 'items table table-responsive table-hover table-bordered table-striped',
    'pagerCssClass'=>'clearfix',
    'pager' => array(
		'htmlOptions'=>array(
			'class'=>'pagination',
		),
		'header'=>'',
		'firstPageLabel' => '<<',
		'prevPageLabel'  => '<',
		'nextPageLabel'  => '>',
		'lastPageLabel'  => '>>',
		'selectedPageCssClass' => 'active disabled',
    ),
	'ajaxUpdate'=>false,
    'filter'=>$model,
    'columns'=>array(
		'id',
		'username',
		'email',
		'created_date',
		'activated',
		array(
			'name' => 'blocked',
			'value' => 'CHtml::checkBox("my-checkbox", $data->blocked ? "checked" : "", array("value" => "0", "id" => $data->id))',
			'type' =>'raw'
		),
        /*
    'last_login_date',
    'last_login_ip',
    */
		array(
			'class'=>'CButtonColumn',
            'buttons' => array(
                'view' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-eye"></i>',
                    'imageUrl' => false,
                ),
                'update' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-upload"></i>',
                    'imageUrl' => false,
                    'visible' => 'false'
                ),
                'delete' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-remove"></i>',
                    'imageUrl' => false,
                ),
            ),
		),
	),
)); ?>

<style>
    .items.table thead{background:#ecf0f5;}
</style>
<script>
    $(function(){

       $('table.items thead').find('input').addClass('form-control');

    });

</script>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-switch.min.css">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-switch.min.js" type="text/javascript"></script>

<script>
	$(document).ready(function() {
		$("[name='my-checkbox']").bootstrapSwitch({
			onText: "Yes",
			offText: "No",
			onSwitchChange: function() {
				var that = $(this);
				var blocked = $(this).is(':checked')? '1' : '0';
				$.ajax({
					type: 'POST',
					url: '<?php echo Yii::app()->request->baseUrl; ?>/admin/users/update',
					data: {
						id: that.attr('id'),
						blocked: blocked
					},
					success: function(data) {

					}
				})
			}
		});
	});


</script>
