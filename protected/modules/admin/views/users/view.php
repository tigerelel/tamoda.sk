<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>

<h1>View Users #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'first_name',
		'last_name',
		'age',
		array(
			'name' => 'sex',
			'value' => function($data) {
				$model_sex = null;
				if (isset($data->sex)) {

					$model_sex = $data->sex==1 ? 'male' : 'female';
				}
				return $model_sex;
			}
		),
		'email',
		'created_date',
		'modified_date',
		'last_login_date',
		'last_login_ip',
		array(
			'name' => 'activated',
			'value' => $model->activated==0 ? 'No' : 'Yes'
		),
		array(
				'name' => 'blocked',
				'value' => $model->blocked==0 ? 'No' : 'Yes'
		),
	),
)); ?>
