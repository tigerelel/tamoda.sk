<?php
/* @var $this SubscriptionsController */
/* @var $model Subscriptions */

$this->breadcrumbs=array(
	'Subscriptions'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Subscriptions', 'url'=>array('admin')),
);
?>

<h1>Create Subscriptions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>