<?php
/* @var $this SubscriptionsController */
/* @var $model Subscriptions */

$this->breadcrumbs=array(
	'Subscriptions'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Subscriptions', 'url'=>array('index')),
	array('label'=>'Create Subscriptions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#subscriptions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>
    Manage
    Subscriptions
    <a href="/tamoda.sk/admin/Subscriptions/create" class="pull-right">
        <i class="fa fa-plus"></i>
    </a>
</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'subscriptions-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => false,
    'htmlOptions' => array(),
    'itemsCssClass' => 'items table table-responsive table-hover table-bordered table-striped',
    'pagerCssClass'=>'clearfix',
    'pager' => array(
    'htmlOptions'=>array(
    'class'=>'pagination',
    ),
    'header'=>'',
    'firstPageLabel' => '<<',
    'prevPageLabel'  => '<',
    'nextPageLabel'  => '>',
    'lastPageLabel'  => '>>',
    'selectedPageCssClass' => 'active disabled',
    ),
    'filter'=>$model,
    'columns'=>array(
		'id',
		'subscriber_id',
		'newsletter_id',
		array(
			'class'=>'CButtonColumn',
            'buttons' => array(
                'view' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-eye"></i>',
                    'imageUrl' => false,
                ),
                'update' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-upload"></i>',
                    'imageUrl' => false,
                ),
                'delete' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-remove"></i>',
                    'imageUrl' => false,
                ),
            ),
		),
	),
)); ?>

<style>
    .items.table thead{background:#ecf0f5;}
</style>
<script>
    $(function(){
       $('table.items thead').find('input').addClass('form-control');
    });
</script>