<?php
/* @var $this SubscriptionsController */
/* @var $model Subscriptions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="form-group">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>
	</div>

    <div class="form-group">
		<?php echo $form->label($model,'subscriber_id'); ?>
		<?php echo $form->textField($model,'subscriber_id',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>
	</div>

    <div class="form-group">
		<?php echo $form->label($model,'newsletter_id'); ?>
		<?php echo $form->textField($model,'newsletter_id',array('size'=>11,'maxlength'=>11, 'class'=>'form-control')); ?>
	</div>

	<div class="clearfix buttons text-right">
		<?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->