<?php
/* @var $this SubscriptionsController */
/* @var $model Subscriptions */

$this->breadcrumbs=array(
	'Subscriptions'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create Subscriptions', 'url'=>array('create')),
	array('label'=>'Update Subscriptions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Subscriptions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Subscriptions', 'url'=>array('admin')),
);
?>

<h1>View Subscriptions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'subscriber_id',
		'newsletter_id',
	),
)); ?>
