<?php
/* @var $this CategoriesController */
/* @var $model Categories */

?>

<h1>
	Manage Adverts
</h1>
<div class="collapse navbar-collapse">
	<ul class="nav navbar-nav nav-tabs">
		<li><?php echo CHtml::link('Pending Approval', array('products/', 't' => 'pending'), array('class' => 'active')); ?></li>
		<li><?php echo CHtml::link('Approved', array('products/', 't' => 'active')); ?></li>
		<li><?php echo CHtml::link('Rejected', array('products/', 't' => 'rejected')); ?></li>
		<li><?php echo CHtml::link('All adverts', array('products/')); ?></li>
	</ul>
</div>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">

				<div class="box-body">
					<input type="button" id="button" class="btn btn-adn" style="position: absolute;" value="Delete selected row">
					<table id="prod-table" class="table table-bordered table-hover">
						<thead>
						<tr>
							<th>Name</th>
							<th>Price</th>
							<th>Location</th>
							<th>Status</th>
							<th>User</th>
							<th>Created</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($model as $advert) { ?>
							<tr id="<?php echo $advert->id; ?>">
								<td><?php echo $advert->name; ?></td>
								<td>€ <?php echo round($advert->price); ?></td>
								<td><?php echo $advert->location; ?></td>
								<td><?php echo $advert->status; ?></td>
								<td><?php echo $advert->user->username; ?></td>
								<td><?php echo $advert->created_date; ?></td>
								<td class="button-column">
									<?php echo CHtml::link('<i class="fa fa-eye"></i>', array('products/view', 'id' => $advert->id)); ?>

								</td>

							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->

		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<link rel="stylesheet" href="../templates/AdminLTE/plugins/datatables/dataTables.bootstrap.css">

<script src="../templates/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../templates/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
	$(function () {

		var table = $('#prod-table').DataTable({
			"paging": true,
			"lengthChange": false,
//			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false
		});

		$('#button').hide();

		$('#prod-table tbody').on( 'click', 'tr', function () {

			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
				$('#button').hide();
			}
			else {
				table.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				$('#button').show();
			}

			if ($(this).find('.parent').text() == '') {
				console.log('ssd')
			}
		});

		$('#button').click( function () {
			var answer = confirm('Are you sure you want to delete this item?');
			if (answer) {
				$.ajax({
					type: 'POST',
					data: {
						id: $('table').find('.selected').attr('id')
					},
					url: '<?php echo Yii::app()->request->baseUrl;?>/admin/products/delete',
					success: function() {
						table.row('.selected').remove().draw( false );
					}
				})
			}

		});
	});

	var url = window.location.href;

	$('ul.nav a[href="'+ url +'"]').parent().addClass('active');

	$('ul.nav a').filter(function() {
		return this.href == url;
	}).parent().addClass('active');

</script>
