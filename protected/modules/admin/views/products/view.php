
<h1>View Advert #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		array(
			'name' => 'User',
			'value' => $model->user->username
		),
		'location',
		array(
			'name' => 'Category',
			'value' => $model->categories->title
		),
		array(
			'name' => 'Filters',
			'type' => 'html',
			'value' => function($data) {
				$pfilters = '';
				foreach ($data->filters as $filter) {
					if ($filter->other_filter) {
						foreach (json_decode($filter->other_filter) as $key => $v) {

							foreach ($v as $f) {
								if ($key == 'color') {
									$pfilters .= '<span style="background: '.$f.';padding: 0 15px;margin-left: 10px;"></span>';
								} else {
									$pfilters .= '<span style="margin-left: 10px;">'.$f.'</span>';
								}
							}
						}
					}
				}
				return $pfilters;
			}
		),
		array(
			'name' => 'Photos',
			'type' => 'html',
			'value' => function($data) {
				$photos = '';
				foreach ($data->media as $photo) {
					$photos .= '<div class="col-md-10">';
					if (file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $photo['photo'])) {
						$photos .= CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/' . $photo['photo'], '', array('id' => $photo['photo'], 'class' => 'img-rounded', 'width' => 100, 'height' => 90));
					}
					$photos .= '</div>';
				}
				return $photos;
			}
		),
		array(
			'name' => 'type',
			'value' => $model->status==0 ? 'new' : 'used'
		),
		'status',
		array(
			'name' => 'price',
			'value' => '€ '.round($model->price)
		),
		'created_date',
	),
)); ?>
<?php if ($model->status == 'pending') { ?>
	<?php echo CHtml::link('Approve', array('products/approve', 'id' => $model->id), array('class' => 'btn btn-info', 'confirm' => 'are you sure you want to approve?')); ?>
	<?php echo CHtml::link('Reject', array('products/reject', 'id' => $model->id), array('class' => 'btn btn-danger', 'confirm' => 'are you sure you want to reject?')); ?>
<?php } ?>