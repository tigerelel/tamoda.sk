<?php


class Filters extends CWidget{
    public function run(){
        $language_id = 1;
        $filters = FilterGroupLabel::model()->with('filterGroup')->findAll(array('condition' => 'languages_id='.$language_id));

        $this->render('filters', array(
            'filters' => $filters,
        ));
    }
}