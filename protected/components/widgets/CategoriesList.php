<?php
/**
 * Created by PhpStorm.
 * User: gohar
 * Date: 12/9/15
 * Time: 10:46 AM
 */

class CategoriesList extends CWidget{
	public $category = [];
    public $products = [];
    public function run(){
        //$categoriesList = Categories::model()->findAll();
//        $products = Products::model()->with('productTop')->findAll(array('condition' => '', 'order' => 'productTop.id DESC'));
        $this->render('categoriesList', array(
            'category' => $this->category,
            'products' => $this->products
        ));
    }
}