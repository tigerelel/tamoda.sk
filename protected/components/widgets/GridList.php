<?php
/**
 * Created by PhpStorm.
 * User: gohar
 * Date: 12/9/15
 * Time: 10:46 AM
 */

class GridList extends CWidget{
	public $from = 0;
	public $to = 0;
	public $totalCount = 0;
    public function run(){
       
        $this->render('gridlist', array(
            'from' => $this->from,
            'to' => $this->to,
            'totalCount' => $this->totalCount
        ));
    }
}