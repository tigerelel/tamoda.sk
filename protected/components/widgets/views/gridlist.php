<div class="shop-short-wrapper">
    <div class="shop-sort">
        <ul class="grid-list-button">
            <li class="active"><a data-toggle="tab" href="#grid"><i class="fa fa-th"></i></a></li>
            <li><a data-toggle="tab" href="#list"><i class="fa fa-th-list"></i></a></li>
        </ul>
    </div>
    <div style="text-align:center; position:relative" class="loading-result"></div>
    
    <p id="result-text" class="ordered-result">Showing <?=$from ?> &ndash;<?=$to?> of <?=$totalCount?> result</p>
    <div class="orderby-wrapper">
        <label>Sort By</label>
        <select class="orderby" id = "orderby" name="orderby">
            <option value="p.id DESC" class="order">Default sorting</option>
            <option value="p.created_date DESC" class="order">Sort by newness</option>
            <option value="p.price ASC" class="order">Sort by price: low to high</option>
            <option value="p.price DESC" class="order">Sort by price: high to low</option>
        </select>
    </div>
</div>