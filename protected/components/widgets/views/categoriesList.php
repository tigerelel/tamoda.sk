 <div class="shop-sidebar">
     <?php if ($category) { ?>
        <?php $subCategories = Categories::model()->findAllByAttributes(array(), 'parent_id='.$category->id); ?>
        <?php if (isset($category->parentCategory->parentCategory)) {
            $subCategories = Categories::model()->findAllByAttributes(array(), 'parent_id='.$category->parent_id);
        } ?>
        <div class="area-heading">
            <h3>categories</h3>
        </div>
        <div class="catagory-list">
            <ul>
                <?php foreach ($subCategories as $subCategory) {
                    $count = Products::model()->with('categories')->countByAttributes(array(), 'categories_id='.$subCategory->id.' OR categories.parent_id='.$subCategory->id);
                    ?>
                    <li class="<?= $subCategory->id==$category->id?'active':'';?>">
                        <?php echo CHtml::link($subCategory->title, array('site/category', 'subId' => $subCategory->id));?> <?= $count>0 ?'<span class="count">('.$count.')</span>':'';?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    <div class="rance-wrapper">
        <div class="area-heading">
            <h5>By Price</h5>
        </div>
        <div class="price-range">

            <div class="clearfix">
                <input class="amount-left" type="text" id="amount-left" readonly>
                <input class="amount-right" type="text" id="amount-right" readonly>
            </div>
            <div id="slider-range"></div>

            <?php if ($category) { ?>
                <input id="price-filter0" class="price_to" type="hidden" name="price_filter[]" value="<?php echo Products::model()->maxPrice($category->id); ?>">
                <input id="price-filter1" class="price_from" type="hidden" name="price_filter[]" value="<?php echo Products::model()->minPrice($category->id); ?>">
            <?php } else { ?>
                <input id="price-filter0" class="price_to" type="hidden" name="price_filter[]" value="<?php echo Products::model()->maxPrice(NULL, $products); ?>">
                <input id="price-filter1" class="price_from" type="hidden" name="price_filter[]" value="<?php echo Products::model()->minPrice(NULL, $products); ?>">
            <?php } ?>

            <div class="price-box">
                <div class="relative pull-left price-amount">
                    <input class="price-text-input" type="text" name="min_price">
                    <span class="price-text-input-euro">&euro;</span>
                </div>
                <span class="range-seperator">to</span>
                <div class="relative pull-right price-amount">
                    <input class="price-text-input" type="text" name="max_price">
                    <span class="price-text-input-euro">&euro;</span>
                </div>
            </div>
        </div>
    </div>

    <div class="rance-wrapper">
        <div class="area-heading">
            <h5>By Destination</h5>
        </div>
        <div class="price-range dest-box">
            <div class="clearfix">
                <input class="destination" type="text" id="dest" readonly >
            </div>
            <div id="one-slide"></div>
            <input class="ajax-dest" type="hidden" name="destination" value="0">
            <div class="relative pull-left">
                <input class="dest-text-input" type="number" name="dest" min="0" readonly>
            </div>
            <span data-original-title="Enter your location" data-toggle="tooltip" data-placement="top" style="line-height: 20px!important">
                 <button title="Enter your location" style="margin-left: 5px;" type="button" data-toggle="modal" data-target="#loc"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
            </span>
            <div class="error loc-error" style="display:none;font-size:12px;padding-top: 5px;float:left;">Select your location please</div>
        </div>
    </div>

    <div class="rance-wrapper">
        <ul class="advert-color">
            <?php $colors = json_decode(FiltersNew::model()->find('type=1')->filters);
            $categoryId=$category?$category->id:'0';
            if ($colors) {
                foreach ($colors as $key=>$color) { ?>
                    <?php if ($key < 14) { ?>
                        <li>
                            <label for="<?='color-'.$categoryId.'-'.$key; ?>" style="background: <?php echo $color; ?>;">
                                <?php echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $color, 'class' => 'color-box', 'id' => 'color-'.$categoryId.'-'.$key)); ?>
                            </label>
                            <div style="background: <?=$color;?>;">
                                <input type="hidden" name="color" class="color-box" value="<?=$key;?>">
                            </div>
                        </li>
                        <?php $key++;
                    } else { ?>
                        <li class="show-more-color" >
                            <label for="<?='color-'.$categoryId.'-'.$key; ?>" style="background: <?php echo $color; ?>;">
                                <?php echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $color, 'class' => 'color-box', 'id' => 'color-'.$categoryId.'-'.$key)); ?>
                            </label>
                            <div style="background: <?=$color;?>;">
                                <input type="hidden" name="color" class="color-box" value="<?=$key;?>">
                            </div>
                        </li>
                    <?php }
                }
            }?>
        </ul>
        <br/>
        <a class="pull-left pointer show-more-color-btn" style="padding-top: 10px;">Show more</a>
    </div>
<!--    --><?php //$sizes = FilterGroup::model()->with('filterAdditional')->findByAttributes(array('type' => 'size Standard'));
//    $categorySize = CategoriesFilter::model()->findByAttributes(array('categories_id' => $category->id, 'filter_group_id' => $sizes->id));
//    ?>
<!--    --><?php //if ($sizes && $categorySize) { ?>
<!--        <div class="rance-wrapper">-->
<!---->
<!--            <div class="area-heading">-->
<!--                <span>Size</span>-->
<!--            </div>-->
<!--            <ul class="advert-size">-->
<!--                --><?php //foreach($sizes->filterAdditional as $size) { ?>
<!--                    <li>-->
<!--                        <input id="--><?//=$size['title'];?><!--" type="checkbox" name="filtersAdditional[]" class="size-box" value="--><?php //echo $size['id'];?><!--">-->
<!--                        <label for="--><?//=$size['title'];?><!--">--><?php //echo $size['title'];?><!--</label>-->
<!--                    </li>-->
<!--                --><?php //} ?>
<!--            </ul>-->
<!--        </div>-->
<!--    --><?php //} ?>
    <div>
        <input class="geoloc_lat" type="hidden" name="geolocation_lat" value="0">
        <input class="geoloc_lng" type="hidden" name="geolocation_lng" value="0">
        <button value="id DESC" class="filter-btn" type="submit">Filter<i class="fa fa-refresh"></i></button>
    </div>
</div>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjB6-iML5AvdAI3Y6ZUd1UT1jkiD_eXa8&libraries=places&sensor=false"
         async defer></script>

 <script>
     $(document).ready(function() {

         $('.filter-btn').on("click" ,function(e){
             e.preventDefault();
             $(this).find('i').addClass('fa-spin');
             var page = 1;
             var layout = $('#grid').hasClass('active') ? "grid" : "list";
             var dest = $('.ajax-dest').val();
             var current_cat = '<?=$categoryId;?>';
             var price_from = $('.price_from').val();

             if ($('input[name=min_price]').val() != '') {
                 var price_from_num = $('input[name=min_price]').val();
                 if (!isNaN(price_from_num)) {
                     price_from = price_from_num;
                 } else {
                     $('input[name=min_price]').val('');
                     return false;
                 }
             }

             var price_to = $('.price_to').val();

             if ($('input[name=max_price]').val() != '') {
                 var price_to_num = $('input[name=max_price]').val();
                 if (!isNaN(price_to_num)) {
                     price_to = price_to_num;
                 } else {
                     $('input[name=max_price]').val('');
                     return false;
                 }
             }
             var geoloc_lat = $('.geoloc_lat').val();
             var geoloc_lng = $('.geoloc_lng').val();
             var filters = [];
             var order_by = $('.orderby option:selected').val();

             $('input[name="filtersAdditional[]"]:checked').each(function(i) {
                 filters[i]  = this.value;
             });

             var $this = $(this);
             $.ajax({
                 url: '<?php echo Yii::app()->request->baseUrl;?>/site/filterProduct' ,
                 type: "GET",
                 data: {
                     current_cat: current_cat,
                     price_to :price_to,
                     price_from :price_from,
                     dest : dest,
                     filters : filters,
                     geoloc_lat:geoloc_lat,
                     geoloc_lng :geoloc_lng,
                     order: order_by,
                     page: page,
                     is_cat:true

                 },
                 success: function(data)
                 {
                     $(".products-area").empty();
                     $(".products-area").append(data);
                     $this.find('i').removeClass('fa-spin');
                     $(".tab-pane").removeClass('active in');
                     $("#" + layout).addClass('active in');

                 },
                 error: function(xhr,status,error){
                     console.log(status);
                 }
             })
         });

         $('#orderby').on("change" ,function(e){
             e.preventDefault();
             $('.loading-result').append('<i class="fa fa-refresh fa-spin"></i>');
             console.log($('.loading-result i'));
             var layout = $('#grid').hasClass('active') ? "grid" : "list";
             var page = 1;
             var dest = $('.ajax-dest').val();
             var current_cat = '<?=$categoryId?>';
             var price_from = $('.price_from').val();
             var price_to = $('.price_to').val();
             var geoloc_lat = $('.geoloc_lat').val();
             var geoloc_lng = $('.geoloc_lng').val();
             var filters = [];
             var order_by = $(this).val();

             $('input[name="filtersAdditional[]"]:checked').each(function(i) {
                 filters[i]  = this.value;
             });

             var $this = $(this);
             $.ajax({
                 url: '<?php echo Yii::app()->request->baseUrl;?>/site/filterProduct' ,
                 type: "GET",
                 data: {
                     current_cat: current_cat,
                     price_to :price_to,
                     price_from :price_from,
                     dest : dest,
                     filters : filters,
                     geoloc_lat:geoloc_lat,
                     geoloc_lng :geoloc_lng,
                     order: order_by,
                     page: page,
                     is_cat:true,

                 },
                 success: function(data)
                 {
                     $(".products-area").empty();
                     $(".products-area").append(data);
                     $('.loading-result').find('i').remove();
                     $(".tab-pane").removeClass('active in');
                     $("#" + layout).addClass('active in');

                 },
                 error: function(xhr,status,error){
                     console.log(status);
                 }
             })
         });

         $('.show-more-color-btn').on('click', function() {
             var display = $(this).parent().find('ul.advert-color li.show-more-color').css('display');
             if(display == 'none') {
                 $(this).parent().find('ul.advert-color li.show-more-color').css({display: 'block'});
                 $(this).html('Show Less');
             } else {
                 $(this).parent().find('ul.advert-color li.show-more-color').css({display: 'none'});
                 $(this).html('Show More');
             }
         });

         $('.pac-input').on('change', initAutocomplete());

         $('.dest-text-input').on('click', function() {
             if ($('.geoloc_lat').val() == 0 && $('.geoloc_lng').val() == 0 ) {
                 $('.loc-error').show();
             }
         });

         function initAutocomplete() {

             var autocomplete = new google.maps.places.Autocomplete(
                 (document.getElementById('pac-input')),
                 {types: ['(regions)']}
             );
             autocomplete.addListener('place_changed', function() {
                 var place = autocomplete.getPlace();
                 var position = {coords: {
                     latitude: place.geometry.location.lat(),
                     longitude: place.geometry.location.lng()
                 }
                 };
                 showPosition(position);
             });

         }

         function getLocation() {
             if (navigator.geolocation) {
                 navigator.geolocation.getCurrentPosition(showPosition);
             }
         }

         function showPosition(position) {
             $('.geoloc_lat').val(position.coords.latitude);
             $('.geoloc_lng').val(position.coords.longitude);

             $.ajax({
                 url: '<?php echo Yii::app()->request->baseUrl;?>/site/maxDestination',
                 type: "GET",
                 data: {
                     current_cat: <?= $categoryId; ?>,
                     geoloc_lat: position.coords.latitude,
                     geoloc_lng: position.coords.longitude

                 },
                 success: function (data) {

                     $('.ajax-dest').val(parseInt(data));


                     $('.dest-text-input').attr('max', parseFloat(data));
                     $('.dest-text-input').attr('placeholder', 'max - ' + data + ' km')

                     $( "#one-slide").slider({
                         disabled: ($('.geoloc_lat').val() > 0 && $('.geoloc_lng').val() > 0) ? false : true,
                         range: "min",
                         max: parseFloat($('.ajax-dest').val()),
                         change: function( event, ui ) {

                             $('#dest').val(ui.value+" km");
                             $('.ajax-dest').val(ui.value);
                         },
                         slide: function( event, ui ) {

                             $( "#dest").val(ui.value +" km");
                         }

                     });
                     $( "#dest" ).val($( "#one-slide" ).slider( "values", 0 )+" km") ;
                     $('input[name=dest]').removeAttr('readonly');
                     $('.loc-error').hide();
                 }
             });
         }
         getLocation();

         $('ul.advert-color li input[type=checkbox]').on('change', function(){
             if($(this).is(':checked')) {
                 $(this).parent().css({'box-shadow' : '0 0 0 2px #BFBFBF, 0 0 0 3px #525252'});
             } else {
                 $(this).parent().css({'box-shadow' : 'none'});
             }
         });
     });

 </script>