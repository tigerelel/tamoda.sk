
        <div class="col-md-3 col-lg-2">

            <div class="shop-sidebar">
                <!--CATEGORY WIDGET-->
                <div class="shop-catagories">
                    <div class="area-heading">
                        <h3>Filters</h3>
                    </div>
                    <div class="catagory-list">
                        <ul>
                            <?php foreach($filters as $filter){?>
                                <li>
                                    <a href="#" class="categories-list"><?php echo $filter->name;?></a>
                                    <?php if(isset($_GET['filter']) and !empty($_GET['filter'])) {
                                            $style = "display:block";
                                            $checked = "checked";
                                        }else{
                                        $style = "display:none";
                                        $checked = "";
                                    } ?>
                                    <ul class="sub-cat-list" style="<?php echo $style?>">
                                        <?php foreach($filter->filterGroup->filterLabel as $key => $filter_name){
                                            if($filter_name['languages_id']==1){
                                                if($filter_name['filter_group_id']== $filter->filter_group_id){?>
                                                    <li><input type="checkbox" name="filter[]" value="<?php echo $filter_name['filter_id'];?>"><?php echo $filter_name['name'];?></a></li>
                                                <?php }
                                            }
                                            ?>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!--RANE WIDGET-->
                <div class="rance-wrapper">
                    <div class="area-heading">
                        <h3>By price</h3>
                    </div>
                    <div class="price-range">
                        <div id="slider-range"></div>
                        <span>Price</span>
                        <input type="text" id="amount" readonly >
                        <input class="price-filter1" type="hidden" name="price_filter[]" value="">
                        <input class="price-filter2" type="hidden" name="price_filter[]" value="">
                    </div>
                </div>
            </div>
        </div>