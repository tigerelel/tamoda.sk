<?php
$user = Users::model()->findByAttributes(array('username' => CHtml::encode($_GET['username'])));
$revs=UserReviews::model()->countByAttributes(array('to_user' => $user->id));
$userId = $user->id;
if ($revs) { ?>

    <div id="chart_div" style="width: 2000px; height: 150px; position: relative;"></div>

<?php } else { ?>
    No feedback received yet
<?php } ?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/charts-loader.js"></script>

<script>
    <?php if ($revs) { ?>
        $(document).ready(function() {

            google.charts.load('current', {'packages': ['corechart']});

            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Topping');
                data.addColumn('number', 'Slices');
                data.addRows([
                    ['1 star', <?=UserReviews::model()->countByAttributes(array('rating' => 1, 'to_user' => $userId));?>],
                    ['2 stars', <?=UserReviews::model()->countByAttributes(array('rating' => 2, 'to_user' => $userId));?>],
                    ['3 stars', <?=UserReviews::model()->countByAttributes(array('rating' => 3, 'to_user' => $userId));?>],
                    ['4 stars', <?=UserReviews::model()->countByAttributes(array('rating' => 4, 'to_user' => $userId));?>],
                    ['5 stars', <?=UserReviews::model()->countByAttributes(array('rating' => 5, 'to_user' => $userId));?>],
                ]);

                var options = {

                    legend: { position: 'labeled', textStyle: {color: 'blue'} },
                    colors: ['#E0E0E0', '#C0C0C0', '#A8A8A8', '#808080', '#505050'],
                    <?php if ($shop) { ?>
                        chartArea:{left:'0',top:'20',width:"150%",height:"70%"}
                    <?php } else { ?>
                        chartArea:{left:'30%',top:'3%',width:"150%",height:"70%"}
                    <?php } ?>
                };

                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }
        });
    <?php } ?>
</script>