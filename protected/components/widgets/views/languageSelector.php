
<li class="dropdown">
    <a data-toggle="dropdown" class="dropdown-toggle" href="">
        <span><?php echo Yii::app()->language; ?></span> <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu currency">
    <?php

    if(sizeof($languages) < 4) { // если языков меньше четырех - отображаем в строчку
        // Если хотим видить в виде флагов то используем этот код
        /*
        foreach($languages as $key=>$lang) {
            if($key != $currentLang) {
                echo CHtml::link(
                    '<img src="/images/'.$key.'.gif" title="'.$lang.'" style="padding: 1px;" width=16 height=11>',
                    $this->getOwner()->createMultilanguageReturnUrl($key));                };
        }
        */
        // Если хотим в виде текста то этот код

//        $lastElement = end($languages);
        foreach($languages as $key=>$lang) {
            if ($key != $currentLang) { ?>
                <li>
                    <a class="change-language" data-id="<?php echo $key; ?>" href=""><?php echo $lang; ?></a>
                </li>
                <!--        -->

                <?php
            }
        }
    }
    else {
        // Render options as dropDownList
        echo CHtml::form();
        foreach($languages as $key=>$lang) {
            echo CHtml::hiddenField($key, $this->getOwner()->createMultilanguageReturnUrl($key));
        }
        echo CHtml::dropDownList('language', $currentLang, $languages,
            array(
                'submit'=>'',
            )
        );
        echo CHtml::endForm();
    }
    ?>
        </ul>
    </li>

<script>
    $(document).ready( function() {
        $( '.change-language').on( 'click', function() {
            var dataId = $(this).attr("data-id");
            $.ajax
            ({
                url: '<?php echo Yii::app()->request->baseUrl;?>/site/language/' + dataId,
                type: "post",
                data: {
                    id: dataId
                },
                success: function(data)
                {
                    location.reload();
                },
                error: function(data)
                {
                },
                complete: function()
                {
                }
            });
        })
    });
</script>