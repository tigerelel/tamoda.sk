<?php
/**
 * Created by PhpStorm.
 * User: gohar
 * Date: 12/9/15
 * Time: 10:46 AM
 */

class LanguageSelector extends CWidget{
    public function run(){
        $currentLang = Yii::app()->language;
        $languages = CHtml::listData(Languages::model()->findAll(),'id','title');
        $this->render('languageSelector', array(
            'currentLang' => $currentLang,
            'languages'=>$languages
        ));
    }
}