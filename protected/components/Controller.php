<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $_newMessages;

    public function __construct($id,$module=null) {

        parent::__construct($id,$module);

    }

    public $pageOgDesc = '';
    public $pageOgImage = '';
//
//	public function humanTiming ($time)
//	{
//		$time = time() - $time; // to get the time since that moment
//		$time = ($time<1)? 1 : $time;
//		$tokens = array (
//			31536000 => 'year',
//			2592000 => 'month',
//			604800 => 'week',
//			86400 => 'day',
//			3600 => 'hour',
//			60 => 'minute',
//			1 => 'second'
//		);
//
//		foreach ($tokens as $unit => $text) {
//			if ($time < $unit) continue;
//			$numberOfUnits = floor($time / $unit);
//			return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
//		}
//
//	}

}