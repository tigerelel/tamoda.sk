<?php
/**
 * Created by PhpStorm.
 * User: tigranpc
 * Date: 12/8/15
 * Time: 11:49 AM
 */

class FrontHelpers {
    public static function toSQLDateTime($dateTime = false){
        if($dateTime){
            if(!is_int($dateTime)){
                $dateTime = strtotime($dateTime);
            }
        } else {
            $dateTime = time();
        }
        return date("Y-m-d H:i:s", $dateTime);
    }


    public static function hashString($string){
        return hash("sha256", $string);
    }

    public static function checkPassword($userInput, $savedPass){
        $userInput = hash("sha256", $userInput);
        if($userInput == $savedPass){
            return true;
        }
        return false;
    }

    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }

    public static function confirmationEmail($confirmationID, $emailTo, $type, $randomPassword, $username) {
        if ($type=='recover') {
            $regURL = CHtml::link('password', Yii::app()->createAbsoluteUrl('/profile/recoverPassword', array('key' => $confirmationID)), array('target' => '_blank'));
            $subject = 'Recover password from Tamoda';
            $message = 'goto this link to recover your '.$regURL.'.';
            $from = 'Tigran Harutyunyan <tigerelel@gmail.com>';

            $headers = "From: " . $from . "\r\n";
            $headers .= "Reply-To: " . $emailTo . " \r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        } elseif($type=='activation') {
            $regURL = CHtml::link(Yii::app()->createAbsoluteUrl('/profile/register', array('hash' => $confirmationID)), Yii::app()->createAbsoluteUrl('/profile/register', array('hash' => $confirmationID)), array('target' => '_blank'));
            $subject = 'Confirmation email from Tamoda';
            $message = 'Hi '.$username.', to complete account creation please follow the link: '.$regURL.'.<br> Your temporary password :  "'.$randomPassword.'"  in case of having issues with confirmation link.';
            $from = 'Tigran Harutyunyan <tigerelel@gmail.com>';

            $headers = "From: " . $from . "\r\n";
            $headers .= "Reply-To: " . $emailTo . " \r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        }

        $send = mail($emailTo, $subject, $message, $headers);
    }

    public static function passwordResetEmail($password, $emailTo) {
        $subject = 'Reset Password';
        $message = 'Your new password - ' . $password;
        $from = 'Tamoda';

        $headers = "From: " . $from . "\r\n";
        $headers .= "Reply-To: " . $emailTo . " \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $send = mail($emailTo, $subject, $message, $headers);
    }


    public static function subscriptionEmail($subject, $message, $emailTo){

        $from = 'Tamoda';
        $headers = "From: " . $from . "\r\n";
        $headers .= "Reply-To: " . $emailTo . " \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $send = mail($emailTo, $subject, $message, $headers);

    }

    public static function referToFriendEmail($name, $email, $emailTo, $body, $ad) {
        $subject = 'Announcements on tamoda.sk';

        $headers = "From: " . $name .' <'.$email.'>' . "\r\n";
        $headers .= "Reply-To: "  .$emailTo ."\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $message = '<html><body>';
        $message .= '<table border="0" cellpadding="0" cellspacing="0" width="800" style="background-color:#ffffff;"><tbody>';
        $message .= '<tr>';
        $message .= '<td align="left" valign="top">';
        $message .= '<div style="padding:5px 20px 20px 20px;font-size: 14px;line-height: 140%;">';
        $message .= 'Hi, <br><br>';
        $message .= 'Your friend, who is listed as the sender of this message, want you to be informed of the following ad';
        $message .= ' ('.CHtml::link(Yii::app()->createAbsoluteUrl('/products/advert', array('id' => $ad)), Yii::app()->createAbsoluteUrl('/products/advert', array('id' => $ad)), array('target' => '_blank', 'rel' => 'noopener')).')';
        $message .= '<br><br>';
        $message .= 'Sender: <strong> '.$name.' </strong> ('.$email.')<br><br>';
        $message .= 'Text message<br>';
        $message .= '<strong>'.$body.'</strong>';
        $message .= '</div></td></tr>';
        $message .= '</tbody>';
        $message .= "</table>";
        $message .= "</body></html>";

        mail($emailTo, $subject, $message, $headers);

    }

    public static function contactAdminEmail($name, $email, $adminEmail, $message) {
        $subject = 'Contact from user';

        $headers = "From: " . $name .' <'.$email.'>' . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($adminEmail, $subject, $message, $headers);
    }
}