<?php
/**
 * Created by PhpStorm.
 * User: Gevorgyan
 * Date: 5/14/2016
 * Time: 1:35 PM
 */
$userId = Yii::app()->session['user_id'];
//echo CHtml::link('Edit shop', array('shops/edit', 'id' => Yii::app()->session['user_id']));?>



<div class="col-sm-8 col-md-offset-3">
    <h3><?=$shop->name;?></h3>
    <?php if (!empty($shop->logo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/shop_logos/' . $shop->logo)) { ?>

        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/shop_logos/' .  $shop->logo, '', array('id' => 'shop_logo', 'data-name' => $shop->logo, 'width' => 100, 'class' => 'pull-right')); ?>

    <?php } ?>
    <button type="button" class="btn but_2" data-toggle="modal" data-target="#edit_shop">Edit shop</button>
</div>
<?php $this->renderPartial('/shops/_edit_shop', array(
    'shop'=>$shop,
    'userId' => $userId
)); ?>

<div class="row">
    <div class="col-md-3 col-lg-2">
        <div class="shop-sidebar">
            <div class="area-heading">
                <h3>categories</h3>
            </div>
            <div class="catagory-list">
                <?php if(!empty($cats)) { ?>
                    <ul>
                        <li><?=CHtml::link('All Categories', array('shops/view', 'id' => $userId));?></li>
                        <?php foreach ($cats as $key=>$cat) { ?>
                            <li>
                                <?php echo CHtml::link($cat, array('shops/view', 'id' => $userId, 'category' => $key));?>
                                <?php if (!empty($subcats)) { ?>
                                    <div class="list-left" style="padding-top: 8px;">
                                        <?php foreach ($subcats as $id=>$subcat) { ?>
                                            <?php if ($subcat['parentId'] == $key) { ?>
                                                <div style="padding-bottom: 8px;">
                                                    <?php echo CHtml::link($subcat['title'], array('shops/view', 'id' => $userId, 'category' => $id));?>
                                                    <?php if (!empty($subcats)) { ?>
                                                        <div class="list-left">
                                                           <?php foreach ($subsubcats as $subId=>$subsubcat) { ?>
                                                                <?php if ($subsubcat['parentId'] == $id) { ?>
                                                                    <div style="padding-top: 8px;">
<!--                                                                            <a href="--><?//=Yii::app()->request->baseUrl . '/shops/'.$userId.'/'.$subId;?><!--">--><?//=$subsubcat['title'];?><!--</a>-->
                                                                        <?php echo CHtml::link($subsubcat['title'], array('shops/view', 'id' => $userId, 'category' => $subId));?>
                                                                    </div>
                                                                <?php } ?>
                                                           <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
            <div class="rance-wrapper" style="width: 250px;">
                <div class="profile_section col-sm-12">
                    Member since: <span><?=date("d/m/Y",strtotime($shop->user->created_date));?></span>
                    Location: <span>NA</span>
                </div>
                <h3 class="col-sm-12 profile_section_title">Activity</h3>
                <div class="profile_section col-sm-12">
                    <dl>
                        <dd><?= CHtml::link(Products::model()->countAds("active"), array('profile/myAds'), array('class' => 'active')); ?></dd>
                        <dt>Active ads:</dt>
                        <dd><?= CHtml::link(Products::model()->countAds("sold"), array('profile/myAds', 't' => 'sold')); ?></dd>
                        <dt>Sold ads:</dt>
                        <dd><?= CHtml::link(Products::model()->countAds("withdrawn"), array('profile/myAds', 't' => 'withdrawn')); ?></span></dd>
                        <dt>Withdrawn ads:</dt>
                    </dl>
                    <div class="clear"></div>
                    <div>
                        <?= CHtml::link('View all adverts', array('products/search', 'q' => $shop->user->username)); ?>
                    </div>
                </div>
                <h3 class="col-sm-12 profile_section_title">Feedback Score</h3>
                <div class="profile_section">
                    <div class="col-sm-12 col-md-12" style="margin-left: 57px;">
                        <?php $this->widget('application.components.widgets.FeedbackScore', array('shop' => true)); ?>
                    </div>
                </div>
            </div>
        </div>
<!--        --><?php
//        $this->widget('application.components.widgets.CategoriesList', [
//            'category' => $category
//        ]);
//        ?>
    </div>
    <div class="col-md-9 col-lg-10">
        <?php $from = ($pages->pageSize*($page-1)+1); $to = ($pages->pageSize*($page-1) + $countPage);
//        $this->widget('application.components.widgets.GridList', [
//            'from' => $from,
//            'to' => $to,
//            'totalCount' => $totalCount,
//        ]);
        ?>
        <div class="shop-area products-area">

            <?php $this->renderPartial('//site/_shop_area', array(
                'pages' => $pages,
                'totalCount' => $totalCount,
                'products' => $products,
                'page' => $page,
                'countPage' => $countPage,
                'shop' => true,
            ))
            ?>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dropzone.css" media="all" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/dropzone.js"></script>

<script>

    Dropzone.options.myAwesomeDropzone = {
        url: '<?php echo $this->createAbsoluteUrl('shops/uploadShopLogo', array('id' => $userId)); ?>',
        previewsContainer: ".dropzone-previews",
        maxFiles: 1,
        maxFilesize: 10,
        addRemoveLinks: true,
        acceptedFiles: ".jpg,.png,.gif",
        removedfile: function (file) {
            $.ajax({
                type: 'GET',
                url: '<?php echo $this->createAbsoluteUrl('profile/deleteProfilePic', array('id' => $userId)); ?>',
                data: {},
                success: function () {
//                    $('#pic').attr('src', '<?//=Yii::app()->request->baseUrl . '/images/shop_logos/avatar.png';?>//');
                }
            });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function (file, response) {
            var src = '<?=Yii::app()->request->baseUrl . '/images/shop_logos/';?>' + $.parseJSON(response)
            $('#shop_logo').attr('src', src);
        },
        init: function () {
            if ($('#shop_logo').attr('data-name') != undefined) {
                var $this = this;
                var mockFile = {name: $('#shop_logo').attr('data-name'), type: 'image/*'};
                $this.options.addedfile.call($this, mockFile);
                $this.options.thumbnail.call($this, mockFile, "<?=Yii::app()->request->baseUrl;?>/images/shop_logos/" + mockFile.name);
                mockFile.previewElement.classList.add('dz-success');
                mockFile.previewElement.classList.add('dz-complete');
            }
        }
    }
</script>
