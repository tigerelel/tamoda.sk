<div id="edit_shop" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="text-center">EDIT SHOP</h4>
            </div>
            <div class="modal-body">
                <div class="contact-admin-form">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                        'action' => $this->createUrl('shops/edit', array('id' => $userId))
                    )); ?>

                    <div class="form-group">
                        <?=$form->labelEx($shop,'name'); ?>
                        <?=$form->textField($shop,'name', array('class' => 'form-control')); ?>
                        <?=$form->error($shop,'name'); ?>
                    </div>
                    <div class="form-group">
                        <?=$form->labelEx($shop,'logo'); ?>
                        <div class="dropzone dropzone-previews" id="my-awesome-dropzone">
                            <div class="dz-message" data-dz-message><span>Drop or <a>click here</a> to upload a photo</span></div>
                            <div class="fallback">
                                <input name="logo" type="file" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo CHtml::submitButton('Edit', array('class' => 'btn btn-success form-control')); ?>
                    </div>

                    <?php $this->endWidget(); ?>
                    <br>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>