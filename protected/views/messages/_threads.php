<table class="table table-responsive">
    <thead>
    <tr>
        <td></td>
        <td></td>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($threads as $thread) {

            $countNewMsg = Messages::model()->count('user_id!=' . Yii::app()->session['user_id'] . ' AND t.read=0 AND t.thread_id=' . $thread->id);
            $from = $thread->seller_id != Yii::app()->session['user_id'] ? $thread->seller : $thread->buyer;
        ?>

        <tr class="<?= $thread->id == $messages['id'] ? 'msg-tr' : ''; ?>"
            data-name="<?= $thread->product->name; ?>" data-id="<?= $thread->product->id; ?>">

            <td id="thread<?= $thread->id; ?>" data-thread="<?= $thread->id; ?>"
                data-buyer="<?= $thread->buyer->id; ?>" data-seller="<?= $thread->seller->id; ?>">
                <?php if (!empty($from->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $from->usersAdditional->avatar)) { ?>
                    <?php CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' . $from->usersAdditional->avatar, '', array('class' => 'msg-img cart-img')); ?>
                <?php } else {
                    echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', '', array('class' => 'client-img msg-img'));
                } ?>
                <a href="<?= Yii::app()->request->baseUrl . '/profile?username=' . $from->username; ?>"
                   class="<?= $countNewMsg > 0 ? 'new-msg' : ''; ?>"><?= $from->username; ?><?= $countNewMsg ? ' <span class="msg-count">('.$countNewMsg.')</span>' : ''; ?>
            </td>
            <td>
                <input type="checkbox" data-id="<?= $thread->id; ?>"
                       class="message-checkbox <?= $countNewMsg ? ' unread' : ' read'; ?>">
            </td>

        </tr>
        <?php } ?>
    </tbody>
</table>
