<?php if ($check) { ?>
    <div class="messages col-md-12 col-sm-12">

        <?php if ($newMessage->user->id != Yii::app()->session['user_id']) { ?>

            <div class="col-md-2 col-sm-2 img-box">
                <?php if ($newMessage->user->id != Yii::app()->session['user_id']) { ?>
                    <a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $newMessage->user->username; ?>">
                        <?php if (!empty($newMessage->user->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $newMessage->user->usersAdditional->avatar)) { ?>
                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $newMessage->user->usersAdditional->avatar, ''); ?>
                        <?php } else { ?>
                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                        <?php } ?>
                    </a>
                <?php } ?>
            </div>

            <div class="msg-cont pull-left col-md-8 col-sm-5 <?=($newMessage->read==0 && $newMessage->user_id != Yii::app()->session['user_id'])?' new-msg-cr':'';?>" id="msg<?=$newMessage->id;?>">
                <p class="msg-cont msg-cont-date text-center">
                    <span class=""><?php echo date('M d, H:i', strtotime($newMessage->created)); ?></span>
                </p>
                <?php if ($newMessage->user->id != Yii::app()->session['user_id']) { ?>
                    <a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $newMessage->user->username; ?>"><?= $newMessage->user->username; ?></a>
                <?php } ?>

                <p class="msg-text msg-text-not-me"><?php echo $newMessage->message; ?></p>

            </div>

        <?php } else { ?>

            <div class="test pull-right col-md-8 col-sm-5">
                <p class="msg-cont msg-cont-date text-center">
                    <span class=""><?php echo date('M d, H:i', strtotime($newMessage->created)); ?></span>
                </p>
                <p class="test msg-text msg-text-me"><?php echo $newMessage->message; ?></p>
            </div>
        <?php } ?>
    </div>

    <input type="hidden" class="newthread" value="<?=$newMessage->thread_id;?>">
    <script>

        $('#last_msg').val('<?=$newMessage ? $newMessage->id : '';?>');

        $("#msg-area").mCustomScrollbar({
            mouseWheel: {
                enable: true
            },
            autoDraggerLength: true,
            //setTop: '-500px',
            scrollButtons:{ enable: true }
        });
        $('#msg-area').mCustomScrollbar('scrollTo', 'bottom');

    </script>

<?php } ?>
