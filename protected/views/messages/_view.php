
<?php if (!empty ($messages)) { ?>

    <?php foreach ($messages->messages as $message) { ?>

        <div class="messages col-md-12 col-sm-12">

            <?php if ($message->user->id != Yii::app()->session['user_id']) { ?>

                <div class="col-md-2 col-sm-2 img-box">
                    <?php if ($message->user->id != Yii::app()->session['user_id']) { ?>
                        <a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $message->user->username; ?>">
                            <?php if (!empty($message->user->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $message->user->usersAdditional->avatar)) { ?>
                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $message->user->usersAdditional->avatar, ''); ?>
                            <?php } else { ?>
                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                            <?php } ?>
                        </a>
                    <?php } ?>
                </div>

                <div class="msg-cont pull-left col-md-8 col-sm-5" id="msg<?=$message->id;?>">
                    <p class="msg-cont msg-cont-date text-center">
                        <span class=""><?php echo date('M d, H:i', strtotime($message->created)); ?></span>
                    </p>
                    <?php if ($message->user->id != Yii::app()->session['user_id']) { ?>
                        <a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $message->user->username; ?>"><?= $message->user->username; ?></a>
                    <?php } ?>

                    <p class="msg-text msg-text-not-me"><?php echo $message->message; ?></p>

                </div>

            <?php }  else { ?>

                <div class="test pull-right col-md-8 col-sm-5">
                    <p class="msg-cont msg-cont-date text-center">
                        <span class=""><?php echo date('M d, H:i', strtotime($message->created)); ?></span>
                    </p>
                    <p class="test msg-text msg-text-me"><?php echo $message->message; ?></p>
                </div>
            <?php } ?>
        </div>
    <?php } ?>

<?php } ?>

<script>
    $(window).load(function() {
        $("#msg-area").mCustomScrollbar('scrollTo', 'bottom');
    });
</script>
