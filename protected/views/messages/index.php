<!--BLOG AREA START-->
<section class="blog-area-wrapper">
	<div class="container-fluid">
		<div class="row">
			<h1 class="sin-page-title">Messages</h1>
				<div class="col-md-12">
					<?php if(!empty($threads)) { ?>
						<div class="col-md-offset-3 col-md-8">
							<div style="margin: 0 50px 50px 0;">
								<button data-url='<?=$this->createUrl("messages/markAllRead")?>' class="btn btn-primary mark mark-read pull-right">Mark as read</button>
							</div>
							<div class="error" style="display: none;">Something went wrong</div>
						</div>
						<div id="content2" class="col-md-offset-2 col-md-3">

							<?php $this->renderPartial('_threads', array('threads' => $threads, 'messages' => $messages)); ?>

						</div>
						<span class="col-md-6 col-sm-8">
							<label>Advert: </label> <span id="adv-label"><?php echo CHtml::link($messages->product->name, array('products/advert', 'id' => $messages->product->id)); ?></span>
						</span>
						<div class="col-md-6 col-sm-8"  id="msg-area" style="position:relative; overflow:visible; max-height:500px; ">
							<div id="content1" class="main-menu col-md-14 col-sm-14">

								<?php $this->renderPartial('_view', array('messages' => $messages)); ?>

							</div>
						</div>
						<div class="col-md-6 col-sm-8 col-md-offset-5">
							<div style="margin: 0 auto;position: relative;padding-top: 15px; margin-left: 40px;">
								<?php $form=$this->beginWidget('CActiveForm', array(
									'id'=>'message-form',
									'enableAjaxValidation' => false,
									'enableClientValidation'=>true,
									'clientOptions'=>array(
										'validateOnSubmit'=>true,
									)
								)); ?>

								<input type="hidden" name="thread_id" id="thread_id" value="<?=!empty($conv) ? $conv->id : $messages->id;?>">

								<div class="form-group">
									<?php echo CHtml::activeTextArea($newMessage,'message', array('placeholder' => 'Your message here...', 'class' => 'form-control', 'rows' => 4, 'style'=>'width:100%;')); ?>
								</div>

								<?php echo CHtml::submitButton('Send PM', array('class' => 'btn btn-info pull-right')); ?>

								<?php $this->endWidget(); ?>
							</div>
						</div>
						<input type="hidden" id="last_msg" value="<?=$lastMessage->id;?>">
					<?php } else { ?>
						No messages
					<?php } ?>

			</div>
		</div>
	</div>
</section>
<!--BLOG AREA END-->
<?php if(!empty($threads)) { ?>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css" type="text/css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">

	$("#msg-area").mCustomScrollbar({
		mouseWheel: {
			enable: true
		},
		autoDraggerLength: true,
		scrollButtons:{ enable: true }
	});

	$(document).on('click','table td', function() {

			var $this = $(this);
			$this.parents().find('.msg-tr').removeClass('msg-tr');
			$this.parent().toggleClass('msg-tr');
			$('#adv-label').html('<a href="<?=Yii::app()->request->baseUrl;?>/products/advert/'+$this.parent().data('id')+'">'+$this.parent().data('name')+'</a>');

			if ($this.data('thread') != $('#thread_id').val()) {
				$.ajax({
					url: '<?=Yii::app()->request->baseUrl;?>/messages/view',
					dataType: "html",
					cache: false,
					type: 'POST',
					data: {
						thread_id: $this.data('thread')
					},
					success: function(data)
					{
						$('#content1').html(data);
						$('#thread_id').val($this.data('thread'));
						$('#buyer_id').val($this.data('buyer'));
						$('#seller_id').val($this.data('seller'));

						if ($('.msg-notification').text().length > 0) {
							var count = parseInt($('.msg-notification').text())-1;

							$('.msg-notification').text(count);
							if (count == 0) {
								$('.msg-notification').empty();
							}
						}
						$this.find('.new-msg').removeClass('new-msg');
						$this.find('.msg-count').remove();
						$('#msg-area').mCustomScrollbar('scrollTo', 'bottom');
					}
				});
			}
		});


	$(window).load(function() {
		$("#msg-area").mCustomScrollbar('scrollTo', 'bottom');
	});

	function send() {

		if ($('#message-form').valid()) {
			$.ajax({
				url: '<?=$this->createUrl('messages/sendRoomEvent');?>',
				type: 'POST',
				data: $('#message-form').serialize(),
				success: function(data)
				{
					var obj = $.parseJSON(data);
					$.ajax({
						url: '<?=Yii::app()->request->baseUrl;?>/messages/loadNewMessages/'+$('#thread_id').val(),
						dataType: "html",
						cache: false,
						type: 'POST',
						data: {
							last_msg: obj.last_msg
						},
						success: function(data)
						{
							$('#content1').append(data);
						}
					});
					$('textarea').val('');
				}
			})
		}
	}

	$('#message-form').on('keypress', function(e) {

		e = e || window.event;
		if (e.keyCode == 13) {
			send();
			$('#Messages_message').blur();
			e.preventDefault();
		}

	});

	$('#message-form').on('submit', function(e) {
		send();
		e.preventDefault();
	});

	(function(){
		setInterval(function() {

			$.ajax({
				url: '<?=Yii::app()->request->baseUrl;?>/messages/loadNewMessages/'+$('#thread_id').val(),
				dataType: "html",
				cache: false,
				type: 'post',
				data: {
					last_msg: $('#last_msg').val()
				},
				success: function(data)
				{
					$('#content1').append(data);

				}
			});
		}, 1000);
	})();

	(function() {
		setInterval(function () {
			$.ajax({
				url: '<?=Yii::app()->request->baseUrl;?>/messages/loadThreads/',
				type: 'post',
				data: {
					thread_id: $('#thread_id').val()
				},
				success: function (data) {
					$('#content2').html(data);
				}
			})

		}, 5000);
	})();


	$('.message-checkbox').on('click', function() {
        $(this).toggleClass("active-check");
    });

    $('.mark').on("click", function(e) {

		e.preventDefault();

		var $this = $(this);
		if ($('.message-checkbox:checked').length > 0) {

			var threads = [];
			var read = $(this).hasClass("mark-read") ? 1 : 0;
			$('.message-checkbox').each(function(i, el) {
				if ($(el).hasClass("active-check"))
					threads[i] = $(el).data("id");
			});

			$.ajax({
				url: $(this).data("url"),
				type: "POST",
				data: {
					checked: threads,
				},
				success: function(data)
				{
					window.location.reload();
				},
				error: function(xhr,status,error){
					console.log(status);
				}
			})
		}
    });

	$('textarea').on('click', function() {
		$.ajax({
			url: '<?=Yii::app()->request->baseUrl;?>/messages/MarkAllRead',
			type: "POST",
			data: {
				thread_id: $('#thread_id').val(),
			},
			success: function(data)
			{
				$(document).find('.new-msg-cr').removeClass('new-msg-cr')
			}
		});
	});

</script>

<?php } ?>