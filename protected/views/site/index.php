<!--SLIDER AREA START-->
<section class="slider-area-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="row">
                    <!--SLIDER AREA START-->
                    <div class="slider">
                        <div id="mainSlider" class="nivoSlider">
                            <?php
                            foreach($images as $image) { ?>
                                <img src="<?php echo Yii::app()->request->baseUrl?>/images/sliders/<?php echo $image['image'];?>"/>
                            <?php }?>
                        </div>
                    </div>
                    <!--SLIDER AREA END-->
                </div>
            </div>
        </div>
    </div>
</section>

<!--SLIDER AREA END-->

<?php $categoriesList = Categories::model()->findAll();
$slide_count = 0;
foreach ($categoriesList as $key => $category) { ?>
    <?php if ($category->parent_id == 0) { ?>
        <section class="category-area-one control-car mar-bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="cat-area-heading col-xs-12">
                        <h4 class="col-md-3 col-lg-2"><a
                                href="<?php echo Yii::app()->baseUrl . '/site/category/' . $category->id; ?>"><?php echo $category['title']; ?></a>
                        </h4>
                        <div class="col-md-9 col-lg-10">
                            <p style="border-top: 1px solid #e5e5e5;position: relative;right: 13px;width: 101%;"></p>
                            <?php foreach ($categoriesList as $key_sub => $category_sub) {
                                if ($category_sub->parent_id == $category->id) { ?>
                                    <span class="cat-menu-li"><a class="cat-bar" cat_id="<?php echo $category_sub['id'] ?>" href=""><?php echo $category_sub['title'] ?></a></span>  |
                                <?php }
                            } ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-2">
                        <div class="hidden-xs hidden-md hidden-sm col-lg-12">
                            <div class="single-banner">
                                <div class="overlay"></div>
                                <p>
                                    <?php if (isset($category->image)) { ?>
                                        <a href="<?php echo Yii::app()->baseUrl . '/site/category/' . $category->id; ?>">
                                            <img src="<?php echo Yii::app()->baseUrl . '/images/cat_images/' . $category->image ?>">
                                        </a>
                                    <?php } ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9 col-lg-10 no-padding">
                        <div class="col-md-12 col-lg-12 no-padding get-box">
                            <div class="cat-carousel-area">
                                <!--CATEGORY CAROUSEL START-->
                                <div class="tab-carosel-start">
                                    <!--SINGLE CAROUSEL-->
                                    <?php
                                    if (!empty ($products)) {
                                        foreach ($products as $product) {
                                            if ($product->categories->parentCategory->parent_id == $category->id || (!$product->categories->parentCategory->parent_id && $product->categories->parent_id == $category->id)) {
                                                ?>
                                                <div class="col-md-12 ajax-product">
                                                    <?php if (isset($product->productTop)) { ?>
                                                        <span class="top-ad">TOP AD</span>
                                                    <?php } ?>
                                                    <div class="single-product ajax-box" title="">
                                                        <div class="product-image">
                                                            <div class="show-img">
                                                                <?php if (!empty($product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $product->mainPhoto->photo)) { ?>
                                                                    <a href="<?php echo Yii::app()->baseUrl . '/products/advert/' . $product['id']; ?>"><img src="<?php echo Yii::app()->request->baseUrl . '/images/product_images/' . $product->mainPhoto->photo; ?>"></a>
                                                                <?php } else { ?>
                                                                    <a href="<?php echo Yii::app()->baseUrl . '/products/advert/' . $product['id']; ?>"><img src="<?php echo Yii::app()->request->baseUrl . '/images/product_images/none.jpg'; ?>"></a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="prod-info">
                                                            <h2 class="pro-name">
                                                                <a href="<?php echo Yii::app()->baseUrl . '/products/advert/' . $product->id; ?>"><?php echo $product->name; ?></a>
                                                            </h2>
                                                            <div class="rating"></div>
                                                            <div class="price-box">
                                                                <div class="price">
                                                                    <?php if ($product->giveaway != 1) { ?>
                                                                        <span>&#8364;<?php echo $product->price; ?></span>
                                                                    <?php } else { ?>
                                                                        Free
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div>Entered: <?= date("M d, Y H:i", strtotime($product->created_date)); ?></div>
                                                        </div>
                                                        <a href="<?php echo Yii::app()->baseUrl . '/products/advert/' . $product->id; ?>">
                                                            <div class="info-pop">
                                                                <h4><?php echo $product->name; ?></h4>
                                                                <p><strong>Description:</strong><?php echo substr($product->description, 0, 70) . '...'; ?></p>
                                                                <p><strong>Location:</strong><?php echo $product->location; ?></p>
                                                                <p><strong>Seller:</strong><?php echo $product->user->username; ?></p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                        <?php }
                                        }
                                    } ?>
                                </div>
                                <!--CATEGORY CAROUSEL END-->
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12 no-padding ajax-box" style="display: none">
                            <div class="cat-carousel-area">
                                <div class="tab-carosel-start">
                                    <div class="col-md-12 ajax-product">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12 clearfix">
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'method' => 'GET',
                                'htmlOptions' => array('class' => 'filter-form',),
                                'clientOptions' => array(
                                    'validateOnSubmit' => true,
                                ),
                            )); ?>

                            <div class="one-fifth clearfix"><!--rance-wrapper-->
                                <input type="hidden" class="hidden-category" value="<?php echo $category->id; ?>" name="current_cat">
                                <div class="area-heading">
                                    <h3>By price</h3>
                                </div>
                                <div class="price-range">
                                    <div class="clearfix">
                                        <input class="amount-left" type="text" id="<?php echo 'amount-left-' . $slide_count; ?>" readonly>
                                        <input class="amount-right" type="text" id="<?php echo 'amount-right-' . $slide_count; ?>" readonly>
                                    </div>
                                    <div id="<?php echo 'slider-range' . $slide_count; ?>">
                                        <input id="<?php echo 'price-filter' . $slide_count; ?>" class="price_from" type="hidden" name="price_filter[]" value="<?php echo Products::model()->minPrice($category->id); ?>">
                                        <input id="<?php echo 'price-filter' . intval($slide_count + 1); ?>" class="price_to" type="hidden" name="price_filter[]" value="<?php echo Products::model()->maxPrice($category->id); ?>">
                                    </div>
                                    <div id="min_price-error" class="error" style="display: none;"></div>
                                    <div id="max_price-error" class="error" style="display: none;"></div>
                                </div>
                                <div class="price-box">
                                    <div class="relative pull-left">
                                        <input class="price-text-input" type="text" name="min_price">
                                        <span class="price-text-input-euro">&euro;</span>
                                    </div>
                                    <span class="range-seperator">to</span>
                                    <div class="relative pull-right">
                                        <input class="price-text-input" type="text" name="max_price">
                                        <span class="price-text-input-euro">&euro;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="one-fifth clearfix"><!--rance-wrapper-->
                                <div class="area-heading">
                                    <h3>By destination</h3>
                                </div>
                                <div class="price-range2">
                                    <div class="clearfix">
                                        <input class="destination" type="text" id="<?php echo 'dest' . $slide_count; ?>" readonly>
                                    </div>
                                    <div id="<?php echo 'one-slide' . $slide_count; ?>"></div>
                                    <input class="ajax-dest <?php echo 'destination' . $slide_count; ?>" type="hidden" name="<?php echo 'destination' . $slide_count; ?>" value="0">
                                    <div id="dest-error" class="error" style="display: none;"></div>
                                </div>
                                <div class="price-box dest-box">
                                    <div class="relative pull-left">
                                        <input class="dest-text-input" type="number" name="dest" min="0" readonly>
                                    </div>
                                    <span class="loc-text" data-original-title="Enter your location" data-toggle="tooltip" data-placement="top">
                                        <button style="margin-left: 5px;" type="button" data-toggle="modal" data-target="#loc"><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                                    </span>
                                    <div class="error loc-error" style="display:none;font-size:12px;padding-top: 5px;position:absolute;">Select your location please</div>
                                </div>

                            </div>
                            <div class="one-fifth clearfix">
                                <div class="area-heading">
                                    <h3>By Color</h3>
                                </div>
                                <ul class="advert-color">
                                    <?php $colors = json_decode(FiltersNew::model()->find('type=1')->filters);
                                    if ($colors) {
                                        foreach ($colors as $key => $color) { ?>
                                            <?php if ($key < 16) { ?>
                                                <li>
                                                    <label for="<?= 'color-' . $category->id . '-' . $key; ?>" style="background: <?php echo $color; ?>;">
                                                        <?php echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $color, 'class' => 'color-box', 'id' => 'color-' . $category->id . '-' . $key)); ?>
                                                    </label>
                                                    <div style="background: <?= $color; ?>;">
                                                        <input type="hidden" name="color" class="color-box" value="<?= $key; ?>">
                                                    </div>
                                                </li>
                                                <?php $key++;
                                            } else { ?>
                                                <li class="show-more-color">
                                                    <label for="<?= 'color-' . $category->id . '-' . $key; ?>" style="background: <?php echo $color; ?>;">
                                                        <?php echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $color, 'class' => 'color-box', 'id' => 'color-' . $category->id . '-' . $key)); ?>
                                                    </label>
                                                    <div style="background: <?= $color; ?>;">
                                                        <input type="hidden" name="color" class="color-box" value="<?= $key; ?>">
                                                    </div>
                                                </li>
                                            <?php }
                                        }
                                    } ?>
                                </ul>
                                <ul>
                                    <?php
                                    foreach ($others as $k => $other) {
                                        $prod = Products::model()->findByPk($k);
                                        if (($prod->categories->parentCategory->parentCategory && $prod->categories->parentCategory->parentCategory->id == $category->id)
                                        OR ($prod->categories->parentCategory->id == $category->id)) { ?>
                                            <ul class="advert-color">
                                                <?php foreach ($other as $k => $oth) {
                                                    if ($oth) { ?>
                                                        <li class="show-more-color">
                                                            <label for="<?= 'color-' . $prod['id'] . '-' . $k; ?>" style="background: <?php echo $oth; ?>;">
                                                                <?php echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $oth, 'class' => 'color-box', 'id' => 'color-' . $prod['id'] . '-' . $k)); ?>
                                                            </label>
                                                            <div style="background: <?= $oth; ?>;">
                                                                <input type="hidden" name="color" class="color-box" value="<?= $k; ?>">
                                                            </div>
                                                        </li>
                                                    <?php }
                                                } ?>
                                            </ul>
                                        <?php }
                                        }
                                    ?>
                                </ul>
                                <a class="pull-left pointer show-more-color-btn" style="padding-top: 10px;">Show more</a>
                            </div>

                            <!---->
                            <!--                            --><?php //$categorySize = CategoriesFilter::model()->findByAttributes(array('categories_id' => $category->id));
                            //
                            //                            if ($categorySize) { ?>
                            <!--                                <div class="one-fifth clearfix">-->
                            <!--                                    <div class="area-heading">-->
                            <!--                                        <h3>By Size</h3>-->
                            <!--                                    </div>-->
                            <!--                                    <ul class="advert-size">-->
                            <!--                                        --><?php //foreach($sizes->filterAdditional as $size) { ?>
                            <!--                                            <li>-->
                            <!--                                                <input id="-->
                            <?//=$size['title'].$slide_count;?><!--" type="checkbox" name="filtersAdditional[]" class="size-box" value="-->
                            <?php //echo $size['id'];?><!--">-->
                            <!--                                                <label for="-->
                            <?//=$size['title'].$slide_count;?><!--">--><?php //echo $size['title'];?><!--</label>-->
                            <!--                                            </li>-->
                            <!--                                        --><?php //} ?>
                            <!--                                    </ul>-->
                            <!--                                </div>-->
                            <!--                            --><?php //} ?>
                            <div class="one-fifth">
                                <div class="form-group">&nbsp;</div>
                                <input class="geoloc_lat" type="hidden" name="geolocation_lat" value="0">
                                <input class="geoloc_lng" type="hidden" name="geolocation_lng" value="0">
                                <button data-order="id DESC" type="submit" class="filter-btn filter-c pull-left">Filter
                                    <i class="fa fa-refresh"></i>
                                </button>
                                <h5><span class="pull-right"><a href="<?php echo Yii::app()->baseUrl . '/site/category/' . $category->id; ?>">SHOW MORE</a></span></h5>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php $slide_count++;
    }
} ?>
    <!--CATEGORY ONE AREA END-->

<div class="container-fluid col-md-offset-2">
    <div class="recent_header">
        <h3>recent adverts</h3>
    </div>
    <div class="shop-area products-area">
        <div class="shop-product shop-page">
            <div class="tab-content">
                <div id="grid" class="tab-pane fade in active">
                    <div class="product-grid">
                        <div class="row" id="recent_ads">
                            <?php $this->renderPartial('_recent_ads', array(
                                'recentAds' => $recentAds,
                                'end' => false
                            )); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="" class="pull-right" id="more_ads">View More Results </a>

</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjB6-iML5AvdAI3Y6ZUd1UT1jkiD_eXa8&libraries=places&sensor=false"
        async defer></script>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-colorselector.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-colorselector.js"></script>

<script>
    var page = 1;
    $('#more_ads').on('click', function(e) {
        e.preventDefault();
        page++;
        $.ajax({
            url: '<?php echo Yii::app()->request->baseUrl;?>/site/showMoreRecentAds?page=' + page,
            type: "GET",
            data: {},
            success: function (data) {
                $('#recent_ads').append(data);
                if ($('#end'+page).val() == 1) {
                    $('#more_ads').remove();
                }
            }
        });
    })

    $(document).ready(function() {
        getLocation();

        $('.show-more-color-btn').on('click', function() {
            var display = $(this).parent().find('ul.advert-color li.show-more-color').css('display');
            if (display == 'none') {
                $(this).parent().find('ul.advert-color li.show-more-color').css({display: 'block'});
                $(this).html('Show Less');
            } else {
                $(this).parent().find('ul.advert-color li.show-more-color').css({display: 'none'});
                $(this).html('Show More');
            }
        });

        $('ul.advert-color li input[type=checkbox]').on('change', function(){
            if($(this).is(':checked')) {
                $(this).parent().css({'box-shadow' : '0 0 0 2px #BFBFBF, 0 0 0 3px #525252'});
            } else {
                $(this).parent().css({'box-shadow' : 'none'});
            }
        });

        $('.colorselector').colorselector();

        $('.color-btn').each(function(i, v) {
           $(v).removeClass('selected');
        });
        $('.btn-colorselector').css({'background-color': ''});

        $('.pac-input').on('change', initAutocomplete());

        function initAutocomplete() {

            var autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('pac-input')),
                {types: ['(regions)']}
            );
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                var position = {coords: {
                        latitude: place.geometry.location.lat(),
                        longitude: place.geometry.location.lng()
                    }
                };
                showPosition(position);
            });

        }

        $('.dest-text-input').on('click', function() {
            if ($('.geoloc_lat').val() == 0 && $('.geoloc_lng').val() == 0 ) {
                $('.loc-error').show();
            }
        });

        $('.filter-c').click(function(e) {

            e.preventDefault();

            var $this = $(this);

            var dest = $(this).closest('form').find('.ajax-dest').val();
            if ($(this).closest('form').find('input[name=dest]').val() > 0) {

                var destA = $(this).closest('form').find('input[name=dest]').val();

//                if (!isNaN(destA)) {
//                    dest = destA;
//                } else {
//                    var destEl = $(this).parent().parent().find('input[name=dest]');
//                    destEl.val('');
//                    destEl.parent().parent().parent().find('#dest-error').text('Destination must be a number').show().animate({opacity: 2.0}, 2000).fadeOut("slow");
//                    return false;
//                }
            }

            var current_cat = $(this).parent().parent().find('.hidden-category').val();

            var price_from = $(this).parent().parent().find('.price_from').val();

            if ($(this).parent().parent().find('input[name=min_price]').val() != '') {

                var price_from_num = $(this).parent().parent().find('input[name=min_price]').val();

                if (!isNaN(price_from_num)) {
                    price_from = price_from_num;
                } else {
                    var minInp = $(this).parent().parent().find('input[name=min_price]');
                    minInp.val('');
                    minInp.parent().parent().parent().find('#min_price-error').text('Min price must be a number').show().animate({opacity: 2.0}, 2000).fadeOut("slow");
                    return false;
                }
            }

            var price_to = $(this).parent().parent().find('.price_to').val();

            if ($(this).parent().parent().find('input[name=max_price]').val() != '') {
                var price_to_num = $(this).parent().parent().find('input[name=max_price]').val();
                if (!isNaN(price_to_num)) {
                    price_to = price_to_num;
                } else {
                    var maxInp = $(this).parent().parent().find('input[name=max_price]');
                    maxInp.val('');
                    maxInp.parent().parent().parent().find('#max_price-error').text('Max price must be a number').show().animate({opacity: 2.0}, 2000).fadeOut("slow");
                    return false;
                }
            }
            var color_id = [];
            var geoloc_lat = $('.geoloc_lat').val();
            var geoloc_lng = $('.geoloc_lng').val();
            var filters = [];
            var order = $(this).attr('data-order');

            $(this).parent().parent().find($('input[name="filtersAdditional[]"]:checked')).each(function(i) {
                filters[i]  = this.value;
            });

            if ($(this).parent().parent().find('.color-btn.selected').length > 0 &&
                $(this).parent().parent().find('.color-btn.selected').attr('data-value').length > 0) {
                filters.push($(this).parent().parent().find('.color-btn.selected').attr('data-value'));
            }

            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl;?>/site/filterProduct' ,
                type: "GET",
                data: {
                    current_cat: current_cat,
                    price_to :price_to,
                    price_from :price_from,
                    dest : dest,
                    filters : filters,
                    geoloc_lat:geoloc_lat,
                    geoloc_lng :geoloc_lng,
                    order: order

                },
                success: function(data)
                {
                    var obj = JSON.parse(data);

                    $this.parent().parent().parent().parent().find('.single-product, .top-ad').remove();
                    if (obj.length < 6) {
                        $('.owl-controls').attr('style', 'display:none;');
                    } else {
                        $('.owl-controls').attr('style', 'display:block;');
                    }
                    $.each( obj, function( key, value ) {

                        var src = "<?php echo Yii::app()->baseUrl.'/images/product_images/';?>"+value['image'];
                        var name = value['name'];
                        var id = value['id'];
                        var price = value['giveaway'] == 0 ?  '<span>&#8364;' + value['price'] + '</span>' : 'Free';
                        var description = value['description'];
                        var location = value['location'];
                        var username = value['username'];
                        var top = value['top'];
                        var url = "<?php echo Yii::app()->baseUrl.'/products/advert/';?>"+id;
                        var userurl = "<?php echo Yii::app()->baseUrl.'/profile?username=';?>"+username;
                        var ajaxprod = $this.parent().parent().parent().parent().find('.ajax-product').eq(key);

                        if (top != undefined) {
                            ajaxprod.append('<span class="top-ad">TOP AD</span>');
                        }

                        ajaxprod.append(

                            '<div class="single-product" title="">' +
                                '<div class="product-image">' +
                                    '<div class="show-img">' +
                                        '<a class="product-image" href="<?php echo Yii::app()->baseUrl.'/products/advert/';?>'+id+'"><img src="'+src+'" alt=""></a>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="prod-info">' +
                                    '<h2 class="pro-name">' +
                                    '<a href="<?php echo Yii::app()->baseUrl.'/products/advert/';?>'+id+'">'+name+'</a>' +
                                    '</h2>' +
                                    '<div class="rating">' +
                                    '</div>' +
                                    '<div class="price-box">' +
                                        '<div class="price">' +
                                            price +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<a href="<?php echo Yii::app()->baseUrl.'/products/advert/';?>'+id+'">'+
                                    '<div class="info-pop">'+
                                        '<h4>'+name+'</h4>'+
                                        '<p><strong>Description :</strong>'+description+'</p>'+
                                        '<p><strong>Location :</strong>'+location+'</p>'+
                                        '<p><strong>Seller :</strong><a href="'+userurl+'">'+username+'</a></p>'+
                                        '<a href="'+url+'">I am interested</a>'+
                                    '</div>'+
                                '</a>'+
                            '</div>'
                        );
                    });
                }
            })
        });

        $('.cat-bar').click(function(event) {
            console.log('asd')
            event.preventDefault();
            var $this = $(this);
            var cat_id = $(this).attr('cat_id');
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl;?>/site/AdvertsList',
                type: "GET",
                data: {
                    cat_id: cat_id
                },
                success: function(data)
                {
                    var data1 = JSON.parse(data);

                    $this.parent().parent().parent().parent().find('.single-product, .top-ad').remove();

                    $.each( data1, function( key, value ) {

                        var src = "<?php echo Yii::app()->baseUrl.'/images/product_images/';?>"+value['image'];
                        var name = value['name'];
                        var id = value['id'];
                        var price = value['giveaway'] == 0 ?  '<span>&#8364;' + value['price'] + '</span>' : 'Free';
                        var description = value['description'];
                        var location = value['location'];
                        var username = value['username'];
                        var url = "<?php echo Yii::app()->baseUrl.'/products/advert/';?>"+id;
                        var userurl = "<?php echo Yii::app()->baseUrl.'/profile?username=';?>"+username;
                        var top = value['top'];

                        var ajaxprod = $this.parent().parent().parent().parent().find('.ajax-product').eq(key);

                        if (top != undefined) {
                            ajaxprod.append('<span class="top-ad">TOP AD</span>');
                        }

                        ajaxprod.append(
                            '<div class="single-product" title="">' +
                                '<div class="product-image">' +
                                    '<div class="show-img">' +
                                        '<a class="product-image" href="<?php echo Yii::app()->baseUrl.'/products/advert/';?>'+id+'"><img src="'+src+'" alt=""></a>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="prod-info">' +
                                    '<h2 class="pro-name">' +
                                        '<a href="<?php echo Yii::app()->baseUrl.'/products/advert/';?>'+id+'">'+name+'</a>' +
                                    '</h2>' +
                                    '<div class="rating">' +
                                    '</div>' +
                                    '<div class="price-box">' +
                                        '<div class="price">' +
                                            price +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<a href="<?php echo Yii::app()->baseUrl.'/products/advert/';?>'+id+'">'+
                                    '<div class="info-pop">'+
                                        '<h4>'+name+'</h4>'+
                                        '<p><strong>Description :</strong>'+description+'</p>'+
                                        '<p><strong>Location :</strong>'+location+'</p>'+
                                        '<p><strong>Seller :</strong><a href="'+userurl+'">'+username+'</a></p>'+
                                        '<a href="'+url+'">I am interested</a>'+
                                    '</div>'+
                                '</a>'+
                            '</div>'
                        );
                    });
                },
                error: function(data)
                {
                },
                complete: function()
                {
                }
            });
        });

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
        }

        function showPosition(position) {

            $('.geoloc_lat').val(position.coords.latitude);
            $('.geoloc_lng').val(position.coords.longitude);

            $(".hidden-category" ).each(function( i, v ) {
                var cat_id = v.value;
                $.ajax({
                    url: '<?php echo Yii::app()->request->baseUrl;?>/site/maxDestination',
                    type: "GET",
                    data: {
                        current_cat: cat_id,
                        geoloc_lat: position.coords.latitude,
                        geoloc_lng: position.coords.longitude
                    },
                    success: function (data) {
                        $('.destination'+i).val(parseInt(data));
                        var input = $('.destination'+i).closest('div .one-fifth').find("input.dest-text-input");

                        input.attr('max', parseFloat(data));
                        input.attr('placeholder', 'max - ' + data + ' km')
                        $( "#one-slide"+i).slider({
                            disabled: ($('.geoloc_lat').val() > 0 && $('.geoloc_lng').val() > 0) ? false : true,
                            range: "min",
                            max: parseFloat(data),
                            change: function( event, ui ) {

                                $('.destination'+i).val(ui.value);
                            },
                            slide: function( event, ui ) {

                                $( "#dest"+i).val(ui.value +" km");
                            }
                        });
                        $( "#dest"+i ).val($( "#one-slide"+i ).slider( "values", 0 )+" km") ;
                        $('input[name=dest]').removeAttr('readonly');

                        $('.loc-error').hide();
                        $('.loc-text').tooltip('disable');
                    }
                });
            });
        }

    });

</script>