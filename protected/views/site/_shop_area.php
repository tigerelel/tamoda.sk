<?php $filter = isset($is_filter) ? "-filter": "";
$from = ($pages->pageSize*($page-1)+1); $to = ($pages->pageSize*($page-1) + $countPage);

?> 
    <div class="shop-product shop-page">
        <div class="tab-content">
            <div id="grid" class="tab-pane fade in active">
                <div class="product-grid">
                    <div class="row">
                        <?php
                        if (!empty($products)) {
                            foreach ($products as $product) { ?>
                                <div class="col-sm-4 col-md-4 col-lg-3">
                                    <div class="single-product">
                                        <div class="product-image">
                                            <div class="show-img">
                                                <?php if (!empty($product['photo']) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $product['photo'])) { ?>
                                                    <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                                                        <img src="<?php echo Yii::app()->request->baseUrl . '/images/product_images/' .  $product['photo'];?>">
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                                                        <img src="<?php echo Yii::app()->request->baseUrl.'/images/product_images/none.jpg';?>">
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="prod-info">
                                            <h2 class="pro-name">
                                                <?php echo CHtml::link($product['name'], array('products/advert', 'id' => $product['id'])); ?>
                                            </h2>
                                            <div class="rating">
                                            </div>
                                            <div class="price-box">
                                                <?php if ($product['giveaway'] != 1) { ?>
                                                    <span>&#8364;<?php echo $product['price'];?></span>
                                                <?php } else { ?>
                                                    Free
                                                <?php } ?>
                                            </div>
                                            <div class="prod-date">Entered : <?=date("M d, Y H:i", strtotime($product['created_date']));?></div>
                                            <?php if (Yii::app()->session['user_id'] && $product['users_id'] != Yii::app()->session['user_id'] ) { ?>
                                                <div class="actions watchlist">
                                                    <span class="new-pro-wish <?= (!isset($watchlist) ? ($product['fav'] ? ' favourite':'') : ' favourite'); ?>">
                                                        <a href="#" data-toggle="tooltip" title="<?= (!isset($watchlist) ? ($product['fav'] ? 'Remove from watch ad':'Watch Ad') : 'Remove from watch ad'); ?>" onclick="favourite(event, <?php echo $product['id']; ?>, $(this))">
                                                            <i class="fa fa-heart-o"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                                            <div class="info-pop">
                                                <h4><?php echo $product['name'];?></h4>
                                                <p><strong>Description: </strong><?php echo substr($product['description'],0,70).'...';?></p>

                                                <?php if (!isset($shop)) { ?>
                                                    <p><strong>Location: </strong><?php echo $product['location'];?></p>
                                                    <p><strong>Seller: </strong><?php echo $product['username'];?></p>
                                                <?php } else { ?>
                                                    <p><?=$product['category'];?></p>
                                                <?php } ?>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                        } ?>
                    </div>
                </div>
            </div>
            <?php if (!isset($watchlist)) { ?>
                <div id="list" class="tab-pane fade">
                    <div class="product-list">
                        <div class="row">
                            <?php if(!empty($products)){
                                foreach($products as $product) { ?>
                                    <div class="col-md-12 ">

                                        <div class="single-list-product">
                                            <div class="col-sm-4 col-md-4 product-image">
                                                <div class="show-img">
                                                    <?php if (!empty($product['photo']) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $product['photo'])) { ?>
                                                        <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                                                            <img src="<?php echo Yii::app()->request->baseUrl . '/images/product_images/' .  $product['photo'];?>">
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                                                            <img src="<?php echo Yii::app()->request->baseUrl.'/images/product_images/none.jpg';?>">
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-8 col-md-8">
                                                <div class="prod-list-detail">
                                                    <div class="prod-info">
                                                        <h2 class="pro-name">
                                                            <?php echo CHtml::link($product['name'], array('products/advert', 'id' => $product['id'])); ?>
                                                        </h2>
                                                        <div class="price-box">
                                                            <div class="price">
                                                                <?php if ($product['giveaway'] != 1) { ?>
                                                                    <span>&#8364;<?php echo $product['price'];?></span>
                                                                <?php } else { ?>
                                                                    Free
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="rating">
                                                        </div>
                                                        <p><?=$product['category'];?></p>
                                                        <p class="prod-des"><?php echo $product['description']?></p>
                                                        <?php if (Yii::app()->session['user_id'] && $product['users_id'] != Yii::app()->session['user_id']) { ?>
                                                            <div class="actions">
                                                                <span class="new-pro-wish <?= ($product['fav'] ? ' favourite':''); ?>">
                                                                    <a href="#" data-toggle="tooltip" title="<?= ($product['fav'] ? 'Remove from watch ad':'Watch Ad'); ?>" onclick="favourite(event, <?php echo $product['id']; ?>, $(this))">
                                                                        <i class="fa fa-heart-o"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="pager-list<?=$filter?>">
        <?php $this->widget('CLinkPager', array(
                'currentPage' => $pages->getCurrentPage(),
                'itemCount' => $totalCount,
                'pageSize' => $pages->pageSize,
                'maxButtonCount' => 5,
            ));
         ?>

        <!-- Pagination buttons will be generated here -->
    </div>
<script>

    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });

    $(document).ready(function(){
        $("#result-text").text('Showing <?=$from?>-<?=$to?> of <?=$totalCount?> result');
        $(".pager-list-filter a").each(function(i,el){
                $(el).attr("href", "");
        });
    });

    $('.pager-list-filter a').on("click" ,function(e){
        e.preventDefault();
        var layout = $('#grid').hasClass('active') ? "grid" : "list";
        var page = 1;
        var dest = $('.ajax-dest').val();
        var current_cat = '<?=isset($category_id) ? $category_id : false?>';
        var price_from = $('.price_from').val();

        if ($('input[name=min_price]').val() != '') {
            var price_from_num = $('input[name=min_price]').val();
            if (!isNaN(price_from_num)) {
                price_from = price_from_num;
            } else {
                $('input[name=min_price]').val('');
                return false;
            }
        }

        var price_to = $('.price_to').val();

        if ($('input[name=max_price]').val() != '') {
            var price_to_num = $('input[name=max_price]').val();
            if (!isNaN(price_to_num)) {
                price_to = price_to_num;
            } else {
                $('input[name=max_price]').val('');
                return false;
            }
        }
        var geoloc_lat = $('.geoloc_lat').val();
        var geoloc_lng = $('.geoloc_lng').val();
        var filters = [];
        var order_by = $('.orderby option:selected').val();
        var q = '<?=isset($q) ? $q : false;?>';

        var class_name = $(this).attr('class').length > 0 ? $(this).attr('class').split(' ')[0] : "";
        switch (class_name) {
            case "first":
                page = 1;
                break;
            case "previous":
                page = '<?=$page-1?>';
                break;
            case "last":
                page = '<?=(int)$totalCount/$pages->pageSize ? (int)$totalCount/$pages->pageSize : 1?>';
                break;
            case "next":
                page = '<?=$page+1?>';
                break;
            default:
                page = 1;
                break;
        }
        if($(this).hasClass("page")){
            page = $(this).text();
        }
        
        $('input[name="filtersAdditional[]"]:checked').each(function(i) {
            filters[i]  = this.value;
        });
        
        $.ajax({
            url: '<?php echo Yii::app()->request->baseUrl;?>/site/filterProduct' ,
            type: "GET",
            data: {
                current_cat: current_cat,
                price_to :price_to,
                price_from :price_from,
                dest : dest,
                filters : filters,
                geoloc_lat:geoloc_lat,
                geoloc_lng :geoloc_lng,
                order: order_by,
                page: page,
                is_cat:true,
                q:q

            },
            success: function(data)
            {
                $(".products-area").empty();
                $(".products-area").append(data);
                $(".tab-pane").removeClass('active in');
                $("#" + layout).addClass('active in');
                
            },
            error: function(xhr,status,error){
                console.log(status);
            }
        })
    });
    function favourite(e, id, $this) {

        e.preventDefault();

        var title = $this.attr("data-original-title") == "Watch Ad" ? 'Remove from watch ad' : 'Watch Ad';
        $this.attr("data-original-title", title);

        $this.parent().toggleClass('favourite');


        if ($('.watch-ad a span').text() == "Watch Ad") {
            $('.watch-ad a span').text("remove from watch ad");
        }
        else {
            $('.watch-ad a span').text("Watch Ad");
        }

        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/toggleFavourite',
            data: {
                product_id: id
            },
            success: function() {}
        });
    }

</script>