
<div class="shop-area">
    <div class="container-fluid">
        <div class="cat-crumb">
            <?php
            if (isset($_GET['id'])) {
                $category = Categories::model()->findByPk(CHtml::encode($_GET['id']));

                $this->widget(
                    'zii.widgets.CBreadcrumbs',
                    array(
                        'homeLink'=>$category->title,
                        'links' => array(''),
                        'htmlOptions'=>array ('class'=>'bread-crumb')
                    )
                );
            }
            else{
                $category = Categories::model()->findByPk($category_id);
            }

            if (isset($_GET['subId'])) {
                $category = Categories::model()->findByPk(CHtml::encode($_GET['subId']));
                if ($category->parentCategory->parentCategory) {
                    $this->widget(
                        'zii.widgets.CBreadcrumbs',
                        array(
                            'homeLink'=>CHtml::link($category->parentCategory->parentCategory->title,
                                array('site/category', 'id' => $category->parentCategory->parent_id)),
                            'links' => array(
                                $category->parentCategory->title => Yii::app()->baseUrl.'/site/category?subId='.$category->parent_id,
                                $category->title
                            ),
                            'htmlOptions'=>array ('class'=>'bread-crumb')
                        )
                    );
                } else {
                    $category = Categories::model()->findByPk($category_id);
                    $this->widget(
                        'zii.widgets.CBreadcrumbs',
                        array(
                            'homeLink'=>CHtml::link($category->parentCategory->title,
                                array('site/category', 'id' => $category->parent_id)),
                            'links' => array(
                                $category->title
                            ),
                            'htmlOptions'=>array ('class'=>'bread-crumb')
                        )
                    );
                }
            }
            ?>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-2">
              <?php
                   $this->widget('application.components.widgets.CategoriesList', [
                          'category' => $category
                    ]);
                ?>
            </div>
            <div class="col-md-9 col-lg-10">
                <?php $from = ($pages->pageSize*($page-1)+1); $to = ($pages->pageSize*($page-1) + $countPage);
                   $this->widget('application.components.widgets.GridList', [
                            'from' => $from,
                            'to' => $to,
                            'totalCount' => $totalCount,
                        ]);
                ?>
                 <div class="shop-area products-area">

                   <?php $this->renderPartial('_shop_area', array(
                        'pages' => $pages,
                        'totalCount' => $totalCount,
                        'products' => $products,
                        'page' => $page,
                        'countPage' => $countPage,
                        'category_id'=>$category_id,
                        'q' => false,
                        ))
                    ?>
                 </div>
            </div>
        </div>
    </div>
</div>
