
<h3><button type="button" class="btn contact-btn" data-toggle="modal" data-target="#contact">Contact</button></h3>
<div id="contact" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="text-center">CONTACT FORM</h4>
            </div>
            <div class="modal-body">
                <div class="contact-admin-form">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                        'action' => $this->createUrl('site/contactAdmin')
                    )); ?>

                    <?php $contact = new ContactForm('contactAdmin');?>

                    <div class="form-group">
                        <?=$form->labelEx($contact,'name'); ?>
                        <?=$form->textField($contact,'name', array('class' => 'form-control')); ?>
                        <?=$form->error($contact,'name'); ?>
                    </div>
                    <div class="form-group">
                        <?=$form->labelEx($contact,'email'); ?>
                        <?=$form->textField($contact,'email', array('class' => 'form-control')); ?>
                        <?=$form->error($contact,'email'); ?>

                    </div>
                    <div class="form-group">
                        <?=$form->labelEx($contact,'body'); ?>
                        <?=$form->textArea($contact,'body', array('class' => 'form-control')); ?>
                        <?=$form->error($contact,'body'); ?>
                    </div>

                    <div id="recaptcha-token2" class="g-recaptcha pull-left"></div>

                    <div class="form-group">
                        <?php echo CHtml::submitButton('Send', array('class' => 'btn btn-success form-control', 'name' => 'accept')); ?>
                    </div>


                    <?php $this->endWidget(); ?>
                    <br>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<script src='https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit'></script>
<script type="text/javascript">
    var CaptchaCallback = function(){

        if ($('#recaptcha-token1').length > 0) {
            grecaptcha.render('recaptcha-token1', {'sitekey' : '6Ldt8x4TAAAAAGSVyjBa5wkqEb03RCIhaIijMARx'});
        }
        if ($('#recaptcha-token2').length > 0) {
            grecaptcha.render('recaptcha-token2', {'sitekey' : '6Ldt8x4TAAAAAGSVyjBa5wkqEb03RCIhaIijMARx'});
        }
    };

</script>
