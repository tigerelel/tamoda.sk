<?php if (!empty($recentAds)) {
    foreach ($recentAds as $product) { ?>
        <div class="col-sm-4 col-md-4 col-lg-3">
            <div class="single-product">
                <div class="product-image">
                    <div class="show-img">
                        <?php if (!empty($product['photo']) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $product['photo'])) { ?>
                            <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                                <img src="<?php echo Yii::app()->request->baseUrl . '/images/product_images/' .  $product['photo'];?>">
                            </a>
                        <?php } else { ?>
                            <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>"><img src="<?php echo Yii::app()->request->baseUrl.'/images/product_images/none.jpg';?>"></a>
                        <?php } ?>
                    </div>
                </div>
                <div class="prod-info">
                    <h2 class="pro-name">
                        <?php echo CHtml::link($product['name'], array('products/advert', 'id' => $product['id'])); ?>
                    </h2>
                    <div class="rating">
                    </div>
                    <div class="price-box">
                        <div class="price">
                            <?php if ($product['giveaway'] == 0) { ?>
                                <span>&#8364;<?php echo $product['price'];?></span>
                            <?php } else { ?>
                                Free
                            <?php } ?>
                        </div>
                    </div>
                    <div class="prod-date">Entered : <?=date("M d, Y H:i", strtotime($product['created_date']));?></div>
                    <?php if (Yii::app()->session['user_id'] && $product['users_id'] != Yii::app()->session['user_id'] ) { ?>
                        <div class="actions watchlist">
                                <span class="new-pro-wish <?= (!isset($watchlist) ? ($product['fav'] ? ' favourite':'') : ' favourite'); ?>">
                                    <a href="#" data-toggle="tooltip" title="<?= (!isset($watchlist) ? ($product['fav'] ? 'Remove from watch ad':'Watch Ad') : 'Remove from watch ad'); ?>" onclick="favourite(event,  <?php echo $product['id']; ?>, $(this))">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                </span>
                        </div>
                    <?php } ?>
                </div>
                <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$product['id'];?>">
                    <div class="info-pop">
                        <h4><?php echo $product['name'];?></h4>
                        <p><strong>Description: </strong><?php echo substr($product['description'],0,70).'...';?></p>
                        <p><strong>Location: </strong><?php echo $product['location'];?></p>
                        <p><strong>Seller: </strong><?php echo $product['username'];?></p>
                    </div>
                </a>
            </div>
        </div>
    <?php }
} ?>
<input type="hidden" id="end<?=isset($page)?$page:0?>" value="<?=$end?$end:'';?>">
