<div class="shop-area">
    <div class="container-fluid">
        <div class="row">
            <h4 class="sin-page-title">my watchlist</h4>
            <div class="col-md-9 col-lg-10 col-lg-offset-1">
                <div class="shop-area">
                    <div class="shop-area products-area">
                       <?php $this->renderPartial('//site/_shop_area', array(
                            'pages' => $pages, 
                            'totalCount' => $totalCount, 
                            'products' => $products, 
                            'page' => $page, 
                            'countPage' => $countPage, 
                            'category_id' => false,
                            'q'=>false,
                            'watchlist' => true
                            ))
                        ?>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>


