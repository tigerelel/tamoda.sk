<?php
/* @var $this ProductsController */
/* @var $model Products */

$this->breadcrumbs=array(
	'Products'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Products', 'url'=>array('index')),
	array('label'=>'Create Products', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#products-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<div class="container-fluid">
    <h1>
        <a href="create" class="pull-right">
            <i class="fa fa-plus"></i>
        </a>
    </h1>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> Products</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <td style="width: 1px;" class="text-center">
                        </td>
                        <td class="text-left">
                            Category
                        </td>
                        <td class="text-left">
                            Name
                        </td>
                        <td class="text-right">
                            Description
                        </td>
                        <td class="text-right">
                            Image
                        </td>
                        <td class="text-right">
                            Location
                        </td>
                        <td class="text-right">Action</td>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td class="text-center">
                                <input type="checkbox" name="selected[]" value="2">
                            </td>
                            <td class="text-left"><?php echo $product->categories->categoriesLabels[0]->title; ?></td>
                            <td class="text-left"><?php echo CHtml::link('name', array('view', 'id' => $product->id)); ?></td>
                            <td class="text-right">desc</td>
                            <td class="text-right">image</td>
                            <td class="text-right"><?php echo $product->location; ?></td>
                            <td class="text-right">
                                <?php echo CHtml::link('<i class="fa fa-pencil"></i>', array('update', 'id' => $product->id)); ?>
                                <?php echo CHtml::link('<i class="fa fa-remove"></i>', array('delete', 'id' => $product->id)); ?>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>