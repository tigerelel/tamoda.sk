
<div class="row">
	<div class="col-xs-12 col-sm-12 col-lg-offset-3 col-md-6 col-lg-6">

		<div class="area-heading">
			<h1 class="text-center">UPDATE ADVERT</h1>
		</div>

		<div>
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'product-form',
				'enableAjaxValidation' => false,
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'categories_id'); ?>

				<?php if (isset($category)) {

					echo $form->dropDownList(
						$category,
						'id',
						CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title'),
						array('class'=>'prof-form required', 'empty' => 'Choose category', 'name' => 'Products[categories_id]'));
				}  else {
					echo $form->dropDownList($model,'categories_id', $categories, array('class'=>'prof-form', 'empty' => 'Choose category'));
				} ?>
			</div>

			<div id="subs">
				<?php if (isset($subcategory)) { ?>
					<div class="form-group">
						<?php echo $form->dropDownList(
							$subcategory,
							'id',
							CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$category->id)), 'id', 'title'),
							array('class'=>'prof-form required', 'empty' => 'Choose subcategory', 'id' => 'subcategory_id', 'name' => 'Products[subcategories_id]')
						); ?>
					</div>
				<?php } ?>
			</div>
			<div id="subsubs">
				<?php if (isset($subcategory)) { ?>
					<div class="form-group">
						<?php echo $form->dropDownList(
							$model,
							'categories_id',
							CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$subcategory->id)), 'id', 'title'),
							array('class'=>'prof-form required', 'empty' => 'Choose sub subcategory', 'name' => 'Products[subSubcategories_id]')
						); ?>
					</div>
				<?php } ?>
			</div>

			<div class="form-group colors">

				<?php $colors = json_decode(FiltersNew::model()->find('type=1')->filters); ?>

				<?php foreach ($colors as $key=>$color) {

					$check = false;
                    if (!empty($adColors)) {
                        foreach (json_decode($adColors->other_filter) as $key2 => $val) {
                            foreach ($val as $k=>$c) {
                                if ($c == $color) {
                                    $check = true;
                                }
                            }
                        }
                    } ?>

					<div class="each-color">
						<label for="<?='color'.$key; ?>" style="border:1px dotted #545454;background: <?php echo $color; ?>; height: 22px; width: 22px;border-radius: 11px;"><?php echo CHtml::checkBox('colors[color][]', $check?'checked':'', array('value' => $color, 'class' => 'filter-box', 'id' => 'color'.$key)); ?></label>
					</div>

				<?php } ?>

				<?php
                if (!empty($adColors)) {
                    $arr = [];

					foreach (json_decode($adColors->other_filter) as $key2 => $val) {
						foreach ($val as $k=>$c) {
							if (!in_array($c, $colors) && $c != '') {
								$arr[$k] = $c;
							}
						}
					} ?>
                    <input type="hidden" name="color_id" value="<?= $adColors->id; ?>">

                    <?php foreach ($arr as $a) { ?>
                        <div class="each-color">
                            <label for="<?= 'color' . ++$key; ?>"
                                   style="border:1px dotted #545454;background: <?php echo $a; ?>; height: 22px; width: 22px;border-radius: 11px;">
                                <?php echo CHtml::checkBox('colors[color][]', 'checked', array('value' => $a, 'class' => 'filter-box', 'id' => 'color' . $key)); ?>
                            </label>
                        </div>
                    <?php }
                } ?>

			</div>
			<br><br><br>
			<div class="form-group other-colr">
				Other color: <input type='text' id="custom" name="colors[color][]" />
			</div>

                <div id="filters-add" class="form-group">
                    <?php
                    if ($adSizes) {
                        foreach (json_decode($adSizes->other_filter) as $k=>$s1) {

                        $sizeModel = FiltersNew::model()->findByPk($k);
                        $sizeCharts = json_decode($sizeModel->filters);
                    ?>
                        <table id="prod_table" class="table table-bordered table-hover" style="display: table;">
                            <thead>
                            <tr>
                                <th><?php echo $sizeModel->name; ?></th>
                                <?php foreach ($sizeCharts[0] as $k=>$s) {
                                    if ($k != 'id') { ?>
                                        <th><?= $k; ?></th>
                                    <?php }
                                } ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($sizeCharts as $key=>$size) { ?>
                                <tr>
                                    <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]" <?=in_array($size->id, $s1)?'checked':'';?>></td>
                                    <?php foreach ($size as $k=>$s) { ?>
                                        <?php if ($k != 'id') { ?>
                                            <td>
                                                <?php if (is_array($s)) {
                                                    foreach ($s as $key=>$val) {
                                                        foreach ($val as $v) { ?>
                                                            <?=$v.'<br>';
                                                        }
                                                    }
                                                } else {
                                                    echo $s;
                                                } ?>
                                            </td>
                                        <?php }
                                    } ?>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <?php } ?>
                        <input type="hidden" name="size_id" value="<?=$adSizes->id;?>">
                    <?php } ?>
                    <?php $this->renderPartial('_sizes'); ?>
                </div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo CHtml::activeTextField($model,'name',array('size'=>60,'maxlength'=>150, 'class'=>'prof-form')); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'description'); ?>
				<?php echo CHtml::activeTextArea($model,'description',array('class'=>'prof-form', 'style' => 'height: 136px;')); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'location'); ?>
				<?php echo $form->textField($model,'location',array('class'=>'controls', 'id' => 'pac-input', 'placeholder' => 'Enter Location', 'required' => true)); ?>
				<div id="map"></div>
				<input type="hidden" id="startLat" value="<?php echo isset($model->lat) ? $model->lat : (isset($_POST['lat']) ? $_POST['lat'] : ''); ?>" name="lat">
				<input type="hidden" id="startLng" value="<?php echo isset($model->lng)?$model->lng : (isset($_POST['lng']) ? $_POST['lng'] : ''); ?>" name="lng">
			</div>

			<div class="clearfix">
				<div class="form-group col-md-4 col-sm-4">
					<?php echo $form->dropDownList($model,'new',array(1=>'new',0 => 'used'), array('class'=>'prof-form', 'options' =>
						(isset($_POST['Products']['new']) ? array($_POST['Products']['new']=>array('selected'=>true)) : ''))); ?>
				</div>
			</div>

			<div class="clearfix">
				<div class="form-group">
					<label class="col-sm-2" for="givaway">Giveaway</label>
					<?= $form->checkBox($model, 'giveaway', array('id' => 'giveaway')); ?>
<!--										<input type="checkbox" id="givaway" name="giveaway" --><?//=$model->giveaway==1 ? 'checked' : '';?><!--">-->
				</div>
			</div>
			<div class="clearfix">
				<div class="form-group col-md-4 col-sm-4" id="price">

					<?php echo $form->labelEx($model,'price'); ?>
					<div class="prof-form valid">
						<div class="col-xs-1 no-padding-right no-padding-left">
							<span class="pull-left" >€</span>
						</div>
						<div class="col-xs-11 no-padding-right no-padding-left">
							<?php echo $form->textField($model,'price',array('type' => 'number', 'size'=>10,'maxlength'=>10, 'class'=>'input-text no-border', 'value' => '')); ?>
						</div>
					</div>

				</div>
			</div>

			<div class="form-group" id="payment">
				<?php $payments = json_decode($model->payment);
				$payments2 = (isset($_GET['advert']['payment']) ? $_GET['advert']['payment'] : []);
				?>
				<div class="clearfix">
					<div class="col-sm-3"><?php echo $form->labelEx($model,'payment'); ?></div>
					<div class="col-sm-3"><?php echo CHtml::checkBox('payment[]', (isset($payments) && in_array('cash on delivery', $payments) || in_array('cash on delivery', $payments2))?'checked':'', array('value' => 'cash on delivery', 'id' => 'payment_delivery')); ?><label for="payment_delivery">cash on delivery</label></div>
					<div class="col-sm-3"><?php echo CHtml::checkBox('payment[]', (isset($payments) && in_array('pay pall', $payments) || in_array('pay pall', $payments2))?'checked':'', array('value' => 'pay pall', 'id' => 'pay-pall')); ?><label for="pay-pall">pay-pall</label></div>
					<div class="col-sm-3"><?php echo CHtml::checkBox('payment[]', (isset($payments) && in_array('bank transfer', $payments)) || in_array('bank transfer', $payments2)?'checked':'', array('value' => 'bank transfer', 'id' => 'bank_transfer')); ?><label for="bank_transfer">bank transfer</label></div>
				</div>
			</div>

			<div class="form-group">
				<?php $delivery = json_decode($model->delivery);
				$delivery2 = (isset($_GET['advert']['delivery']) ? $_GET['advert']['delivery'] : []);
				?>
				<div class="clearfix">
					<div class="col-sm-3"><?php echo $form->labelEx($model, 'delivery'); ?></div>
					<div class="col-sm-3"><?php echo CHtml::checkBox('delivery[]', isset($delivery) && in_array('personally', $delivery)  || in_array('personally', $delivery2)?'checked':'', array('value' => 'personally', 'id' => 'personally')); ?><label for="personally">Personally</label></div>
					<div class="col-sm-3"><?php echo CHtml::checkBox('delivery[]', isset($delivery) && in_array('by post', $delivery)  || in_array('by post', $delivery2)?'checked':'', array('value' => 'by post', 'id' => 'by_post')); ?><label for="by_post">by post</label></div>
					<div class="col-sm-3"><?php echo CHtml::checkBox('delivery[]', isset($delivery) && in_array('via currier', $delivery)  || in_array('via currier', $delivery2)?'checked':'', array('value' => 'via currier', 'id' => 'via_currier')); ?><label for="via_currier">via currier</label></div>
				</div>
			</div>

			<div class="form-group">

				<div class="uploaded-photos col-md-12">
					<div id="img-1"></div>
					<?php if (isset($_POST['main'])) {
						$main = ProductsMedia::model()->findByPk($_POST['main']);
					} ?>

					<?php if (isset($productImages)) { ?>
						<?php foreach ($productImages as $key=>$file) {
							$advImage = ProductsMedia::model()->findByPk($file->id);
							if ($advImage && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $advImage['photo'])) { ?>
								<input type="hidden" class="up-images" data-id="<?=$advImage->id;?>" data-name="<?=$advImage->photo;?>" data-main="<?=$advImage->main_photo;?>" data-order="<?=$advImage->sort_order;?>">
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</div>

				<?php if (!empty($productVideo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_videos/' . $productVideo['title'])) { ?>
					<input type="hidden" class="up-video" data-id="<?=$productVideo['id'];?>" data-name="<?=$productVideo['title'];?>">
				<?php } ?>

				<div>
					<div class="dropzone dropzone-previews" id="my-awesome-dropzone">
						<div class="fallback">
							<input name="file[]" type="file" multiple />
						</div>
					</div>
					<input type="hidden" name="main">
					<br>
				</div>
			</div>

			<div class="form-group">
				<label>Put video link from Youtube or Vimeo</label>
				<?php echo $form->textField($model,'video_link',array('class'=>'prof-form')); ?>
			</div>

			<div class="form-group">
				<label>Or upload your own</label>

				<div class="dropzone dropzone-video-previews" id="dropzone-video">
					<div class="dz-message" data-dz-message><span>Drop or <a>click here</a> to upload a video</span></div>
					<div class="fallback">
						<input type="file" accept="video/*" name="video" />
					</div>
				</div>
			</div>

			<?php echo CHtml::submitButton('Cancel', array('name' => 'cancel', 'class' => 'btn btn-success pull-right')); ?>
			<?php echo CHtml::submitButton('Edit', array('name' => 'edit', 'class' => 'btn btn-default pull-right', 'onclick'=>"checkFilter(this);return false;")); ?>

			<?php $this->endWidget(); ?>
	</div>
</div>

	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dropzone.css" media="all" />

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/dropzone.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjB6-iML5AvdAI3Y6ZUd1UT1jkiD_eXa8&libraries=places&callback=initAutocomplete" async defer></script>

	<script src='<?php echo Yii::app()->request->baseUrl; ?>/js/spectrum.js'></script>

	<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/spectrum.css' />

	<script>

		function initAutocomplete() {

			var map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: <?php echo $model->lat?$model->lat: 48.1500; ?>, lng: <?php echo $model->lng?$model->lng: 17.1167; ?>},
				zoom: 13,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});

//			if (navigator.geolocation) {
//				navigator.geolocation.getCurrentPosition(function(position) {
//
//					var pos = {
//						lat: position.coords.latitude,
//						lng: position.coords.longitude
//					};
//
//					map.setCenter(pos);
//
//					var marker = new google.maps.Marker({
//						position: {lat: pos.lat, lng: pos.lng},
//						map: map
//					});
//
//					var infowindow = new google.maps.InfoWindow({
//						content: marker.getPosition()+''
//					});
//
//					document.getElementById('startLat').value = pos.lat;
//					document.getElementById('startLng').value = pos.lng;
//
//					getAddressFromLatLang(pos.lat, pos.lng)
//
//				})
//			}

			// Create the search box and link it to the UI element.
			var input = document.getElementById('pac-input');
			var searchBox = new google.maps.places.SearchBox(input);
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

			map.addListener('bounds_changed', function() {
				searchBox.setBounds(map.getBounds());
			});

			var markers = [];

			searchBox.addListener('places_changed', function() {
				var places = searchBox.getPlaces();

				if (places.length == 0) {
					return;
				}

				markers.forEach(function(marker) {
					marker.setMap(null);
				});
				markers = [];

				// For each place, get the icon, name and location.
				var bounds = new google.maps.LatLngBounds();

				places.forEach(function(place) {
					var icon = {
						url: place.icon,
						size: new google.maps.Size(71, 71),
						origin: new google.maps.Point(0, 0),
						anchor: new google.maps.Point(17, 34),
						scaledSize: new google.maps.Size(25, 25)
					};

					// Create a marker for each place.
					markers.push(new google.maps.Marker({
						map: map,
						icon: icon,
						title: place.name,
						position: place.geometry.location
					}));

					if (place.geometry.viewport) {
						// Only geocodes have viewport.
						bounds.union(place.geometry.viewport);
					} else {
						bounds.extend(place.geometry.location);
					}

					document.getElementById('startLat').value = place.geometry.location.lat();
					document.getElementById('startLng').value = place.geometry.location.lng();
				});

				map.fitBounds(bounds);
			});
		}

		var file_ids = [];
		var sort_order = 0;

		Dropzone.options.myAwesomeDropzone = {

			url: '<?php echo $this->createAbsoluteUrl('products/uploadPhoto', array('id' => $model->id)); ?>',
			previewsContainer: ".dropzone-previews",
			uploadMultiple: true,
			parallelUploads: 10,
			maxFiles: 10,
			maxFilesize: 10,
			addRemoveLinks: true,
//			dictDefaultMessage: '',
			removedfile: function(file) {
				var myDropzone = this;
				if (file.id !== undefined) {
					$.ajax({
						type: 'GET',
						url: '<?php echo $this->createAbsoluteUrl('products/deletePhoto'); ?>',
						data: {
							photo_id: file.id,
							media: 'photo'
						},
						success: function(data) {
							if ($('input[name="main"]').val()==file.id) {
								$('input[name="main"]').attr('value', '');
							}

							if (myDropzone.previewsContainer.children.length > 1) {

								$('#my-awesome-dropzone .dz-default').removeClass('dz-message');
								$('#my-awesome-dropzone .dz-default span').hide();

							} else {
								$('#my-awesome-dropzone .dz-default').addClass('dz-message');
								$('#my-awesome-dropzone .dz-default span').show();
							}

						}
					});
				}
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

			},
			success: function(file, response) {

				file.id = $.parseJSON(response);

				if ($('input[name=main]').val() == '') {
					$('input[name=main]').val(file.id);
					$('.dz-image').css({'border': '1px #f03e62 solid'});
				}

				$(file.previewTemplate).append('<span class="dz-main " onclick="makeMain('+file.id+', this)" data-dz-main="">Set as main</span>');
				$(file.previewTemplate).append('<input id="'+file.id+'" type="hidden" name="file[]" value="'+file.id+'">');

				file_ids.push(file.id);

				$(file.previewTemplate).append('<input type="hidden" name="sort_order[]" value="'+sort_order+'">');

				sort_order++;

			},
			init: function() {

				var $this = this;
				this.on("sending", function(file, xhr, formData) {
					formData.append('sort_order', sort_order);
				});

				$.each($('.up-images'), function(i, v) {

					var mockFile = { name: $(v).attr('data-name'),  type: 'image/jpeg', id: $(v).attr('data-id') };
					$this.options.addedfile.call($this, mockFile);
					$this.options.thumbnail.call($this, mockFile, "<?=Yii::app()->request->baseUrl;?>/images/product_images/"+ $(v).attr('data-name'));
					mockFile.previewElement.classList.add('dz-success');
					mockFile.previewElement.classList.add('dz-complete');
					$(mockFile.previewElement).append('<span class="dz-main " onclick="makeMain('+$(v).attr('data-id')+', this)" data-dz-main="">Set as main</span>');
					$(mockFile.previewElement).append('<input id="'+$(v).attr('data-id')+'" type="hidden" name="file[]" value="'+$(v).attr('data-id')+'">');
					$(mockFile.previewElement).append('<input type="hidden" name="sort_order[]" value="'+$(v).attr('data-order')+'">');
					file_ids.push($(v).attr('data-id'));
					if ($(v).attr('data-main') == 1) {
						$(mockFile.previewElement).find('.dz-image').css({'border': '1px #f03e62 solid'});
						$('input[name="main"]').attr('value', $(v).attr('data-main'));
					}
				});
			}
		}

		Dropzone.options.dropzoneVideo = {

			url: '<?php echo $this->createAbsoluteUrl('products/uploadVideo', array('id' => $model->id)); ?>',
			previewsContainer: ".dropzone-video-previews",
			uploadMultiple: false,
			maxFilesize: 10,
			addRemoveLinks: true,
			acceptedFiles: ".mp4,.mkv,.avi",
			removedfile: function(file) {

				if (file.id !== undefined) {
					$.ajax({
						type: 'GET',
						url: '<?php echo $this->createAbsoluteUrl('products/deletePhoto'); ?>',
						data: {
							video_id: file.id,
							media: 'video'
						}
					});
				}
				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
			},
			success: function(file, response) {
				file.id = $.parseJSON(response);
				$(file.previewTemplate).append('<input type="hidden" name="video_file" value="'+file.id+'">')
			},
			init: function() {

				if ($('.up-video').length > 0) {

					var $this = this;
					var mockFile = { name: $('.up-video').attr('data-name'), type: 'video/*' };
					$.each($('.up-video'), function(i, v) {
						$this.options.addedfile.call($this, mockFile);
						mockFile.previewElement.classList.add('dz-success');
						mockFile.previewElement.classList.add('dz-complete');
						$(mockFile.previewElement).append('<input id="' + $(v).attr('data-id') + '" type="hidden" name="video_file" value="' + $(v).attr('data-name') + '">');
					})
				}

			}
		}

		function makeMain($id, $this) {

			$.ajax({
				type: 'GET',
				url: '<?php echo $this->createAbsoluteUrl('products/setMainPhoto'); ?>',
				data: {
					all_id: file_ids,
					set_main: $id
				},
				success: function(data) {
					$($this).parents().find('.dz-image').css({'border': ''});
					$($this).parent().find('.dz-image').css({'border': '1px #f03e62 solid'});
					$('input[name="main"]').attr('value', $id)
				}
			});
		}

		$('#giveaway').change(function() {
			$('#price').toggle();
			$('#payment').toggle();
			$('#price input').val('')
			$('#payment input').removeAttr('checked');
		});


		if ($('#giveaway').is(":checked")) {
			$('#price').hide();
			$('#payment').hide();
		}

		function checkFilter($this) {

			$('#product-form').validate();

			var checkedAtLeastOne,
				checkMain = false;
			$('.filter-box').each(function() {
				if ($(this).is(":checked")) {
					checkedAtLeastOne = true;
				}
			});


			if ($('input[name="main"]').val().length > 0) {

				checkMain = true;
			}
			if (!checkedAtLeastOne) {

				alert('Please select at least one color.');

			} else if ($('.dz-preview').length > 1 && !checkMain) {

				alert('Select main photo');

			} else {

				if ($('#product-form').valid()) {

					if ($($this).attr('value') != 'Preview') {
						<?php if (!empty(Yii::app()->session['user_id'])) { ?>
						$('input[name=choose_ad_type]').live("click", function() {

							$('#product-form').submit();
						});
						<?php } else { ?>
						$('#modalRegisterOrLogin').modal('show');
						<?php } ?>

					} else {
						$('input[name=preview]').live("click", function(){
							$('#product-form').submit();
						});
					}
				}
			}
			return false;
		}

		$('#Products_categories_id, #Categories_id').on('change', function() {

			$.ajax({
				type: 'GET',
				url: '<?php echo Yii::app()->request->baseUrl; ?>/products/getSubCats',
				data: {
					categories_id: $(this).val()
				},
				success: function(data) {
					var obj = $.parseJSON(data);

					var subs = '<div class="form-group">'+
						'<select onchange="getSubSubCats()" id="Products_subcategories_id" class="prof-form" name="Products[subcategories_id]">'+
						'<option value="">Choose subcategory</option>';

					$.each(obj, function(key, val) {
						subs += '<option value="'+key+'">'+val+'</option>';
					})
					subs += '</select></div>';
					$('#subs').html(subs);
					$('#subsubs').html('');
					$( "#Products_subcategories_id" ).rules( "add", {
						required: true
					});
					$('#product-form').valid();
					$(".table").css('display', 'none');
				}
			})
		});

		function getSubSubCats() {
			var id = $('#Products_subcategories_id :selected').val() != undefined ? $('#Products_subcategories_id :selected').val() : <?=$model->categories->parentCategory->id;?>;
//			var id = $(this).val();
			$.ajax({
				type: 'GET',
				url: '<?php echo Yii::app()->request->baseUrl; ?>/products/getSubSubCats',
				data: {
					id: id
				},
				success: function(data) {
					var obj = $.parseJSON(data);
                    console.log(obj)
					if (Object.keys(obj[0]).length > 0) {
						var subsubs = '<div class="form-group">'+
							'<select required="true" id="Products_subSubcategories_id" class="prof-form" name="Products[subSubcategories_id]">'+
							'<option value="">Choose sub subcategory</option>';

						$.each(obj[0], function(key, val) {
							subsubs += '<option value="'+key+'">'+val+'</option>';
						})
						subsubs += '</select></div>';
						$('#subsubs').html(subsubs);
						$( "#Products_subSubcategories_id" ).rules( "add", {
							required: true
						});
						$('#product-form').valid();
					} else {
						$('#subsubs').html('');
					}
					if (obj.filter_id != undefined) {
						$('#prod_table').remove();

						$('table').css('display', 'none');
						$("#table-"+obj.filter_id).css('display', 'table');
						$('input[name="sizes['+obj.filter_id+'][]"]').rules( "add", {
							required: true,
							messages: {
								required: "Please select at least one size."
							}
						});
						$('#product-form').valid();
					} else {
						$(".table").css('display', 'none');
					}
				}
			});
		}

		$('#subcategory_id').on('change', getSubSubCats);

		$('.colors .each-color input[type=checkbox]').on('change', function(){
			if($(this).is(':checked')) {
				$(this).parent().css({'box-shadow' : '0 0 0 2px #BFBFBF, 0 0 0 3px #525252'});
			} else {
				$(this).parent().css({'box-shadow' : 'none'});
			}
		});

		$('.colors input[type=checkbox').each(function() {
			if($(this).is(':checked')) {
				$(this).parent().css({'box-shadow' : '0 0 0 2px #BFBFBF, 0 0 0 3px #525252'});
			} else {
				$(this).parent().css({'box-shadow' : 'none'});
			}
		});

		$("#custom").spectrum({
			allowEmpty: true,
			change: function(color) {
				$("#custom").val(color.toHexString());
			}
		});

	</script>

	<style>
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		#map {
			width: 100%;
			height: 250px;
		}
		.controls {
			margin-top: 10px;
			border: 1px solid transparent;
			border-radius: 2px 0 0 2px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			height: 32px;
			outline: none;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}

		#pac-input {
			background-color: #fff;
			font-family: Roboto;
			font-size: 15px;
			font-weight: 300;
			margin-left: 12px;
			padding: 0 11px 0 13px;
			text-overflow: ellipsis;
			width: 300px;
		}

		#pac-input:focus {
			border-color: #4d90fe;
		}

		.pac-container {
			font-family: Roboto;
		}

		#type-selector {
			color: #fff;
			background-color: #4d90fe;
			padding: 5px 11px 0px 11px;
		}

		#type-selector label {
			font-family: Roboto;
			font-size: 13px;
			font-weight: 300;
		}

	</style>