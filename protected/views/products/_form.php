<?php
/* @var $this ProductsController */
/* @var $model Products */
/* @var $form CActiveForm */
?>

<div>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'product-form',
        'enableAjaxValidation' => false,
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model,'categories_id'); ?>

        <?php if (isset($category) || !empty($_POST['Products']['categories_id'])) {
            $category = Categories::model()->findByPk($_POST['Products']['categories_id']);
            echo $form->dropDownList(
                $category,
                'id',
                CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title'),
                array('class'=>'prof-form required', 'empty' => 'Choose category', 'name' => 'Products[categories_id]'));
         }  else {
            echo $form->dropDownList($model,'categories_id', $categories, array('class'=>'prof-form', 'empty' => 'Choose category'));
         } ?>
    </div>

    <div id="subs">
        <?php if (isset($subcategory) || !empty($_POST['Products']['subcategories_id'])) {
            $subcategory = Categories::model()->findByPk($_POST['Products']['subcategories_id']);
        ?>
            <div class="form-group">
                <?php echo $form->dropDownList(
                    $subcategory,
                    'id',
                    CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$category->id)), 'id', 'title'),
                    array('class'=>'prof-form required', 'empty' => 'Choose subcategory', 'id' => 'subcategory_id', 'name' => 'Products[subcategories_id]')
                ); ?>
            </div>
        <?php } ?>
    </div>
    <div id="subsubs">
        <?php if (isset($subcategory) || !empty($_POST['Products']['subSubcategories_id'])) { ?>
            <div class="form-group">
                <?php echo $form->dropDownList(
                    $model,
                    'categories_id',
                    CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id='.$subcategory->id)), 'id', 'title'),
                    array('class'=>'prof-form required', 'empty' => 'Choose sub subcategory', 'name' => 'Products[subSubcategories_id]')
                ); ?>
            </div>
        <?php } ?>
    </div>

    <div class="form-group colors">

        <?php $colors = json_decode(FiltersNew::model()->find('type=1')->filters); ?>

        <?php foreach ($colors as $key=>$color) {
            $check = false;
            if (isset($_POST['colors'])) {
                foreach ($_POST['colors']['color'] as $k => $v) {
                    if ($v == $color) {
                        $check = true;
                    }
                }
            } ?>

            <div class="each-color">
                <label for="<?='color'.$key; ?>" style="border:1px dotted #545454;background: <?php echo $color; ?>; height: 22px; width: 22px;border-radius: 11px;"><?php echo CHtml::checkBox('colors[color][]', $check?'checked':'', array('value' => $color, 'class' => 'filter-box', 'id' => 'color'.$key)); ?></label>
            </div>
        <?php } ?>

        <?php
        if (isset($_POST['colors'])) {
            $arr = [];
            foreach ($_POST['colors']['color'] as $k=>$c) {
                if (!in_array($c, $colors) && $c != '') {
                    $arr[$k] = $c;
                }
            }
            foreach ($arr as $a) { ?>

            <div class="each-color">
                <label for="<?='color'.++$key; ?>" style="border:1px dotted #545454;background: <?php echo $a; ?>; height: 22px; width: 22px;border-radius: 11px;">
                    <?php echo CHtml::checkBox('colors[color][]', 'checked', array('value' => $a, 'class' => 'filter-box', 'id' => 'color'.$key)); ?>
                </label>
            </div>
        <?php }
        } ?>

    </div>
    <br><br><br>
    <div class="form-group other-colr">
        Other color: <input type='text' id="custom" name="colors[color][]" />
    </div>

    <div id="filters-add" class="form-group">
        <?php $this->renderPartial('_sizes'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo CHtml::activeTextField($model,'name',array('size'=>60,'maxlength'=>150, 'class'=>'prof-form')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo CHtml::activeTextArea($model,'description',array('class'=>'prof-form', 'style' => 'height: 136px;')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'location'); ?>
        <?php echo $form->textField($model,'location',array('class'=>'controls', 'id' => 'pac-input', 'placeholder' => 'Enter Location', 'required' => true,
            'value' => (isset($_POST['Products']['location']) ? CHtml::encode($_POST['Products']['location']) : ''))); ?>
        <div id="map"></div>
        <input type="hidden" id="startLat" value="<?php echo isset($model->lat) ? $model->lat : (isset($_POST['lat']) ? $_POST['lat'] : ''); ?>" name="lat">
        <input type="hidden" id="startLng" value="<?php echo isset($model->lng)?$model->lng : (isset($_POST['lng']) ? $_POST['lng'] : ''); ?>" name="lng">
    </div>

    <div class="clearfix">
        <div class="form-group col-md-4 col-sm-4">
            <?php echo $form->dropDownList($model,'new',array(1=>'new',0 => 'used'), array('class'=>'prof-form', 'options' =>
                (isset($_POST['Products']['new']) ? array($_POST['Products']['new']=>array('selected'=>true)) : ''))); ?>
        </div>
    </div>
    <div class="clearfix">
        <div class="form-group">
            <label class="col-sm-2" for="givaway">Giveaway</label>
            <input type="checkbox" id="givaway" name="giveaway" <?=isset($_POST['giveaway'])&&$_POST['giveaway']=='on' ? 'checked' : '';?>>
            <!--            --><?//= CHtml::checkBox($model, 'giveaway', array('name' => 'giveaway', 'id' => 'giveaway')); ?>
        </div>
    </div>
    <div class="clearfix">
        <div class="form-group col-md-4 col-sm-4" id="price">
            <?php echo $form->labelEx($model,'price'); ?>
            <div class="prof-form valid">
                <div class="col-xs-1 no-padding-right no-padding-left">
                    <span class="pull-left" >€</span>
                </div>
                <div class="col-xs-11 no-padding-right no-padding-left">
                    <?php echo $form->textField($model,'price',array('type' => 'number', 'size'=>10,'maxlength'=>10, 'class'=>'input-text no-border', 'value' =>
                        isset($_POST['Products']['price']) ? CHtml::encode($_POST['Products']['price']) :'')); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group" id="payment">
       <?php $payments = json_decode($model->payment);
            $payments2 = (isset($_GET['advert']['payment']) ? $_GET['advert']['payment'] : []);
       ?>
        <div class="clearfix">
            <div class="col-sm-3"><?php echo $form->labelEx($model,'payment'); ?></div>
            <div class="col-sm-3"><?php echo CHtml::checkBox('payment[]', (isset($payments) && in_array('cash on delivery', $payments) || in_array('cash on delivery', $payments2))?'checked':'', array('value' => 'cash on delivery', 'id' => 'payment_delivery')); ?><label for="payment_delivery">cash on delivery</label></div>
            <div class="col-sm-3"><?php echo CHtml::checkBox('payment[]', (isset($payments) && in_array('pay pall', $payments) || in_array('pay pall', $payments2))?'checked':'', array('value' => 'pay pall', 'id' => 'pay-pall')); ?><label for="pay-pall">pay-pall</label></div>
            <div class="col-sm-3"><?php echo CHtml::checkBox('payment[]', (isset($payments) && in_array('bank transfer', $payments)) || in_array('bank transfer', $payments2)?'checked':'', array('value' => 'bank transfer', 'id' => 'bank_transfer')); ?><label for="bank_transfer">bank transfer</label></div>
        </div>
    </div>

    <div class="form-group">
        <?php $delivery = json_decode($model->delivery);
            $delivery2 = (isset($_GET['advert']['delivery']) ? $_GET['advert']['delivery'] : []);
        ?>
        <div class="clearfix">
            <div class="col-sm-3"><?php echo $form->labelEx($model, 'delivery'); ?></div>
            <div class="col-sm-3"><?php echo CHtml::checkBox('delivery[]', isset($delivery) && in_array('personally', $delivery)  || in_array('personally', $delivery2)?'checked':'', array('value' => 'personally', 'id' => 'personally')); ?><label for="personally">Personally</label></div>
            <div class="col-sm-3"><?php echo CHtml::checkBox('delivery[]', isset($delivery) && in_array('by post', $delivery)  || in_array('by post', $delivery2)?'checked':'', array('value' => 'by post', 'id' => 'by_post')); ?><label for="by_post">by post</label></div>
            <div class="col-sm-3"><?php echo CHtml::checkBox('delivery[]', isset($delivery) && in_array('via currier', $delivery)  || in_array('via currier', $delivery2)?'checked':'', array('value' => 'via currier', 'id' => 'via_currier')); ?><label for="via_currier">via currier</label></div>
        </div>
    </div>

    <div class="form-group">

        <div class="uploaded-photos col-md-12">
            <div id="img-1" class=" ">
            </div>
            <?php if (isset($_POST['main'])) {
                $main = ProductsMedia::model()->findByPk($_POST['main']); ?>
                <input id="main" type="hidden" value="<?=$main->id;?>">
            <?php } ?>

            <?php if (isset($_POST['file'])) { ?>
                    <?php foreach ($_POST['file'] as $key=>$file) {
                        $advImage = ProductsMedia::model()->findByPk($file);
                        if ($advImage && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $advImage['photo'])) { ?>
                            <input type="hidden" class="up-images" data-id="<?=$advImage->id;?>" data-name="<?=$advImage->photo;?>" data-order="<?=$advImage->sort_order;?>">
                        <?php } ?>
                <?php } ?>
            <?php } ?>

            <?php if (isset($_POST['video_file'])) {
                    $adVideo = ProductVideos::model()->findByAttributes(array('title' => $_POST['video_file']));

                    if ($adVideo && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_videos/' . $adVideo['title'])) { ?>
                        <input type="hidden" class="up-video" data-id="<?=$adVideo['id'];?>" data-name="<?=$adVideo['title'];?>">
                    <?php } ?>
            <?php } ?>
        </div>

        <div>
            <div class="dropzone dropzone-previews" id="my-awesome-dropzone">
                <div class="dz-message" data-dz-message><span>Drop or <a>click here</a> to upload photos</span></div>
                <div class="fallback">
                    <input name="file[]" type="file" multiple />
                </div>
            </div>
            <input type="hidden" name="main" value="<?=isset($_POST['main']) ? $_POST['main'] : ''?>">
            <br>
        </div>
    </div>

    <div class="form-group">
        <label>Put video link from Youtube or Vimeo</label>
        <?php echo $form->textField($model,'video_link',array('class'=>'prof-form', 'value' => isset($_POST['Products']['video_link']) ? CHtml::encode($_POST['Products']['video_link']) :'')); ?>
    </div>

    <div class="form-group">
        <label>Or upload your own</label>

        <div class="dropzone dropzone-video-previews" id="dropzone-video">
            <div class="dz-message" data-dz-message><span>Drop or <a>click here</a> to upload a video</span></div>
            <div class="fallback">
                <input type="file" accept="video/*" name="video_file" />
            </div>
        </div>
    </div>

    <div class="clearfix buttons text-right">

        <?php if (Yii::app()->session['user_id']) { ?>
            <?php echo CHtml::submitButton('Preview', array('class' => 'btn btn-primary', 'name' => 'preview', 'onclick'=>"checkFilter(this);return false;")); ?>
            <?php echo CHtml::submitButton('Next: Choose ad type', array('id' => 'btn-next', 'name' => 'choose_ad_type', 'class' => 'btn btn-warning', 'onclick'=>"checkFilter(this);return false;")); ?>
        <?php } else { ?>
            <?php echo CHtml::submitButton('Next', array('id' => 'btn-next', 'name' => 'choose_ad_type', 'class' => 'btn btn-warning', 'data-target' => '#modalRegisterOrLogin', 'onclick'=>"checkFilter(this);return false;")); ?>
        <?php } ?>
    </div>

    <div>
        <div class="form-group">&nbsp;</div>
    </div>

    <?php $this->endWidget(); ?>
</div>

<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #map {
        width: 100%;
        height: 250px;
        margin-bottom: 20px;
    }
    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dropzone.css" media="all" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
<link rel='stylesheet' href='<?php echo Yii::app()->request->baseUrl; ?>/css/spectrum.css' />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/dropzone.js"></script>
<script src='<?php echo Yii::app()->request->baseUrl; ?>/js/spectrum.js'></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjB6-iML5AvdAI3Y6ZUd1UT1jkiD_eXa8&libraries=places&callback=initAutocomplete"
        async defer></script>

<script>
    var file_ids = [];
    var sort_order = 0;

    Dropzone.options.myAwesomeDropzone = {

        url: '<?php echo $this->createAbsoluteUrl('products/uploadPhoto'); ?>',
        previewsContainer: ".dropzone-previews",
        uploadMultiple: true,
        parallelUploads: 10,
        maxFiles: 10,
        maxFilesize: 10,
        addRemoveLinks: true,
        removedfile: function(file) {

            if (file.id !== undefined) {
                $.ajax({
                    type: 'GET',
                    url: '<?php echo $this->createAbsoluteUrl('products/deletePhoto'); ?>',
                    data: {
                        photo_id: file.id,
                        media: 'photo'
                    },
                    success: function(data) {
                        if ($('input[name="main"]').val()==file.id) {
                            $('input[name="main"]').attr('value', '');
                        }
                    }
                });
            }
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {

            file.id = $.parseJSON(response);

            if ($('input[name=main]').val() == '') {
                $('input[name=main]').val(file.id);
                $('#my-awesome-dropzone').find($('.dz-image')).css({'border': '1px #f03e62 solid'});
            }

            $(file.previewTemplate).append('<span class="dz-main " onclick="makeMain('+file.id+', this)" data-dz-main="">Set as main</span>');
            $(file.previewTemplate).append('<input id="'+file.id+'" type="hidden" name="file[]" value="'+file.id+'">');

            file_ids.push(file.id);

            $(file.previewTemplate).append('<input type="hidden" name="sort_order[]" value="'+sort_order+'">');

            sort_order++;

        },
        init: function() {
            var $this = this;
            this.on("sending", function(file, xhr, formData) {
                formData.append('sort_order', sort_order);
            });

            $.each($('.up-images'), function(i, v) {

                var mockFile = { name: $(v).attr('data-name'),  type: 'image/jpeg' };
                $this.options.addedfile.call($this, mockFile);
                $this.options.thumbnail.call($this, mockFile, "<?=Yii::app()->request->baseUrl;?>/images/product_images/"+ $(v).attr('data-name'));
                mockFile.previewElement.classList.add('dz-success');
                mockFile.previewElement.classList.add('dz-complete');
                $(mockFile.previewElement).append('<span class="dz-main " onclick="makeMain('+$(v).attr('data-id')+', this)" data-dz-main="">Set as main</span>');
                $(mockFile.previewElement).append('<input id="'+$(v).attr('data-id')+'" type="hidden" name="file[]" value="'+$(v).attr('data-id')+'">');
                $(mockFile.previewElement).append('<input type="hidden" name="sort_order[]" value="'+$(v).attr('data-order')+'">');
                file_ids.push($(v).attr('data-id'));
                if ($(v).attr('data-id') == $('#main').val()) {
                    $(mockFile.previewElement).find('.dz-image').css({'border': '1px #f03e62 solid'});
                    $('input[name="main"]').attr('value', $(v).attr('data-main'));
                }
            });
        }
    }

    Dropzone.options.dropzoneVideo = {

        url: '<?php echo $this->createAbsoluteUrl('products/uploadVideo'); ?>',
        previewsContainer: ".dropzone-video-previews",
        uploadMultiple: false,
        maxFilesize: 10,
        addRemoveLinks: true,
        acceptedFiles: ".mp4,.mkv,.avi",
        removedfile: function(file) {

            if (file.id !== undefined) {
                $.ajax({
                    type: 'GET',
                    url: '<?php echo $this->createAbsoluteUrl('products/deletePhoto'); ?>',
                    data: {
                        video_id: file.id,
                        media: 'video'
                    }
                });
            }
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {
            file.id = $.parseJSON(response);
            $(file.previewTemplate).append('<input type="hidden" name="video_file" value="'+file.id+'">')
        },
        init: function() {

            if ($('.up-video').length > 0) {
                var $this = this;
                var mockFile = { name: $('.up-video').attr('data-name'), type: 'video/*' };
                $.each($('.up-video'), function(i, v) {
                    $this.options.addedfile.call($this, mockFile);
                    mockFile.previewElement.classList.add('dz-success');
                    mockFile.previewElement.classList.add('dz-complete');
                    $(mockFile.previewElement).append('<input id="' + $(v).attr('data-id') + '" type="hidden" name="video_file" value="' + $(v).attr('data-name') + '">');
                })
            }

        }
    }

    $('#my-awesome-dropzone').sortable({
        axis: 'x',
        cursor: 'move',
        distance: 5,
        opacity: 0.6,
        update: function (event, ui) {

            var orderData = [];

            $.each($('#my-awesome-dropzone').find('.dz-preview'), function(i,v) {
                var p = $(v).find('input[name="file[]"]').val()
                orderData.push({order: i, photo: p})
            });

            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createAbsoluteUrl('products/sortPhotos'); ?>',
                data: {
                    sort_order: orderData
                },
                success: function() {}
            });
        }
    });

    function makeMain($id, $this) {

        $.ajax({
            type: 'GET',
            url: '<?php echo $this->createAbsoluteUrl('products/setMainPhoto'); ?>',
            data: {
                all_id: file_ids,
                set_main: $id
            },
            success: function(data) {
                $($this).parents().find('.dz-image').css({'border': ''});
                $($this).parent().find('.dz-image').css({'border': '1px #f03e62 solid'});
                $('input[name="main"]').attr('value', $id)
            }
        });
    }

    function getAddressFromLatLang(lat,lng) {

        var geocoder = new google.maps.Geocoder();
        var latLng = new google.maps.LatLng(lat, lng);

        geocoder.geocode( { 'latLng': latLng}, function(results, status) {

            document.getElementById('pac-input').value = results[0]['formatted_address'];

        });
    }

    function initAutocomplete() {

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?php echo $model->lat?$model->lat: 48.1500; ?>, lng: <?php echo $model->lng?$model->lng: 17.1167; ?>},
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {

                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                map.setCenter(pos);

                var marker = new google.maps.Marker({
                    position: {lat: pos.lat, lng: pos.lng},
                    map: map
                });


                var infowindow = new google.maps.InfoWindow({
                    content: marker.getPosition()+''
                });

                document.getElementById('startLat').value = pos.lat;
                document.getElementById('startLng').value = pos.lng;

                getAddressFromLatLang(pos.lat, pos.lng)

            })
        }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];

        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            places.forEach(function(place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }

                document.getElementById('startLat').value = place.geometry.location.lat();
                document.getElementById('startLng').value = place.geometry.location.lng();
            });

            map.fitBounds(bounds);
        });
    }

    var catPrev, namePrev = false;
    $('#Products_name, #Products_description').on('keyup', function() {

        if ($('#Products_name').val().length>0 && $('#Products_description').val().length>0) {
            namePrev = true;
        } else {
            namePrev = false;
        }
        if (catPrev && namePrev) {
            $('#preview-btn').attr('disabled', false);
        } else {
            $('#preview-btn').attr('disabled', true);
        }
    });

	$('#Products_categories_id, #Categories_id').change(function() {

		$.ajax({
			type: 'GET',
			url: '<?php echo Yii::app()->request->baseUrl; ?>/products/getSubCats',
			data: {
				categories_id: $(this).val()
			},
			success: function(data) {
				var obj = $.parseJSON(data);

				var subs = '<div class="form-group">'+
							'<select onchange="getSubSubCats()" id="Products_subcategories_id" class="prof-form" name="Products[subcategories_id]">'+
								'<option value="">Choose subcategory</option>';

				$.each(obj, function(key, val) {
					subs += '<option value="'+key+'">'+val+'</option>';
				})
				subs += '</select></div>';
				$('#subs').html(subs);
                $('#subsubs').html('');
                $( "#Products_subcategories_id" ).rules( "add", {
                    required: true
                });
                $('#product-form').valid();
                $(".table").css('display', 'none');
			}
		})
	});


    function getSubSubCats() {

        if ($('#Products_subcategories_id :selected').val().length>0) {
            catPrev = true;
        } else {
            catPrev = false;
        }
        if (catPrev && namePrev) {
            $('#preview-btn').attr('disabled', false);
        } else {
            $('#preview-btn').attr('disabled', true);
        }
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/getSubSubCats',
            data: {
                id: $('#Products_subcategories_id :selected').val()
            },
            success: function(data) {
                var obj = $.parseJSON(data);

                if (Object.keys(obj[0]).length > 0) {
                    var subsubs = '<div class="form-group">'+
                        '<select onchange="getSubSubCatFilters()" required="true" id="Products_sub_subcategories_id" class="prof-form" name="Products[subSubcategories_id]">'+
                        '<option value="">Choose sub subcategory</option>';

                    $.each(obj[0], function(key, val) {
                        subsubs += '<option value="'+key+'">'+val+'</option>';
                    })
                    subsubs += '</select></div>';
                    $('#subsubs').html(subsubs);
                    $( "#Products_sub_subcategories_id" ).rules( "add", {
                        required: true
                    });
                    $('#product-form').valid();
                } else {
                    $('#subsubs').html('');
                }
                if (obj.filter_id != undefined) {
                    $('table').css('display', 'none');
                    $("#table-"+obj.filter_id).css('display', 'table');
                    $('input[name="sizes['+obj.filter_id+'][]"]').rules( "add", {
                        required: true,
                        messages: {
                            required: "Please select at least one size."
                        }
                    });
                    $('#product-form').valid();
                } else {
                    $(".table").css('display', 'none');
                }
            }
        });
    }

    function getSubSubCatFilters() {
//        $.ajax({
//            type: 'GET',
//            url: '<?php //echo Yii::app()->request->baseUrl; ?>///products/getSubsubFilters',
//            data: {
//                id: $('#Products_sub_subcategories_id').val()
//            },
//            success: function(data) {
//                var obj = $.parseJSON(data);
//
//                if (obj.length > 0) {
//                    var filters = '';
//                        $.each(obj, function(key, val) {
//                            filters += val.filters['group_title'].bold();
//
//                            $.each (obj[key].filters, function(k,v) {
//                                filters += k!='group_title' ? '<input class="filter-box filtersAdditional" type="checkbox" name="filtersAdditional[]" value="'+k+'">'+v : '<br>';
//                            });
//                        })
//                    $('#filters-add').html(filters);
//                } else {
//                    $('#filters-add').html('');
//                }
//            }
//        });
    }

	$('#filter_group').change(function() {

		var group_id = $('#filter_group :selected').val();

        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/getFilters',
            data: {
                filter_group_id: group_id
            },
            success: function(data) {
                var obj = $.parseJSON(data);

                var filters = '<div class="form-group">'+
                        '<label>Filters</label>'+
                        '<select id="filter_id" class="prof-form" name="ProductFilter['+i+'][filter_id]" >';

                $.each(obj, function(key, val) {

                    filters += '<option value="'+key+'">'+val+'</option>';
                })
                filters += '</select><button onclick="" id = "'+group_id+'" class="btn btn-danger btn-sm del-filter" type="button"><i class="fa fa-minus-circle"></i></button></div>';
                $('#filters').append(filters)

            }
        })
	});

	$('#filters').on('click', '.del-filter', function() {

		$(this).parent().remove();

        if ($(this).attr('filter-id')) {
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createAbsoluteUrl('products/update', array('id' => $model->id)); ?>',
                data: {
                    filter_id: $(this).attr('filter-id')
                },
                success() {}
            });
        }
	});


	$('#givaway').change(function() {
		$('#price').toggle();
        $('#payment').toggle();


    });

    if ($('#givaway').is(":checked")) {
        $('#price').hide();
        $('#payment').hide();
    }

    <?php if (empty(Yii::app()->session['user_id'])) { ?>

        $('input[name="choose_ad_type"]').on('click', function(e) {

            if ($('#product-form').valid()) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/products/createTempProduct',
                    data: $('#product-form').serialize(),
                    success: function (data) {

                    }
                });
            }
        });
    <?php } ?>


    function checkFilter($this) {

        $('input[value="Create"]').attr('data-toggle', '');
        $('#product-form').validate();

        var checkedAtLeastOne,
            checkMain = false;
        $('.filter-box').each(function() {
            if ($(this).is(":checked")) {
                checkedAtLeastOne = true;
            }
        });
//        if ($('.jscolor').val().length > 0) {
//            checkedAtLeastOne = true;
//        }

        if ($('input[name="main"]').val().length > 0) {

            checkMain = true;
        }
        if (!checkedAtLeastOne) {

            alert('Please select at least one color.');

        } else if ($('.dz-preview').length > 1 && !checkMain) {

            alert('Select main photo');

        } else {

            if ($('#product-form').valid()) {

                if ($($this).attr('value') != 'Preview') {
                    <?php if (!empty(Yii::app()->session['user_id'])) { ?>
                       $('input[name=choose_ad_type]').live("click", function() {

                            $('#product-form').submit();
                       });
                    <?php } else { ?>
                        $('#modalRegisterOrLogin').modal('show');
                    <?php } ?>

                } else {
                    $('input[name=preview]').live("click", function(){
                        $('#product-form').submit();
                    });
                }
            }
        }
        return false;
    }

    $(document).on('click', '.delete', function() {
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: '<?php echo $this->createAbsoluteUrl('products/deletePhoto'); ?>',
            data: {
                photo_id: $this.parent().find('.main').attr('id')
            },
            success() {
                $this.next().remove();
                $this.remove();
                if(!$('.upload').length || $('.upload').length < 10) {
                    $('.qq-upload-button').show();
                }
            }
        });
    });

    $('.colors .each-color input[type=checkbox]').on('change', function(){
        if($(this).is(':checked')) {
            $(this).parent().css({'box-shadow' : '0 0 0 2px #BFBFBF, 0 0 0 3px #525252'});
        } else {
            $(this).parent().css({'box-shadow' : 'none'});
        }
    });

    $('.colors input[type=checkbox').each(function() {
        if($(this).is(':checked')) {
            $(this).parent().css({'box-shadow' : '0 0 0 2px #BFBFBF, 0 0 0 3px #525252'});
        } else {
            $(this).parent().css({'box-shadow' : 'none'});
        }
    });

    $("#custom").spectrum({
        allowEmpty: true,
        change: function(color) {
            $("#custom").val(color.toHexString());
        }
    });

</script>

<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #map {
        width: 100%;
        height: 250px;
    }
    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>
