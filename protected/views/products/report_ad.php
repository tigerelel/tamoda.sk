<?php 
$product_id = isset($_GET['product_id']) ? $_GET['product_id'] : 0;
 ?>

<div id="main">
    <div id="helpWrapper" class="container">
        <div class="col-md-offset-1">
            <div id="helpHeader">
                <div id="helpBreadcrumbs">
                    <h1>Report Advert</h1>
                </div>
            </div>
            <div class="main-holder">
                <div id="helpContent">
                    <div class="report_intro">
                        <p>
                            Hi, in order to help us help you with your issue/report,
                            please select the relevant issue below where we have added some
                            details of what you can do here and what we need from you.
                        </p>

                        <br/>
                        <b>
                            Reporting a site member
                        </b>
                        <p>
                            Before sending this report please be sure that you re-familiarize yourself with some of our house rules below
                        </p>
                        <p>
                            If you are involved in a dispute please note:
                        </p>
                        <ul class="unordered">
                            <li>
                                If a sale was agreed on the thread and you are not happy with the result please leave feedback for the other site member
                            </li>
                            <li>
                                Regarding a refund or the exchange of goods purchased please be aware that your interactions with other users, including payment and delivery of goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and the other users. Adverts.ie accepts no responsibility for transactions that take place on the site.
                            </li>
                        </ul>

                    </div>
                    <div id="accordion">
                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">Wrong category</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="Wrong category" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                            <textarea placeholder="Describe your issue" data-error-msg="Feedback must be 75 words or less"
                                                      data-limit-type="words" data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="category report btn btn-default" data-type=1 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">No valid asking price</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="No valid asking price" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                            <textarea placeholder="Describe your issue" data-error-msg="Feedback must be 75 words or less"
                                                      data-limit-type="words" data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="price report btn btn-default" data-type=2 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">Duplicate ad</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="Duplicate ad" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                           <textarea placeholder="Describe your issue" data-error-msg="Feedback must be 75 words or less"
                                                     data-limit-type="words" data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="duplicate-ad report btn btn-default" data-type=3 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">Property ad</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="Property ad" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                            <textarea placeholder="Describe your issue" data-error-msg="Feedback must be 75 words or less"
                                                      data-limit-type="words" data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="property-ad report btn btn-default" data-type=4 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">Inappropriate material</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="Inappropriate material" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                            <textarea placeholder="Could you please specify what is inappropriate about the ad" data-error-msg="Feedback must be 75 words or less"
                                                      data-limit-type="words" data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="material report btn btn-default " data-type=5 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">Copyright infringement</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="Copyright infringement" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                            <textarea
                                                placeholder="Describe your issue" data-error-msg="Feedback must be 75 words or less" data-limit-type="words"
                                                data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="copyright report btn btn-default" data-type=6 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="accordionContent">
                            <h3>
                                <a href="#">Suspected stolen item</a>
                            </h3>
                            <div class="boxContent">
                                <ul>
                                    <li>
                                        <a class ="stolen report" data-type=7 href="#">
                                            Reporting a stolen item</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Commercial Seller -->
                        <div class="accordionContent" class="report">
                            <h3>
                                <a href="#">Not marked as sold</a>
                            </h3>
                            <div class="boxContent" style="margin:10px 0">
                                <form id="reportContentForm" method="post" class='reportForm' enctype="multipart/form-data" action="">
                                    <input type="hidden" name="reason" value="Not marked as sold" />
                                    <table class="modal_form">
                                        <tr>
                                            <td>
                                            <textarea placeholder="Describe your issue" data-error-msg="Feedback must be 75 words or less"
                                                      data-limit-type="words" data-max="75"  rows="5" cols="40" id="message" name="message" class="limitInput" ></textarea>
                                                <div class="char_counter">Words: <span class="limit_counter">0</span>/75</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="submit" id="btn-reportcontent" class="no-sold report btn btn-default" data-type=8 value="Report" />
                                                <img id="loader-reportcontent" class="loader hidden" src="http://c3.adverts.ie/i/ajax-loader.gif" alt="Loading" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/report_ad.css">
<!-- <script src='http://c1.adverts.ie/js/report/report-content.js'></script> -->

    <!-- Setting data for JS  -->
    <script type="text/javascript">

        var pageConfig = (function(init_data){
             // The object to be returned
            var PageConfig = function(){
                this.data = init_data;
                // Returns the config object
                this.get = function(to_get)
                {
                    // Extend to get a clone of the object & prevent passing by reference
                    return jQuery.extend(true, {}, this.data);
                }
            };
            return new PageConfig();
        })({"production":true});

    </script>
    <script type="text/javascript">
        $(function(){
            allPanels=$('.accordionContent > div').hide();
            $(".accordionContent > h3").on('click',function(e){
                if($(this).next('div:visible').css('display')=='block'){
                        $(this).next('div:visible').slideUp("fast");$(this).css('border-bottom','none');
                    }
                else{
                    $(this).css('border-bottom','1px solid #dddddd');$(this).next('div:hidden').slideDown("fast");
                }
                return false;
            });
        });
    </script>
    <script>
    $(function () {
        $('.report').on('click', function(e){
            e.preventDefault();
            var url ="<?=$this->createUrl('products/createreport')?>";
            var self = $(this);
            var text = self.closest('div').find('#message').val();
            var class_name = self.attr('class').split(' ')[0];
            var type = $(this).data("type");
            
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    type: type,
                    product_id: '<?=$product_id?>',
                    text: text
                },
                success: function(data){
                    return true;
                }
            });
        });
    });
</script>
    

