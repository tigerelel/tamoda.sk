<section class="single-product-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-12">
            </div>
        </div>
        <div class="row">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'product-preview-form'
            )); ?>
                <div class="col-md-8 col-sm-12 col-md-offset-2">
                    <div class="gallery-sm col-sm-12">
                        <div class="col-sm-5 col-md-5 no-padding">
                            <h2 class="pro-name text-center"><?php echo CHtml::encode($product['name']); ?></h2>
                            <input type="hidden" value="<?php echo isset($product['name'])?CHtml::encode($product['name']):''; ?>" name="Products[name]">
                            <div class="col-md-offset-1">
                                <div id="img-1" class="col-md-offset-2">
                                    <a href="" class="">
                                        <?php if (!empty($mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $mainPhoto->photo)) {
                                            echo CHtml::image(Yii::app()->request->baseUrl . '/timthumb.php?src=/images/product_images/' .  $mainPhoto->photo.'&h=300&w=300', ''); ?>
                                            <input id="<?php echo $mainPhoto->id; ?>" type="hidden" name="main" value="<?php echo $mainPhoto->id; ?>">
                                        <?php } else { ?>
                                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg'); ?>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-7 col-md-7">
                            <div class="prod-list-detail">
                                <div class="prod-info">
                                    <div class="price-box">
                                        <div class="price form-group">
                                            <?php if (!isset($_POST['giveaway'])) { ?>
                                                <span id="old-price" style="">€<?=$product['price']; ?></span>
                                                <input type="hidden" value="<?php echo isset($product['price'])?$product['price']:''; ?>" name="Products[price]">
                                            <?php } else { ?>
                                                Free
                                                <input type="hidden" value="1" name="giveaway">
                                            <?php } ?>
                                        </div>
                                    </div>

                                     <span class="prod-meta form-group"><strong>COLOUR :</strong>
                                         <?php foreach ($filters as $key=>$val): ?>
                                             <span style="background: <?=$val;?>;padding: 0 15px;margin-left: 10px;"></span>
                                             <input type="hidden" value="<?=$val;?>" name="colors[color][]">
                                         <?php endforeach; ?>
                                     </span>
                                    <?php
                                    if ($_POST['sizes']) { ?>
                                        <span class="prod-meta form-group"><strong>SIZE :</strong>
                                            <?php
                                            $newF = [];
                                            foreach ($_POST['sizes'] as $key => $size) {
                                                $test = json_decode(FiltersNew::model()->findByPk($key)->filters);
                                                foreach ($test as $k => $f) {
                                                    foreach ($size as $s) {
                                                        if ($f->id == $s) {
                                                            $newF[$s] = $f;
                                                        }
                                                    }
                                                }
                                            }
//                                            ?>

                                         <span class="prod-meta form-group"><strong>SIZE :</strong>
                                             <?php $i = 0;
                                             foreach ($newF as $k => $s) {
                                                 foreach ($s as $k2=>$val) {

                                                     if ($k2 == 'EUR' && !is_array($val)) {
                                                         $i++;
                                                         echo $val;
                                                     } else {
                                                         if ($k2 == 'standard') {
                                                             $i++;
                                                             echo $val;
                                                         }
                                                     }
                                                 }
                                                 if ($i < count($newF)) {
                                                     echo ', ';
                                                 }
                                             } ?>
                                        <input type="hidden" value="<?=CHtml::encode($sizes);?>" name="sizes">
                                    <?php } ?>

                                     <span class="prod-meta form-group"><strong>TYPE :</strong>
                                         <span style="margin-left: 10px;"><?= $product['new']==1?'New':'Used'; ?></span>
                                         <input type="hidden" value="<?php echo isset($product['new'])?$product['new']:''; ?>" name="Products[new]">
                                     </span>
                                     <span class="prod-meta form-group"><strong>CREATED :</strong>
                                         <span style="margin-left: 10px;"><?=date('d-m-Y H:i');?></span>
                                     </span>
                                     <?php if (!empty($_POST['Products']['video_link']) || !empty ($_POST['video_file'])) { ?>
                                         <span class="prod-meta form-group"><strong>VIDEO :</strong>
                                            <span style="margin-left: 10px;">
                                                <?php if (!empty($_POST['Products']['video_link'])) { ?>
                                                    <span id="my-video">
                                                        <a class="video"  title="" href="<?=$_POST['Products']['video_link'];?>">
                                                            <i class="fa fa-youtube-play fa-2" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                    <input type="hidden" name="Products[video_link]" value="<?=CHtml::encode($_POST['Products']['video_link']);?>">
                                                <?php } ?>

                                                <?php if (isset($_POST['video_file']) && !empty($_POST['video_file'])) { ?>
                                                    <span id="my-video_file">
                                                         <a class="video"  title="" href="<?=Yii::app()->request->baseUrl . '/images/product_videos/'.$_POST['video_file'];?>">
                                                             <i class="fa fa-play-circle fa-2" aria-hidden="true"></i>
                                                         </a>
                                                    </span>
                                                    <input type="hidden" name="video_file" value="<?=$_POST['video_file'];?>">
                                                <?php } ?>
                                            </span>
                                         </span>
                                    <?php } ?>

                                    <span class="prod-meta form-group"><strong>SELLER :</strong>
                                        <?php $user = Users::model()->findByPk(Yii::app()->session['user_id']); ?>
                                         <span style="margin-left: 10px;"><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $user->username; ?>" class=""><?=$user->username; ?></a></span>
                                         (<?php echo isset($user->reviews) ? count($user->reviews):''; ?> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/raty/star-on.png" alt="" title="">)
                                     </span>

                                    <?php $category = Categories::model()->findByAttributes(array('id' => $_POST['Products']['subSubcategories_id'])); ?>
                                    <input type="hidden" value="<?php echo $category->id; ?>" name="Products[subSubcategories_id]">
                                    <input type="hidden" value="<?php echo $category->parentCategory->id; ?>" name="Products[subcategories_id]">
                                    <input type="hidden" value="<?php echo $category->parentCategory->parent_id; ?>" name="Products[categories_id]">
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($photos)) { ?>
                        <div class="col-sm-12 col-md-12 gallery-sm" style="margin-top: 15px;">
                            <div class="single-zoom-thumb">
                                <ul class="zoom-slider" id="gallery_01">
                                    <?php foreach ($photos as $media) { ?>
                                        <li>
                                            <a class="fancybox-button" rel="fancybox-button" href="<?php echo Yii::app()->request->baseUrl.'/images/product_images/'.$media->photo;?>">
                                                <img src="<?=Yii::app()->request->baseUrl.'/timthumb.php?src=/images/product_images/'.$media->photo;?>&h=400&w=400">
                                            </a>
                                            <input id="<?= $media->id; ?>" type="hidden" name="file[]" value="<?= $media->id; ?>">
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="col-sm-5 gallery-sm" style="margin-top: 15px;">
                        <span class="prod-meta"><strong>DESCRIPTION :</strong>
                            <span class="prod-des"><?php echo CHtml::encode($product['description']); ?></span>
                        </span>
                        <input type="hidden" value="<?php echo isset($product['description'])?CHtml::encode($product['description']):''; ?>" name="Products[description]">
                        <br>
                        <?php if (!empty($_POST['payment'])) { ?>

                            <span class="prod-meta"><strong>PAYMENT :</strong>
                                <?php foreach ($_POST['payment'] as $key=>$val)  { ?>
                                    <?php echo $val!=end($_POST['payment'])?$val.', ':$val; ?>
                                    <input type="hidden" value="<?php echo $val; ?>" name="payment[]">
                                <?php } ?>
                            </span>
                            <br>
                        <?php } ?>

                        <?php if (!empty($_POST['delivery'])) { ?>

                            <span class="prod-meta"><strong>DELIVERY :</strong>
                                <?php foreach ($_POST['delivery'] as $key=>$val) { ?>
                                    <?php echo $val!=end($_POST['delivery'])?$val.', ':$val; ?>
                                    <input type="hidden" value="<?php echo $val; ?>" name="delivery[]">
                                <?php } ?>
                            </span>
                            <br>
                        <?php } ?>

                        <div>
                            <p class="prod-meta"><strong>LOCATION : </strong><?= $product['location']; ?>  <span id="loc-dist"></span></p>
                            <input type="hidden" value="<?php echo $product['location']; ?>" name="Products[location]">
                            <div id="map">
                                <img width="600" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $product['location']; ?>&zoom=13&scale=false&size=600x300&maptype=roadmap&
                                    format=png&visual_refresh=true&markers=color:red%7Clabel:C%7C<?php echo $_POST['lat'];?>,<?php echo $_POST['lng'];?>">
                                <input type="hidden" id="startLat" value="<?php echo $_POST['lat']; ?>" name="lat">
                                <input type="hidden" id="startLng" value="<?php echo $_POST['lng']; ?>" name="lng">
                            </div>
                        </div>

                    </div>

                    <input type="hidden" value="<?=isset($_POST['giveaway'])?$_POST['giveaway']:'';?>" name="giveaway">

                    <div class="col-md-12 col-sm-12 clearfix buttons text-right form-group">
                        <?php echo CHtml::submitButton('Back', array('class' => 'btn btn-primary', 'name' => 'back')); ?>
                        <!--                    --><?php //if (Yii::app()->session['user_id']) { ?>
                        <?php echo CHtml::submitButton('Next: Choose ad type', array('id' => 'btn-next', 'class' => 'btn btn-primary', 'name' => 'choose_ad_type')); ?>
                        <!--                    --><?php //} else { ?>
                        <!--                        --><?php //echo CHtml::button('Publish', array('class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modalRegisterOrLogin')); ?>
                        <!--                    --><?php //} ?>
                    </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</section>
<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #map {
        width: 100%;
        height: 250px;
        margin-bottom: 20px;
    }
    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>


<script>
    var type =0;
    $(document).on("click",'.priority_info_block', function() {
        var self = $(this);
        var class_name =  self.attr('class').split(' ')[1];

        switch (class_name) {
            case "basic":
                type = 0;
                break;
            case "premium":
                type = 15;
                break;
            case "gold":
                type = 35;
                break;
        }
        $('.priority_info_block').removeClass("selected");
        self.addClass("selected");
        $('#payment').val(type);
        $('#product_id').val(product_id);
        $('#btn-prev-next').removeAttr("disabled");

    });

    $(document).on('click', '#btn-prev-next', function() {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/create',
            data: $('#product-preview-form').serialize()+ "&type="+type,
            dataType : "json",
            success: function(data) {
                if (type == 0) {
                    window.location.href = '<?=$this->createUrl('products/advert'); ?>/'+data;
                }
                $('#product_id').val(data);
                $('#btn-prev-next').hide();
                $('#btn-save').show();

            }
        })
    });

    function getId(url) {

        var matchYtb = url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);

        var matchVm = url.match(/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);

        if (matchYtb && matchYtb[2].length == 11) {
            return 'https://youtube.com/embed/'+matchYtb[2];
        } else if(matchVm && matchVm[2]) {
            return 'http://player.vimeo.com/video/'+matchVm[2];
        } else {
            return undefined;
        }
    }

    var vidId = getId('<?=isset($_POST['Products']['video_link']) ? $_POST['Products']['video_link']: '';?>');

    var vidFileId = '<?=isset($_POST['video_file']) ? Yii::app()->request->baseUrl . '/images/product_videos/'. $_POST['video_file']: '';?>'

    var ext = vidFileId.split('.').pop();


    if (vidFileId != '') {

        var content = '<video width="600" height="340" controls> <source src="'+vidFileId+'" type="video/'+ext+'" /></video>'
    }

    $("#my-video a").click(function() {
        $.fancybox({
            'padding'		: 0,
            'autoScale'		: false,
            'transitionIn'	: 'none',
            'transitionOut'	: 'none',
            'title'			: this.title,
            'width'			: 640,
            'height'		: 385,
            'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'			: 'swf',
            'swf'			: {
                'wmode'				: 'transparent',
                'allowfullscreen'	: 'true'
            }
        });

        return false;
    });

    $("#my-video_file a").click(function() {
        $.fancybox({
            content: content,
            'type':'iframe',
        });

        return false;
    });



</script>
