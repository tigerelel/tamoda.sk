
<!--SINGLE PRODUCT AREA START-->

<section class="single-product-area">
    <div class="container-fluid">
        <div class="cat-crumb">
            <?php
            if ($model->categories->parentCategory->parentCategory) {
                $this->widget(
                    'zii.widgets.CBreadcrumbs',
                    array(
                        'homeLink' => CHtml::link($model->categories->parentCategory->parentCategory->title,
                            array('site/category', 'id' => $model->categories->parentCategory->parent_id)),
                        'links' => array(
                            $model->categories->parentCategory->title => Yii::app()->baseUrl . '/site/category?subId=' . $model->categories->parent_id,
                            $model->categories->title
                        ),
                        'htmlOptions' => array('class' => 'bread-crumb')
                    )
                );
            } else {
                $this->widget(
                    'zii.widgets.CBreadcrumbs',
                    array(
                        'homeLink' => CHtml::link($model->categories->parentCategory->title,
                            array('site/category', 'id' => $model->categories->parent_id)),
                        'links' => array(
                            $model->categories->title
                        ),
                        'htmlOptions' => array('class' => 'bread-crumb')
                    )
                );
            }
            ?>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-12">
                <div class="area-heading">
                    <h3>categories</h3>
                </div>
                <div class="catagory-list">
                    <ul>
                        <?php $subCategories = Categories::model()->findAllByAttributes(array(), 'parent_id='.$model->categories->parentCategory->id); ?>
                        <?php foreach ($subCategories as $category) {
                            $count = Products::model()->with('categories')->countByAttributes(array(), 'categories_id='.$category->id.' OR categories.parent_id='.$category->id);
                            ?>
                            <li class="<?= $category->id==$category->id?'active':'';?>">
                                <?php echo CHtml::link($category->title, array('site/category', 'subId' => $category->id));?> <?= $count>0 ?'<span class="count">('.$count.')</span>':'';?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="gallery-sm col-sm-12">
                    <div class="col-sm-5 col-md-5 no-padding">
                        <div class="post-share pull-left">
                            <div class="protfolio-social">
                                <ul class="social-icons">
                                    <?php $url=urlencode($this->createAbsoluteUrl('products/advert', array('id'=>$model->id)));?>
                                    <li><a data-original-title="Facebook" data-toggle="tooltip" data-placement="top" title="" href="http://www.facebook.com/sharer/sharer.php?u=<?=$url;?>" class="fb social-provider-icon"><i class="fa fa-facebook"></i></a></li>
                                    <li><a data-original-title="Twitter" data-toggle="tooltip" title="" href="http://twitter.com/share?url=<?=$url;?>" class="twt social-provider-icon"><i class="fa fa-twitter"></i></a></li>
                                    <li><a data-original-title="Google-plus" data-toggle="tooltip" title="" href="https://plus.google.com/share?url=<?=$url;?>" class="gle social-provider-icon"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <h2 class="pro-name text-center"><?php  echo $model->name;?></h2>
                        <div class="col-md-offset-1">
                            <div id="img-1" class="col-md-offset-2">
                                <a href="" class="">
                                    <?php if (!empty($model->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $model->mainPhoto->photo)) {
                                        echo CHtml::image(Yii::app()->request->baseUrl . '/timthumb.php?src=/images/product_images/' .  $model->mainPhoto->photo.'&h=300&w=300', '', array('id' => 'zoom1'));
                                    } else {
                                        echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg');
                                    } ?>
                                </a>
                            </div>
                        </div>
                    </div>
    <!--                &nbsp;&nbsp;&nbsp;-->
                    <div class="col-sm-7 col-md-7">

                        <div class="prod-list-detail">
                            <div class="prod-info">
                                <div class="price-box">
                                    <div class="price">
                                        <?php if ($model->giveaway == 0) { ?>
                                            <span id="old-price" style="">&#8364;<?php echo $model->price; ?></span>

                                            <?php if (!empty($model->user->id) && $model->user->id == Yii::app()->session['user_id']) { ?>
                                                <span id="change-old-price" class="glyphicon glyphicon-pencil"
                                                      style="font-size: 13px;vertical-align: top; margin-top: 5px; cursor: pointer;"></span>
                                                <?php $form = $this->beginWidget('CActiveForm', array(
                                                    'enableAjaxValidation' => false,
                                                    'id' => 'price-form',
                                                )); ?>
                                                <?php echo CHtml::textField('price', '', array('class' => 'input-text', 'placeholder' => 'change price')); ?>
                                                <a href="#" id="save-price"><i class="fa fa-check"></i></a>
                                                <a href="#" id="close-price"><i class="fa fa-times"></i></a>
                                                <?php $this->endWidget(); ?>
                                            <?php }
                                        } else { ?>
                                            Free
                                            <?php if (Yii::app()->session['user_id'] && $model->users_id != Yii::app()->session['user_id']) { ?>
                                                <div class="giveaway">
                                                    <span class="q-giveaway">Do you want it?</span>
                                                    <span>
                                                        <button type="button" class="btn btn-warning btn-md" data-toggle="modal" data-target="#request_item">Request item</button>
                                                        <div id="request_item" class="modal fade" style="display: none;">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title">Confirm your request</h4>
                                                                    </div>
                                                                    <div class="modal-body">

                                                                        <?php $form = $this->beginWidget('CActiveForm', array(
                                                                            'id' => 'request-form',
                                                                            'enableAjaxValidation' => false,
                                                                            'enableClientValidation' => true,
                                                                            'clientOptions' => array(
                                                                                'validateOnSubmit' => true,
                                                                            ),
                                                                        )); ?>

                                                                        <div class="opt-com form-group" style="display:none;">
                                                                            <textarea name="request_comment" placeholder="Enter a condition here..." class="form-control"></textarea>
                                                                        </div>
                                                                        <div class="form-group" id="request-comment"><a
                                                                                href="">Add a condition</a>(optional)
                                                                        </div>
                                                                        <input type="hidden" name="type" value="request">
                                                                        <div class="form-group">
                                                                            <?php echo CHtml::submitButton('Confirm', array('class' => 'btn btn-success', 'name' => 'request')); ?>
                                                                        </div>
                                                                        <?php $this->endWidget(); ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>

                                <?php
                                    $session = Yii::app()->session;
                                    $session->open();
                                    $user_id = $session['user_id'];
                                ?>

                                <?php
                                if ($model->giveaway == 0) {
                                    if ((Yii::app()->session['user_id'] && $model->user->id!= Yii::app()->session['user_id']) || !Yii::app()->session['user_id']) { ?>
                                        <div class="ask_question clearfix">
                                            <div class="actions clearfix">
                                                <?php $form=$this->beginWidget('CActiveForm', array(
                                                    'id' => 'offer-form',
                                                    'enableAjaxValidation' => false,
                                                    'enableClientValidation'=>true,
                                                    'clientOptions'=>array(
                                                        'validateOnSubmit'=>true,
                                                    ),
                                                )); ?>
                                                    <div class="no-padding-left col-md-4" style="min-width: 200px;">
                                                        <div class="prof-form valid">
                                                            <div class="col-xs-1 no-padding-right no-padding-left">
                                                                <span class="pull-left" >€</span>
                                                            </div>
                                                            <div class="col-xs-11 no-padding-right no-padding-left">
                                                                <?php echo CHtml::activeTextField($newOffer, 'offer_price', array('class' => 'input-text no-border offer_price')); ?>
    <!--                                                            --><?php //echo $form->error($newOffer,'offer_price'/*, array('style' => 'padding:10px;position:absolute;width:205px;left:-40px')*/); ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="type" value="offer">

                                                <?php if (Yii::app()->session['user_id']) { ?>

                                                    <div class="no-padding-left col-md-3" style="">
                                                        <?php echo CHtml::submitButton('Place Offer', array('class' => 'button', 'name' => 'submit', 'style' => 'padding: 0 21px')); ?>
                                                    </div>
                                                    <div class="no-padding-left col-md-3" style="">
                                                        <?php echo CHtml::submitButton('Ask Question', array('class' => 'button', 'id' => 'ask_question_form_show', 'name' => 'submit')); ?>
                                                    </div>

                                                <?php
                                                } else {
                                                    $cookie = new CHttpCookie('prod_id', $model->id);
                                                    $cookie->expire = time()+3600*30;
                                                    Yii::app()->request->cookies['prod_id'] = $cookie;
                                                    ?>

                                                    <div class="no-padding-left col-md-3">
                                                        <?php echo CHtml::button('Place Offer', array('class' => 'button nlogin', 'name' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#modalRegisterOrLogin', 'style' => 'padding: 0 21px')); ?>
                                                    </div>
                                                    <div class="no-padding-left col-md-3">
                                                        <?php echo CHtml::button('Ask Question', array('class' => 'button', 'name' => 'submit', 'data-toggle' => 'modal', 'data-target' => '#modalRegisterOrLogin')); ?>
                                                    </div>
                                                <?php } ?>

                                                <?php $this->endWidget(); ?>
                                            </div>
                                            <div id="ask_question_form">
                                                <a class="close">x</a>
                                                <?php $form=$this->beginWidget('CActiveForm', array(
                                                    'id'=>'question-form',
                                                    'enableAjaxValidation' => false,
                                                    'enableClientValidation'=>true,
                                                    'clientOptions'=>array(
                                                        'validateOnSubmit'=>true,
                                                    ),
                                                )); ?>
                                                <textarea name="ProductsComments[question]"></textarea>

                                                <input type="hidden" name="type" value="question">

                                                <?php echo CHtml::submitButton('Ask Question', array('class' => 'button', 'id' => 'ask_question_button', 'name' => 'submit')); ?>
                                                <?php $this->endWidget(); ?>

                                            </div>
                                        </div>
                                    <?php }
                                } ?>
                                <?php if ($model->user->id!= Yii::app()->session['user_id']) {
                                    $checkAccepted = ProductsComments::model()->findByAttributes(array('user_id' => Yii::app()->session['user_id'], 'confirmed' => 1, 'product_id' => $model->id));
                                    if ($checkAccepted) {
                                ?>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#sendmsg"><i class="glyphicon glyphicon-envelope"></i> Send Message</button>
                                <?php }
                                } ?>
                                <div class="actions">
                                    <?php if (Yii::app()->session['user_id'] && $model->user->id!= Yii::app()->session['user_id']) { ?>
                                        <span class="<?= !empty($model->favourite) ? 'favourite':''; ?>">
                                            <a href="" data-toggle="tooltip" onclick="favourite(<?=$model->id;?>, $(this)); return false;"><i class="fa fa-heart-o"></i>
                                                <span><?= !empty($model->favourite) ? ' remove from watch ad':'Watch Ad'; ?></span>
                                            </a>
                                        </span>
                                    <?php } ?>
                                </div>

                                <?php $filters = ProductFilter::model()->findAllByAttributes(array('product_id' => $model->id, 'type' => 1)); ?>

                                <span class="prod-meta form-group"><strong>COLOUR :</strong>
                                    <?php if ($filters) {
                                        foreach ($filters as $filter) {
                                            if ($filter->other_filter) {
                                                foreach (json_decode($filter->other_filter) as $key => $color) {
                                                    if ($key == 'color') {
                                                        foreach ($color as $c) {
                                                            if ($c) { ?>
                                                            <span style="background: <?= $c; ?>;padding: 0 15px;margin-left: 10px; border: 1px dotted black;"></span>
                                                        <?php }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } ?>
                                </span>

                                <?php if (!empty($newF)) { ?>

                                    <span class="prod-meta form-group"><strong>SIZE :</strong>
                                        <?php $i = 0;
                                        foreach ($newF as $k => $s) {
                                            foreach ($s as $k2=>$val) {

                                                if ($k2 == 'EUR' && !is_array($val)) {
                                                    $i++;
                                                    echo $val;
                                                } else {
                                                    if ($k2 == 'standard') {
                                                        $i++;
                                                        echo $val;
                                                    }
                                                }
                                            }
                                            if ($i < count($newF)) {
                                                echo ', ';
                                            }
                                        } ?>
                                        <a href="#size_guide" class="pull-right" data-toggle="modal" data-target="#size_guide">Size Guide</a>
                                        <div id="size_guide" class="modal fade" style="display: none;">
                                            <div class="modal-dialog" style="height: 100%; width: 50%;">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body col-sm-12">
                                                        <?php $this->renderPartial('_size_guide', array('size_filter_id' => $size_filter_id, 'model' => $model)); ?>
                                                    </div>
                                                    <div class="modal-footer">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                <?php } ?>


                                <span class="prod-meta  form-group"><strong>TYPE :</strong>
                                    <span style="margin-left: 10px;"><?= $model->new==1?'New':'Used'; ?></span>
                                </span>

                                <span class="prod-meta form-group"><strong>CREATED :</strong>
                                     <span style="margin-left: 10px;"><?=date("d-m-Y H:i", strtotime($model->created_date));?></span>
                                </span>

                                <?php if (!empty($model->video_link) || !empty($model->video_file)) { ?>
                                    <span class="prod-meta  form-group"><strong>VIDEO :</strong>
                                        <span style="margin-left: 10px;">
                                                    <?php if (!empty($model->video_link)) { ?>

                                                        <span id="my-video">
                                                            <a class="video"  title="" href="<?=$model->video_link;?>">
                                                                <i class="fa fa-youtube-play fa-2" aria-hidden="true"></i>
                                                            </a>
                                                        </span>

                                                    <?php } ?>
                                            <?php if (!empty($model->video_file)) { ?>

                                                <span id="my-video_file">
                                                            <a class="video"  title="" href="<?=Yii::app()->request->baseUrl . '/images/product_videos/'.$model->video_file->title;?>">
                                                                <i class="fa fa-play-circle fa-2" aria-hidden="true"></i>
                                                            </a>
                                                        </span>

                                            <?php } ?>
                                        </span>
                                     </span>
                                <?php } ?>

                                <span class="prod-meta form-group"><strong>SELLER :</strong>
                                     <span style="margin-left: 10px;"><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $model->user->username; ?>" class=""><?=$model->user->username; ?></a></span>
                                     (<?php echo isset($model->user->reviews) ? count($model->user->reviews):''; ?> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/raty/star-on.png" alt="" title="">)
                                 </span>

                                <div class="ad-actions">
                                    <?php if (Yii::app()->session['user_id'] && Yii::app()->session['user_id'] == $model->users_id) { ?>

                                            <a class="btn but_2" href='<?=$this->createUrl("products/update", array('id' => $model->id));?>'>Edit Ad</a>

                                    <?php } ?>

                                        <button type="button" class="btn but_2" data-toggle="modal" data-target="#refer_to">Refer to a friend</button>


                                    <?php if ((Yii::app()->session['user_id'] && Yii::app()->session['user_id'] != $model->users_id) || !Yii::app()->session['user_id']) { ?>

                                            <a id="report_link" class="clickable inverted btn btn-default but_2" href='<?=$this->createUrl("products/reportad", array('product_id' => $model->id));?>'>Report Ad</a>

                                    <?php } ?>

                                        <a class="pull-right print_icon" href="javascript:window.print()"><i class="fa fa-print fa-2" aria-hidden="true"></i></a>
                                </div>

                                <div id="refer_to" class="modal fade" style="display: none;">
                                    <div class="modal-dialog" style="width: 20%;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="text-center">Sending the offer to a friend</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="accept-comment-form">
                                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                                        'enableAjaxValidation' => false,
                                                        'enableClientValidation'=>true,
                                                        'clientOptions'=>array(
                                                            'validateOnSubmit'=>true,
                                                        ),
                                                        'action' => $this->createUrl('site/referToFriend')
                                                    )); ?>

                                                    <div class="form-group">
                                                        <?=$form->labelEx($contact,'name'); ?>
                                                        <?=$form->textField($contact,'name', array('class' => 'form-control')); ?>
                                                        <?=$form->error($contact,'name'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?=$form->labelEx($contact,'email'); ?>
                                                        <?=$form->textField($contact,'email', array('class' => 'form-control')); ?>
                                                        <?=$form->error($contact,'email'); ?>

                                                    </div>
                                                    <div class="form-group">
                                                        <?=$form->labelEx($contact,'emailTo'); ?>
                                                        <?=$form->textField($contact,'emailTo', array('class' => 'form-control')); ?>
                                                        <?=$form->error($contact,'emailTo'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?=$form->labelEx($contact,'body'); ?>
                                                        <?=$form->textArea($contact,'body', array('class' => 'form-control', 'value' => 'I am sending you an interesting offer from the portal tamoda.sk')); ?>
                                                        <?=$form->error($contact,'body'); ?>
                                                    </div>

                                                    <input type="hidden" value="<?=$model->id;?>" name="ad_id">

                                                    <div id="recaptcha-token1" class="g-recaptcha pull-left"></div>

                                                    <div class="form-group">
                                                        <?php echo CHtml::submitButton('Send', array('class' => 'btn btn-success form-control', 'name' => 'accept')); ?>
                                                    </div>


                                                    <?php $this->endWidget(); ?>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (!empty($model->media)) { ?>
                    <div class="col-sm-12 col-md-12 gallery-sm" style="margin-top: 15px;">
                        <div class="single-zoom-thumb">
                            <ul class="zoom-slider" id="gallery_01">
                                <?php if (isset($model->media)) {
                                    foreach ($model->media as $media) {
                                        if (!empty($media->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $media->photo)) { ?>
                                            <li>
                                                <a class="fancybox-button" rel="fancybox-button" href="<?php echo Yii::app()->request->baseUrl . '/images/product_images/' . $media->photo; ?>">
                                                    <img src="<?= Yii::app()->request->baseUrl . '/timthumb.php?src=/images/product_images/' . $media->photo; ?>&h=400&w=400">
                                                </a>
                                            </li>
                                        <?php }
                                    }
                                } ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>

                <div class="col-sm-5 gallery-sm" style="margin-top: 15px;">
                    <?php if (!empty($model->description)): ?>
                        <span class="prod-meta form-group"><strong>DESCRIPTION :</strong>
                            <?php echo $model->description;?>
                        </span>
                    <?php endif; ?>
                    <?php if (!empty($model->payment)): ?>
                        <?php $payments = json_decode($model->payment); ?>
                        <span class="prod-meta form-group"><strong>PAYMENT :</strong>
                            <?php foreach ($payments as $key=>$val) : ?>
                                <?php echo $val!=end($payments)?$val.', ':$val; ?>
                            <?php endforeach; ?>
                        </span>
                    <?php endif; ?>

                    <?php if (!empty($model->delivery)): ?>
                        <?php $deliveries = json_decode($model->delivery); ?>
                        <span class="prod-meta form-group"><strong>DELIVERY :</strong>
                            <?php foreach ($deliveries as $key=>$val) : ?>
                                <?php echo $val!=end($deliveries)?$val.', ':$val; ?>
                            <?php endforeach; ?>
                        </span>
                    <?php endif; ?>

                    <div>
                        <p class="prod-meta form-group"><strong>LOCATION : </strong><?php  echo $model->location ? $model->location: 'NA'; ?>  <span id="loc-dist"></span></p>
                        <input class="geoloc_lat" type="hidden" name="geolocation_lat">
                        <input class="geoloc_lng" type="hidden" name="geolocation_lng">
                        <div id="map"></div>
                    </div>

                </div>

                <div class="product-review-area col-sm-7" style="position: relative; overflow: visible;max-height: 500px;">
                    <div class="row">
                        <div class="review-wrapper clearfix" >
                            <div class="con tab-content">
                                <div class="product-comment pull-left">
                                    <?php foreach ($model->comments as $comment) { ?>
                                        <div class="comment-a <?=isset($comment->replyToUser) && $comment->replyToUser->request==1?'com-req' : '';?>">
                                            <a id="comment_<?php echo $comment->id; ?>"></a>
                                            <?php if (!empty($comment->user->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $comment->user->usersAdditional->avatar)) { ?>
                                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $comment->user->usersAdditional->avatar, ''); ?>
                                            <?php } else { ?>
                                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                                            <?php } ?>
                                            <div class="comment-text">
                                                <p class="meta">
                                                    <strong><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $comment->user->username; ?>"><?php echo $comment->user->username; ?></a></strong>
                                                    &ndash; <?php echo date("F j, Y",strtotime($comment->created_date));?>
                                                </p>

                                                <?php if ($user_id) { ?>
                                                    <div class="com-action-btn">
                                                        <?php if ($model->user->id == $user_id && $comment->user_id != $model->users_id) { ?>
                                                            <button type="button" class="btn btn-primary btn-sm reply range-seperator" data-user-name="<?=$comment->user->username;?>" data-user-id="<?=$comment->user->id;?>">Reply</button>
                                                        <?php } ?>

                                                        <?php if (!empty ($comment->offer_price)) { ?>

                                                            <?php if ($model->users_id == $user_id) { ?>
                                                                <?php if ($comment->confirmed === NULL) { ?>
                                                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#responsive<?=$comment->id;?>">Accept</button>

                                                                    <div id="responsive<?= $comment->id;?>" class="modal fade" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                    <?php $checkAccepted = ProductsComments::model()->findByAttributes(array('product_id' => $model->id, 'confirmed' => 1));
                                                                                    if ($checkAccepted) { ?>
                                                                                        <i class="glyphicon glyphicon-warning-sign"></i>You have already accepted an offer for this item.
                                                                                                                                        Accepting this offer will cancel the previously accepted offer.
                                                                                        <br>
                                                                                    <?php } ?>
                                                                                    <h4 class="modal-title">Accept offer from <?php echo $comment->user->username; ?></h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <label>Offer &#8364;<?= $comment->offer_price; ?>: </label> | <a href="#" class="accept-comment">Add a comment(optional)</a>
                                                                                    <div class="accept-comment-form">
                                                                                        <?php $form=$this->beginWidget('CActiveForm', array(
                                                                                            'enableAjaxValidation' => false,
                                                                                            'enableClientValidation'=>true,
                                                                                            'clientOptions'=>array(
                                                                                                'validateOnSubmit'=>true,
                                                                                            ),
                                                                                        )); ?>
                                                                                        <div class="opt-com" style="display:none;">
                                                                                            <textarea name ="accept_comment" placeholder="Enter a comment here..."></textarea>
                                                                                            <input type="hidden" name="replyTo" value="<?= $comment->id; ?>">

                                                                                        </div>
                                                                                        <input type="hidden" name="type" value="accept">

                                                                                        <?php echo CHtml::submitButton('Accept offer', array('class' => 'btn btn-success', 'name' => 'accept')); ?>

                                                                                        <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Close</button>
                                                                                        <?php $this->endWidget(); ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } elseif ($comment->user_id == $user_id) { ?>

                                                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#withdraw<?php echo $comment->id;?>">Withdraw offer</button>
                                                                    <div id="withdraw<?php echo $comment->id;?>" class="modal fade" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                    <h4 class="modal-title">Withdraw your offer</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <h5>Are you sure you want to withdraw your offer of €<?php echo $comment->offer_price; ?> ?</h5>
                                                                                    <p>Your offer will be removed, and the seller will be notified.</p>
                                                                                    <?php echo CHtml::link('Withdraw your offer', array('products/withdrawOffer', 'id' => $comment->id), array('class' => 'btn btn-danger')); ?>
                                                                                    <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Close</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                            <?php } ?>

                                                        <?php } elseif (!empty($comment->reply_to)) {

                                                            $replyTo = ProductsComments::model()->findByPk($comment->reply_to);

                                                            if ($replyTo && $replyTo->confirmed == 1) { ?>

                                                                <?php if ($model->users_id == $user_id) { ?>
                                                                    <button type="button" class="btn btn-danger btn-sm pull-right range-seperator" data-toggle="modal" data-target="#cancel">Cancel</button>
                                                                    <div id="cancel" class="modal fade" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                    <h4 class="modal-title">Cancel your accepted offer from <b><?= $replyTo->user->username; ?></b></h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                                                                        'enableAjaxValidation' => false,
                                                                                        'enableClientValidation'=>true,
                                                                                        'clientOptions'=>array(
                                                                                            'validateOnSubmit'=>true,
                                                                                        ),
                                                                                    )); ?>
                                                                                    <p>You are cancelling the offer from <b><?= $replyTo->user->username; ?></b>:</p>
                                                                                    <?php echo CHtml::activeTextArea($newComment,'comment', array('value' => 'Still Available:')); ?>
                                                                                    <?php echo $form->error($newComment,'comment'); ?>
                                                                                    <input type="hidden" name="replyTo" value="<?= $comment->id; ?>">

                                                                                    <?php echo CHtml::submitButton('Cancel offer', array('class' => 'btn btn-warning', 'name' => 'cancel')); ?>
                                                                                    <?php $this->endWidget(); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>

                                                                <?php if ($comment->user_id == $user_id) {
                                                                    $messageTo = $model->users_id;
                                                                } else {
                                                                    $messageTo = $replyTo->user_id;
                                                                } ?>
                                                                <?php if ($model->users_id == $user_id || $replyTo->user_id == $user_id) { ?>
                                                                    <button type="button" class="btn btn-info btn-sm pull-right range-seperator" data-toggle="modal" data-target="#sendmsg"><i class="glyphicon glyphicon-envelope"></i> Send Message</button>
                                                                    <div id="sendmsg" class="modal fade" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                                    <h4 class="modal-title">Send Private Message</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <?php $form=$this->beginWidget('CActiveForm', array(
                                                                                        'enableAjaxValidation' => false,
                                                                                        'enableClientValidation'=>true,
                                                                                        'clientOptions'=>array(
                                                                                            'validateOnSubmit'=>true,
                                                                                        ),
                                                                                    )); ?>
                                                                                    <div class="form-group">Advert: <span><?php echo $model->name; ?></span></div>
                                                                                    <div class="form-group">
                                                                                        <label>Message: </label> <?php echo CHtml::activeTextArea($message,'message',array('class' => 'form-control')); ?>
                                                                                    </div>
                                                                                    <?php echo $form->error($message,'message'); ?>

                                                                                    <input type="hidden" name="Messages[user_id]" value="<?= $messageTo; ?>">
                                                                                    <input type="hidden" name="Threads[seller_id]" value="<?= $model->users_id; ?>">
                                                                                    <input type="hidden" name="Threads[buyer_id]" value="<?= $replyTo->user_id; ?>">

                                                                                    <?php echo CHtml::submitButton('Send PM', array('class' => 'btn btn-info')); ?>
                                                                                    <button type="button" data-dismiss="modal" class="btn btn-default pull-right">Close</button>
                                                                                    <?php $this->endWidget(); ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } else {
                                                            if ($comment->request == 1) {
                                                                if ($comment->user_id == Yii::app()->session['user_id']) { ?>
                                                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#withdraw<?php echo $comment->id; ?>">
                                                                        Withdraw request
                                                                    </button>
                                                                    <div id="withdraw<?php echo $comment->id; ?>"
                                                                         class="modal fade" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-hidden="true">×
                                                                                    </button>
                                                                                    <h4 class="modal-title">Withdraw your request</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <h5>Are you sure you want to withdraw your request?</h5>
                                                                                    <p>Your request will be removed, and the seller will be notified.</p>
                                                                                    <?php $form = $this->beginWidget('CActiveForm', array(
                                                                                        'enableAjaxValidation' => false,
                                                                                        'enableClientValidation' => true,
                                                                                        'clientOptions' => array(
                                                                                            'validateOnSubmit' => true,
                                                                                        ),
                                                                                        'action' => Yii::app()->createUrl('products/withdrawRequest'),
                                                                                    )); ?>
                                                                                    <input type="hidden" name="request_comment_id" value="<?= $comment->id; ?>">

                                                                                    <?php echo CHtml::submitButton('Withdraw request', array('class' => 'btn btn-info')); ?>
                                                                                    <?php $this->endWidget(); ?>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            data-dismiss="modal"
                                                                                            class="btn btn-default">
                                                                                        Close
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <button type="button" class="btn btn-success btn-sm"
                                                                            data-toggle="modal"
                                                                            data-target="#responsive<?= $comment->id; ?>">
                                                                        Accept
                                                                    </button>

                                                                    <div id="responsive<?= $comment->id; ?>"
                                                                         class="modal fade" style="display: none;">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-hidden="true">×
                                                                                    </button>
                                                                                    <?php $checkAccepted = ProductsComments::model()->findByAttributes(array('product_id' => $model->id, 'confirmed' => 1));
                                                                                    if ($checkAccepted) { ?>
                                                                                        <i class="glyphicon glyphicon-warning-sign"></i>
                                                                                        You have already accepted a request for this item.
                                                                                        Accepting this request will cancel the previously accepted request.
                                                                                        <br>
                                                                                    <?php } ?>
                                                                                    <h4 class="modal-title">Accept
                                                                                        request
                                                                                        from <?php echo $comment->user->username; ?></h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    Add a comment(optional)
                                                                                    <div class="accept-comment-form">
                                                                                        <?php $form = $this->beginWidget('CActiveForm', array(
                                                                                            'enableAjaxValidation' => false,
                                                                                            'enableClientValidation' => true,
                                                                                            'clientOptions' => array(
                                                                                                'validateOnSubmit' => true,
                                                                                            ),
                                                                                        )); ?>
                                                                                        <div class="opt-com"
                                                                                             style="display:none;">
                                                                                            <textarea
                                                                                                name="accept_comment"
                                                                                                placeholder="Enter a comment here..."></textarea>
                                                                                            <input type="hidden"
                                                                                                   name="replyTo"
                                                                                                   value="<?= $comment->id; ?>">

                                                                                        </div>
                                                                                        <input type="hidden" name="type"
                                                                                               value="request">

                                                                                        <?php echo CHtml::submitButton('Accept request', array('class' => 'btn btn-success', 'name' => 'accept')); ?>
                                                                                        <?php $this->endWidget(); ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            data-dismiss="modal"
                                                                                            class="btn btn-default">
                                                                                        Close
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php }
                                                                }
                                                            }
                                                        ?>
                                                    </div>
                                                <?php } ?>
                                                <div class="pro-com-des">
                                                    <p><?php echo $comment->comment;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="add-review col-sm-7 pull-right">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'comment-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>
                    <h2>add a comment</h2>
                    <p class="product-form-comment">
<!--                        <label>Your Comment</label>-->
                        <?php echo CHtml::activeTextArea($newComment,'comment',array('rows'=>4, 'id' => 'yourComment', 'placeholder' => 'your comment...')); ?>
                        <input type="hidden" name="ProductsComments[reply_to]" value="">
                        <input type="hidden" name="type" value="question">
                        <?php echo $form->error($newComment,'comment'); ?>
                    </p>
                    <?php if (Yii::app()->session['user_id']) { ?>
                        <p class="form-submit pull-right"><input value="Submit " type="submit" class="submit btn btn-primary comment-submit" name="submit"></p>
                    <?php } else { ?>
                        <?php echo CHtml::button('Submit', array('class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modalRegisterOrLogin')); ?>
                    <?php } ?>
                    <?php $this->endWidget(); ?>
                </div>
                <!--PRODUCT REVIEW AREA END-->

                <div class="col-md-12 col-sm-12 col-md-offset-1" style="padding: 20px;">
                    <?php if (!empty($categoryProducts)) { ?>
                        <div class="adv area-heading">
                            <h3>related products for <?=CHtml::link($model->categories->parentCategory->parentCategory->title, array('site/category', 'id' => $model->categories->parentCategory->parentCategory->id));?></h3>
                        </div>
                        <?php foreach ($categoryProducts as $categoryProduct) { ?>
                            <div class="owl-item col-md-2" style="width: 200px;">
                                <div class="single-product">
                                    <div class="product-image">
                                        <div class="show-img rel-img">
                                            <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$categoryProduct->id;?>">
                                                <?php if (!empty($categoryProduct->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $categoryProduct->mainPhoto->photo)) {
                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/timthumb.php?src=/images/product_images/' .  $categoryProduct->mainPhoto->photo.'&w=400&h=300', '');
                                                } else {
                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg');
                                                } ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="prod-info">
                                        <h2 class="pro-name">
                                            <?php echo CHtml::link($categoryProduct->name, array('products/advert', 'id' => $categoryProduct->id)); ?>
                                        </h2>
                                        <div> </div>
                                        <div class="price-box">
                                            <?php if ($categoryProduct->giveaway == 0) { ?>
                                                <div class="price">
                                                    <span>€<?php echo $categoryProduct->price; ?></span>
                                                </div>
                                            <?php } else { ?>
                                                Free
                                            <?php } ?>
                                        </div>
                                        <?php if (Yii::app()->session['user_id'] && $categoryProduct->user->id!= Yii::app()->session['user_id']) { ?>
                                            <div class="actions watchlist">
                                                <span class="new-pro-wish <?= !empty($categoryProduct->favourite) ? ' favourite':''; ?>">
                                                     <a onclick="favourite(<?=$categoryProduct->id;?>, $(this)); return false;" title="<?= !empty($categoryProduct->favourite) ? 'Remove from watch ad':'Watch Ad'; ?>" href="" data-toggle="tooltip">
                                                         <i class="fa fa-heart-o"></i>
                                                     </a>
                                                </span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <?php if (!empty($subCatProducts)) { ?>
                <div class="col-sm-4 col-md-2">
                    <div class="area-heading">
                        <h3>related products in <?=CHtml::link($model->categories->parentCategory->title, array('site/category', 'subId' => $model->categories->parentCategory->id));?></h3>
                    </div>
                    <div class="product-sidebar-area">
                        <div class="bs-area-wrapper">
                        <?php foreach ($subCatProducts as $subCatProduct) { ?>
                                <div class="single-bestseller">
                                    <div class="product-image">
                                        <div class="show-img rel-img">
                                            <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$subCatProduct->id;?>" class="">
                                                <?php if (!empty($subCatProduct->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $subCatProduct->mainPhoto->photo)) {
                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/timthumb.php?src=/images/product_images/' .  $subCatProduct->mainPhoto->photo.'&w=400&h=300', '');
                                                } else {
                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg');
                                                } ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="bs-area-info">
                                        <div class="prod-info">
                                            <h2 class="pro-name">
                                                <?php echo CHtml::link($subCatProduct->name, array('products/advert', 'id' => $subCatProduct->id)); ?>
                                            </h2>
                                            <div class="rating"></div>
                                            <?php if (Yii::app()->session['user_id'] && $subCatProduct->user->id!= Yii::app()->session['user_id']) { ?>
                                                <div class="actions price-box" style="position: absolute;margin-left: 65px;">
                                                    <span class="new-pro-wish <?= !empty($subCatProduct->favourite) ? ' favourite':''; ?>">
                                                        <a data-toggle="tooltip" onclick="favourite(<?=$subCatProduct->id;?>, $(this)); return false;" title="<?= !empty($subCatProduct->favourite) ? 'Remove from watch ad':'Watch Ad'; ?>" href="">
                                                            <i class="fa fa-heart-o"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            <?php } ?>
                                            <div class="price-box">
                                                <div class="price">
                                                    <?php if ($subCatProduct->giveaway == 0) { ?>
                                                        <span>€<?php echo $subCatProduct->price; ?></span>
                                                    <?php }  else { ?>
                                                        Free
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--SINGLE PRODUCT AREA END-->
<!--PRODUCT REVIEW AREA START-->

<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #map {
        width: 100%;
        height: 250px;
        margin-bottom: 20px;
    }
    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css" type="text/css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2OGzYWtHS8ENx3B6ugbvVOs9BL3R6dBo&libraries=places&callback=initialize" async defer></script>


<script>

    $('#comment-form').submit( function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var $this = $(this);
        if ($this.valid()) {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl; ?>/products/addComment',
                data: $this.serialize()+'&ad_id='+<?=$model->id;?>,
                success: function (data) {
                    var obj = $.parseJSON(data);
                    console.log(obj)
                    $('.product-comment').append('<div class="comment-a">'+
                    '<img class="mCS_img_loaded" src="<?=Yii::app()->request->baseUrl;?>/images/user_pics/'+obj.avatar+'">'+
                        '<div class="comment-text">'+
                            '<p class="meta">'+
                                '<strong><a href="<?=Yii::app()->request->baseUrl;?>/profile?username='+obj.username+'"></a>'+obj.username+'</strong>'+
                                    '</a> - '+obj.date+
                            '</p>'+
                            '<div class="pro-com-des">'+
                                '<p>'+obj.comment+'</p>'+
                            '</div>'+
                    '</div>');
                    $(".product-review-area").mCustomScrollbar('scrollTo', 'bottom');
                    $('#yourComment').val('');
                }
            });
        }
    });
    $('#offer-form').submit( function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var $this = $(this);

        if (($('#ProductsComments_offer_price').val() != '' && !isNaN($('#ProductsComments_offer_price').val()))) {
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->request->baseUrl; ?>/products/addComment',
                data: $this.serialize()+'&ad_id='+<?=$model->id;?>,
                success: function (data) {

                    var obj = $.parseJSON(data);

                    $('.product-comment').append('<div class="comment-a comment-new">'+
                        '<img class="mCS_img_loaded" src="<?=Yii::app()->request->baseUrl;?>/images/user_pics/'+obj.avatar+'">'+
                        '<div class="comment-text">'+
                        '<p class="meta">'+
                        '<strong><a href="<?=Yii::app()->request->baseUrl;?>/profile?username='+obj.username+'"></a>'+obj.username+'</strong>'+
                        '</a> - '+obj.date+
                        '</p>'+
                        '<div class="com-action-btn">'+
                            '<button class="btn btn-danger btn-sm" data-target="#withdraw'+obj.comment_id+'" data-toggle="modal" type="button">Withdraw offer</button>'+
                            '<div id="withdraw'+obj.comment_id+'" class="modal fade" style="display: none;">'+
                                '<div class="modal-dialog">'+
                                    '<div class="modal-content">'+
                                        '<div class="modal-header">'+
                                            '<button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button>'+
                                            '<h4 class="modal-title">Withdraw your offer</h4>'+
                                        '</div>'+
                                        '<div class="modal-body">'+
                                            '<h5>Are you sure you want to withdraw your offer of €'+obj.offered_price+' ?</h5>'+
                                            '<p>Your offer will be removed, and the seller will be notified.</p>'+
                                            '<a class="btn btn-danger" href="<?=Yii::app()->request->baseUrl;?>/products/withdrawOffer/'+obj.comment_id+'">Withdraw your offer</a>'+
                                        '</div>'+
                                        '<div class="modal-footer">'+
                                            '<button class="btn btn-default" data-dismiss="modal" type="button">Close</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="pro-com-des">'+
                        '<p>'+obj.comment+'</p>'+
                        '</div>'+
                        '</div>');
                    $(".product-review-area").mCustomScrollbar('scrollTo', 'bottom');
                    $('#ProductsComments_offer_price').val('');
                    $('html, body').animate({
                        scrollTop: $(".product-comment").offset().top
                    }, 1000);
                }
            });
        } else {
            if (isNaN($('#ProductsComments_offer_price').val()))
                alert('Please enter a valid price.');
            else
                alert('Please enter a price.');
        }
    });

    $(".product-review-area").mCustomScrollbar({
        theme: "minimal-dark",
        mouseWheel: {
            enable: true
        },
        autoDraggerLength: true,
        //setTop: '-500px',
        scrollButtons:{ enable: true },
        callbacks: {
            onScroll: function () {
                $(".product-review-area").css({'margin-top': '15px'})
            }
        }
    });
    $(".product-review-area").mCustomScrollbar('scrollTo', 'bottom');

    $('.accept-comment').click(function(e) {
        e.preventDefault();
        $(this).parent().find('.opt-com').show();
    })

    $('#request-comment').click(function(e) {
        e.preventDefault();
        $('#request_item').find('.opt-com').show();
        $('#request-comment').hide();
    })

    $(".reply").click(function() {
        $this = $(this);
        $('html, body').animate({
            scrollTop: $(".add-review").offset().top
        }, 1000);
        $('#yourComment').val('@'+$this.attr('data-user-name')+': ');
        $('input[name="ProductsComments[reply_to]"]').val($this.attr('data-user-id'));
    });

    function initialize() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: <?php echo $model->lat?$model->lat: 56.0873168; ?>, lng: <?php echo $model->lng?$model->lng: 46.1283994; ?>},
            zoom: 15
            //mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: {lat: <?php echo $model->lat?$model->lat: 56.0873168; ?>, lng: <?php echo $model->lng?$model->lng: 46.1283994; ?>},
            map: map
        });

        var infowindow = new google.maps.InfoWindow({
            content: marker.getPosition()+''
        });
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {

            var geocoder = new google.maps.Geocoder();

            $('.geoloc_lat').val(position.coords.latitude);
            $('.geoloc_lng').val(position.coords.longitude)
            $.ajax({
                url: '<?php echo Yii::app()->request->baseUrl;?>/products/getUserPosition',
                type: "post",
                data: {
                    geoloc_lat: $('.geoloc_lat').val(),
                    geoloc_lng: $('.geoloc_lng').val(),
                    product_id: <?php echo $model->id; ?>

                },
                success: function (data) {
                    var text = Math.round(data) < 1 ? Math.round(data*1000)+' m' : Math.round(data/1000)+' km';
                    $('#loc-dist').text('   - ' + text + ' from you');
                }
            });

        });
    }

    $('#ask_question_form_show').on('click', function(){
        $( "#ask_question_form" ).fadeIn( 1000);
        $('.ask_question .actions').fadeOut( 500);
        return false;
    });
    $('#ask_question_form a.close').on('click', function(){
        $( "#ask_question_form" ).fadeOut( 500);
        $('.ask_question .actions').fadeIn( 1000);
        return false;
    });


    <?php if ($model->user->id == Yii::app()->session['user_id']): ?>
    $('#price-form').hide();
    $('body').click(function(e) {

        if (e.target.id == 'change-old-price') {

            $('#old-price').hide();
            $('#change-old-price').hide();
            $('#price-form').show();
        }
    });

    $('#close-price').click(function(e) {

        $('#price-form').hide();
        $('#old-price').show();
        $('#change-old-price').show();

    })


    $('#save-price').click(function(e) {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/changePrice',
            data: {
                product_id: "<?php echo $model->id; ?>",
                new_price: $('#price').val()
            },
            success: function () {
                var newPrice = parseInt($('#price').val());

                if (!isNaN(newPrice)) {
                    $('#old-price').text('€' + newPrice.toFixed(2));
                }
                $('#price-form').hide();
                $('#old-price').show();
                $('#change-old-price').show();
            }
        });
    });

    <?php endif; ?>
    $(".nlogin").on("click", function(){
        var offer = $(".offer_price").val();
        setCookie("offer", offer, 1);

    });
    $(document).ready(function(){
        if(getCookie("offer")){
            $(".offer_price").val(getCookie("offer"));
            setCookie("offer", 0, 0);
        }
    });

    function setCookie(cname, cvalue, exdays){
        var d = new Date();
        d.setTime(d.getTime() + (exdays*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }

    function favourite(id, $this) {

        var title = $this.attr("data-original-title") == "Watch Ad" ? 'Remove from watch ad' : 'Watch Ad';
        $this.attr("data-original-title", title);

        $this.parent().toggleClass('favourite');

        if ($('.actions a span').text() == "Watch Ad") {
            $('.actions a span').text("remove from watch ad");
        }
        else {
            $('.actions a span').text("Watch Ad");
        }


        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/toggleFavourite',
            data: {
                product_id: id
            },
            success: function() {}
        })
    }

    $(".social-provider-icon").click(function(event) {
        event.preventDefault();
        window.open($(this).attr("href"), "Sharing", "directories=0,height=600,width=800,location=0,menubar=0,status=0,titlebar=0,toolbar=0");
    });

    function getId(url) {

        var matchYtb = url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);

        var matchVm = url.match(/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);

        if (matchYtb && matchYtb[2].length == 11) {
            return 'https://youtube.com/embed/'+matchYtb[2];
        } else if(matchVm && matchVm[2]) {
            return 'http://player.vimeo.com/video/'+matchVm[2];
        } else {
            return undefined;
        }
    }

    var vidId = getId('<?=isset($model->video_link) ? $model->video_link: 'undefined';?>');
    var vidFileId = '<?=isset($model->video_file) ? Yii::app()->request->baseUrl . '/images/product_videos/'. $model->video_file->title: 'undefined';?>'

    var ext = vidFileId.split('.').pop();

    if (vidFileId != undefined) {
        var content = '<video width="600" height="340" controls> <source src="'+vidFileId+'" type="video/'+ext+'" /></video>'
    }

    $("#my-video a").click(function() {
        $.fancybox({
            'padding'		: 0,
            'autoScale'		: false,
            'transitionIn'	: 'none',
            'transitionOut'	: 'none',
            'title'			: this.title,
            'width'			: 640,
            'height'		: 385,
            'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type'			: 'swf',
            'swf'			: {
                'wmode'				: 'transparent',
                'allowfullscreen'	: 'true'
            }
        });

        return false;
    });

    $("#my-video_file a").click(function() {
        $.fancybox({
            content: content,
            'type':'iframe'
        });
        return false;
    });

</script>
