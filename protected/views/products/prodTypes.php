<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/priority.css">
<div id="adType" class="clearfix">
    <div class="container" style="margin-right: 1px;">
        <form id="ad-type-form" action="<?=Yii::app()->createUrl("products/payfortop")?>" method="post" accept-charset="utf-8">
            <?=CHtml::hiddenField('payment',"");?>
            <?=CHtml::hiddenField('product_id',"");?>
                <ul id="standard_priority_page">
                    <li>
                        <div id="priority_standard" class="priority_info_block basic">
                            <h4 class="heading">Basic Ad</h4>
                            <div class="description">
                                <div class="text">
                                    <ul>
                                        <li class="check"><b>Listed</b> on top of basic ads<br> in chosen category</li>
                                    </ul>
                                            <span class="highlight">
                                                <span class="euro">&euro;</span><span class="price">0</span>
                                            </span>
                                    <div class="choose">
                                        <a id="select_basic">No success fee if sold</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div id="priority_block" class="priority_info_block premium">
                            <h4 class="heading">
                                <div class="most_popular">Most Popular</div>
                                Priority Ad
                            </h4>
                            <div class="description">
                                <div class="text">
                                    <ul>
                                        <li class="check"><strong>3 days</strong> on top of basic ads<br>
                                            in chosen category</li>
                                    </ul>
                                            <span class="highlight">
                                                <span class="euro">&euro;</span><span class="price">3</span>
                                            </span>
                                    <div class="choose">
                                        <a id="select_basic">No success fee if sold</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div id="premium_info_block" class="priority_info_block gold">
                            <h4 class="heading">Premium Ad</h4>
                            <div class="description">
                                <div class="text">
                                    <ul>
                                        <li class="check"><strong>7 days</strong> on top of priority ads<br>
                                            in chosen category</li>
                                    </ul>
                                            <span class="highlight">
                                                <span class="euro">&euro;</span><span class="price">5</span>
                                            </span>
                                    <div class="choose">
                                        <a id="select_basic">No success fee if sold</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            <div class="col-md-8">
                <input id="btn-save" type="submit" class="btn priority-ad btn-success" value="Save and Finish" style="display: none;" name="saveAndFin">
                <?php echo CHtml::button('Choose', array('class' => 'btn btn-primary pull-right', 'id' => 'btn-next-save', 'disabled' => true, 'name' => 'chooseAndSave')); ?>
            </div>

        </form>
    </div>
</div>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ad-types-form'
)); ?>

    <?php if ($photos) {
        foreach ($photos as $media) { ?>
            <input id="<?php echo $media->id; ?>" type="hidden" name="file[]" value="<?php echo $media->id; ?>">
        <?php }
    } ?>

    <?php if (!empty($_POST['main'])) { ?>
        <input id="<?=$_POST['main']; ?>" type="hidden" name="main" value="<?=$_POST['main']; ?>">
    <?php } ?>

    <input type="hidden" value="<?php echo isset($product['name'])?CHtml::encode($product['name']):''; ?>" name="Products[name]">
    <input type="hidden" value="<?php echo isset($product['price'])?$product['price']:''; ?>" name="Products[price]">
    <input type="hidden" value="<?php echo isset($product['description'])?CHtml::encode($product['description']):''; ?>" name="Products[description]">
    <?php foreach ($filters as $key=>$val): ?>
        <input type="hidden" value="<?=$val;?>" name="colors[color][]">
    <?php endforeach; ?>

    <input type="hidden" value="<?=is_array($_POST['sizes']) ? CHtml::encode(json_encode($_POST['sizes'])) : CHtml::encode($_POST['sizes']);?>" name="sizes">

    <input type="hidden" value="<?php echo isset($product['new'])?$product['new']:''; ?>" name="Products[new]">

    <?php $category = Categories::model()->findByAttributes(array('id' => $_POST['Products']['subSubcategories_id'])); ?>
    <input type="hidden" value="<?php echo $category->id; ?>" name="Products[subSubcategories_id]">
    <input type="hidden" value="<?php echo $category->parentCategory->id; ?>" name="Products[subcategories_id]">
    <input type="hidden" value="<?php echo $category->parentCategory->parent_id; ?>" name="Products[categories_id]">

    <?php if (!empty($_POST['payment'])) { ?>
        <?php foreach ($_POST['payment'] as $key=>$val) { ?>
            <input type="hidden" value="<?php echo $val; ?>" name="payment[]">
        <?php } ?>
    <?php } ?>

    <?php if (!empty($_POST['delivery'])) { ?>
        <?php foreach ($_POST['delivery'] as $key=>$val) { ?>
            <input type="hidden" value="<?php echo $val; ?>" name="delivery[]">
        <?php } ?>
    <?php } ?>
    <input type="hidden" value="<?=isset($product['location']) ? $product['location'] : ''; ?>" name="Products[location]">
    <input type="hidden" id="startLat" value="<?php echo $_POST['lat']; ?>" name="lat">
    <input type="hidden" id="startLng" value="<?php echo $_POST['lng']; ?>" name="lng">

    <input type="hidden" value="<?=isset($_POST['giveaway']) ? 1 : '';?>" name="giveaway">

    <?php if (isset($_POST['Products']['video_link'])) { ?>
        <input type="hidden" name="Products[video_link]" value="<?=CHtml::encode($_POST['Products']['video_link']);?>">
    <?php } ?>
    <?php if (isset($_POST['video_file'])) { ?>
        <input type="hidden" name="video_file" value="<?=$_POST['video_file'];?>">
    <?php } ?>
<?php $this->endWidget(); ?>