<?php
/* @var $this ProductsController */
/* @var $model Products */

$this->breadcrumbs=array(
	'Products'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Products', 'url'=>array('admin')),
);
?>

<?php if (isset($createRow) && $createRow == 1) { ?>
	<div class="row container-fluid" id="create-row">
		<div class="col-xs-12 col-sm-12 col-lg-offset-3 col-md-6 col-lg-5">
			<div class="area-heading">
				<h1 class="text-center"><?=Yii::t('app', 'CREATE ADVERT'); ?></h1>
			</div>

			<?php $this->renderPartial('_form', array(
					'model'=>$model,
					'categories' => $categories,
					'productFilters' => $productFilters,
					'user' => $user
			)); ?>

		</div>
	</div>
<?php } ?>
<?php if (isset($status) && $status == 1) { ?>
	<div id="preview">
		<?php $this->beginContent('//products/preview', array('product'=>$product,
			'filters' => $filters,
			'sizes' => $sizes,
			'photos' => $photos,
			'mainPhoto' => $mainPhoto,
			'user' => $user
			)); ?>

		<?php $this->endContent(); ?>
	</div>
<?php } ?>

<?php if (isset($type) && $type == 1) { ?>
	<div id="ad-types">
		<?php $this->beginContent('//products/prodTypes', array('product'=>$product,
			'filters' => $filters,
			'sizes' => $sizes,
			'photos' => $photos,
			'mainPhoto' => $mainPhoto,
			'user' => $user
		)); ?>

		<?php $this->endContent(); ?>
	</div>
<?php } ?>

<script>
	var type = 0;
	var class_name = '';
	$(document).on("click",'.priority_info_block', function() {
		var self = $(this);
		class_name =  self.attr('class').split(' ')[1];

		switch (class_name) {
			case "basic":
				type = 0;
				break;
			case "premium":
				type = 15;
				break;
			case "gold":
				type = 35;
				break;
		}
		$('.priority_info_block').removeClass("selected");
		self.addClass("selected");
		$('#payment').val(type);
		$('#product_id').val(product_id);
		$('#btn-next-save').removeAttr("disabled");
		$('#ad_type').val('3');

	});

	$(document).on('click', '#btn-next-save', function() {
		$.ajax({
			type: 'POST',
			url: '<?php echo Yii::app()->request->baseUrl; ?>/products/create',
			data: $('#ad-types-form').serialize()+ "&type="+type,
			dataType : "json",
			success: function(data) {

				$('.priority_info_block').not('.'+class_name).css({'pointer-events':'none', 'opacity':0.4})
				if (type == 0) {
					window.location.href = '<?=$this->createUrl('products/advert'); ?>/'+data;

				} else {
					$('#product_id').val(data);
					$('#btn-next-save').hide();
					$('#btn-save').show();
				}

			}
		});
	});

</script>