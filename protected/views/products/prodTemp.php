<section class="single-product-area">
    <div class="container-fluid">
        <?php if ($type == 1) { ?>

            <div id="ad-types">
                <?php $this->beginContent('//products/prodTypes', array('product'=>$model,
                    'filters' => json_decode($colors->other_filter)->color,
                    'photos' => $photos,
                    'sizes' => $sizes->other_filter
    //                'mainPhoto' => $mainPhoto,
    //                'user' => $user
                )); ?>


                <?php $this->endContent(); ?>
            </div>
        <?php } else { ?>
            <?php $form=$this->beginWidget('CActiveForm', array(
    //        'id'=>'ad-types-form',
            )); ?>
            <div class="row">
                <div class="col-md-8 col-sm-12 col-md-offset-2">
                    <div class="gallery-sm col-sm-12">
                        <div class="col-sm-5 col-md-5 no-padding">
                            <h2 class="pro-name text-center"><?php echo CHtml::encode($model->name); ?></h2>
                            <input type="hidden" value="<?php echo isset($model->name)?CHtml::encode($model->name):''; ?>" name="Products[name]">
                            <div class="col-md-offset-1">
                                <div id="img-1" class="col-md-offset-2">
                                    <a>
                                        <?php if (!empty($model->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $model->mainPhoto->photo)) { ?>
                                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/timthumb.php?src=/images/product_images/' .  $model->mainPhoto->photo.'&h=300&w=300', ''); ?>
                                            <input id="<?php echo $model->mainPhoto->id; ?>" type="hidden" name="main" value="<?php echo $model->mainPhoto->id; ?>">
                                        <?php } else { ?>
                                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg'); ?>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-7">
                            <div class="prod-list-detail">
                                <div class="prod-info">
                                    <div class="price-box">
                                        <div class="price form-group">
                                            <?php if ($model->giveaway == 0) { ?>
                                                <span id="old-price" style="">€<?=$model->price; ?></span>
                                                <input type="hidden" value="<?php echo isset($model->price)?$model->price:''; ?>" name="Products[price]">
                                            <?php } else { ?>
                                                Free
                                                <input type="hidden" value="1" name="giveaway">
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <span class="prod-meta form-group"><strong>COLOUR :</strong>

                                        <?php
                                            foreach (json_decode($colors->other_filter) as $key => $color) {
                                                foreach ($color as $k => $c) {
                                                    ?>
                                                    <span style="background:<?= $c; ?>;padding: 0 15px;margin-left: 10px;"></span>
                                                    <input type="hidden" value="<?= $c; ?>" name="colors[color][]">
                                                <?php
                                                }
                                            }
                                        ?>
                                    </span>

                                    <?php if (!empty($newF)) { ?>
                                        <span class="prod-meta form-group"><strong>SIZE :</strong>
                                            <?php $i = 0;
                                            foreach ($newF as $k => $s) {
                                                foreach ($s as $k2=>$val) {
                                                    if ($k2 != 'id') {
                                                        if (!is_array($val)) {
                                                            if ($i == 1) {
                                                                echo '('.$val.'&nbsp;&nbsp;';
                                                            } else {
                                                                echo $val.'&nbsp;';
                                                            }
                                                            $i++;
                                                        }

                                                    } else {
                                                        $i = 0;
                                                    }
                                                }
                                                if ($i == count(array_slice((array)$s, 1))) {
                                                    echo ')&nbsp;&nbsp;';
                                                }
                                            } ?>
                                            <input type="hidden" value="<?=CHtml::encode($sizes->other_filter);?>" name="sizes">
                                        </span>
                                    <?php } ?>

                                    <span class="prod-meta form-group"><strong>TYPE :</strong>
                                        <span style="margin-left: 10px;"><?= $model->new==1?'New':'Used'; ?></span>
                                        <input type="hidden" value="<?=$model->new;?>" name="Products[new]">
                                        </span>
                                    </span>

                                    <span class="prod-meta form-group"><strong>CREATED :</strong>
                                        <span style="margin-left: 10px;"><?=date("d-m-Y H:i", strtotime($model->created_date));?></span>
                                    </span>

                                    <?php if (!empty($model->video_link) || !empty ($model->video_file)) { ?>
                                        <span class="prod-meta form-group"><strong>VIDEO :</strong>
                                            <span style="margin-left: 10px;">
                                                <?php if (!empty($model->video_link)) { ?>
                                                    <span id="my-video">
                                                        <a class="video"  title="" href="<?=$model->video_link;?>">
                                                            <i class="fa fa-youtube-play fa-2" aria-hidden="true"></i>
                                                        </a>
                                                    </span>
                                                    <input type="hidden" name="Products[video_link]" value="<?=CHtml::encode($model->video_link);?>">
                                                <?php } ?>

                                                <?php if (isset($model->video_file) && !empty($model->video_file->title)) { ?>
                                                    <span id="my-video_file">
                                                         <a class="video"  title="" href="<?=Yii::app()->request->baseUrl . '/images/product_videos/'.$model->video_file->title;?>">
                                                             <i class="fa fa-play-circle fa-2" aria-hidden="true"></i>
                                                         </a>
                                                    </span>
                                                    <input type="hidden" name="video_file" value="<?=$model->video_file->title;?>">
                                                <?php } ?>
                                            </span>
                                         </span>
                                    <?php } ?>

                                    <span class="prod-meta form-group"><strong>SELLER :</strong>
                                        <?php $user = Users::model()->findByPk(Yii::app()->session['user_id']); ?>
                                        <span style="margin-left: 10px;"><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $user->username; ?>" class=""><?=$user->username; ?></a></span>
                                        (<?php echo isset($user->reviews) ? count($user->reviews):''; ?> <img src="<?php echo Yii::app()->request->baseUrl;?>/images/raty/star-on.png" alt="" title="">)
                                    </span>

                                    <?php $category = Categories::model()->findByAttributes(array('id' => $model->categories_id)); ?>
                                    <input type="hidden" value="<?php echo $category->id; ?>" name="Products[subSubcategories_id]">
                                    <input type="hidden" value="<?php echo $category->parentCategory->id; ?>" name="Products[subcategories_id]">
                                    <input type="hidden" value="<?php echo $category->parentCategory->parent_id; ?>" name="Products[categories_id]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (isset($model->media) && !empty($model->media)) { ?>
                        <div class="col-sm-12 col-md-12 gallery-sm" style="margin-top: 15px;">
                            <div class="single-zoom-thumb">
                                <ul class="zoom-slider" id="gallery_01">
                                    <?php foreach ($model->media as $media) { ?>
                                        <li>
                                            <a class="fancybox-button" rel="fancybox-button" href="<?php echo Yii::app()->request->baseUrl.'/images/product_images/'.$media->photo;?>">
                                                <img src="<?=Yii::app()->request->baseUrl.'/timthumb.php?src=/images/product_images/'.$media->photo;?>&h=400&w=400">
                                            </a>
                                        </li>
                                        <input id="<?=$media->id; ?>" type="hidden" name="file[]" value="<?=$media->id; ?>">
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="col-sm-5 gallery-sm" style="margin-top: 15px;">
                        <span class="prod-meta"><strong>DESCRIPTION :</strong><?php echo CHtml::encode($model->description); ?></span>
                        <input type="hidden" value="<?php echo isset($model->description)?CHtml::encode($model->description):''; ?>" name="Products[description]">
                        <br>
                        <?php if (!empty($model->payment)) { ?>
                            <?php $payments = json_decode($model->payment); ?>
                            <span class="prod-meta"><strong>PAYMENT :</strong>
                                <?php foreach ($payments as $key=>$val) : ?>
                                    <?php echo $val!=end($payments)?$val.', ':$val; ?>
                                    <input type="hidden" value="<?php echo $val; ?>" name="payment[]">
                                <?php endforeach; ?>
                                    </span>
                            <br>
                        <?php } ?>
                        <?php if (!empty($model->delivery)) { ?>
                            <?php $deliveries = json_decode($model->delivery); ?>
                            <span class="prod-meta"><strong>DELIVERY :</strong>
                                <?php foreach ($deliveries as $key=>$val) : ?>
                                    <?php echo $val!=end($deliveries)?$val.', ':$val; ?>
                                    <input type="hidden" value="<?php echo $val; ?>" name="delivery[]">
                                <?php endforeach; ?>
                                    </span>
                            <br>
                        <?php } ?>
                        <span class="prod-meta"><strong>LOCATION : </strong><?php echo $model->location; ?> <span id="loc-dist"></span></span>
                        <input type="hidden" value="<?php echo $model->location; ?>" name="Products[location]">
                        <input type="hidden" id="startLat" value="<?php echo $model->lat; ?>" name="lat">
                        <input type="hidden" id="startLng" value="<?php echo $model->lng; ?>" name="lng">

                        <div id="map">
                            <img width="600" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $model->location; ?>&zoom=13&scale=false&size=600x300&maptype=roadmap&
                                        format=png&visual_refresh=true&markers=color:red%7Clabel:C%7C<?php echo $model->lat;?>,<?php echo $model->lng;?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 clearfix buttons text-right form-group">
                    <?php echo CHtml::submitButton('Next: Choose ad type', array('id' => 'btn-next', 'name' => 'choose_ad_type', 'class' => 'btn btn-warning')); ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>
        <?php } ?>
    </div>
</section>

<script>

    function getId(url) {

        var matchYtb = url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);

        var matchVm = url.match(/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/);

        if (matchYtb && matchYtb[2].length == 11) {
            return 'https://youtube.com/embed/'+matchYtb[2];
        } else if(matchVm && matchVm[2]) {
            return 'http://player.vimeo.com/video/'+matchVm[2];
        } else {
            return undefined;
        }
    }

    var vidId = getId('<?=isset($model->video_link) ? $model->video_link->title: '';?>');
    var vidFileId = '<?=isset($model->video_file) ? Yii::app()->request->baseUrl . '/images/product_videos/'. $model->video_file->title: '';?>'

    if (vidFileId != '') {
        $('#my-video_file').html('<video width="350" height="210" controls> <source src="'+vidFileId+'" type="video/mp4" /></video>');
    }

    var type = 0;
    var class_name = '';
    $(document).on("click",'.priority_info_block', function() {
        var self = $(this);
        class_name =  self.attr('class').split(' ')[1];

        switch (class_name) {
            case "basic":
                type = 0;
                break;
            case "premium":
                type = 15;
                break;
            case "gold":
                type = 35;
                break;
        }
        $('.priority_info_block').removeClass("selected");
        self.addClass("selected");
        $('#payment').val(type);
        $('#product_id').val(product_id);
        $('#btn-next-save').removeAttr("disabled");
        $('#ad_type').val('3')

    });

    $(document).on('click', '#btn-next-save', function() {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->request->baseUrl; ?>/products/create',
            data: $('#ad-types-form').serialize()+ "&type="+type,
            dataType : "json",
            success: function(data) {

                $('.priority_info_block').not('.'+class_name).css({'pointer-events':'none', 'opacity':0.4})
                if (type == 0) {
                    window.location.href = '<?=$this->createUrl('products/advert'); ?>/'+data;

                } else {
                    $('#product_id').val(data);
                    $('#btn-next-save').hide();
                    $('#btn-save').show();
                }

            }
        });
    });

</script>

<style>
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #map {
        width: 100%;
        height: 250px;
        margin-bottom: 20px;
    }
    #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

</style>



