<?php $categoriesFilter = CategoriesFilter::model()->findByAttributes(array('categories_id' => $model->categories->parent_id));

$sizeModel = FiltersNew::model()->findByPk($categoriesFilter->filter_new);
$sizeCharts = json_decode($sizeModel->filters);
?>

<h5 class="text-center">Size Guide</h5>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover">
    <thead>
    <tr>
        <?php foreach ($sizeCharts[0] as $k=>$s) {
            if ($k != 'id') { ?>
                <th><?= $k; ?></th>
            <?php }
        } ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($sizeCharts as $key=>$size) { ?>
        <tr>
            <?php foreach ($size as $k=>$s) { ?>
                <?php if ($k != 'id') { ?>
                    <td>
                        <?php if (is_array($s)) {
                            foreach ($s as $key=>$val) {
                                foreach ($val as $v) { ?>
                                    <?=$v.'<br>';
                                }
                            }
                        } else {
                            echo $s;
                        } ?>
                    </td>
                <?php }
            } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>

