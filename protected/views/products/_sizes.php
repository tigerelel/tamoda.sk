<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Oblecenie W'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
        <tr>
            <th><?php echo $sizeModel->name; ?></th>
            <?php foreach ($sizeCharts[0] as $k=>$s) {
                if ($k != 'id') { ?>
                    <th><?= $k; ?></th>
            <?php }
            } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sizeCharts as $key=>$size) { ?>
            <tr>
                <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
                <?php foreach ($size as $k=>$s) { ?>
                    <?php if ($k != 'id') { ?>
                        <td>
                            <?php if (is_array($s)) {
                                foreach ($s as $key=>$val) {
                                    foreach ($val as $v) { ?>
                                        <?=$v.'<br>';
                                    }
                                }
                            } else {
                                echo $s;
                            } ?>
                        </td>
                    <?php }
                } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Obuv W'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
        <tr>
            <th><?php echo $sizeModel->name; ?></th>
            <?php foreach ($sizeCharts[0] as $k=>$s) {
                if ($k != 'id') { ?>
                    <th><?= $k; ?></th>
                <?php }
            } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sizeCharts as $size) { ?>
            <tr>
                <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
                <?php foreach ($size as $k=>$s) { ?>
                    <?php if ($k != 'id') { ?>
                        <td>
                            <?php if (is_array($s)) {
                                foreach ($s as $key=>$val) {
                                    foreach ($val as $v) { ?>
                                        <?=$v.'<br>';
                                    }
                                }
                            } else {
                                echo $s;
                            } ?>
                        </td>
                    <?php }
                } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Oblecenie M'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
    <tr>
        <tr>
            <th><?php echo $sizeModel->name; ?></th>
            <?php foreach ($sizeCharts[0] as $k=>$s) {
                if ($k != 'id') { ?>
                    <th><?= $k; ?></th>
                <?php }
            } ?>
        </tr>

    </tr>
    </thead>
    <tbody>
        <?php foreach ($sizeCharts as $size) { ?>
            <tr>
                <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
                <?php foreach ($size as $k=>$s) { ?>
                    <?php if ($k != 'id') { ?>
                        <td>
                            <?php if (is_array($s)) {
                                foreach ($s as $key=>$val) {
                                    foreach ($val as $v) { ?>
                                        <?=$v.'<br>';
                                    }
                                }
                            } else {
                                echo $s;
                            } ?>
                        </td>
                    <?php }
                } ?>
            </tr>
        <?php } ?>
    </tbody>
</table>

<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Obuv M'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
    <tr>
    <tr>
        <th><?php echo $sizeModel->name; ?></th>
        <?php foreach ($sizeCharts[0] as $k=>$s) {
            if ($k != 'id') { ?>
                <th><?= $k; ?></th>
            <?php }
        } ?>
    </tr>

    </tr>
    </thead>
    <tbody>
    <?php foreach ($sizeCharts as $size) { ?>
        <tr>
            <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
            <?php foreach ($size as $k=>$s) { ?>
                <?php if ($k != 'id') { ?>
                    <td>
                        <?php if (is_array($s)) {
                            foreach ($s as $key=>$val) {
                                foreach ($val as $v) { ?>
                                    <?=$v.'<br>';
                                }
                            }
                        } else {
                            echo $s;
                        } ?>
                    </td>
                <?php }
            } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Oblecenie K'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
    <tr>
    <tr>
        <th><?php echo $sizeModel->name; ?></th>
        <?php foreach ($sizeCharts[0] as $k=>$s) {
            if ($k != 'id') { ?>
                <th><?= $k; ?></th>
            <?php }
        } ?>
    </tr>

    </tr>
    </thead>
    <tbody>
    <?php foreach ($sizeCharts as $size) { ?>
        <tr>
            <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
            <?php foreach ($size as $k=>$s) { ?>
                <?php if ($k != 'id') { ?>
                    <td><?=$s;?></td>
                <?php }
            } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Obuv K'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
    <tr>
    <tr>
        <th><?php echo $sizeModel->name; ?></th>
        <?php foreach ($sizeCharts[0] as $k=>$s) {
            if ($k != 'id') { ?>
                <th><?=$k;?></th>
            <?php }
        } ?>
    </tr>

    </tr>
    </thead>
    <tbody>
    <?php foreach ($sizeCharts as $size) { ?>
        <tr>
            <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
            <?php foreach ($size as $k=>$s) { ?>
                <?php if ($k != 'id') { ?>
                    <td><?=$s;?></td>
                <?php }
            } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php
$sizeModel = FiltersNew::model()->findByAttributes(array('name' => 'Stockings K'));
$sizeCharts = json_decode($sizeModel->filters);
?>
<table id="table-<?=$sizeModel->id;?>" class="table table-bordered table-hover" style="display: none;">
    <thead>
    <tr>
    <tr>
        <th><?php echo $sizeModel->name; ?></th>
        <?php foreach ($sizeCharts[0] as $k=>$s) {
            if ($k != 'id') { ?>
                <th><?=$k;?></th>
            <?php }
        } ?>
    </tr>

    </tr>
    </thead>
    <tbody>
    <?php foreach ($sizeCharts as $size) { ?>
        <tr>
            <td><input type="checkbox" value="<?=$size->id;?>" name="sizes[<?=$sizeModel->id;?>][]"></td>
            <?php foreach ($size as $k=>$s) { ?>
                <?php if ($k != 'id') { ?>
                    <td><?=$s;?></td>
                <?php }
            } ?>
        </tr>
    <?php } ?>
    </tbody>
</table>