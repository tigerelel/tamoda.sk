<?php if (!empty($products)) { ?>

    <div class="shop-area" xmlns="http://www.w3.org/1999/html">
        <div class="container-fluid">
            <div class="cat-crumb">
                <?php
                if (isset($_GET['id'])) {
                    $category = Categories::model()->findByPk(CHtml::encode($_GET['id']));

                    if (isset($category->parentCategory->parentCategory)) {
                        $this->widget(
                            'zii.widgets.CBreadcrumbs',
                            array(
                                'homeLink'=> CHtml::link('Home', Yii::app()->homeUrl),
                                'links' => array(
                                    $category->parentCategory->parentCategory->title => Yii::app()->baseUrl.'/site/category/'.$category->parentCategory->parent_id,
                                    $category->parentCategory->title => Yii::app()->baseUrl.'/site/category?subId='.$category->parent_id,
                                    $category->title,

                                ),
                                'htmlOptions'=>array ('class'=>'bread-crumb')
                            )
                        );
                    } elseif (isset($category->parentCategory)) {
                        $this->widget(
                            'zii.widgets.CBreadcrumbs',
                            array(
                                'homeLink'=> CHtml::link('Home', Yii::app()->homeUrl),
                                'links' => array(
                                    $category->parentCategory->title => Yii::app()->baseUrl.'/site/category/'.$category->parent_id,
                                    $category->title,

                                ),
                                'htmlOptions'=>array ('class'=>'bread-crumb')
                            )
                        );
                    } else {
                        $this->widget(
                            'zii.widgets.CBreadcrumbs',
                            array(
                                'homeLink'=> CHtml::link('Home', Yii::app()->homeUrl),
                                'links' => array($category->title),
                                'htmlOptions'=>array ('class'=>'bread-crumb')
                            )
                        );
                    }

                } else {
                    $this->widget(
                        'zii.widgets.CBreadcrumbs',
                        array(
                            'homeLink'=> CHtml::link('Home', Yii::app()->homeUrl),
                            'links' => array('All categories'),
                            'htmlOptions'=>array ('class'=>'bread-crumb')
                        )
                    );
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-2">
                    <?php
                        $this->widget('application.components.widgets.CategoriesList', array(
                            'category' => false,
                            'products' => $products
                        ));
                    ?>
<!--                        <div class="shop-sidebar">-->
        <!--                    <div class="catagory-list">-->
        <!--                        --><?php //if(!empty($cats)) { ?>
        <!--                            <ul>-->
        <!--                                --><?php //foreach ($cats as $key=>$cat) { ?>
        <!--                                    <li>-->
        <!--                                        --><?php //echo CHtml::link($cat, array('products/index', 'id' => $key, 'q' => CHtml::encode($_GET['q'])));?>
        <!--                                        --><?php //if (!empty($subcats)) { ?>
        <!--                                            <div class="list-left" style="padding-top: 8px;">-->
        <!--                                                --><?php //foreach ($subcats as $id=>$subcat) { ?>
        <!--                                                    --><?php //if ($subcat['parentId'] == $key) { ?>
        <!--                                                        <div style="padding-bottom: 8px;">-->
        <!--                                                            --><?php //echo CHtml::link($subcat['title'], array('products/index', 'id' => $id, 'q' => CHtml::encode($_GET['q'])));?>
        <!--                                                            --><?php //if (!empty($subcats)) { ?>
        <!--                                                                <div class="list-left">-->
        <!--                                                                   --><?php //foreach ($subsubcats as $subId=>$subsubcat) { ?>
        <!--                                                                        --><?php //if ($subsubcat['parentId'] == $id) { ?>
        <!--                                                                            <div style="padding-top: 8px;">-->
        <!--                                                                                --><?php //echo CHtml::link($subsubcat['title'], array('products/index', 'id' => $subId, 'q' => CHtml::encode($_GET['q'])));?>
        <!--                                                                            </div>-->
        <!--                                                                        --><?php //} ?>
        <!--                                                                   --><?php //} ?>
        <!--                                                                </div>-->
        <!--                                                            --><?php //} ?>
        <!--                                                        </div>-->
        <!--                                                    --><?php //} ?>
        <!--                                                --><?php //} ?>
        <!--                                            </div>-->
        <!--                                        --><?php //} ?>
        <!--                                    </li>-->
        <!--                                --><?php //} ?>
        <!--                            </ul>-->
        <!--                        --><?php //} ?>
        <!--                    </div>-->

<!--                            <div class="rance-wrapper">
                                <div class="price-range">
                                    <div id="slider-range"></div>
                                    <span>Price</span>
                                    <input id="amount" type="text" readonly="">
                                    <input id="price-filter0" class="price_to" type="hidden" name="price_filter[]" value="<?php echo Products::model()->maxPrice(NULL, $products); ?>">
                                    <input id="price-filter1" class="price_from" type="hidden" name="price_filter[]" value="<?php echo Products::model()->minPrice(NULL, $products); ?>">

                                </div>
                            </div>

                            <div class="rance-wrapper">
                                <div class="price-range">
                                    <div id="one-slide"></div>
                                    <span>Destination</span>
                                    <input class="destination" type="text" id="dest" readonly >

                                    <input class="ajax-dest" type="hidden" name="destination" value="0">
                                </div>
                            </div>
                            <div class="rance-wrapper">
<!--                                <ul class="advert-color">-->
<!--                                    --><?php //$colors = json_decode(FiltersNew::model()->find('type=1')->filters);
//                                    if ($colors) {
//                                        foreach ($colors as $key=>$color) { ?>
<!--                                            --><?php //if ($key < 14) { ?>
<!--                                                <li>-->
<!--                                                    <label for="--><?//='color-'.$key; ?><!--" style="background: --><?php //echo $color; ?><!--;">-->
<!--                                                        --><?php //echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $color, 'class' => 'color-box', 'id' => 'color-'.$key)); ?>
<!--                                                    </label>-->
<!--                                                    <div style="background: --><?//=$color;?><!--;">-->
<!--                                                        <input type="hidden" name="color" class="color-box" value="--><?//=$key;?><!--">-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                                --><?php //$key++;
//                                            } else { ?>
<!--                                                <li class="show-more-color" >-->
<!--                                                    <label for="--><?//='color-'.$key; ?><!--" style="background: --><?php //echo $color; ?><!--;">-->
<!--                                                        --><?php //echo CHtml::checkBox('filtersAdditional[]', '', array('value' => $color, 'class' => 'color-box', 'id' => 'color-'.$key)); ?>
<!--                                                    </label>-->
<!--                                                    <div style="background: --><?//=$color;?><!--;">-->
<!--                                                        <input type="hidden" name="color" class="color-box" value="--><?//=$key;?><!--">-->
<!--                                                    </div>-->
<!--                                                </li>-->
<!--                                            --><?php //}
//                                        }
//                                    }?>
<!--                                </ul>-->
<!--                                <br/>-->
<!--                                <a class="pull-left pointer show-more-color-btn" style="padding-top: 10px;">Show more</a>-->
<!--                            </div>-->

<!--                            <div>-->
<!--                                <input class="geoloc_lat" type="hidden" name="geolocation_lat" value="0">-->
<!--                                <input class="geoloc_lng" type="hidden" name="geolocation_lng" value="0">-->
<!--                                <button value="id DESC" class="filter-btn" type="submit">Filter<i class="fa fa-refresh"></i></button>-->
<!--                            </div>-->
<!--                        </div>-->
                </div>
                <div class="col-md-9 col-lg-10">
                    <div class="shop-area">
                        <?php $from = ($pages->pageSize*($page-1)+1); $to = ($pages->pageSize*($page-1) + $countPage);
                            $this->widget('application.components.widgets.GridList', [
                                'from' => $from,
                                'to' => $to,
                                'totalCount' => $totalCount,
                            ]);
                            ?>
                        <div class="shop-area products-area">
                           <?php $this->renderPartial('//site/_shop_area', array(
                                'pages' => $pages,
                                'totalCount' => $totalCount,
                                'products' => $products,
                                'page' => $page,
                                'countPage' => $countPage,
                                'category_id' => false,
                                'q'=>$q
                                ))
                            ?>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } elseif (empty($_GET['q'])) { ?>

    <h5 class="text-center">Please provide a search key</h5>

<?php } else { ?>

    <h5 class="text-center">No adverts found</h5>

<?php } ?>

