<div class="row">
    <div class="col-xs-12 col-sm-12 col-lg-offset-2 col-md-10 col-lg-6">
        <div class="form">

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'recover-form',
                'enableAjaxValidation' => false,
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>

            <!--    <p class="note">Fields with <span class="required">*</span> are required.</p>-->

            <!--    --><?php //echo $form->errorSummary($model); ?>
            <div class="form-group">
                <?php echo $form->labelEx($model,'New password'); ?>
                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>64, 'class'=>'form-control prof-form', 'id' => 'password')); ?>
<!--                --><?php //echo $form->error($model,'password'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model,'Repeat password'); ?>
                <input type="password" size="60" maxlength="64" class="form control prof-form" name="new_password"">
            </div>

            <div class="clearfix buttons text-right">
                <?php echo CHtml::submitButton('Recover', array('class' => 'btn btn-primary')); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>
