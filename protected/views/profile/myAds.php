
  <?php $payerId = isset($_GET['PayerID']) ? $_GET['PayerID'] : false;
  $product_id = isset($_GET['product_id']) ? $_GET['product_id'] : false;
  $token = isset($_GET['token']) ? $_GET['token'] : false;
  $ok = false;

  if($payerId && $product_id)
  {
    $result = Yii::app()->Paypal->GetExpressCheckoutDetails($token);
    if($result['ACK'] == 'Success')
        Yii::app()->user->setFlash('success', "Successfuly paid, thanks");
    $ok = true;


  }
  if($product_id && !$payerId)
  {
    Yii::app()->user->setFlash('fail', "Canceled");
  }

  $this->renderPartial('_payForTopModal', array('basic' => false))?>
  <!--PORTFOLIO AREA START-->
<!--    <section class="porfolio-mixitup-area">-->
        <div class="container-fluid">
            <div class="row">
                <h1 class="sin-page-title">My Ads</h1>

                <div class="col-md-offset-2 col-md-8">
                    <?php echo CHtml::link('< Back to my profile', array('profile/myProfile'), array('class' => 'btn btn-sm pull-right')); ?><br>
                    <div class="navbar-collapse">
                        <ul class="nav navbar-nav nav-tabs"><?php $active_count=0;?>
                            <li><?php echo CHtml::link('Active ('.Products::model()->countAds("active").')', array('profile/myAds'), array('class' => 'active')); ?></li>
                            <li><?php echo CHtml::link('Sold ('.Products::model()->countAds("sold").')', array('profile/myAds', 't' => 'sold')); ?></li>
                            <li><?php echo CHtml::link('Pending ('.Products::model()->countAds("pending").')', array('profile/myAds', 't' => 'pending')); ?></li>
                            <li><?php echo CHtml::link('Withdrawn ('.Products::model()->countAds("withdrawn").')', array('profile/myAds', 't' => 'withdrawn')); ?></li>
                        </ul>
                    </div><br>
                    <!--mixit-up content start-->
                    <div class="mixitup-content">
                        <?php if (!empty($products)) { ?>

                            <?php foreach ($products as $product) { ?>
                                <div class="single-list-product">

                                    <div class="col-sm-4 col-md-4 product-image">
                                        <div class="show-img">
                                            <a href="<?php echo Yii::app()->baseUrl . '/products/advert/' . $product->id; ?>">
                                                <?php if (isset($product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $product->mainPhoto->photo)) { ?>
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product_images/<?php echo $product->mainPhoto->photo ?>">
                                                <?php } else { ?>
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/product_images/none.jpg">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-sm-8 col-md-8">
                                        <div class="prod-list-detail">
                                            <h2 class="pro-name">
                                                <?php echo CHtml::link($product->name, array('products/advert', 'id' => $product->id)); ?>
                                            </h2>

                                            <div class="price-box">
                                                <div class="price">
                                                    <span>&#8364;<?php echo $product->price; ?></span>
                                                </div>
                                            </div>
                                            <p><?php echo substr($product->description, 0, 70) . '...'; ?></p>
                                            <p><?= date("d/m/y H:i", strtotime($product->created_date)); ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-md-8">
                                        <div style="margin-top: 25px;">
                                            <?php echo CHtml::link('Edit this Ad', array('products/update', 'id' => $product->id), array('class' => 'text-info')); ?>
                                            |
                                            <?php echo CHtml::link('Delete', array('products/deleteProd', 'id' => $product->id), array('confirm' => 'are you sure?')); ?>

                                                <?php $top = $product->productTop;
                                                $min_day = 0;
                                                if ($top) {
                                                    $expiration_date = date_create($top->expiration_date);
                                                    $date_added = date_create($top->date_added);
                                                    $today = date_create(date("Y-m-d"));
                                                    $interval = date_diff($date_added, $expiration_date);
                                                    $expired = date_diff($today, $expiration_date);
                                                    $expired = (int)$expired->format('%R%a');
                                                    $min_day = (int)$interval->format('%R%a');
                                                }
                                                switch ($product->status){
                                                    case "pending":
                                                        $adType = $product->ad_type==2 ? "Proirity" : "Premium"?>
                                                        <form id="ad-type-form" action="<?=Yii::app()->createUrl("products/payfortop")?>" method="post" accept-charset="utf-8">
                                                              <?=CHtml::hiddenField('payment', $product->ad_type == 2 ? 3 : 5);?>
                                                              <?=CHtml::hiddenField('product_id', $product->id);?>
                                                              <input type="submit" class="btn btn-pay-for-ad" value="Pay for <?=$adType?>">
                                                        </form>
                                                       <?php
                                                        break;
                                                    case "active":
                                                    if(!$product->productTop){
                                                         echo " | ".CHtml::link(
                                                            'bump', "#"
                                                            ,
                                                            array(
                                                                'class' => 'text-info top-link ',
                                                                'data-id' => $product->id
                                                            )
                                                        );

                                                    }
                                                    //else echo " | You have already paid for week";

                                                    break;

                                                    default:

                                                        break;
                                                    }

                                                 ?>

                                        </div>
                                    </div>
                                </div>
                            <?php }
                        } else { ?>
                            No ads
                        <?php } ?>
                    </div>
                    <!--mixit-up content end-->
                </div>
            </div>
        </div>
<!--    </section>-->
    <!--PORTFOLIO AREA END-->

    <script>
        function deleteProd(e, prod_id) {
            if (confirm('are you sure?')) {
                $.ajax({
                    type: 'GET',
                    url: '<?php echo Yii::app()->request->baseUrl; ?>/products/deleteProd',
                    data: {
                        prod_id: prod_id
                    },
                    success: function(data) {
                        if (data == 'success') {
                            e.parent().parent().parent().remove();

                        }
                    }
                });
            }
        }

        var url = window.location.href;

        $('ul.nav a[href="'+ url +'"]').parent().addClass('active');

        $('ul.nav a').filter(function() {
            return this.href == url;
        }).parent().addClass('active');


        var type = 0;
        var class_name = '';
        


        $(document).on('click', '.top-link', function() {
            var self = $(this);
            var id = self.data("id");
            $("#product_id").val(id);
            $("#payForTopModal").modal("show", {position:"center"});
             

        });

    $(document).ready(function()
    {
        var url = '<?=$this->createUrl("products/settop")?>';
        var ok = '<?=$ok?>';

        if(ok)
            $.ajax({
              url: url,
              type: "POST",
              data: {
                       product_id: '<?=$product_id?>',
                       payer_id: '<?=$payerId?>',
                       token:'<?=$token?>'
                    },

              success: function(data){
                window.location.href = '<?=$this->createUrl('products/advert', array('id' => $product_id)); ?>';
              },
              error: function(xhr, status){
                console.log(status);
              }
            });
    });
    </script>
