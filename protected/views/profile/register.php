
<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-lg-offset-2 col-md-10 col-lg-6">
        <div class="form">
            <h4 class="text-center">Sign up with social</h4>
            <script src="//ulogin.ru/js/ulogin.js"></script>
            <div class="uLogin" data-ulogin="display=buttons;fields=email,first_name,last_name;providers=facebook,google,twitter;redirect_uri=<?= Yii::app()->createAbsoluteUrl('profile/register'); ?>">
                <div class="col-md-4"> <div class="facebook form-group clearfix" data-uloginbutton="facebook"><div class="pull-left social_icon"> <i class="fa fa-facebook"></i></div> Sign up with Facebook </div></div>
                <div class="col-md-4"> <div class="google form-group clearfix" data-uloginbutton="google"><div class="pull-left social_icon"> <i class="fa fa-google-plus"></i></div> Sign up with Google Plus</div></div>
                <div class="col-md-4"> <div class="twitter form-group clearfix" data-uloginbutton="twitter"><div class="pull-left social_icon"> <i class="fa fa-twitter"></i></div> Sign up with Twitter</div></div>
            </div>
            <h4 class="text-center">or create an account using your email address</h4>
            <div class="col-md-12">
                <?php
                if(isset($_GET['hash']) && !empty($_GET['hash'])) {
                    $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'users-confirmation-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>

<!--                    --><?//= $form->errorSummary($secModel); ?>

                    <div class="form-group">
                        <?= $form->labelEx($secModel,'password'); ?>
                        <?= $form->passwordField($secModel,'password', array('class' => 'prof-form', 'id' => 'password')); ?>
                        <?= $form->error($secModel,'password'); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->labelEx($secModel,'confirm_password'); ?>
                        <?= $form->passwordField($secModel,'confirm_password', array('class' => 'prof-form', 'id' => 'confirm_password')); ?>
                        <?= $form->error($secModel,'confirm_password'); ?>
                    </div>
                    <div class="clearfix buttons">
                        <?= CHtml::submitButton('Submit', array('class' => 'btn btn-primary filter-btn')); ?>
                    </div>
                    <?php $this->endWidget();
                }
                else {

                    $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'users-register-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),

                    )); ?>

<!--                    --><?//= $form->errorSummary($newModel); ?>

                    <div class="form-group">
                        <?= $form->labelEx($newModel,'username'); ?>
                        <?= CHtml::activeTextField($newModel,'username', array('class' => 'prof-form', 'id' => 'username_reg')); ?>
                        <label id="avbl_username_error" class="text-danger" style="display: none;">This username is already taken! Try another.</label>
                        <?= $form->error($newModel,'username'); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->labelEx($newModel,'email'); ?>
                        <?= CHtml::activeEmailField($newModel,'email', array('class' => 'prof-form', 'id' => 'email_reg')); ?>
                        <label id="avbl_email_error" class="text-danger" style="display: none;">This email is already in use</label>
                        <?= $form->error($newModel,'email'); ?>
                    </div>

                    <div class="clearfix buttons">
                        <?= CHtml::submitButton('Submit', array('class' => 'filter-btn')); ?>
                    </div>
                    <?php $this->endWidget(); ?>

                <?php } ?>
            </div>
        </div>

        <div class="form-group">&nbsp;</div>

        <div class="col-md-12"><div class="form-group">
                <label>Already have an account? <a href="<?= Yii::app()->createAbsoluteUrl('profile/login'); ?>"> Log in</a></label>
            </div>
        </div>
    </div>
</div>
