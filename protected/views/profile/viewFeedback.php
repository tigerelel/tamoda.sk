<div class="product-review-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-offset-2 col-md-6 col-lg-6">
                <div class="review-wrapper clearfix">
                    <div class="comment-a">
                        <?php if (!empty($review->fromUser->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/users/' . $review->fromUser->usersAdditional->avatar)): ?>
                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/users/' .  $review->fromUser->usersAdditional->avatar, ''); ?>
                        <?php endif; ?>
                        <div class="comment-text">
                            <div class="rating">
                                <?php for( $k=0; $k<$review->rating; $k++ ){?>
                                    <img src="<?php echo Yii::app()->request->baseUrl;?>/images/raty/star-on.png" alt="" title="">
                                <?php } ?>
                            </div>
                            <p class="meta">
                                <strong><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $review->fromUser->username; ?>"><?php echo $review->fromUser->username; ?></a></strong>
                                &ndash; <?php echo date("F j, Y",strtotime($review->created));?>
                            </p>
                            <div class="pro-com-des">
                                <p><?php echo $review->review;?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
