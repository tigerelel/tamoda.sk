<div class="row">
    <div id="checkout-login" class="coupon-content  col-xs-12 col-sm-12 col-lg-offset-2 col-md-10 col-lg-6">
        <div class="coupon-info">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableAjaxValidation'=>false,
            )); ?>

            <h4 class="text-center">Sign in with social</h4>
            <script src="//ulogin.ru/js/ulogin.js"></script>
            <div class="uLogin" data-ulogin="display=buttons;fields=email,username;providers=facebook,google,twitter;redirect_uri=<?php echo Yii::app()->createAbsoluteUrl('profile/login'); ?>">
                <div class="col-md-4"> <div class="facebook form-group clearfix" data-uloginbutton="facebook"><div class="pull-left social_icon"> <i class="fa fa-facebook"></i></div> Sign in with Facebook </div></div>
                <div class="col-md-4"> <div class="google form-group clearfix" data-uloginbutton="google"><div class="pull-left social_icon"> <i class="fa fa-google-plus"></i></div> Sign in with Google Plus</div></div>
                <div class="col-md-4"> <div class="twitter form-group clearfix" data-uloginbutton="twitter"><div class="pull-left social_icon"> <i class="fa fa-twitter"></i></div> Sign in with Twitter</div></div>
            </div>
            <h4 class="text-center">or with registered account</h4>

            <div class="col-md-12">
                <div class="form-group">
                    <label>Username or email <span class="required">*</span></label>
                    <?= $form->textField($model,'username',array('size'=>60,'maxlength'=>64, 'class'=>'prof-form')); ?>
                    <?= $form->error($model,'username'); ?>
                </div>

                <div class="form-group">
                    <?= $form->labelEx($model, 'password'); ?>
                    <?= $form->passwordField($model,'password',array('size'=>60,'maxlength'=>64, 'class'=>'prof-form')); ?>
                    <?= $form->error($model,'password'); ?>
                </div>

                <div class="form-group">
                    <label>
                        <?= CHtml::checkBox('remember_pass'); ?>
                        Remember me
                    </label>
                </div>

                <div class="form-group">
                    <?= CHtml::submitButton('Login', array('class' => 'filter-btn')); ?>
                </div>

                <?php $this->endWidget(); ?>

                &nbsp;
                <div class="form-group lost-password">
                    <a href="<?php echo Yii::app()->baseUrl.'/profile/forgotPassword';?>">Lost your password?</a>
                </div>

                <div class="form-group">
                    <label>Don't have an account? <a href="<?php echo Yii::app()->createAbsoluteUrl('profile/register'); ?>"> Register</a></label>
                </div>

        </div>
        </div>
    </div>


