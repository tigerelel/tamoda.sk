<div class="row">

    <div class="col-xs-12 col-sm-12 col-lg-offset-3 col-md-10 col-lg-4">
        <div class="form">
            <h5>Please enter your email address and we will mail you a link to reset your password. </h5>
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'forgot-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'htmlOptions' => array(
                  'class' => 'form-horizontal'
                ),
                'enableAjaxValidation'=>false,
            )); ?>

            <div class="form-group">
                <?php echo $form->textField($model,'email',array('class'=>'form-control prof-form', 'placeholder' => 'Your email')); ?>
                <?php echo $form->error($model,'email'); ?>


            </div>
            <div class="clearfix buttons text-right">
                <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-primary')); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>
