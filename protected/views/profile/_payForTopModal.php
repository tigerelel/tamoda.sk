<div id="payForTopModal" class="modal fade" style="padding:35px 50px;display: none; ">
  <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/priority.css">
    <div id="adType" class="clearfix"  >
      <form id="ad-type-form" action="<?=Yii::app()->createUrl("products/payfortop")?>" method="post" accept-charset="utf-8">
          <?=CHtml::hiddenField('payment',"");?>
          <?=CHtml::hiddenField('product_id',"");?>
          <div class="container" style="width:43%;background-color:#fff;">
              <ul id="standard_priority_page">
                  <?php  if (!isset($basic)) { ?>
                      <li>
                          <div id="priority_standard" class="basic">
                              <h4 class="heading">Basic Ad</h4>
                              <div class="description">
                                  <div class="text">
                                      <ul>
                                          <li class="check"><b>Listed</b> on top of basic ads<br> in chosen category</li>
                                      </ul>
                          <span class="highlight">
                            <span class="euro">$</span><span class="price">0</span>
                          </span>
                                      <div class="choose">
                                          <a id="select_basic">No success fee if sold</a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </li>
                  <?php } ?>
                  <li>
                      <div id="priority_block" class="priority_info_block premium">
                          <h4 class="heading">
                              <div class="most_popular">Most Popular</div>
                              Priority Ad
                          </h4>
                          <div class="description">
                              <div class="text">
                                  <ul>
                                      <li class="check"><strong>3 days</strong> on top of basic ads<br>
                                          in chosen category</li>
                                  </ul>
                                  <span class="highlight">
                                    <span class="euro">$</span><span class="price">3</span>
                                  </span>
                                  <div class="choose">
                                      <a id="select_basic">No success fee if sold</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </li>

                  <li>
                      <div id="premium_info_block" class="priority_info_block gold">
                          <h4 class="heading">Premium Ad</h4>
                          <div class="description">
                              <div class="text">
                                  <ul>
                                      <li class="check"><strong>7 days</strong> on top of priority ads<br>
                                          in chosen category</li>
                                  </ul>
                                  <span class="highlight">
                                    <span class="euro">$</span><span class="price">5</span>
                                  </span>
                                  <div class="choose">
                                      <a id="select_basic">No success fee if sold</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </li>
              </ul>
              <input id="btn-save" type="submit" class="btn btn-primary pull-right" style="margin-bottom:20px" disabled="disabled" value="Choose" name="saveAndFin">
         
          </div>
      </form>
  </div>
</div>

<script type="text/javascript">
      var type = 0;
      var class_name = '';
      $(document).on("click",'.priority_info_block', function() {
        var self = $(this);
        class_name =  self.attr('class').split(' ')[1];

        switch (class_name) {
          case "basic":
            type = 0;
            break;
          case "premium":
            type = 3;
            break;
          case "gold":
            type = 5;
            break;
        }
        $('.priority_info_block').removeClass("selected");
        self.addClass("selected");
        $('#payment').val(type);
        $('#btn-save').removeAttr("disabled");

      });
</script>