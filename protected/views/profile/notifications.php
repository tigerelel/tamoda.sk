<section class="blog-area-wrapper">
    <div class="container-fluid">
        <div class="row">
            <h1 class="sin-page-title">Notifications</h1>
            <div class="col-md-offset-3 col-md-6">

                <?php if (!empty($notes)): ?>
                    <div style="padding:0 7px 20px;">
                        <?php
                        $unread = false;
                        foreach($notes as $note) {
                            if ($note->read==0) {
                                $unread = true;
                            }
                        }
                        if ($unread) {
                            echo CHtml::link('<b>Mark all as read</b>', array('profile/markAsRead'), array('class' => 'pull-right'));
                        } ?>
                    </div>
                    <ul class="notifications">
                        <?php foreach($notes as $note) { ?>
                            <li data-id="<?=$note->id;?>" class="note-overlay cart-sub-total <?=$note->read==0?"unread":"read";?>">
                                <?php if ($note->comment_id) { ?>
                                    <?php if (!empty($note->comment->product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $note->comment->product->mainPhoto->photo)) { ?>
                                        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/' .  $note->comment->product->mainPhoto->photo); ?>
                                    <?php } else { ?>
                                        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg'); ?>
                                    <?php } ?>
                                    <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$note->comment->product_id.'#comment_'.$note->comment_id;?>"><?= $note->text; ?></a>
                                <?php } ?>
                                <?php if ($note->product_id) { ?>
                                    <?php if (!empty($note->product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $note->product->mainPhoto->photo)) { ?>
                                        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/' .  $note->product->mainPhoto->photo, ''); ?>
                                    <?php } else {
                                        echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg');
                                    } ?>
                                    <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$note->product_id;?>"><?= $note->text; ?></a>
                                <?php } ?>
                                <?php if ($note->review_id) { ?>
                                    <?php if (!empty($note->review->fromUser->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $note->review->fromUser->usersAdditional->avatar)) { ?>
                                        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $note->review->fromUser->usersAdditional->avatar, ''); ?>
                                    <?php } else { ?>
                                        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                                    <?php } ?>
                                    <a href="<?php echo Yii::app()->baseUrl.'/profile/viewFeedback/'.$note->review_id;?>"><?= $note->text; ?></a>
                                <?php } ?>
                                <div class="pull-right"><?=date("M d, Y H:i", strtotime($note->created));;?></div>
                            </li>
                        <?php } ?>
                    </ul>
                <?php else: ?>
                    No notifications
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>