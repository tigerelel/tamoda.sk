<!--SINGLE PRODUCT AREA START-->
<section class="single-product-area account">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-offset-2 col-md-6 col-lg-5">
                <div class="prod-list-detail">
                    <div class="prod-info">
                        <div class="col-xs-3">
                            <?php if (!empty($user->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $user->usersAdditional->avatar)) { ?>
                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $user->usersAdditional->avatar, '', array('id' => 'pic', 'data-name' => $user->usersAdditional->avatar)); ?>
                            <?php } else { ?>
                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                            <?php } ?>
                        </div>
<!--                        <h2 class="pro-name"></h2>-->
                        <div class="price-box">
                            <span class="prod-meta">
                                <span><?php echo isset($user->username) ? $user->username : ''?></span>
                            </span>
                            <span class="prod-meta">
                                <strong>Member since: </strong> <span><?=date("d/m/Y",strtotime($user->created_date));?></span>
                            </span>
                            <span class="prod-meta">
                                <strong>Location : </strong><span>NA</span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">&nbsp;</div>
                <div class="form-group">&nbsp;</div>
                <br>
            </div>
            <?php $t = false;
            if (Yii::app()->session['user_id'] && $user->id != Yii::app()->session['user_id']) { ?>
                <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#feedback">Leave Feedback</button>

                <div id="feedback" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <?php if (!empty($checkAccepted2)) {
                                foreach ($checkAccepted2 as $k=>$check) {
                                    $checkFeedback = UserReviews::model()->findByAttributes(array('from_user' => Yii::app()->session['user_id'], 'to_user' => $user->id, 'product_id' => $check->product_id));
                                    if (!$checkFeedback) {
                                        $arr[$k]['product_id'] = $check->product_id;
                                        $arr[$k]['product_name'] = $check->product->name;
                                        $arr[$k]['prod_status'] = $check->product->status;
                                    }
                                } ?>
                                <?php if (!empty($arr)) { ?>
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Leave Feedback</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="add-review">
                                            <?php $form=$this->beginWidget('CActiveForm', array(
                                                //'enableClientValidation'=>true,
                                                //'enableAjaxValidation'=>false,
                                                'id' => 'review-form',
                                                'clientOptions'=>array(
                                                    'validateOnSubmit'=>true,
                                                ),
                                                //'action' => $this->createUrl('profile/leaveFeedback')
                                            )); ?>
                                            <h2>add a review</h2>
                                            <p>Username : <?= $user->username; ?></p>
                                            Ad :
                                            <select name="product_id" class="prod-opt" id="prodtest">
<!--                                                <option>Select ad</option>-->
                                                <?php foreach ($arr as $k=>$a) { ?>
                                                    <option value="<?=$a['product_id'];?>" data-status="<?=$a['prod_status'];?>"><?=$a['product_name'];?></option>
                                                <?php } ?>
                                            </select>
                                            <p class="comment-form-rating">
                                                <label>Your Rating</label>
                                            </p>
                                            <p>
                                                <?php  $this->widget('ext.dzRaty.DzRaty', array(
                                                    'model' => $newReview,
                                                    'attribute' => 'rating',
                                                )); ?>
                                            </p>
                                            <br><br>
                                            <?php echo $form->error($newReview,'rating'); ?>
                                            <p class="product-form-comment">
                                                <label>Your Review</label>
                                                <?php echo $form->textArea($newReview, 'review', array('cols' => 45, 'rows' => 8, 'required' => true)); ?>
                                                <?php echo $form->error($newReview,'review'); ?>
                                            </p>
                                            <?php if ($checkAccepted2[0]->user_id != Yii::app()->session['user_id']) { ?>
                                                <p class="m-sold"></p>
<!--                                                <p>-->
<!--                                                    Mark Ad as Sold? <input type="checkbox" name="sold">-->
<!--                                                </p>-->
                                            <?php } ?>
                                            <p class="form-submit">
                                                <input type="submit" class="submit btn btn-primary" value="submit" id="review-submit">
                                            </p>

                                            <?php $this->endWidget(); ?>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title">Unable to leave feedback</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h5>You cannot leave more than one feedback per ad.</h5>
                                        <p>Feedback is used to give a rating to another user based on a completed transaction
                                            and can only be left within 45 days of the acceptance of the offer</p>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Unable to leave feedback</h4>
                                </div>
                                <div class="modal-body">


                                    <!--                                                                --><?php //} else { ?>
                                    <!--                                                                    <h5>You can only leave feedback once an offer has been accepted.</h5>-->
                                    <!--                                                                --><?php //} ?>
                                    <p>Feedback is used to give a rating to another user based on a completed transaction
                                        and can only be left within 45 days of the acceptance of the offer</p>
                                </div>
                            <?php } ?>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-lg-offset-2 col-lg-8">
                <div class="product-review-area">
                    <ul class="review-menu">
                        <li class="active"><a data-toggle="tab" href="#pr-feedback">feedback (<?=$count;?>)</a></li>
                        <li><a data-toggle="tab" href="#pr-comments">comments (<?=$commentCount;?>)</a></li>
                    </ul>
                </div>
                <div id="profile-reviews" class="product-review-area" style="margin-left: 150px;margin-bottom: 20px;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="review-wrapper clearfix">
                                    <div class="con tab-content" style="">
                                        <div id="pr-feedback" class="tab-pane fade in active">
                                            <div class="product-comment">
                                                <div class="profile_section">
                                                    <div class="col-sm-12 col-md-12" style="margin-left: 57px;">
                                                        <?php $this->widget('application.components.widgets.FeedbackScore', array('shop' => false)); ?>
                                                    </div>
                                                </div>
                                                <?php foreach($user->reviews as $review) { ?>
                                                    <div class="comment-a">
                                                        <?php if (!empty($review->fromUser->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/users/' . $review->fromUser->usersAdditional->avatar)) { ?>
                                                            <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/users/' .  $review->fromUser->usersAdditional->avatar, ''); ?>
                                                        <?php } else { ?>
                                                            <img src="<?=Yii::app()->request->baseUrl.'/images/user_pics/avatar.png';?>" class="img-rounded msg-img" width="70" height="70">
                                                        <?php } ?>
                                                        <div class="comment-text">
                                                            <div class="rating">
                                                                <?php for ( $k=0; $k<$review->rating; $k++ ) { ?>
                                                                    <img src="<?php echo Yii::app()->request->baseUrl;?>/images/raty/star-on.png" alt="" title="">
                                                                <?php } ?>
                                                            </div>
                                                            <p class="meta">
                                                                <strong><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $review->fromUser->username; ?>"><?php echo $review->fromUser->username; ?></a></strong>
                                                                &ndash; <?php echo date("F j, Y",strtotime($review->created));?>
                                                            </p>
                                                            <div class="pro-com-des">
                                                                <p><?php echo $review->review;?></p>
                                                            </div>
                                                            <?php if ($review->from_user == Yii::app()->session['user_id']) { ?>
                                                                <p><?php echo CHtml::link('Remove feedback', array(
                                                                        'profile/removeFeedback',
                                                                        'id' => $review->id,
                                                                        'return_url' => urlencode('/profile?').urlencode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY))
                                                                    ));?></p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pr-comments">
                                            <?php foreach ($comments as $product) { ?>
                                                <div class="comment-a">
                                                    <div class="pull-left">
                                                        <a href="<?=Yii::app()->baseUrl . '/products/advert/' . $product->id;?>">
                                                            <?php if (isset($product->mainPhoto) && !empty($product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot').'/images/product_images/'.$product->mainPhoto->photo)) { ?>
                                                                <img src="<?=Yii::app()->request->baseUrl.'/images/product_images/'.$product->mainPhoto->photo;?>" class="img-rounded" width="80" height="80">
                                                            <?php } else { ?>
                                                                <img src="<?=Yii::app()->request->baseUrl.'/images/product_images/none.jpg';?>" class="img-rounded msg-img" width="80" height="80">
                                                            <?php } ?>
                                                        </a>
                                                    </div>
                                                    <div class="comment-text">
                                                        <?php foreach ($product->comments as $comment) { ?>
                                                            <div class="meta">
                                                                <strong><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $comment->user->username; ?>"><?php echo $comment->user->username; ?></a></strong>
                                                                &ndash; <?php echo date("F j, Y",strtotime($comment->created_date));?>
                                                                <p><?=CHtml::link($comment->comment, array('products/advert', 'id' => $product->id, '#' => 'comment_'.$comment->id));?></p>
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <br>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css" type="text/css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script>

    $(document).ready(function() {

        $("#profile-reviews").mCustomScrollbar({
            theme:"minimal-dark"
        });

        $('#review-submit').click(function(e) {
            $('input[name="score"]').attr('required',true);

            $("#review-form").valid();

        });

        var url = document.location.toString();
        if (url.match('#')) {
            $('ul li a[href="#' + url.split('#')[1] + '"]').tab('show');
        }

        <?php if ($checkAccepted2 && $checkAccepted2[0]->user_id != Yii::app()->session['user_id']) { ?>

            if ($('#prodtest :selected').data('status') != 'sold') {
                $('.m-sold').html('<p>Mark Ad as Sold? <input type="checkbox" name="sold"> </p>');
            } else {
                $('.m-sold').html('');
            }

            $('#prodtest').on('change', function() {

                if ($('#prodtest :selected').data('status') != 'sold') {
                    $('.m-sold').html('<p>Mark Ad as Sold? <input type="checkbox" name="sold"> </p>');
                } else {
                    $('.m-sold').html('');
                }
            });

        <?php } ?>

    });
</script>