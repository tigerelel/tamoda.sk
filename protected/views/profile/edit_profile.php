<!--SINGLE PRODUCT AREA START-->
<section class="single-product-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-offset-4 col-md-6 col-lg-6">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'profile-form',
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                <div class="col-sm-7 col-md-7">
                    <div class="prod-list-detail">
                        <div class="prod-info">
                            <h2 class="pro-name"></h2>
                            <div class="price-box">
                                <div>
                                    <strong>FIRST NAME </strong></br>
                                    <?php
                                        if (!empty($sec_model)) {
                                            echo $form->textField($sec_model,'first_name',array('size'=>60,'maxlength'=>64, 'class'=>'prof-form'));
                                        } else {?>
                                            <input name="Users[first_name]" type="text" class="prof-form">
                                        <?php }
                                    ?>
                                </div>
                                </br>
                                <div>
                                    <strong>LAST NAME  </strong></br>
                                    <?php
                                    if (!empty($sec_model)) {
                                        echo $form->textField($sec_model,'last_name',array('class'=>'form-control','class'=>'prof-form'));
                                    } else { ?>
                                        <input name="Users[last_name]" type="text" class="prof-form">
                                    <?php }
                                    ?>
                                </div>
<!--                                </br>-->
<!--                                <div>-->
<!--                                    <strong>SEX  </strong></br>-->
<!--                                    --><?php
//                                        if(!empty($sec_model)){
//                                            echo $form->radioButtonList($sec_model,'sex',array('1' => 'male', '0' => 'female'));
//                                        }else{?>
<!--                                            <input type="radio" name="Users[sex]" value="1" "> Male<br>-->
<!--                                            <input type="radio" name="Users[sex]" value="0" > Female<br>-->
<!--                                        --><?php //}
//                                    ?>
<!--                                </div>-->
                                </br>
                                <div>
                                    <strong>Email </strong></br>
                                    <?php
                                    if (!empty($sec_model)) {
                                        echo $form->textField($model,'email',array('class'=>'prof-form'));
                                    } else { ?>
                                        <input type="text" name="Users[email]" class="prof-form">
                                    <?php }
                                    ?>
                                </div>
                                </br>
                                <div>
                                    <strong>USERNAME </strong></br>
                                    <?php
                                    if (!empty($sec_model)) {
                                        echo $form->textField($model,'username',array('size'=>60,'maxlength'=>64, 'class'=>'prof-form'));
                                    } else {?>
                                        <input type="text" name="Users[username]" class="prof-form">
                                    <?php }
                                    ?>
                                </div><br>
                                <h3>Change Password</h3>
                                <div>
                                    <strong>OLD PASSWORD </strong></br>
                                    <span><input name="Users[oldPassword]" type="password" class="prof-form"></span>
                                </div>
                                </br>
                                <div>
                                    <strong>NEW PASSWORD </strong></br>
                                    <span><input name="Users[newPassword]" type="password" class="prof-form"></span>
                                </div>
                                <div>
                                    <strong>Get newsletters</strong></br>
                                    <?php
                                        $id = Yii::app()->session['user_id'];
                                        $checked = Subscribers::model()->findByAttributes(array('user_id' => $id));
                                        if (!empty($checked)) { ?>
                                            <span><?php echo $form->checkBox($sub_model, 'user_id',array('checked' => 'checked')); ?></span>
                                        <?php } else {?>
                                            <span><?php echo $form->checkBox($sub_model, 'user_id'); ?></span>
                                        <?php }?>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="clearfix buttons text-right">
                        <?php echo CHtml::submitButton('Edit', array('class' => 'btn btn-primary')); ?>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</section>
<!--SINGLE PRODUCT AREA END-->

