<!--SINGLE PRODUCT AREA START-->
<section class="single-product-area account">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-lg-offset-2 col-md-6 col-lg-5">
                <div class="prod-list-detail">
                    <div class="prod-info">
                        <h2 class="pro-name">My Details</h2>
                        <hr/>
                        <div class="col-xs-3">
                            <?php if (!empty($user->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $user->usersAdditional->avatar)) { ?>
                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $user->usersAdditional->avatar, '', array('id' => 'pic', 'data-name' => $user->usersAdditional->avatar)); ?>
                            <?php } else { ?>
                                <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', '', array('id' => 'pic')); ?>
                            <?php } ?>
                           <br><br>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#change_photo">Edit photo</button>
                        </div>
                        <?php if ($userProducts > 20) { ?>
                            <h5><?=Chtml::link('My shop', array('shops/view', 'id' => Yii::app()->session['user_id']), array('class' => 'pull-right'));?></h5>
                        <?php } ?>
                        <div id="change_photo" class="modal fade clearfix" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                                    </div>
                                    <div class="modal-body">
                                        <h4 class="modal-title text-center">Select your profile pic</h4>
                                        <div class="col-sm-4 col-xs-3 no-padding col-xs-offset-4">
                                            <?php $form=$this->beginWidget('CActiveForm', array(
                                                'id'=>'change-photo-form',
                                                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                                                'enableAjaxValidation'=>false,
                                            )); ?>
                                            <br>
                                            <div class="dropzone dropzone-previews" id="my-awesome-dropzone">
                                                <div class="dz-message" data-dz-message><span>Drop or <a>click here</a> to upload photo</span></div>
                                                <div class="fallback">
                                                    <input name="avatar" type="file" />
                                                </div>
                                            </div>
                                        </div>
                                        <?php $this->endWidget(); ?>
                                    </div>
                                    <div class="modal-footer">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <span class="prod-meta">
                                <div class="row">
                                    <div class="col-xs-4"> <strong >USERNAME </strong></div>
                                    <div class="col-xs-8"><span><?php echo isset($user->username) ? $user->username : ''?></span></div>
                                </div>
                            </span>
                            <span class="prod-meta">
                                <div class="row">
                                    <div class="col-xs-4"> <strong>FULL NAME </strong></div>
                                    <div class="col-xs-4">
                                        <span>
                                            <?php echo isset($user->usersAdditional->first_name) ?  $user->usersAdditional->first_name : ''?>
                                            <?php echo isset($user->usersAdditional->last_name) ?  $user->usersAdditional->last_name : ''?>
                                        </span>
                                    </div>
                                </div>
                            </span>
                            <span class="prod-meta">
                                <div class="row">
                                    <div class="col-xs-4"> <strong>EMAIL </strong></div>
                                    <div class="col-xs-8"><span><?php echo isset($user->email) ? $user->email: ''?></span></div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="clearfix buttons text-right">
                    <a href="<?php echo Yii::app()->baseUrl.'/profile/editProfile/'.$user->id;?>" class="pull-right">Edit Profile</a>
                    <div class="form-group">&nbsp;</div>
                    <div class="form-group">&nbsp;</div>
                    <div class="form-group">&nbsp;</div>
                    <div class="form-group">&nbsp;</div>
                </div>

                <div class="prod-list-detail">

                    <div class="prod-info">
                        <h2 class="pro-name">My Feedback</h2>
                        <hr>
                        <div class="col-sm-6">
                            <?php $this->widget('application.components.widgets.FeedbackScore', array('shop' => false)); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">

                <div class="prod-list-detail">
                    <div class="prod-info">
                        <h2 class="pro-name">My Adverts</h2>
                        <hr/>
                        <div class="menu" style="float:left;">
                            <nav>
                                <ul>
                                    <li><?php echo CHtml::link('Active ads ('.Products::model()->countAds("active").')', array('profile/myAds'), array('class' => 'active')); ?></li>
                                    <li><?php echo CHtml::link('Sold ads ('.Products::model()->countAds("sold").')', array('profile/myAds', 't' => 'sold')); ?></li>
                                    <li><?php echo CHtml::link('Pending ads ('.Products::model()->countAds("pending").')', array('profile/myAds', 't' => 'pending')); ?></li>
                                    <li><?php echo CHtml::link('Withdrawn ads ('.Products::model()->countAds("withdrawn").')', array('profile/myAds', 't' => 'withdrawn')); ?></li>
                                    <li><a href="<?php echo Yii::app()->baseUrl.'/profile?username='.$user->username .'#pr-comments';?>">My comments/offers (<?=$commentCount;?>)</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <div class="form-group">&nbsp;</div>
                <div class="form-group">&nbsp;</div>
                <div class="form-group">&nbsp;</div>
                <div class="form-group">&nbsp;</div>

                <div class="prod-list-detail">
                    <div class="prod-info">
                        <h2 class="pro-name">Watchlist</h2>
                        <hr/>
                        <a href="<?php echo Yii::app()->baseUrl.'/products/favourites'; ?>">Manage your watchlist</a>
                    </div>
                </div>

                <div class="form-group">&nbsp;</div>
                <div class="prod-list-detail">
                    <div class="prod-info">
                        <div id="profile-reviews" class="product-review-area">
                            <div class="review-wrapper">
                                <ul class="review-menu">
                                    <li><a data-toggle="tab" href="#pr-reviews">reviews(<?php echo count($user->reviews);?>)</a></li>
                                </ul>
                                <div class="con tab-content">
                                    <div class="tab-pane fade in active" id="pr-reviews">
                                        <div class="product-comment">
                                            <?php foreach($user->reviews as $review){ ?>
                                                <div class="comment-a">
                                                    <?php if (!empty($review->fromUser->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $review->fromUser->usersAdditional->avatar)): ?>
                                                        <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $review->fromUser->usersAdditional->avatar, ''); ?>
                                                    <?php endif; ?>
                                                    <div class="comment-text">
                                                        <div class="rating">
                                                            <?php for( $k=0; $k<$review->rating; $k++ ){?>
                                                                <img src="<?php echo Yii::app()->request->baseUrl;?>/images/raty/star-on.png" alt="" title="">
                                                            <?php } ?>
                                                        </div>
                                                        <p class="meta">
                                                            <strong><a href="<?php echo Yii::app()->baseUrl . '/profile?username=' . $review->fromUser->username; ?>"><?php echo $review->fromUser->username; ?></a></strong>
                                                            &ndash; <?php echo date("F j, Y",strtotime($review->created));?>
                                                        </p>
                                                        <div class="pro-com-des">
                                                            <p><?php echo $review->review;?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--SINGLE PRODUCT AREA END-->

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/dropzone.css" media="all" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/dropzone.js"></script>


<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.mCustomScrollbar.css" type="text/css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>

    $(document).ready(function() {
        $("#profile-reviews").mCustomScrollbar({
            theme: "minimal-dark"
        });
    });

    Dropzone.options.myAwesomeDropzone = {
        url: '<?=Yii::app()->request->baseUrl.'/profile/uploadProfilePic/'.$user->id; ?>',
        previewsContainer: ".dropzone-previews",
        maxFiles: 1,
        maxFilesize: 10,
        addRemoveLinks: true,
        acceptedFiles: ".jpg,.png,.gif",
        removedfile: function(file) {
            $.ajax({
                type: 'GET',
                url: '<?=Yii::app()->request->baseUrl.'/profile/deleteProfilePic'; ?>',
                data: {
                    id: '<?=$user->id;?>'
                },
                cashe: false,
                success: function(data) {
                    console.log(data)
                    if (data == 'success') {
                        $('#pic').attr('src', '<?=Yii::app()->request->baseUrl . '/images/user_pics/avatar.png';?>');

                        var _ref;
                        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                    }
                }
            });

        },
        success: function(file, response) {
            var src = '<?=Yii::app()->request->baseUrl . '/images/user_pics/';?>'+ $.parseJSON(response)
            $('#pic').attr('src', src);
        },
        init: function() {
            if ($('#pic').attr('data-name') != undefined) {
                var $this = this;
                var mockFile = { name: $('#pic').attr('data-name'),  type: 'image/*' };
                $this.options.addedfile.call($this, mockFile);
                $this.options.thumbnail.call($this, mockFile, "<?=Yii::app()->request->baseUrl;?>/images/user_pics/"+ mockFile.name);
                mockFile.previewElement.classList.add('dz-success');
                mockFile.previewElement.classList.add('dz-complete');
            }
        }
    }

</script>
