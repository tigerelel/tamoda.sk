<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
    <meta content="<?= $this->pageOgDesc;?>" property="og:description">
    <meta content="<?= $this->pageOgImage;?>" property="og:image">


    <?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.validate.js" type="text/javascript"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/validate.js" type="text/javascript"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/vendor/modernizr-2.8.3.min.js"></script>

<!--    <link rel="shortcut icon" type="image/x-icon" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/img/favicon.ico">-->

    <!-- Google Fonts CSS
		============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>


    <!-- jquery-ui CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.css">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">

    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/owl.carousel.css">

    <!-- owl.theme CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/owl.theme.css">

    <!-- owl.transitions CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/owl.transitions.css">

    <!-- font-awesome.min CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">

    <!-- nivo-slider css
    ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/nivo-slider.css">

    <!-- fancybox CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fancybox/jquery.fancybox.css">

    <!-- animate CSS
   ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css">

    <!-- normalize CSS
   ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.css">

    <!--Responsive Mobile Menu
    ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/meanmenu.min.css" />

    <!-- main CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">

    <!-- style CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">

    <!-- responsive CSS
    ============================================ -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/responsive.css">



	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->




</head>

<body class="home-1">
<?php
if(CController::getAction()->id!='login')
{
    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
}
?>
<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
        <div class="box-container">
            <!--HEADER AREA START-->

            <?php if ($this->getRoute()=='profile/register' or $this->getRoute()=='profile/login') { ?>
                <section class="header-area">
                    <div class="">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3 no-padding logo-div">
                                    <!--Logo start-->
                                    <div class="logo">
                                        <a href="<?php echo Yii::app()->homeUrl; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.png" alt="logo"></a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 no-padding-right col-padding-md position-inherit  hidden-xs hidden-sm">
                                    <div class="clearfix position-inherit" style="background: #F03E62">
                                        <!--TOP RIGHT CART MENU START-->
                                        <div class="top-right-menu-wrapper position-inherit">

                                            <div class="main-menu pull-right">
                                                <nav>
                                                    <ul>
                                                        <li><a href="<?php echo Yii::app()->baseUrl.'/profile/login';?>">Login</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl.'/profile/register';?>">Register</a></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                        <!--TOP RIGHT CART MENU END-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">&nbsp;</div>
                </section>
            <?php } else { ?>
                <section class="header-area">
                    <div class="">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3 col-lg-2 no-padding hidden-xs hidden-sm">
                                    <!--Logo start-->
                                    <div class="logo">
                                        <a href="<?php echo Yii::app()->homeUrl; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.png" alt="logo"></a>
                                    </div>
                                </div>
                                <?php
                                $session = Yii::app()->session;
                                $session->open();
                                $user_id = $session['user_id'];
                                $subtotal = 0;
                                ?>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 no-padding-right col-padding-md position-inherit hidden-xs hidden-sm">
                                    <div class="clearfix position-inherit" style="background: #F03E62">
                                        <div class="col-lg-offset-1 col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding position-inherit">
                                            <!--MAIN MENU START-->
                                            <div class="main-menu hidden-sm hidden-xs hidden-md  position-inherit">
                                                <nav class=" position-inherit">
                                                    <ul>
                                                        <?php $categories = Categories::model()->findAll(); ?>
                                                        <?php foreach($categories as $key => $category){
                                                            if($category->parent_id==0){?>
                                                                <li><?php echo CHtml::link($category['title'], array('site/category', 'id' => $category->id)); ?>
                                                                    <div class="mega-menu">
                                                                        <?php foreach ($categories as $key_sub => $category_sub) {
                                                                            if ($category_sub->parent_id==$category->id) { ?>
                                                                                <div class="mega-catagory">
                                                                                    <h4><?php echo CHtml::link($category_sub['title'], array('site/category', 'subId' => $category_sub->id)); ?></h4>
                                                                                    <?php foreach ($categories as $key_sub_sub => $category_sub_sub) {
                                                                                        if ($category_sub_sub->parent_id==$category_sub->id){?>
                                                                                            <?php echo CHtml::link($category_sub_sub['title'], array('site/category', 'subId' => $category_sub_sub->id)); ?>
                                                                                        <?php }
                                                                                    } ?>
                                                                                </div>
                                                                            <?php }
                                                                        } ?>
                                                                    </div>
                                                                </li>
                                                            <?php }
                                                        }?>
                                                    </ul>
                                                </nav>
                                            </div>
                                            <!--MAIN MENU END-->
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 no-padding-left">
                                            <!--TOP RIGHT CART MENU START-->
                                            <div class="top-right-menu-wrapper">
                                                <?php if (Yii::app()->session['user_id']) { ?>
                                                    <div class="cart-wrapper">
                                                        <a href="<?php echo Yii::app()->baseUrl.'/profile/notifications';?>">
                                                            <div class="plus-icon">
                                                                <?php $notes = Notifications::model()->countByAttributes(array('user_id' => Yii::app()->session['user_id'], 'read' => 0)); ?>
                                                                <span class="label label-danger pull-right notification"><?=$notes>0 ? $notes : ''; ?></span>
                                                                <i class="glyphicon glyphicon-bell"></i>
                                                            </div>
                                                        </a>
                                                        <div class="cart-drop">
                                                            <?php $notes = Notifications::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user_id']), array('order' => 'id DESC', 'limit' => 5));
                                                            if (!empty($notes)) { ?>
                                                                <div style="padding:0 7px 20px;">
                                                                    Notifications
                                                                    <?php
                                                                    $unread = false;
                                                                    foreach($notes as $note) {
                                                                        if ($note->read==0) {
                                                                            $unread = true;
                                                                        }
                                                                    }
                                                                    if ($unread) {
                                                                        echo CHtml::link('<b>Mark all as read</b>', array('profile/markAsRead'), array('class' => 'pull-right'));
                                                                    } ?>
                                                                </div>
                                                                <ul class="notifications">
                                                                    <?php foreach($notes as $note) { ?>
                                                                        <li data-id="<?=$note->id;?>" class="note-overlay cart-sub-total <?=$note->read==0?"unread":"read";?>">
                                                                            <?php if (!empty($note->comment_id)) { ?>
                                                                                <?php if (!empty($note->comment->product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $note->comment->product->mainPhoto->photo)) { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/' .  $note->comment->product->mainPhoto->photo); ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg'); ?>
                                                                                <?php } ?>
                                                                                <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$note->comment->product_id.'#comment_'.$note->comment_id;?>"><?= $note->text; ?></a>
                                                                            <?php } ?>
                                                                            <?php if ($note->product_id) { ?>
                                                                                <?php if (!empty($note->product->mainPhoto->photo) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/product_images/' . $note->product->mainPhoto->photo)) { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/' .  $note->product->mainPhoto->photo, ''); ?>
                                                                                <?php }  else {
                                                                                    echo CHtml::image(Yii::app()->request->baseUrl . '/images/product_images/none.jpg');
                                                                                } ?>
                                                                                <a href="<?php echo Yii::app()->baseUrl.'/products/advert/'.$note->product_id;?>"><?= $note->text; ?></a>
                                                                            <?php } ?>
                                                                            <?php if ($note->review_id) { ?>
                                                                                <?php if (!empty($note->review->fromUser->usersAdditional->avatar) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $note->review->fromUser->usersAdditional->avatar)) { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' .  $note->review->fromUser->usersAdditional->avatar, ''); ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                                                                                <?php } ?>
                                                                                <a href="<?php echo Yii::app()->baseUrl.'/profile/viewFeedback/'.$note->review_id;?>"><?= $note->text; ?></a>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>

                                                                <div class="cart-checkout">
                                                                    <a href="<?php echo Yii::app()->baseUrl.'/profile/notifications';?>">View all notifications</a>
                                                                </div>

                                                            <?php } else { ?>
                                                                No notifications
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                    <div class="plus-account" style="width: auto;">
                                                        <div class="plus-icon">
                                                            <a style="color: #fff;" href="<?php echo Yii::app()->baseUrl.'/profile/myProfile';?>"><?php echo Yii::app()->session['username']; ?></a>
                                                        </div>
                                                        <div class="plus-menu">
                                                            <ul>
                                                                <li><a href="<?php echo Yii::app()->baseUrl.'/profile/myProfile';?>">my account</a></li>
                                                                <li><a href="<?php echo Yii::app()->baseUrl.'/profile/myAds';?>">my ads</a></li>
                                                                <li><a href="<?php echo Yii::app()->baseUrl.'/products/create';?>">place ad</a></li>
                                                                <li><a href="<?php echo Yii::app()->baseUrl.'/products/favourites'; ?>">watchlist</a></li>
                                                                <li><a href="<?php echo Yii::app()->baseUrl.'/profile/logout';?>">logout</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="plus-account cart-wrapper">
                                                        <a href="<?php echo Yii::app()->baseUrl.'/messages';?>">
                                                            <div class="plus-icon">
                                                                <?php $countNewMsg = Messages::model()->with('thread')->count(
                                                                    'user_id!='.Yii::app()->session['user_id'].' AND t.read=0 AND (thread.seller_id='.Yii::app()->session['user_id'].
                                                                    ' OR thread.buyer_id='.Yii::app()->session['user_id'].')'); ?>
                                                                <span class="label label-danger pull-right msg-notification"><?=$countNewMsg>0?$countNewMsg:''; ?></span>
                                                                <i class="fa fa-comments"></i>
                                                            </div>
                                                        </a>
                                                        <div class="cart-drop">
                                                            <?php
                                                            $q = Yii::app()->db->createCommand(
                                                                'SELECT   m.*, t.seller_id, t.buyer_id, u_a.`avatar`, u.`username`
                                                                FROM     threads t
                                                                INNER JOIN (
                                                                  SELECT * FROM messages
                                                                  ORDER BY id DESC
                                                                ) m
                                                                JOIN users u ON u.id = m.user_id
                                                                JOIN users_additional u_a ON u_a.`users_id` = u.`id`
                                                                JOIN threads t2 ON t2.`id` = m.thread_id
                                                                WHERE t2.`buyer_id` = '.Yii::app()->session['user_id'].' OR t2.`seller_id` = '.Yii::app()->session['user_id'].'
                                                                GROUP BY m.thread_id LIMIT 5');
                                                            $newMsgs = $q->queryAll();
                                                            if (!empty($newMsgs)) { ?>
                                                                <div style="padding:0 7px 20px;">
                                                                    Messages
                                                                    <?php
                                                                    $unread = false;
                                                                    if ($countNewMsg > 0) {
                                                                        echo CHtml::link('<b>Mark all as read</b>', array('#'), array('class' => 'pull-right mark_all_unread_uq'));
                                                                    } ?>
                                                                </div>
                                                                <ul class="notifications note-messages">
                                                                    <?php foreach($newMsgs as $newMsg) {
                                                                            if ($newMsg['read'] == 0 && $newMsg['user_id'] !=Yii::app()->session['user_id']) {
                                                                                $unread = true;
                                                                            } ?>
                                                                            <li data-id="<?= $newMsg['id']; ?>"
                                                                                class="note-overlay cart-sub-total <?= $newMsg['read'] == 0 && $newMsg['user_id'] !=Yii::app()->session['user_id'] ? "unread" : "read"; ?>">
                                                                                <?php if (!empty($newMsg['avatar']) && file_exists(YiiBase::getPathOfAlias('webroot') . '/images/user_pics/' . $newMsg['avatar'])) { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/' . $newMsg['avatar'], ''); ?>
                                                                                <?php } else { ?>
                                                                                    <?php echo CHtml::image(Yii::app()->request->baseUrl . '/images/user_pics/avatar.png', ''); ?>
                                                                                <?php } ?>
                                                                                <a class="text-msg" href="<?php echo Yii::app()->baseUrl . '/messages?m=' . $newMsg['thread_id']; ?>"><?= strlen($newMsg['message']) > 30 ? substr($newMsg['message'], 0, 30).'...' : $newMsg['message']; ?></a>
                                                                                <?php if ($newMsg['read'] == 0) { ?>
                                                                                    <a class="mark-read-msg" href="#" class="pull-right mark_unread_uq" data-id="<?= $newMsg['id']; ?>">Mark as read</a>
                                                                                <?php } ?>
                                                                            </li>
                                                                        <?php } ?>
                                                                </ul>
                                                                <div class="cart-checkout">
                                                                    <a href="<?php echo Yii::app()->baseUrl.'/messages';?>">View all messages</a>
                                                                </div>
                                                            <?php }  else { ?>
                                                                No messages
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="main-menu pull-right">
                                                        <nav>
                                                            <ul>
                                                                <li><a class="login_link" data-toggle="modal" data-target="#modalRegisterOrLogin" href="#">Login</a></li>
                                                                <li><a class="signup_link" data-toggle="modal" data-target="#modalRegisterOrLogin"href="#">Register</a></li>
                                                            </ul>
                                                        </nav>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <!--TOP RIGHT CART MENU END-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--MOBILE MENU START-->
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 ">
                                <div class="mobile-menu ">
                                    <nav class="mobile-menu-start position-inherit">
                                        <ul>
                                            <?php if (Yii::app()->session['user_id']) { ?>
                                                <li><a href="<?php echo Yii::app()->baseUrl.'/profile/myProfile';?>">my account</a></li>
                                                <li><a href="<?php echo Yii::app()->baseUrl.'/profile/myAds';?>">my ads</a></li>
                                                <li><a href="<?php echo Yii::app()->baseUrl.'/products/create';?>">place ad</a></li>
                                                <li><a href="<?php echo Yii::app()->baseUrl.'/products/favourites'; ?>">watchlist</a></li>
                                                <li><a href="<?php echo Yii::app()->baseUrl.'/profile/logout';?>">logout</a></li>
                                            <?php } else { ?>
                                                <li><a class="login_link" data-toggle="modal" data-target="#modalRegisterOrLogin" href="#">Login</a></li>
                                                <li><a class="signup_link" data-toggle="modal" data-target="#modalRegisterOrLogin"href="#">Register</a></li>
                                            <?php } ?>
                                            <?php foreach ($categories as $key => $category) {
                                                if ($category->parent_id==0) { ?>
                                                    <li><a href="<?php echo Yii::app()->request->baseUrl .'/site/category/'.$category->id;?>"><?php echo $category['title'];?></a>
                                                        <ul>
                                                            <?php foreach ($categories as $key_sub => $category_sub) {
                                                                if ($category_sub->parent_id==$category->id) { ?>
                                                                    <li>
                                                                        <a href="<?php echo Yii::app()->request->baseUrl .'/site/category?subId='.$category_sub->id;?>">
                                                                            <?php echo $category_sub['title']?>
                                                                        </a>
                                                                        <ul>
                                                                            <?php foreach($categories as $key_sub_sub => $category_sub_sub){
                                                                                if ($category_sub_sub->parent_id==$category_sub->id) { ?>
                                                                                    <li>
                                                                                        <a href="<?php echo Yii::app()->request->baseUrl .'/site/category?subId='.$category_sub_sub->id;?>">
                                                                                            <?php echo $category_sub_sub['title']?>
                                                                                        </a>
                                                                                    </li>
                                                                                <?php }
                                                                            } ?>
                                                                        </ul>
                                                                    </li>
                                                                <?php }
                                                            } ?>
                                                        </ul>
                                                    </li>
                                                <?php }
                                            } ?>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-bottom">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-lg-offset-3 col-md-6 col-lg-6">
                                    <div class="categorys-product-search">
                                            <?php
                                            $model = new Categories;
                                            $categoriesList = CHtml::listData(Categories::model()->findAll(array('condition' => 'parent_id=0')), 'id', 'title');
                                            $form=$this->beginWidget('CActiveForm', array(
                                                'id'=>'search-form',
                                                'method' => 'GET',
                                                'htmlOptions' => array('class' => 'search-form-cat',),
                                                'clientOptions'=>array(
                                                    'validateOnSubmit'=>true,
                                                ),
                                                'action' => Yii::app()->createUrl('/products/search')
                                            ));
                                            ?>
                                            <div class="search-product form-group">

                                                <select id="Categories_id" class="cat-search " name="cat_id" selected="selected">
                                                    <option value="">All categories</option>
                                                    <?php foreach ($categoriesList as $k=>$list) { ?>
                                                        <option value="<?=$k;?>" <?=isset($_GET['cat_id']) && $_GET['cat_id']==$k ? 'selected' : '';?>><?=$list;?></option>
                                                    <?php } ?>
                                                </select>
                                                <input autocomplete="off" id="tags" type="text" name="q" class="form-control search-form" placeholder="Enter your search key. " value="<?=isset($_GET['q']) ? CHtml::encode($_GET['q']) : '' ; ?>"/>
                                                <button class="search-button" value="Search" name="s" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        <?php $this->endWidget(); ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                                    <div class="header-shipping">
                                        <a class="pull-right btn-lg btn" href="<?php echo Yii::app()->baseUrl.'/products/create';?>">PLACE AD</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>

            <!--HEADER AREA END-->
            <!--MOBILE MENU END-->
            <?php echo $content; ?>

            <!--FOOTER AREA START-->
            <section class="footer-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="footer-top">
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <ul>
                                    <li><a href="#">How to advertise </a></li>
                                    <li><a href="#">Terms &amp; Conditions</a></li>
                                    <li><a href="#">FAQ</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <ul>
                                    <li><a href="#">Prices of services </a></li>
                                    <li><a href="#">Comments &amp; ideas</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <?php $this->widget('application.extensions.fbLikeShareButton.fbLikeShareButton', array(
                                    'fbbutton' => array(
                                        'url'=>'http://tamoda.smartdroid.biz', //http://www.facebook.com/fb_page
                                        'action'=>'like', //Display button - like or recommend
                                        'width'=>'20', //Layout width
                                        'height'=>'30', //Layout height
                                        'layout'=>'standard', //Layout - standard, button_count, button, box_count
                                        'color_scheme'=>'light', //Layout color scheme - light, dark
                                        'show_share'=>'true', //Display share button - true, false
                                        'show_faces'=>'false', //Show user faces - true, false
                                    )
                                )); ?>
                                <ul class="social-icons">
                                    <li>Follow us on</li>
                                    <li><a href=""><i class="fa fa-facebook-square"></i> Facebook </a></li>
                                    <li><a href=""><i class="fa fa-twitter-square"></i> Twitter </a></li>
                                </ul>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <?php $this->renderPartial('//site/_contactAdmin', array(
                                    'model'=>new ContactForm('contactAdmin')
                                )); ?>
                                <ul>
                                    <li><a href="callto:12345678"><i class="fa fa-phone"></i>12345678 </a></li>
                                    <li><a href="mailto:support@tamoda.com"><i class="fa fa-envelope-o"></i>support@tamoda.com </a></li>
                                    <li></li>
                                </ul>

                                <a href=""></a>
                            </div>

                        </div>
                        <div class="footer-bottom row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                                <div class="footer-copyright ">
                                    Copyright &copy; 2015 <a href="http://admin@bootexperts.com/">BootExperts</a>. All rights reserved
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                                <div class="social-icon-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <!-- Modal -->

    <div id="modalRegisterOrLogin" class="modal fade" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="login-form-modal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4>Sign in to continue</h4>
                    </div>
                    <div class="modal-body">
                        <?php $userLogin = new Users('login'); ?>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'action' => Yii::app()->createUrl('//profile/login'),
                            'id'=>'login-form-modal',
                            'enableAjaxValidation'=>false,
                        )); ?>

                        <h4 class="text-center">Sign in with social</h4>
                        <script src="//ulogin.ru/js/ulogin.js"></script>
                        <div class="uLogin" data-ulogin="display=buttons;fields=email,username;providers=facebook,google,twitter;redirect_uri=<?php echo Yii::app()->createAbsoluteUrl('profile/login'); ?>">
                            <div class="col-md-4"> <div class="facebook form-group clearfix" data-uloginbutton="facebook"><div class="pull-left social_icon"> <i class="fa fa-facebook"></i></div> Sign in with Facebook </div></div>
                            <div class="col-md-4"> <div class="google form-group clearfix" data-uloginbutton="google"><div class="pull-left social_icon"> <i class="fa fa-google-plus"></i></div> Sign in with Google Plus</div></div>
                            <div class="col-md-4"> <div class="twitter form-group clearfix" data-uloginbutton="twitter"><div class="pull-left social_icon"> <i class="fa fa-twitter"></i></div> Sign in with Twitter</div></div>
                        </div>
                        <h4 class="text-center">or with registered account</h4>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Username or email <span class="required">*</span></label>
                                <?php echo $form->textField($userLogin,'username',array('size'=>60,'maxlength'=>64, 'class'=>'prof-form', 'id' => 'email')); ?>
                                <?php echo $form->error($userLogin,'username'); ?>
                            </div>

                            <div class="form-group">
                                <label>Password  <span class="required">*</span></label>
                                <?php echo $form->passwordField($userLogin,'password',array('size'=>60,'maxlength'=>64, 'class'=>'prof-form', 'id' => 'password')); ?>
                                <?php echo $form->error($userLogin,'password'); ?>
                            </div>
                            <div class="form-group">
                                <label>
                                    <?php echo CHtml::checkBox('remember_pass', array('class' => 'form-control')); ?>
                                    Remember me
                                </label>
                            </div>
                            <div class="form-group">
                                <?php echo CHtml::submitButton('Login', array('class' => 'btn form-control')); ?>
                            </div>
                            <p class="lost-password">
                                <a href="<?php echo Yii::app()->baseUrl.'/profile/forgotPassword';?>">Lost your password?</a>
                            </p>
                            <?php $this->endWidget(); ?>
                        </div>
                        <div class="signup-text">
                            Don't have an account yet?
                            <a class="signup_link" href="#">Sign up for free</a>
                        </div>
                    </div>
                </div>
                <div class="register-form-modal" style="display: none;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4>Sign Up to continue</h4>
                    </div>
                    <div class="modal-body">
                        <?php $userReg = new Users('register'); ?>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'action' => Yii::app()->createUrl('//profile/register'),
                            'id'=>'users-register-form',
                            'enableAjaxValidation'=>false,
                        )); ?>

                        <h4 class="text-center">Sign up with social</h4>
                        <script src="//ulogin.ru/js/ulogin.js"></script>
                        <div class="uLogin" data-ulogin="display=buttons;fields=email,first_name,last_name;providers=facebook,google,twitter;redirect_uri=<?php echo Yii::app()->createAbsoluteUrl('profile/register'); ?>">
                            <div class="col-md-4"> <div class="facebook form-group clearfix" data-uloginbutton="facebook"><div class="pull-left social_icon"> <i class="fa fa-facebook"></i></div> Sign up with Facebook </div></div>
                            <div class="col-md-4"> <div class="google form-group clearfix" data-uloginbutton="google"><div class="pull-left social_icon"> <i class="fa fa-google-plus"></i></div> Sign up with Google Plus</div></div>
                            <div class="col-md-4"> <div class="twitter form-group clearfix" data-uloginbutton="twitter"><div class="pull-left social_icon"> <i class="fa fa-twitter"></i></div> Sign up with Twitter</div></div>
                        </div>

                        <h4 class="text-center">or create an account using your email address</h4>

                        <div class="col-md-12">

                            <div class="form-group">
                                <?php echo $form->labelEx($userReg,'username'); ?>
                                <?php echo $form->textField($userReg,'username', array('class' => 'prof-form', 'id' => 'username_reg')); ?>
                                <?php echo $form->error($userReg,'username'); ?>
                                <label id="avbl_username_error" class="text-danger" style="display: none;">This username is already taken! Try another.</label>
                            </div>
                            <div class="form-group">
                                <?php echo $form->labelEx($userReg,'email'); ?>
                                <?php echo $form->emailField($userReg,'email', array('class' => 'prof-form', 'id' => 'email_reg')); ?>
                                <?php echo $form->error($userReg,'email'); ?>
                                <label id="avbl_email_error" class="text-danger" style="display: none;">This email is already in use</label>
                            </div>

                            <input type="hidden" name="Products">

                            <div class="form-group">&nbsp;</div>
                            <div class="clearfix buttons">
                                <?php echo CHtml::submitButton('Submit', array('class' => 'btn form-control')); ?>
                            </div>
                            <?php $this->endWidget(); ?>
                            <p class="lost-password"></p>
                        </div>
                        <div class="login-text signup-text">
                            Already have an account?
                            <a class="login_link" href="#">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="loc" class="modal fade" style="display: none; top: 250px; z-index: 20px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <input id="pac-input" class="prof-form text-center" type="text" name="" placeholder="Enter your location">
                </div>
                <div class="modal-footer">
                    <div class="col-md-8 col-md-offset-2">
                        <button type="btn" class="btn pull-left loc-ok">Ok</button>
                        <button type="btn" class="btn pull-right loc-cancel">Cancel</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php
$products = Products::model()->findAll();
$productsList = CHtml::listData($products, 'id', 'name');

$str = '';
foreach($productsList as $list){
    $str = $str.','."$list";
}
$str = ltrim($str,',')

?>
    <!--FOOTER AREA END-->


<!--        <script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/vendor/jquery-1.11.3.min.js"></script>-->

        <!-- price-slider js -->

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/price-slider.js"></script>

        <!-- bootstrap js
        ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>

        <!-- nevo slider js
        ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.nivo.slider.pack.js"></script>

        <!-- owl.carousel.min js
     ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/owl.carousel.min.js"></script>

        <!-- jquery.collapse js
            ============================================ -->
<!--        <script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/jquery.collapse.js"></script>-->

        <!-- count down js
        ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.countdown.min.js" type="text/javascript"></script>

        <!--zoom plugin
        ============================================ -->
<!--        <script src='--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/jquery.elevatezoom.js'></script>-->

        <!-- wow js
     ============================================ -->
<!--        <script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/wow.js"></script>-->

        <!--zoom plugin
         ============================================ -->


        <!--Mobile Menu Js
        ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.meanmenu.js"></script>

        <!-- jquery.fancybox.pack js -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fancybox/jquery.fancybox.pack.js"></script>

        <!-- jquery.scrollUp js
        ============================================ -->
<!--        <script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/jquery.scrollUp.min.js"></script>-->

        <!-- mixit-up js
        ============================================ -->
<!--        <script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/js/jquery.mixitup.min.js"></script>-->

        <!-- plugins js
     ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>

        <!-- main js
     ============================================ -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>

        <script>

            $('.loc-cancel, .loc-ok').on('click', function(e) {
                e.preventDefault();
                $('#loc').modal('hide');
                return false;
            })

            var baseUrl = '<?=$this->createUrl('/')?>';

            $( document ).ready(function() {
                var tes = "<?php echo $str;?>";
                var array = tes.split(",");

                $( "#tags" ).autocomplete({
                    source: array
                });
            });

            $('.signup_link').click(function(e) {
                e.preventDefault();
                $('.register-form-modal').attr('style', 'display:block');
                $('.login-form-modal').attr('style', 'display:none');
            });
            $('.login_link').click(function(e) {
                e.preventDefault();
                $('.register-form-modal').attr('style', 'display:none');
                $('.login-form-modal').attr('style', 'display:block');
            });

            $('.notifications li').click(function(e) {

                var $this = $(this);
                if ($this.hasClass('unread')) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->request->baseUrl; ?>/profile/markAsRead',
                        data: {
                            note_id: $this.data('id')
                        },
                        success:  function() {
                            $this.removeClass('unread');
                            var count = parseInt($('.notification').text())-1;
                            $('.notification').text(count);
                            if (count == 0) {
                                $('.notification').empty();
                            }
                        }
                    });
                }
                window.location.href = $this.find('a').attr('href');

            });

            $('.mark_unread_uq').on('click', function() {

                var $this = $(this);

                $.ajax({
                    type: "POST",
                    url: "<?=$this->createUrl('messages/markOneRead');?>",
                    data: {
                        id: $this.data('id')
                    },
                    success: function () {
                        var count = parseInt($('.msg-notification').text())-1;
                        $this.parent().removeClass('unread');
                        $('.msg-notification').text(count)
                        if (count == 0) {
                            $('.msg-notification').empty();
                        }
                        $this.remove();
                    }
                });
                return false;
            });

            $('.mark_all_unread_uq').on('click', function() {
                $('.note-messages').children('li').removeClass('unread')
                var $this = $(this);
                $.ajax({
                    type: "POST",
                    url: "<?=$this->createUrl('messages/markAllRead');?>",
                    success: function () {
                        $('.note-messages').children('li').removeClass('unread')
                        $('.msg-notification').empty();
                    }
                });
                return false;
            });


            $('#modalRegisterOrLogin').on('shown.bs.modal', function() {
                $('.box-container').addClass('blur-bg');
            });
            $('#modalRegisterOrLogin').on('hidden.bs.modal', function (e) {
                $('.box-container').removeClass('blur-bg');
            })

            $(".alert-dismissable").animate({opacity: 2.0}, 3000).fadeOut("slow");

            $(document).ready(function() {

                var validUsr = false;
                var validEmail = false;
                $('#users-register-form').on('change', function () {

                    var usr = $("#username_reg").val();
                    var email = $('#email_reg').val();

                    if (usr.length >= 3) {
                        $.ajax({
                            type: "GET",
                            async: false,
                            url: baseUrl + '/profile/checkUserNameOrEmail',
                            data: {username: usr},
                            success: function (msg) {
                                if (msg == 'false') {
                                    $('#avbl_username_error').show();
                                    validUsr = false
                                } else {
                                    $('#avbl_username_error').hide();
                                    validUsr = true;
                                }
                            }
                        });
                    }

                    if (email.length >= 6) {
                        $.ajax({
                            type: "GET",
                            async: false,
                            url: baseUrl + '/profile/checkUserNameOrEmail',
                            data: {email: email},
                            success: function (msg) {
                                if (msg == 'false') {
                                    $('#avbl_email_error').show();
                                    validEmail = false;
                                } else {
                                    $('#avbl_email_error').hide();
                                    validEmail = true;
                                }
                            }
                        });
                    }
                });
                $('#users-register-form').submit(function() {
                    if (validUsr && validEmail) {
                        return true;
                    }
                    return false;
                })
            });

        </script>
</body>
</html>
