<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

	<div id="content">
		<?php foreach(Yii::app()->user->getFlashes() as $key => $message): ?>
			<div class="col-lg-offset-5 text-center alert alert-dismissable alert-<?=$key?>">
				<?=$message?>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		<?php endforeach; ?>
		<?php echo $content; ?>
	</div><!-- content -->


<?php $this->endContent(); ?>