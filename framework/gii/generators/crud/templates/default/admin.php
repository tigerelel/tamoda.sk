<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('admin'),
	'Manage',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>', 'url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>
    Manage
    <?php echo $this->pluralize($this->class2name($this->modelClass)); ?>

    <a href="<?=Yii::app()->createUrl('admin/'.$this->modelClass."/create")?>" class="pull-right">
        <i class="fa fa-plus"></i>
    </a>
</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo "<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
    'dataProvider'=>$model->search(),
    'cssFile' => false,
    'htmlOptions' => array(),
    'itemsCssClass' => 'items table table-responsive table-hover table-bordered table-striped',
    'pagerCssClass'=>'clearfix',
    'pager' => array(
    'htmlOptions'=>array(
    'class'=>'pagination',
    ),
    'header'=>'',
    'firstPageLabel' => '<<',
    'prevPageLabel'  => '<',
    'nextPageLabel'  => '>',
    'lastPageLabel'  => '>>',
    'selectedPageCssClass' => 'active disabled',
    ),
    'filter'=>$model,
    'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		array(
			'class'=>'CButtonColumn',
            'buttons' => array(
                'view' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-eye"></i>',
                    'imageUrl' => false,
                ),
                'update' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-upload"></i>',
                    'imageUrl' => false,
                ),
                'delete' => array(
                    'options' => array('title' => false),
                    'label' => '<i class="fa fa-remove"></i>',
                    'imageUrl' => false,
                ),
            ),
		),
	),
)); ?>

<style>
    .items.table thead{background:#ecf0f5;}
</style>
<script>
    $(function(){
       $('table.items thead').find('input').addClass('form-control');
    });
</script>